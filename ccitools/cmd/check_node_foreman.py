#!/usr/bin/python3

import ccitools.conf
import logging
import sys

from ccitools.cmd.base.foreman import BaseForemanCMD
from ccitools import common

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class CheckNodeForemanCMD(BaseForemanCMD):

    def __init__(self):
        super(CheckNodeForemanCMD, self).__init__(
            description="Checks Nodes if they exist in foreman")

    def check_node_foreman(self, nodes, hostgroup):
        """Confirm if the node exists in Foreman.

        Additionally, a hostgroup may be provided to check whether such node
        belongs to the specified hostgroup in Foreman.
        """
        logger.info("Checking if the following nodes exist in Foreman: %s ",
                    ", ".join(nodes.split()))
        for hostname in nodes.split():
            hostname = common.normalize_fqdn(hostname)
            try:
                found_hostgroup = self.foreman.gethost(hostname)['hostgroup']
                logger.info("Node '%s' found in Foreman under hostgroup '%s'.",
                            hostname, found_hostgroup['title'])

            except Exception:
                logger.error("The machine '%s' is not an OpenStack "
                             "Compute Node. "
                             "Please check if you are really executing "
                             "the correct procedure.", hostname)
                sys.exit(1)

            if hostgroup:
                logger.info("Hostgroup '%s' provided. "
                            "Checking that node '%s' belongs...",
                            hostgroup, hostname)
                if hostgroup not in found_hostgroup['title']:
                    logger.error("Node '%s' does not belong to "
                                 "provided hostgroup '%s'.",
                                 hostname, hostgroup)
                    sys.exit(1)
                else:
                    logger.info("Success!")

    def main(self, args=None):
        self.parser.add_argument(
            "--nodes", required=True,
            help="List of nodes (space-separated values)")
        self.parser.add_argument(
            "--hostgroup",
            help="Check if provided nodes belong to the Foreman hostgroup")

        args = self.parse_args(args)

        self.check_node_foreman(args.nodes, args.hostgroup)


# Needs static method for setup.cfg
def main(args=None):
    CheckNodeForemanCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
