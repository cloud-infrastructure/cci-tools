#!/usr/bin/python3

import ccitools.conf
import defusedxml.ElementTree
import logging
import os
import sys

from ccitools.cmd.base.rundeck import BaseRundeckCMD
from xml.etree.ElementTree import Element  # nosec

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class ExportRundeckDefinitionsCMD(BaseRundeckCMD):
    def __init__(self):
        super(ExportRundeckDefinitionsCMD, self).__init__(
            description="Export Rundeck Definitions")

    # Return a list with all the project names
    def get_project_names(self, projectsinfo_xml):
        project_names = []
        root = defusedxml.ElementTree.fromstring(projectsinfo_xml)
        for name in root.findall('project'):
            project_names.append(name.find('name').text)
        return project_names

    def save_exported_jobs(self, exportedjobs_xml, output):
        """Save the exported jobs in the given output.

        Add <joblist> </joblist> tags at the beginning and the end of the file
        to make the them importable afterwords (Rundeck requirement).
        exportedjobs_xml: job definition str
        output: path to save the output
        """
        root = defusedxml.ElementTree.fromstring(exportedjobs_xml)
        for job in root:
            job_id = job.find('id').text
            with open('%s/%s.xml' % (output, job_id), 'wb') as job_xml:
                newroot = Element("joblist")
                newroot.insert(0, job)
                job_xml.write(defusedxml.ElementTree.tostring(newroot))
                logger.info("'%s' with id '%s' exported",
                            job.find('name').text, job_id)

    def main(self, args):
        self.parser.add_argument(
            "--output",
            dest='output',
            nargs='?',
            default="/tmp",  # nosec
            help="Path where the XMLs are exported")

        args = self.parse_args(args)

        projects = self.get_project_names(self.rundeckcli.list_projects())
        for project in projects:
            project_path = "%s/%s" % (args.output, project)
            if not os.path.exists(project_path):
                os.makedirs(project_path)
            logger.info("Exporting '%s' jobs in '%s'...",
                        project, project_path)
            self.save_exported_jobs(
                self.rundeckcli.export_jobs(project), project_path)


# Needs static method for setup.cfg
def main(args=None):
    ExportRundeckDefinitionsCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)
