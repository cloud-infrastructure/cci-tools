import logging

from ccitools.cmd.rundeck_filter_execution import main
from unittest import mock
from unittest import TestCase


class TestRundeckFilterExecution(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 2)

    @mock.patch('ccitools.cmd.base.rundeck.auth_get_sso_cookie')
    @mock.patch('ccitools.utils.rundeck.requests')
    def test_filter(self, mock_requests, mock_cookie):
        main([
            '--auth', 'sso',
            '--project', 'CC-SysAdmins',
            '--host', 'test_host',
            '--interval', '1d',
            '--columns', 'test_column'
        ])
