import logging

from ccitools.errors import SSHException
from ccitools.verify.service import BarbicanAPIAction
from ccitools.verify.service import CinderSchedulerAction
from ccitools.verify.service import CinderVolumeAction
from ccitools.verify.service import EC2APIAction
from ccitools.verify.service import GlanceAPIAction
from ccitools.verify.service import HaproxyAction
from ccitools.verify.service import HeatEngineAction
from ccitools.verify.service import HttpdAction
from ccitools.verify.service import LibvirtdAction
from ccitools.verify.service import MagnumAPIAction
from ccitools.verify.service import MagnumConductorAction
from ccitools.verify.service import ManilaAPIAction
from ccitools.verify.service import ManilaSchedulerAction
from ccitools.verify.service import ManilaShareAction
from ccitools.verify.service import MistralAPIAction
from ccitools.verify.service import MistralEngineAction
from ccitools.verify.service import MistralEventEngineAction
from ccitools.verify.service import MistralExecutorAction
from ccitools.verify.service import NeutronServerAction
from ccitools.verify.service import NovaComputeAction
from ccitools.verify.service import NovaConductorAction
from ccitools.verify.service import NovaNetworkAction
from ccitools.verify.service import NovaSchedulerAction
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.verify.service.ssh_executor')
@mock.patch('ccitools.verify.base.ssh_executor')
class ServiceWrongActionBase(object):
    def setUp(self, actionClass):
        logging.disable(logging.CRITICAL)
        self.client = actionClass(host='fake.domain')
        self.client.delay = 0

    def test_try_fix(self, mock_common, mock_service):
        self.client.try_fix()
        mock_service.assert_called_with(
            'fake.domain',
            ('systemctl start %s' % self.client.service)
        )

    def test_try_fix_raise(self, mock_common, mock_service):
        mock_service.side_effect = iter([SSHException()])
        self.client.try_fix()

    def test_check_alarm_state(self, mock_common, mock_service):
        mock_common.return_value = [], None
        self.assertFalse(self.client.check_alarm_state())

        mock_common.return_value = (
            ['fake.domain/another_alarm'], None)
        self.assertFalse(self.client.check_alarm_state())

        error_str = (
            'fake.domain/systemd-%s/gauge-running' % self.client.service)
        mock_common.return_value = ([error_str], None)
        self.assertTrue(self.client.check_alarm_state())

        mock_common.return_value = [], None
        self.assertFalse(self.client.check_alarm_state(after_try=True))


class TestBarbicanAPIAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestBarbicanAPIAction, self).setUp(
            actionClass=BarbicanAPIAction)


class TestCinderSchedulerAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestCinderSchedulerAction, self).setUp(
            actionClass=CinderSchedulerAction)


class TestCinderVolumeAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestCinderVolumeAction, self).setUp(
            actionClass=CinderVolumeAction)


class TestEC2APIAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestEC2APIAction, self).setUp(
            actionClass=EC2APIAction)


class TestGlanceAPIAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestGlanceAPIAction, self).setUp(
            actionClass=GlanceAPIAction)


class TestHaproxyAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestHaproxyAction, self).setUp(
            actionClass=HaproxyAction)


class TestHeatEngineAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestHeatEngineAction, self).setUp(
            actionClass=HeatEngineAction)


class TestHttpAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestHttpAction, self).setUp(
            actionClass=HttpdAction)


class TestLibvirtdAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestLibvirtdAction, self).setUp(
            actionClass=LibvirtdAction)


class TestMagnumAPIAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestMagnumAPIAction, self).setUp(
            actionClass=MagnumAPIAction)


class TestMagnumConductorAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestMagnumConductorAction, self).setUp(
            actionClass=MagnumConductorAction)


class TestManilaAPIAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestManilaAPIAction, self).setUp(
            actionClass=ManilaAPIAction)


class TestManilaSchedulerAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestManilaSchedulerAction, self).setUp(
            actionClass=ManilaSchedulerAction)


class TestManilaShareAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestManilaShareAction, self).setUp(
            actionClass=ManilaShareAction)


class TestMistralAPIAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestMistralAPIAction, self).setUp(
            actionClass=MistralAPIAction)


class TestMistralEngineAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestMistralEngineAction, self).setUp(
            actionClass=MistralEngineAction)


class TestMistralEventEngineAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestMistralEventEngineAction, self).setUp(
            actionClass=MistralEventEngineAction)


class TestMistralExecutorAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestMistralExecutorAction, self).setUp(
            actionClass=MistralExecutorAction)


class TestNeutronServerAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestNeutronServerAction, self).setUp(
            actionClass=NeutronServerAction)


class TestNovaComputeAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestNovaComputeAction, self).setUp(
            actionClass=NovaComputeAction)


class TestNovaConductorAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestNovaConductorAction, self).setUp(
            actionClass=NovaConductorAction)


class TestNovaNetworkAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestNovaNetworkAction, self).setUp(
            actionClass=NovaNetworkAction)


class TestNovaSchedulerAction(ServiceWrongActionBase, TestCase):
    def setUp(self):
        super(TestNovaSchedulerAction, self).setUp(
            actionClass=NovaSchedulerAction)
