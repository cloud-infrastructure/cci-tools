#!/usr/bin/python3

import ccitools.conf
import json
import logging
import requests
import sys

from ccitools.cmd import base
from influxdb import InfluxDBClient

logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class ServiceMetricsCMD(base.BaseCMD):

    def __init__(self):
        super(ServiceMetricsCMD, self).__init__(
            description="Send service availability and metrics to Monit")

    def get_last(self, query_result):
        results = list(query_result.get_points())
        if results:
            return results[0]['last']
        else:
            return -1  # Error

    def get_last_from_sums(self, query_result):
        results = list(query_result.get_points())
        # Sometimes it returns None as the last result, that's why
        # we have to do it this way
        for r in reversed(results):
            if r['sum'] is not None:
                return r['sum']
        return -1  # Error

    def service_metrics(self):
        # InfluxDB client
        client = InfluxDBClient(
            host=CONF.influxdb.host,
            port=CONF.influxdb.port,
            username=CONF.influxdb.user,
            password=CONF.influxdb.password,
            database=CONF.influxdb.database,
            ssl=True)

        # Queries to InfluxDB to get some cloud number to send to XSLS
        vmcount = self.get_last(
            client.query(
                "SELECT LAST(count)"
                "  FROM vms_per_status"
                "  WHERE status = 'active' AND time > now() - 3d"
            )
        )
        newvmcount = self.get_last(
            client.query(
                "SELECT LAST(created_in_last_hour)"
                "  FROM vms_changes_total WHERE time > now() - 3d"
            )
        )
        hypervisorcount = self.get_last_from_sums(
            client.query(
                "SELECT SUM(total)"
                "  FROM hypervisors_per_cell"
                "  WHERE time > now() - 3d GROUP BY time(10m)"
            )
        )
        usercount = self.get_last(
            client.query(
                "SELECT LAST(count)"
                "  FROM users_total WHERE time > now() - 3d"
            )
        )
        projectcount = self.get_last(
            client.query(
                "SELECT LAST(count)"
                "  FROM projects_total WHERE time > now() - 3d"
            )
        )

        # TODO(jcastro): The availability of the cloud service should be
        # calculated with Rally maybe? To be tested
        metrics = [
            {
                "producer": "cloud",
                "type": "availability",
                "serviceid": "cloud",
                "service_status": "available",
                "contact": "cloud-infrastructure-infra-admin@cern.ch",
                "webpage": "https://openstack.cern.ch"
            },
            {
                "producer": "cloud",
                "type": "metric",
                "vmcount": vmcount,
                "newvmcount": newvmcount,
                "hypervisorcount": hypervisorcount,
                "usercount": usercount,
                "projectcount": projectcount,
            }
        ]

        data = json.dumps(metrics)
        logger.info("Sending the following data")
        logger.info("\n %s", data)

        response = requests.post(
            url='http://monit-metrics:10012/',
            headers={
                "Content-Type": "application/json; charset=UTF-8"
            },
            data=data,
            timeout=60
        )

        if response.status_code not in [200]:
            logger.error(
                'With document: %s. Status code: %s. Message: %s',
                data,
                response.status_code,
                response.text)
            raise Exception('Error while sending data')

        if not newvmcount:
            raise Exception('No new VMs created')

        if (not vmcount
            or not hypervisorcount
            or not usercount
                or not projectcount):
            raise Exception('No data to be sent')

    def main(self, args):
        self.parse_args(args)
        self.service_metrics()


# Needs static method for setup.cfg
def main(args=None):
    try:
        ServiceMetricsCMD().main(args)
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)


if __name__ == "__main__":
    main()
