import logging

from ccitools.cmd import iterator_project
from ccitools.tests.cmd import fixtures
from collections import namedtuple
from unittest import mock
from unittest import TestCase


class TestIteratorProject(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args_no_auth(self):
        with self.assertRaises(Exception):
            iterator_project.main([])

    @mock.patch('ccitools.utils.cloud.cloud_config')
    def test_default_args_raise_IOError(self, mock_cc):
        mock_cc.OpenStackConfig.side_effect = (
            iter([IOError])
        )

        with self.assertRaises(IOError):
            iterator_project.main([])

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.iterator_project.keystone_client')
    def test_no_projects(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = []

        with self.assertRaises(SystemExit) as cm:
            iterator_project.main([])
        self.assertEqual(cm.exception.code, 1)

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(domain='default')

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.iterator_project.keystone_client')
    def test_quiet(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = fixtures.ITERATOR_PROJECTS

        iterator_project.main(['--quiet'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(domain='default')

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.iterator_project.keystone_client')
    def test_prettytable(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = fixtures.ITERATOR_PROJECTS

        iterator_project.main([])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(domain='default')

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.iterator_project.keystone_client')
    def test_filter(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = fixtures.ITERATOR_PROJECTS

        iterator_project.main(['--filter', 'True'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(domain='default')

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.iterator_project.keystone_client')
    def test_prettytable_wrong_column_name(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = fixtures.ITERATOR_PROJECTS

        with self.assertRaises(ValueError):
            iterator_project.main(['-c', 'a'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(domain='default')

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.iterator_project.keystone_client')
    def test_prettytable_with_columns(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = fixtures.ITERATOR_PROJECTS

        iterator_project.main(['-c', 'id', '-c', 'name'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(domain='default')

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.iterator_project.keystone_client')
    def test_prettytable_with_format(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = fixtures.ITERATOR_PROJECTS
        # Change manually the formater_default
        iterator_project._formatter_default = 'fake'

        iterator_project.main(['-f', 'yaml'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(domain='default')

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.iterator_project.keystone_client')
    def test_script_dry_run(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = fixtures.ITERATOR_PROJECTS

        iterator_project.main(['--dry-run', '--script', 'pass'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(domain='default')

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.iterator_project.keystone_client')
    def test_script(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = fixtures.ITERATOR_PROJECTS

        iterator_project.main(['--script', 'pass'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(domain='default')

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.iterator_project.keystone_client')
    def test_action_dry_run(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = fixtures.ITERATOR_PROJECTS

        iterator_project.main(['--dry-run', '--action', 'echo'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(domain='default')

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.iterator_project.keystone_client')
    def test_action(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = fixtures.ITERATOR_PROJECTS

        iterator_project.main(['--action', 'echo'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(domain='default')

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.iterator_project.keystone_client')
    @mock.patch('ccitools.cmd.iterator_project.call')
    def test_action_wrong_return_code(self, mock_call, mock_kc, mock_ks,
                                      mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = fixtures.ITERATOR_PROJECTS
        mock_call.return_value = -1

        iterator_project.main(['--action', 'echo'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(domain='default')
        mock_call.assert_called_with(['echo'])

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.iterator_project.keystone_client')
    @mock.patch('ccitools.cmd.iterator_project.call')
    def test_action_raise_exception(self, mock_call, mock_kc, mock_ks,
                                    mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = fixtures.ITERATOR_PROJECTS
        mock_call.side_effect = iter([
            OSError,
            OSError
        ])

        iterator_project.main(['--action', 'echo'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(domain='default')
        mock_call.assert_called_with(['echo'])

    def test_merge(self):
        dict_from = {
            'one': 'one_value_modified',
        }
        data_from = namedtuple(
            "One", dict_from.keys())(*dict_from.values())

        dict_to = {
            'one': 'one_value',
            'two': 'two_value'
        }
        data_to = namedtuple(
            "Two", dict_to.keys())(*dict_to.values())
        iterator_project.merge(data_from, data_to)
