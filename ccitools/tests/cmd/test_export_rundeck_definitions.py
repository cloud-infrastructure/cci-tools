import logging

from ccitools.cmd.export_rundeck_definitions import main
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase


class TestExportedRundeckDefinitions(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):
        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)

    def test_non_existing_method(self):
        with self.assertRaises(SystemExit) as cm:
            main(['--auth', 'fake'])
        self.assertEqual(cm.exception.code, 2)

    @mock.patch('ccitools.cmd.base.rundeck.RundeckAPIClient')
    def test_api_token(self, mock_rdeck):
        mock_cli = mock_rdeck.return_value
        mock_cli.list_projects.return_value = (
            fixtures.RUNDECK_PROJECT_LIST_XML
        )
        mock_cli.export_jobs.return_value = (
            fixtures.RUNDECK_EXPORT_PROJECT_JOBS
        )
        main(['--auth', 'token'])

        mock_rdeck.assert_called_with(
            server='cci-rundeck.cern.ch',
            port='443',
            token='secret_token',
            verify=False,
            api_version=20
        )
        mock_cli.list_projects.assert_called_with()
        mock_cli.export_jobs.assert_called_with('fake_project')

    @mock.patch('ccitools.cmd.base.rundeck.auth_get_sso_cookie')
    @mock.patch('ccitools.cmd.base.rundeck.RundeckAPIClient')
    def test_sso(self, mock_rdeck, mock_sso):
        mock_cli = mock_rdeck.return_value
        mock_cli.list_projects.return_value = (
            fixtures.RUNDECK_PROJECT_LIST_XML
        )
        mock_cli.export_jobs.return_value = (
            fixtures.RUNDECK_EXPORT_PROJECT_JOBS
        )
        main(['--auth', 'sso'])

        mock_rdeck.assert_called_with(
            server='cci-rundeck.cern.ch',
            port='443',
            sso_cookie=mock_sso.return_value,
            verify=False,
            api_version=20
        )
        mock_cli.list_projects.assert_called_with()
        mock_cli.export_jobs.assert_called_with('fake_project')
