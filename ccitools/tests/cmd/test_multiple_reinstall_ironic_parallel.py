import logging

from ccitools.cmd.multiple_reinstall_ironic_parallel import main
from unittest import TestCase


class TestMultipleReinstallIronicParallel(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):
        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)
