#!/usr/bin/python3

import logging
import sys

from ccitools.cmd import base
from ccitools.utils.sendmail import MailClient

# configure logging
logger = logging.getLogger(__name__)


class SendCalendarInvitationCMD(base.BaseCMD):

    def __init__(self):
        super(SendCalendarInvitationCMD, self).__init__(
            description="Send calendar invitation via email")

    def main_send_event_reminder(self, exec_mode, organizer, attendees, title,
                                 description, date, duration, alarm):
        """After an intervention is triggered, sends an ICS invitation."""
        mailclient = MailClient("localhost")

        attendees = attendees.split()

        if exec_mode:
            mailclient.send_mail_ics(mail_subject="[Rundeck] %s" % title,
                                     mail_body='',
                                     mail_cc=None,
                                     mail_bcc=None,
                                     mail_content_type='plain',
                                     organizer=organizer,
                                     attendees=", ".join(attendees),
                                     title=title,
                                     description=description,
                                     date=date,
                                     duration=duration,
                                     alarm=alarm)
            logger.info("Intervention calendar invitation sent to: '%s'"
                        % ", ".join(attendees))
        else:
            logger.info("[DRYRUN] Intervention calendar invitation "
                        "not sent to '%s'", ", ".join(attendees))

    def main(self, args=None):
        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true',
                               help="If present, sends email")
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')

        self.parser.add_argument("--organizer", required=True,
                                 help="Event organizer")
        self.parser.add_argument("--attendees", required=True,
                                 help="List of attendees to the event "
                                 "(white-space separated list)")
        self.parser.add_argument("--title", required=True, help="Event title")
        self.parser.add_argument("--description", required=True,
                                 help="Event description")
        self.parser.add_argument("--date", required=True, help="Event date")
        self.parser.add_argument("--duration", type=int, default=1,
                                 help="Event duration")
        self.parser.add_argument("--alarm", default="", help="Alarm event")

        args = self.parse_args(args)

        self.main_send_event_reminder(args.exec_mode,
                                      args.organizer,
                                      args.attendees,
                                      args.title,
                                      args.description,
                                      args.date,
                                      args.duration,
                                      args.alarm)


# Needs static method for setup.cfg
def main(args=None):
    SendCalendarInvitationCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
