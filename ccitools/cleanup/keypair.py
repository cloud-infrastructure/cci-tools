import dateutil.parser
import logging

from ccitools.cleanup import base
from ccitools import common
from datetime import datetime
from datetime import timezone

# configure logging
logger = logging.getLogger(__name__)


class KeypairResourceCleanup(base.ResourceCleanup):
    def __init__(self, cloud, quiet):
        super(KeypairResourceCleanup, self).__init__(
            'KEYPAIRS', cloud, quiet)

    def get_pending(self):
        keypairs_pending = []
        logger.info("Looking for leftover keypairs ...")
        for keypair in self.cloud.get_keypairs(
                user_name='svcprobe',
                cloud='probe',
                tag='cleanup'):
            created_at = dateutil.parser.parse(keypair.created_at)
            days_since_created = (
                datetime.now(timezone.utc)
                - created_at.replace(tzinfo=timezone.utc)).days
            if (base.RALLY_REGEX_KEYPAIR.match(keypair.name)
                    and days_since_created >= 1):
                keypairs_pending.append(keypair)
        return keypairs_pending

    def delete_leftovers_before(self):
        if self.leftovers:
            self.cloud.delete_keypairs(
                self.leftovers)

    def delete_leftovers_after(self):
        pass

    def get_broken(self):
        broken_keypairs = []
        for keypair in self.cloud.get_keypairs(
                user_name='svcprobe',
                cloud='probe'):
            if keypair in self.leftovers:
                broken_keypairs.append(keypair)
        return broken_keypairs

    def format_table(self):
        keypair_fields = [
            ('ID', 'id'),
            ('Name', 'name'),
            ('Created', 'created_at'),
        ]
        keypairs_table = common.get_resources_info_table(
            self.leftovers, keypair_fields)

        return keypairs_table
