import logging

from ccitools.cmd import get_uptime
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.cmd.get_uptime.ssh_executor')
class TestGetHostsUptime(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self, mock_exec):
        with self.assertRaises(SystemExit) as cm:
            get_uptime.main([])
        self.assertEqual(cm.exception.code, 2)

    def test_dryrun_hosts(self, mock_exec):
        get_uptime.SLEEP_TIME = 0
        get_uptime.main([
            '--hosts', 'my_host1 my_host2',
            '--dryrun'
        ])

    def test_perform_hosts(self, mock_exec):
        # Case with 2 nodes all accessible by ssh
        get_uptime.SLEEP_TIME = 0
        mock_exec.return_value = [], None
        get_uptime.main([
            '--hosts', 'my_host1 my_host2',
            '--perform'
        ])

        # Case with a single node that fails
        mock_exec.side_effect = iter([
            Exception('Can\'t connect to '),
            ([], None),
        ])
        cmd = get_uptime.GetHostsUptime()
        cmd.main([
            '--hosts', 'my_host1',
            '--perform'
        ])

    def test_ssh_uptime_dryrun(self, mock_exec):
        get_uptime.SLEEP_TIME = 0
        hosts = "host1 host2"
        self.assertEqual(
            ['host1:0.0', 'host2:0.0'],
            get_uptime.GetHostsUptime().ssh_uptime(
                hosts,
                False
            ),

        )

    def test_ssh_uptime_perform(self, mock_exec):
        get_uptime.SLEEP_TIME = 0
        hosts = "host1 host2"
        self.assertEqual(
            [],
            get_uptime.GetHostsUptime().ssh_uptime(
                hosts, True
            )
        )
