#!/usr/bin/python3

import ccitools.conf
import json
import logging
import requests
import sys

from ccitools.cmd import base
from requests.auth import HTTPBasicAuth

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class JiraTicketCleanup(base.BaseCMD):

    def __init__(self):
        super(JiraTicketCleanup, self).__init__(
            description="JIRA Ticket cleanup")
        self.auth = HTTPBasicAuth(
            CONF.jira.username,
            CONF.jira.password
        )

    def query(
            self, jql, url='https://its.cern.ch/jira/rest/api/2/search',
            headers={'Accept': 'application/json'},
            maxResults=1000, fields='key,updated,status,summary'):

        query = {
            'jql': jql,
            'maxResults': maxResults,
            'fields': fields
        }
        return requests.get(
            url, headers=headers, params=query, auth=self.auth,
            timeout=60
        )

    def comment(
            self, key, txt,
            url="https://its.cern.ch/jira/rest/api/%s/comment",
            headers={
                'Accept': 'application/json',
                'Content-Type': 'application/json'}):

        payload = json.dumps({"body": txt})
        return requests.post(
            url % key, data=payload, headers=headers, auth=self.auth,
            timeout=60)

    def transitions(
            self, key,
            url="https://its.cern.ch/jira/rest/api/2/issue/%s/transitions",
            headers={'Accept': 'application/json'}):

        return requests.get(
            url % key, headers=headers, auth=self.auth, timeout=60)

    def transition(
            self, key, tid, txt, state,
            url="https://its.cern.ch/jira/rest/api/2/issue/%s/transitions",
            headers={
                'Accept': 'application/json',
                'Content-Type': 'application/json'}):

        payload = json.dumps({
            'update': {'comment': [{'add': {'body': txt}}]},
            'fields': {'resolution': {'name': state}},
            'transition': {'id': tid}})

        return requests.post(
            url % key, data=payload, headers=headers, auth=self.auth,
            timeout=60)

    def cleanup_tickets(self, jql, exec_mode):
        try:
            resp = self.query(jql)
            jsn = json.loads(resp.text)
            issues = jsn['issues']
        except Exception as e:
            print("failed to query JIRA issues : %s" % e)
            sys.exit(1)

        for issue in issues:
            logger.info(
                json.dumps(
                    issue,
                    sort_keys=True,
                    indent=4,
                    separators=(",", ": ")))
            try:
                if exec_mode:
                    logger.info("Marking issue %s inactive", issue['key'])
                    resp = self.transitions(issue['key'])
                    resp = self.transition(
                        issue['key'],
                        '2',
                        'Issue automatically closed due to inactivity '
                        '- please reopen if appropriate.',
                        'Unresolved')
                    if not resp.ok:
                        resp = self.transition(
                            issue['key'],
                            '331',
                            'Issue automatically closed due to inactivity '
                            '- please reopen if appropriate.',
                            'Unresolved')
                    if not resp.ok:
                        logger.error("Failed to update JIRA issue %s : %s",
                                     issue,
                                     resp.text)
                        sys.exit(1)
                    logger.info("Issue %s marked inactive :: %s :: %s :: %s",
                                issue['key'],
                                resp.ok,
                                resp.status_code,
                                resp.text)
            except Exception as e:
                logger.error("Failed to update JIRA issue %s : %s", issue, e)
                sys.exit(1)
        logger.info(len(issues))

    def main(self, args=None):
        self.parser.add_argument("--jql",
                                 required=True,
                                 help="Query to jira to filter tickets")

        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true')
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')
        args = self.parse_args(args)

        self.cleanup_tickets(args.jql, args.exec_mode)


# Needs static method for setup.cfg
def main(args=None):
    JiraTicketCleanup().main(args)


if __name__ == "__main__":
    main()
