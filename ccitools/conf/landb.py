from oslo_config import cfg

host = cfg.StrOpt(
    'host',
    default='network.cern.ch',
    help="Host to use for landb operations")

port = cfg.IntOpt(
    'port',
    default=443,
    help="Host to use for landb operations")

protocol = cfg.StrOpt(
    'protocol',
    default='https',
    help="Defalt protocol to use (http/https)")

user = cfg.StrOpt(
    'user',
    default='neutron',
    help="Defalt user for landb")

password = cfg.StrOpt(
    'password',
    default='fake_password',
    help="Defalt password for landb")

version = cfg.IntOpt(
    'version',
    default=6,
    help="Version of SOAP service to use for landb")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    host,
    port,
    protocol,
    user,
    password,
    version,
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
