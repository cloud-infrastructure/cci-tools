#!/usr/bin/python3

import ccitools.conf
import logging
import prettytable
import sys
import tenacity
import urllib.request

from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools import common
from ccitools.utils.sendmail import MailClient
from ccitools.utils.xldap import XldapClient
from datetime import datetime
from dateutil import parser
from string import Template


# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF

MAX_TIMEOUT = 300
RETRY_INTERVAL = 30


class Intervention(object):
    def __init__(self, dictionary):
        for k, v in dictionary.items():
            setattr(self, k, v)


class NotifyInterventionCMD(BaseCloudCMD):

    def __init__(self):
        super(NotifyInterventionCMD, self).__init__(
            description="Sends a notification email to the affected users")

    def get_template_variables(self, interventions):
        vms = []
        projects = []
        summary = prettytable.PrettyTable(["VM", "Date"])
        summary.align["Date"] = 'c'
        summary.align["VM"] = 'c'
        vmsprojects = prettytable.PrettyTable(["VM", "Project"])
        vmsprojects.align["VM"] = 'c'
        vmsprojects.align["Project"] = 'c'
        for intervention in interventions.keys():

            # Format date
            if hasattr(intervention, 'date') and intervention.date != "":
                dt = parser.parse(intervention.date, dayfirst=True)
                date = dt.strftime('%d-%m-%Y')
                hour = dt.strftime('%H:%M')
            else:
                dt_now = datetime.now()
                date = dt_now.strftime('%d-%m-%Y')
                hour = dt_now.strftime('%H:%M')

            # Format new date in case of rescheduling
            if (hasattr(intervention, 'new_date')
                    and (intervention.new_date != "")):
                new_dt = parser.parse(intervention.new_date, dayfirst=True)
                new_date = new_dt.strftime('%d-%m-%Y')
                new_hour = new_dt.strftime('%H:%M')
            else:
                new_date = ""
                new_hour = ""

            # Set duration to 0 if not provided
            if hasattr(intervention, 'duration') and intervention.duration:
                duration = intervention.duration
            else:
                duration = '0'

            for project in interventions[intervention].keys():
                projects.append(project)
                for vm in interventions[intervention][project]:
                    vms.append(vm)
                    summary.add_row(
                        [vm, "%s %s" % (date, hour)])
                    vmsprojects.add_row([vm, project])

            if hasattr(intervention, 'otg') and intervention.otg:
                otg = intervention.otg
                link_placeholder = "Reference: " \
                    "https://cern.service-now.com/" \
                    "service-portal?id=%s&n="
                if 'otg' in intervention.otg.lower():
                    link = link_placeholder % 'outage'
                if 'inc' in intervention.otg.lower():
                    link = link_placeholder % 'ticket'
                if 'rqf' in intervention.otg.lower():
                    link = link_placeholder % 'ticket'
                reference = link + intervention.otg
            else:
                otg = ""
                reference = ""

        return {'summary': summary.get_string(sortby="VM"),
                'otg': otg,
                'project': ', '.join(projects),
                'vmslist': ', '.join(vms),
                'vms': '\n\t'.join(vms),
                'vmcount': len(vms),
                'date': date, 'hour': hour,
                'duration': duration,
                'vmsprojects': vmsprojects.get_string(sortby="Project"),
                'new_date': new_date,
                'new_hour': new_hour,
                'reference': reference}

    @tenacity.retry(stop=tenacity.stop_after_delay(MAX_TIMEOUT),
                    wait=tenacity.wait_fixed(RETRY_INTERVAL),
                    before_sleep=tenacity.before_sleep_log(
                        logger,
                        logging.WARNING))
    def readTemplate(self, url):
        """Read content of a remote template.

        :param url URL where the template is located
        """
        output = urllib.request.urlopen(url)  # nosec
        if output.getcode() == 200:
            return output.read().decode()
        else:
            raise Exception("Template '%s' not found" % url)

    def substitute_template_values(self, interventions, subject, body):
        subject_template = Template(subject)
        body_template = Template(body)
        template_values = self.get_template_variables(interventions)
        return (subject_template.safe_substitute(template_values),
                body_template.safe_substitute(template_values))

    def add_user_to_dict(self, d, user, intervention, project_name, vm):
        if user not in d.keys():
            d[user] = {}
        if intervention not in d[user].keys():
            d[user][intervention] = {}
        if project_name not in d[user][intervention].keys():
            d[user][intervention][project_name] = []
        if vm and vm not in d[user][intervention][project_name]:
            d[user][intervention][project_name].append(vm)
        return d

    def update_users_interventions_vms(self, servers, intervention,
                                       notify_main_user, notify_members,
                                       cloud):
        d = {}

        for server in servers:
            if server.status != 'DELETED':
                if 'landb-responsible' in server.metadata:
                    vm_responsible = server.metadata['landb-responsible']
                else:
                    vm_responsible = server.user_id

                project = cloud.get_project(server.tenant_id)
                d = self.add_user_to_dict(d, vm_responsible.lower(),
                                          intervention, project.name,
                                          server.name)

                if notify_main_user and 'landb-mainuser' in server.metadata:
                    vm_mainuser = server.metadata['landb-mainuser']

                    if vm_responsible != vm_mainuser:
                        d = self.add_user_to_dict(d, vm_mainuser.lower(),
                                                  intervention, project.name,
                                                  server.name)

        if notify_members and intervention.projects != "":
            for project_reference in intervention.projects.split(','):
                project = cloud.find_project(project_reference)
                for member in cloud.get_project_members(project):
                    d = self.add_user_to_dict(
                        d, member.lower(), intervention, project.name, "")
        return d

    def notify_intervention(self, users_interventions_vms, subject, body,
                            mail_from, mail_cc, mail_bcc, content_type,
                            cloud, mailclient, xldap, exec_mode, calendar):

        mail_subject = ''
        mail_body = ''

        for user in users_interventions_vms.keys():
            interventions = users_interventions_vms[user]
            try:
                mail_to = xldap.get_email(user)
            except Exception:
                logger.error("'%s''s email NOT found in LDAP. %s",
                             user, users_interventions_vms[user])
                continue

            mail_subject, mail_body = self.substitute_template_values(
                interventions, subject, body)

            template_vars = self.get_template_variables(interventions)

            summary = template_vars['vmslist']
            day = template_vars['date']
            hour = template_vars['hour']
            date = day + " " + hour
            duration = int(template_vars['duration'])
            event_dsc = 'VMs affected: ' + summary
            if exec_mode:
                try:
                    if calendar:
                        mailclient.send_mail_ics(
                            mail_to=mail_to,
                            mail_subject=mail_subject,
                            mail_body=mail_body,
                            mail_from=mail_from,
                            mail_cc=mail_cc,
                            mail_bcc=mail_bcc,
                            mail_content_type=content_type,
                            organizer=mail_from,
                            attendees=mail_to,
                            title=mail_subject,
                            description=event_dsc,
                            date=date,
                            duration=duration)
                    else:
                        mailclient.send_mail(
                            mail_to=mail_to,
                            mail_subject=mail_subject,
                            mail_body=mail_body,
                            mail_from=mail_from,
                            mail_cc=mail_cc,
                            mail_bcc=mail_bcc,
                            mail_content_type=content_type)
                    logger.info("Mail sent to: %-20s Subject: \"%s\"",
                                mail_to, mail_subject)
                except Exception as ex:
                    logger.error(
                        "Impossible to send email to '%s'. "
                        "Error message: '%s'", mail_to, ex)

            else:
                logger.info('[DRYRUN] Mail sent to: %-20s Subject:"%s"',
                            mail_to, mail_subject)

        if users_interventions_vms:
            example_mail = '\t'.join(('\n' + mail_body.lstrip()).splitlines(
                True))
            logger.info(" Example mail:\n%s", example_mail)
        else:
            logger.info("User list empty. Email not sent")

    def main_notify(self, args, cloud, mailclient, xldap):
        users_interventions_vms = {}
        servers_list = []
        intervention = Intervention(args.intervention[0])

        if hasattr(intervention, 'hypervisors') and \
                intervention.hypervisors != "":
            servers_list += cloud.get_servers_by_hypervisors(
                intervention.hypervisors.split())

        if hasattr(intervention, 'vms') and intervention.vms != "":
            servers_list += cloud.get_servers_by_names(
                intervention.vms.split())

        if hasattr(intervention, 'projects') and intervention.projects != "":
            servers_list += cloud.get_servers_by_projects(
                intervention.projects.split(','))

        users_interventions_vms = self.update_users_interventions_vms(
            servers_list, intervention, args.notify_main_user,
            args.notify_members, cloud)

        self.notify_intervention(users_interventions_vms, args.subject,
                                 self.readTemplate(args.body), args.mail_from,
                                 args.mail_cc, args.mail_cc,
                                 args.mail_content_type,
                                 cloud, mailclient, xldap, args.exec_mode,
                                 args.calendar)

    def main(self, args=None):
        self.parser.add_argument(
            "--subject", required=True, help="email subject")
        self.parser.add_argument(
            "--body", required=True,
            help="Url to the template to be used as email body")
        self.parser.add_argument(
            "--mailfrom", dest='mail_from',
            nargs='?', default="noreply@cern.ch")
        self.parser.add_argument(
            "--mailcc", dest='mail_cc', nargs='?', default="")
        self.parser.add_argument(
            "--mailbcc", dest='mail_bcc', nargs='?', default="")
        self.parser.add_argument(
            "--mail-content-type", default="plain",
            help="Mail content type (plain/html)")
        self.parser.add_argument(
            '--intervention', nargs='*',
            required=True, type=common.escaped_json_loads)
        self.parser.add_argument(
            '--notify-main-user', action='store_true',
            help="If present, also notifies vm's main user")
        self.parser.add_argument(
            '--notify-members', action='store_true',
            help="If present, also notifies project's members")
        self.parser.add_argument(
            '--calendar', action='store_true',
            help="If present, also sends an ICS event "
                 "attached to the notificacion email")

        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true',
                               help="If present, sends email")
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')

        args = self.parse_args(args)

        mailclient = MailClient("localhost")

        xldap = XldapClient(CONF.ldap.server)

        self.main_notify(args, self.cloudclient, mailclient, xldap)


# Needs static method for setup.cfg
def main(args=None):
    NotifyInterventionCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)
