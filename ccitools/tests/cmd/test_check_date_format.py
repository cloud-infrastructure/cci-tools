import logging

from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
class TestCheckDateCMD(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_date_ok(self, mock_crc):
        from ccitools.cmd.check_date_format import main

        main([
             '--date', '20-06-2019 00:00'
             ])

    def test_date_fail(self, mock_crc):
        from ccitools.cmd.check_date_format import main

        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, -1)

    def test_date_fail2(self, mock_crc):
        from ccitools.cmd.check_date_format import main

        with self.assertRaises(SystemExit) as cm:
            main([
                '--date', '20-06'
            ])
        self.assertEqual(cm.exception.code, -1)
