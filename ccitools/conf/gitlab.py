from oslo_config import cfg

url = cfg.StrOpt(
    'url',
    default='https://gitlab.cern.ch',
    help="gitlab url")

private_token = cfg.StrOpt(
    'private_token',
    default='fake_private_token',
    help="private token to use with gitlab api")

version = cfg.IntOpt(
    'version',
    default=4,
    help="Version of gitlab service api to use")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    url,
    private_token,
    version
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
