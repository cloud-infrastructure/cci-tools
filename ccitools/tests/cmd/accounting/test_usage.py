import logging
import requests_mock

from ccitools.cmd.accounting.usage import main
from ccitools.tests.cmd import fixtures
from influxdb.resultset import ResultSet
from unittest import mock
from unittest import TestCase


class TestAccountingUsage(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.accounting.usage.InfluxDBClient')
    @requests_mock.Mocker()
    def test_dryrun_no_data(self, mock_influx, mock_cloud, mock_req):
        mock_req.get(
            'https://gar.cern.ch/public/list_full',
            text=fixtures.CHARGEGROUPS)
        mock_cli = mock_influx.return_value
        mock_cli.query.side_effect = iter([
            ResultSet(fixtures.CORE_METRICS),
            ResultSet(fixtures.VOLUME_METRICS),
            ResultSet(fixtures.FILESHARE_METRICS)
        ])
        mock_cli = mock_cloud.return_value
        mock_cli.list_projects.return_value = []

        with self.assertRaises(SystemExit) as cm:
            main(['--dryrun'])

        self.assertEqual(cm.exception.code, 1)

        mock_influx.assert_called_with(
            'dbod-ccinflux.cern.ch',
            8087,
            'fake_user',
            'fake_password',
            'dblogger',
            ssl=True
        )
