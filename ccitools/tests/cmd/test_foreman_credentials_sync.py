import logging

from ccitools.cmd.foreman_credentials_sync import main
from unittest import TestCase


class TestForemanCredentialsSync(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):
        main([])
