import logging
import requests_mock

from ccitools.cmd.clean_probe_allocation import main
import ccitools.conf
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase

CONF = ccitools.conf.CONF


@mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
@mock.patch('ccitools.utils.cloud.cloud_config')
@requests_mock.Mocker()
class TestCleanProbeAllocation(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self, mock_cc, mock_crc, mock_req):
        mock_req.get(
            CONF.es.server + '/monit_private_openstack_logs_generic-*/_search',
            text=fixtures.PROBE_ALLOCATION)

        mock_crc.return_value = mock_crc
        mock_crc.get_server.return_value = fixtures.SERVER
        main([])

        mock_crc.get_server.assert_called_with(
            'ac55ff16-a14a-433d-b926-0a777b865756')
        mock_crc.get_allocations.assert_not_called()

    def test_dryrun(self, mock_cc, mock_crc, mock_req):
        mock_req.get(
            CONF.es.server + '/monit_private_openstack_logs_generic-*/_search',
            text=fixtures.PROBE_ALLOCATION)

        mock_crc.return_value = mock_crc
        mock_crc.get_server.return_value = fixtures.SERVER

        main(['--dryrun'])

        mock_crc.get_server.assert_called_with(
            'ac55ff16-a14a-433d-b926-0a777b865756')
        mock_crc.get_allocations.assert_not_called()

    def test_instance_not_found_no_allocation(self, mock_cc, mock_crc,
                                              mock_req):
        mock_req.get(
            CONF.es.server + '/monit_private_openstack_logs_generic-*/_search',
            text=fixtures.PROBE_ALLOCATION)

        mock_crc.return_value = mock_crc
        mock_crc.get_server.return_value = None
        mock_crc.get_allocations.return_value = {}
        main([])

        mock_crc.get_server.assert_called_with(
            'ac55ff16-a14a-433d-b926-0a777b865756')
        mock_crc.get_allocations.assert_called_with(
            'ac55ff16-a14a-433d-b926-0a777b865756')
        mock_crc.delete_allocations.assert_not_called()

    def test_instance_not_found_and_allocation(self, mock_cc, mock_crc,
                                               mock_req):
        mock_req.get(
            CONF.es.server + '/monit_private_openstack_logs_generic-*/_search',
            text=fixtures.PROBE_ALLOCATION)

        mock_crc.return_value = mock_crc
        mock_crc.get_server.return_value = None
        mock_crc.get_allocations.return_value = fixtures.ALLOCATIONS
        main([])

        mock_crc.get_server.assert_called_with(
            'ac55ff16-a14a-433d-b926-0a777b865756')
        mock_crc.get_allocations.assert_called_with(
            'ac55ff16-a14a-433d-b926-0a777b865756')
        mock_crc.delete_allocations.assert_called_with(
            'ac55ff16-a14a-433d-b926-0a777b865756', 'regionone')

    def test_instance_not_found_and_allocation_wrong_user(self, mock_cc,
                                                          mock_crc,
                                                          mock_req):
        mock_req.get(
            CONF.es.server + '/monit_private_openstack_logs_generic-*/_search',
            text=fixtures.PROBE_ALLOCATION)

        mock_crc.return_value = mock_crc
        mock_crc.get_server.return_value = None
        mock_crc.get_allocations.return_value = {'user_id': 'svckey'}
        main(['--dryrun'])

        mock_crc.get_server.assert_called_with(
            'ac55ff16-a14a-433d-b926-0a777b865756')
        mock_crc.get_allocations.assert_called_with(
            'ac55ff16-a14a-433d-b926-0a777b865756')
        mock_crc.delete_allocations.assert_not_called()

    def test_dryrun_instance_not_found_and_allocation(self, mock_cc, mock_crc,
                                                      mock_req):
        mock_req.get(
            CONF.es.server + '/monit_private_openstack_logs_generic-*/_search',
            text=fixtures.PROBE_ALLOCATION)

        mock_crc.return_value = mock_crc
        mock_crc.get_server.return_value = None
        mock_crc.get_allocations.return_value = {'user_id': 'svcprobe'}
        main(['--dryrun'])

        mock_crc.get_server.assert_called_with(
            'ac55ff16-a14a-433d-b926-0a777b865756')
        mock_crc.get_allocations.assert_called_with(
            'ac55ff16-a14a-433d-b926-0a777b865756')
        mock_crc.delete_allocations.assert_not_called()
