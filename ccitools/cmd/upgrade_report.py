#!/usr/bin/python3

import ccitools.conf
import logging
import stevedore
import sys

from ccitools.cmd.base.misc import BaseCloudForemanCMD
from ccitools.common import convert_size
from ccitools.common import ssh_executor
from dataclasses import dataclass
from datetime import datetime
from dateutil import parser
from itertools import compress
from keystoneclient.v3 import client as keystone_client
from multiprocessing.pool import ThreadPool
from novaclient import client as nova_client
from osc_lib import utils

logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF

# formatters
_formatter_namespace = 'cliff.formatter.list'
_formatter_default = 'table'


def _load_formatter_plugins():
    # Here so tests can override
    return stevedore.ExtensionManager(
        _formatter_namespace,
        invoke_on_load=True, )


_formatter_plugins = _load_formatter_plugins()
_undesired_fields = ['project_id', 'owner', 'coordinator', 'member']


@dataclass
class Entry():
    cell: str
    id: str
    name: str
    status: str
    project_id: str
    project: str
    flavor: str
    bfv: str
    host: str
    eds: str
    cold: str
    attempts: str


class UpgradeReportCMD(BaseCloudForemanCMD):
    def __init__(self):
        super(UpgradeReportCMD, self).__init__(
            description="Upgrade progress report")
        self.entries = []
        self.cache = {}

    def get_cold_hint(self, size):
        if size > (80 * (1024 ** 3)) and size <= (160 * (1024 ** 3)):
            return "*"
        if size > (160 * (1024 ** 3)) and size <= (320 * (1024 ** 3)):
            return "**"
        if size > (320 * (1024 ** 3)):
            return "***"
        return ""

    def __is_recent_migration_attempt(self, migration, campaign_start_date):
        return migration.status in ('error', 'cancelled') and \
            migration.updated_at and \
            (parser.parse(migration.updated_at)
             - parser.parse(migration.created_at)).seconds > 60 and \
            (parser.parse(migration.updated_at) > campaign_start_date)

    def __analyse_disk_usage(self, hypervisor, instance_id, skip_disk):
        if skip_disk:
            return "XXX", "XXX"

        try:
            cmd = f"stat -c '%s' /var/lib/nova/instances/{instance_id}/disk"
            disk_size_bytes = int(ssh_executor(host=hypervisor,
                                               command=cmd)[0][0])
            return convert_size(disk_size_bytes), \
                self.get_cold_hint(disk_size_bytes)
        except Exception as ex:
            logger.error(
                f"Failed to fetch disk data for {instance_id} running on \
                    {hypervisor}: {ex}")
            return "XXX", "XXX"

    def __populate_cell(self, cell_name, mode, search_constraint,
                        aggregates, skip_migration, skip_disk,
                        campaign_start_date, nova_client):

        if mode:
            # Uses nova to find hosts
            hosts = [host for a in aggregates for host in a.hosts]
        else:
            # Uses foreman to find hosts
            if search_constraint:
                query = f"level2 and {cell_name} and {search_constraint}"
            else:
                query = f"level2 and {cell_name}"

            result = self.foreman.search_query('hosts', query)
            hosts = [h['name'] for h in result]

        logger.info(
            f"Found {len(hosts)} remaining hypervisors for cell {cell_name}")

        for hv in hosts:
            for a in nova_client.servers.list(search_opts={'all_tenants': True,
                                                           'host': hv}):
                migrations = []
                if not skip_migration:
                    migrations = [m for m in nova_client.migrations.list(
                        instance_uuid=a.id) if
                        self.__is_recent_migration_attempt(
                            m, campaign_start_date)]
                try:
                    project_name = self.projects[a.tenant_id]
                except KeyError:
                    project_name = "PROJECT UNKNOWN"
                is_bfv = a.image == ""
                disk_size, cold_hint = ("N/A", "") if is_bfv \
                    else self.__analyse_disk_usage(hv, a.id, skip_disk)

                self.entries.append(
                    Entry(cell=cell_name,
                          id=a.id,
                          name=a.name,
                          status=a.status,
                          project_id=a.tenant_id,
                          project=project_name,
                          flavor=a.flavor['original_name'],
                          bfv=is_bfv,
                          host=getattr(a, 'OS-EXT-SRV-ATTR:host'),
                          eds=disk_size,
                          cold=cold_hint,
                          attempts=len(migrations)))

    def upgrade_report(self, args):
        nc = nova_client.Client(version="2.79",
                                session=self.cloudclient.session)

        logger.info("Loading project list to resolve names")
        kc = keystone_client.Client(session=self.cloudclient.session)
        self.projects = {p.id: p.name for p in kc.projects.list()}

        logger.info("Loading role list to resolve names")
        self.roles = {r.name: r.id for r in kc.roles.list()}

        # Infer cells
        aggs = nc.aggregates.list()
        cells = set([a.metadata['cell'] for a in aggs
                     if 'cell' in a.metadata.keys()])
        logger.info(f"List of cells to report: {cells}")

        if args.cells:
            logger.info(f"Filter only specified cells: {args.cells}")
            cells = cells.intersection(args.cells)

        pool_tasks = []
        pool = ThreadPool(processes=args.parallelism)
        for cell in cells:
            aggregates_in_cell = [
                a for a in aggs if (
                    'cell' in a.metadata.keys()
                    and a.metadata['cell'] == cell
                )
            ]
            pool_tasks.append(
                pool.apply_async(
                    self.__populate_cell, (
                        cell,
                        args.mode,
                        args.old_hv_query,
                        aggregates_in_cell,
                        args.skip_migration_calculation,
                        args.skip_disk_calculation,
                        args.campaign_start_date,
                        nc
                    )
                )
            )

        pool.close()
        pool.join()

        for task in pool_tasks:
            task.get()

        # Sort entries by name
        self.entries.sort(key=lambda x: x.name)

        # Only consider columns that have desired elements
        columns = []
        for entry in self.entries:
            for c in entry.__dict__.keys():
                if c not in columns:
                    columns.append(c)

        # et voilà
        if args.output:
            # Remove columns like owner, coordinator, member from the file
            columns_file = [c for c in columns if c not in _undesired_fields]
            table_formatter = _formatter_plugins['table'].obj
            data = (utils.get_item_properties(s, columns_file, formatters={})
                    for s in self.entries)
            with open(args.output, 'w') as w:
                table_formatter.emit_list(columns_file, data, w, args)

        if not args.quiet:
            formatter = _formatter_plugins[args.formatter].obj
            columns_to_include, selector = self._generate_columns_and_selector(
                args, columns)
            data = (utils.get_item_properties(s, columns, formatters={})
                    for s in self.entries)
            if selector:
                data = (list(compress(row, selector)) for row in data)
            formatter.emit_list(columns_to_include, data, sys.stdout, args)

    def _generate_columns_and_selector(self, args, column_names):
        if not args.columns:
            columns_to_include = column_names
            selector = None
        else:
            columns_to_include = [c for c in column_names if c in args.columns]
            if not columns_to_include:
                raise ValueError(
                    'No recognized column names in %s' % str(args.columns))
            # Set up argument to compress()
            selector = [(c in columns_to_include) for c in column_names]
        return columns_to_include, selector

    def main(self, args=None):
        """Generate upgrade progress report."""
        self.parser.add_argument(
            '--output',
            default=None,
            help="path where the report will be stored")

        self.parser.add_argument(
            '--parallelism',
            type=int,
            default=1,
            help="number of parallel threads")

        fetch_mode = self.parser.add_mutually_exclusive_group()
        fetch_mode.add_argument(
            '--aggregates',
            dest="mode",
            action='store_true',
            help="If present, uses nova to find hypervisors")
        fetch_mode.add_argument(
            '--foreman',
            dest="mode",
            action='store_false',
            help="If present, uses foreman to find hypervisors")

        self.parser.add_argument(
            '--old-hv-query',
            default='',
            help="foreman query to identify hypervisors")
        self.parser.add_argument(
            '--cell',
            action='append',
            default=[],
            dest='cells',
            help='specify the cells to filter, can be repeated. Default: []', )
        self.parser.add_argument(
            '--campaign-start-date',
            type=lambda d: datetime.strptime(d, '%d-%m-%Y'),
            help="date used to compute live migration attempts"
        )
        self.parser.add_argument(
            '--skip-migration-calculation',
            action='store_true',
            help="If present, it skips the calculation of migrations")
        self.parser.add_argument(
            '--skip-disk-calculation',
            action='store_true',
            help="If present, it skips the calculation of migrations")
        self.parser.add_argument(
            '--quiet',
            default=False,
            action='store_true',
            help='Do not display the results at the end', )
        formatter_group = self.parser.add_argument_group(
            title='output formatters',
            description='output formatter options', )
        formatter_choices = sorted(_formatter_plugins.names())
        formatter_default = _formatter_default
        if formatter_default not in formatter_choices:
            formatter_default = formatter_choices[0]
        formatter_group.add_argument(
            '-f',
            '--format',
            dest='formatter',
            action='store',
            choices=formatter_choices,
            default=formatter_default,
            help='the output format, defaults to %s' % formatter_default, )
        formatter_group.add_argument(
            '-c',
            '--column',
            action='append',
            default=[],
            dest='columns',
            metavar='COLUMN',
            help='specify the column(s) to include, can be repeated', )
        for formatter in _formatter_plugins:
            formatter.obj.add_argument_group(self.parser)

        args = self.parse_args(args)
        self.upgrade_report(args)


def main(args=None):
    UpgradeReportCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
