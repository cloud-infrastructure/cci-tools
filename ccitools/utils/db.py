import ccitools.conf
import json
import logging

from sqlalchemy import create_engine, Boolean, Column, String, Integer, Text
from sqlalchemy.dialects.mysql import MEDIUMTEXT
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import sessionmaker

# configure logging
logger = logging.getLogger(__name__)
Base = declarative_base()
CONF = ccitools.conf.CONF


def MediumText():
    return Text().with_variant(MEDIUMTEXT(), 'mysql')


class APIInstanceMapping(Base):
    __tablename__ = 'instance_mappings'
    id = Column(Integer, primary_key=True)
    instance_uuid = Column(String)
    cell_id = Column(String)


class APICellMapping(Base):
    __tablename__ = 'cell_mappings'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    database_connection = Column(String)


class CellInstanceInfoCache(Base):
    __tablename__ = 'instance_info_caches'
    id = Column(Integer, primary_key=True)
    network_info = Column(MediumText())
    instance_uuid = Column(String)


class BlockDeviceMapping(Base):
    __tablename__ = 'block_device_mapping'
    id = Column(Integer, primary_key=True)
    delete_on_termination = Column(Boolean, default=False)
    volume_id = Column(String)
    instance_uuid = Column(String)
    deleted = Column(Integer, default=0)


class NovaDB(object):
    def get_cell_connection_string(self, instance_uuid):
        database_connection = None
        api_engine = create_engine(CONF.nova.api_connection)
        api_session = sessionmaker(bind=api_engine)()
        try:
            query = api_session.query(APIInstanceMapping).filter_by(
                instance_uuid=instance_uuid)
            cell_id = [a for a in query][0].cell_id

            query = api_session.query(APICellMapping).filter_by(
                id=cell_id)
            database_connection = [c for c in query][0].database_connection
        finally:
            api_session.close()
        return database_connection

    def get_preserve_on_delete(self, cell_db, instance_uuid):
        cell_engine = create_engine(cell_db)
        cell_session = sessionmaker(bind=cell_engine)()
        value = None
        try:
            query = cell_session.query(CellInstanceInfoCache).filter_by(
                instance_uuid=instance_uuid)
            value = True
            for entry in query:
                try:
                    info = json.loads(entry.network_info)
                except Exception as e:
                    logger.exception(e)
                    continue
                for connection in info:
                    value &= connection['preserve_on_delete']
        finally:
            cell_session.close()
        return value

    def set_preserve_on_delete(self, cell_db, instance_uuid, preserve):
        cell_engine = create_engine(cell_db)
        cell_session = sessionmaker(bind=cell_engine)()
        try:
            query = cell_session.query(CellInstanceInfoCache).filter_by(
                instance_uuid=instance_uuid)
            for entry in query:
                try:
                    info = json.loads(entry.network_info)
                except Exception as e:
                    logger.exception(e)
                    continue
                for connection in info:
                    connection['preserve_on_delete'] = preserve
                entry.network_info = json.dumps(info,
                                                ensure_ascii=False)
                cell_session.commit()
        finally:
            cell_session.close()

    def get_delete_on_termination(self, cell_db, instance_uuid, volume_id):
        cell_engine = create_engine(cell_db)
        cell_session = sessionmaker(bind=cell_engine)()
        value = None
        try:
            query = cell_session.query(BlockDeviceMapping).filter_by(
                deleted=0).filter_by(instance_uuid=instance_uuid).filter_by(
                volume_id=volume_id)
            for entry in query:
                value = entry.delete_on_termination
        finally:
            cell_session.close()
        return value

    def set_delete_on_termination(self, cell_db, instance_uuid, volume_id,
                                  value):
        cell_engine = create_engine(cell_db)
        cell_session = sessionmaker(bind=cell_engine)()
        try:
            query = cell_session.query(BlockDeviceMapping).filter_by(
                deleted=0).filter_by(instance_uuid=instance_uuid).filter_by(
                volume_id=volume_id)
            for entry in query:
                entry.delete_on_termination = value
                cell_session.commit()
        finally:
            cell_session.close()
