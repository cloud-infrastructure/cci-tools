import logging

from ccitools.errors import SSHException
from ccitools.verify.no_contact import NoContactAction
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.verify.no_contact.ssh_executor')
class TestNoContactAction(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.client = NoContactAction(host='localhost')

    def test_try_fix(self, mock_ssh):
        self.client.try_fix()

    def test_check_alarm_state(self, mock_ssh):
        mock_ssh.return_value = [], None
        self.assertTrue(self.client.check_alarm_state())

        mock_ssh.return_value = ['localhost'], None
        self.assertFalse(self.client.check_alarm_state())

        mock_ssh.return_value = ['nonexistent'], None
        self.assertTrue(self.client.check_alarm_state())

        mock_ssh.side_effect = iter([SSHException('Fake')])
        self.assertTrue(self.client.check_alarm_state())


class TestNoContactActionNonExisting(TestCase):

    def setUp(self):
        self.client = NoContactAction(host='nonexistent')

    def test_try_fix(self):
        self.client.try_fix()

    def test_check_alarm_state(self):
        self.assertTrue(self.client.check_alarm_state())
