#!/usr/bin/python3

import ccitools.conf
import logging
import requests
import subprocess  # nosec
import sys

from ccitools.cmd import base
from datetime import date
from datetime import datetime

# configure logging
logger = logging.getLogger(__name__)

CONF = ccitools.conf.CONF
TODAY = datetime.now()
YEAR = date.today().year


class SnowflakeReminderCMD(base.BaseCMD):
    def __init__(self):
        super(SnowflakeReminderCMD, self).__init__(
            description="Script to send a reminder about the snow week")

    def git_clone(self):
        """Clone git repo."""
        logger.info("Clonning repo...")
        subprocess.run([    # nosec
            "git",
            "clone",
            "-q",
            CONF.snowflake.repository,
            CONF.snowflake.folder
        ])

    def remove_repo(self):
        """Remove git repo."""
        logger.info("Removing repo...")
        subprocess.run(["rm", "-rf", CONF.snowflake.folder])    # nosec

    def get_logbook_lines(self):
        """Get logbook file lines.

        Returns:
            array: logbook lines
        """
        with open(CONF.snowflake.folder + CONF.snowflake.filename) as f:
            lines = f.read().splitlines()
            return lines

    def get_rota_day(self, line):
        """Get future rota day.

        Args:
            line (string): logbook line

        Returns:
            datetime: formatted rota date
        """
        new_line = line.split('#')
        parsed_line = new_line[2].split('-')
        if parsed_line[0].strip() != 'X':
            date = parsed_line[0].strip()
            rota_day = datetime.strptime(date, "%a, %b %d, %Y")
            if rota_day > TODAY:
                return rota_day
            return None

    def get_snowflake(self, line):
        """Get snowflake name.

        Args:
            line (string): logbook line

        Returns:
            string: snowflake name
        """
        new_line = line.split(':')
        return new_line[1].strip()

    def notify_mattermost(self, next_rota, args):
        """Execute a post request to generate a mattermost notification.

        Args:
            next_rota (dict): dictionary containing rota day and snowflake
            args: cli arguments
        """
        url = args.mm_url
        payload = {
            "icon_emoji": ":snowman:",
            "username": "Snowflake Reminder",
            "text": (
                CONF.snowflake.message % (
                    next_rota["day"], next_rota["snowflake"]
                )
            )
        }
        try:
            response = requests.post(
                url, json=payload, timeout=60)
            response.raise_for_status()
        except Exception as ex:
            logger.error("Exception: %s" % (ex))

    def snowflake_reminder(self, args):
        try:
            next_rota = {
                "day": "",
                "snowflake": ""
            }
            self.git_clone()
            lines = self.get_logbook_lines()
            for i in range(0, len(lines)):
                line = lines[i]
                # Lines starting by '##', containing the current year
                # And we discard the ones containing 'X-MAS'
                if line[:2] == '##' and str(YEAR) in line \
                        and 'X-MAS' not in line:
                    day = self.get_rota_day(line)
                    if day is not None:
                        if next_rota["day"] == "" or next_rota["day"] > day:
                            next_rota["day"] = day
                        next_line = lines[i + 1]
                        if next_line != "":
                            next_rota["snowflake"] =\
                                self.get_snowflake(next_line)
                        elif lines[i + 2] != "":
                            next_rota["snowflake"] =\
                                self.get_snowflake(lines[i + 2])
                        else:
                            logger.info("Not able to get snowflake")

            # Convert datetime to string
            next_rota["day"] = \
                datetime.strftime(next_rota["day"], "%a, %b %d, %Y")
            if args.exec_mode:
                self.notify_mattermost(next_rota, args)
            else:
                logger.info(CONF.snowflake.message %
                            (next_rota["day"], next_rota["snowflake"]))
            self.remove_repo()
        except Exception as ex:
            logger.error(
                "Error running snowflake reminder. Exception: %s" % ex
            )

    def main(self, args=None):
        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true',
                               help="If present, performs the action")
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')
        self.parser.add_argument('--mm_url', required=True,
                                 help="Mattermost hook URL")

        args = self.parse_args(args)

        self.snowflake_reminder(
            args
        )


# Needs static method for setup.cfg
def main(args=None):
    SnowflakeReminderCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
