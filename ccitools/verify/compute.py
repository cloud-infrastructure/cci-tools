import logging
import time

from ccitools.common import ssh_executor
from ccitools.errors import SSHException
from ccitools.verify import base

# configure logging
logger = logging.getLogger(__name__)

FETCH_VALUES_SCRIPT = '''
  adapter=$(brctl show | grep -v 'bridge name' | grep -v nic | grep -v tap | cut -f6)
  if [ -z "$adapter" ]; then
    adapter=$(route | grep '^default' | grep -o '[^ ]*$' | head -n1)
  fi
  network_speed=$(ethtool "$adapter" 2>/dev/null | grep Speed | cut -d: -f2 | cut -d'M' -f1)
  if ! [[ $network_speed =~ [0-9]+ ]] ; then
    network_speed=0
  fi
  echo $network_speed
  alarm_speed=$(grep network_speed_wrong /etc/collectd.d/90-alarms-config.conf -A1 | grep FailureMax | xargs | cut -d' ' -f2)
  if ! [[ $alarm_speed =~ [0-9]+ ]] ; then
    alarm_speed=0
  fi
  echo $alarm_speed
'''  # noqa: E501

RENEGOTIATE_SPEED_SCRIPT = '''
adapter=$(brctl show | grep -v "bridge name" | grep -v nic | grep -v tap | cut -f6);
if [ -z "$adapter" ]; then
  adapter=$(route | grep "^default" | grep -o "[^ ]*$" | head -n1);
fi
ethtool -r $adapter
'''  # noqa: E501


class NetworkSpeedAction(base.ActionBase):

    def __init__(self, host, delay=120):
        super(NetworkSpeedAction, self).__init__(host, delay)
        self.pattern = (r'.*\/exec_compute\/gauge-network_speed')

    def try_fix(self):
        """Implement the fix for the exception thrown in collectd."""
        # Try to start the renegotiate the network speed in the machine
        try:
            logger.info("Triggering renegotiate network speed")
            ssh_executor(self.host, RENEGOTIATE_SPEED_SCRIPT)
        except SSHException:
            pass

    def check_alarm_state(self, after_try=False):
        ret = True
        if after_try:
            time.sleep(self.delay)

        logger.info("Fetching current network speed and alarm speed...")
        try:
            out, err = ssh_executor(self.host, FETCH_VALUES_SCRIPT)
            if out[0] == out[1]:
                ret = False
        except SSHException:
            pass

        if ret:
            self.reason = (
                " - %s --> Renegotiate network speed"
                "has not cleaned the alarm." % self.host
            )
        return ret
