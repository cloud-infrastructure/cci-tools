from ccitools import common
from parameterized import parameterized
from unittest import TestCase


class TestCommon(TestCase):

    @parameterized.expand([
        [0, "0 B"],
        [1024 ** 3, "1.0 GB"],
        [161 * (1024 ** 3), "161.0 GB"]
    ])
    def test_convert_size_zero(self, size_input, str_output):
        self.assertEqual(common.convert_size(size_input), str_output)

    @parameterized.expand([
        ["26-10-2023 13:00", "2023-10-26T13:00:00+02:00"],
        ["02-10-2023 14:00", "2023-10-02T14:00:00+02:00"],  # Ambiguous
        ["2023-10-02 14:00", "2023-10-02T14:00:00+02:00"],
        ["1985-02-10 19:20", "1985-02-10T19:20:00+01:00"],
        ["2017-09-08T15:42:43+0200", "2017-09-08T15:42:43+02:00"]
    ])
    def test_date_parser(self, date_input, iso_date_output):
        self.assertEqual(common.date_parser(date_input), iso_date_output)
