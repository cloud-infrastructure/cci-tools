import ccitools.conf

from aitools.foreman import ForemanClient
from ccitools.cmd.base import BaseCMD

CONF = ccitools.conf.CONF


class BaseForemanCMD(BaseCMD):
    def __init__(self, description):
        super(BaseForemanCMD, self).__init__(description=description)

        self.foreman = ForemanClient(
            host=CONF.foreman.host,
            port=CONF.foreman.port,
            timeout=CONF.foreman.timeout,
            deref_alias=True)
