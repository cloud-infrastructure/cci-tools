#!/usr/bin/python3

import ccitools.conf
import logging
import os
import subprocess  # nosec
import sys
import time

from ccitools.cmd.base.cloud import BaseCloudCMD
from novaclient import client as nova_client
# configure logging
# logging.basicConfig(level=logging.INFO,
#                    format="%(asctime)s %(levelname)s %(name)s - %(message)s")
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF

MAX_TIMEOUT = 14400  # 4 hours
SLEEP_TIME = 15
INCREMENT = 0
PING_UNAVAILABLE = 5
PING_FREQUENCY = 2


def ping_instance(hostname):
    """Ping instance hostname."""
    cmd = ['ping', '-c', '1', hostname]
    with open(os.devnull, 'w') as DEVNULL:
        try:
            subprocess.check_call(cmd,  # nosec
                                  stdout=DEVNULL,
                                  stderr=DEVNULL)
            logger.debug("[{} is alive]".format(hostname))
        except Exception as e:
            logger.info("[{} is unreachable]. {}".format(hostname, e))
            return False
        return True


def probe_instance_availability(hostname):
    """Probe the instance availability (ping)."""
    start_time = time.time()
    unavailable_counter = 0
    while (time.time() < start_time + SLEEP_TIME):
        if not ping_instance(hostname):
            unavailable_counter += 1
        else:
            unavailable_counter = 0
        if unavailable_counter > PING_UNAVAILABLE:
            return False
        else:
            time.sleep(PING_FREQUENCY)
    return True


class MigrationInNodeCMD(BaseCloudCMD):

    def __init__(self):
        super(MigrationInNodeCMD, self).__init__(
            description="Migrates all instances running in a compute node")

    def get_migration_id(self, novaclient, server):
        migration_id = None
        try:
            migrationlist = novaclient.migrations.list(instance_uuid=server.id)
        except Exception:
            logger.error("failed to get migration id for {}"
                         .format(server.name))
            return migration_id
        for migration in migrationlist:
            if (migration.status.lower() != 'completed'
               and migration.status.lower() != 'error'
               and migration.status.lower() != 'cancelled'):
                migration_id = migration.id
                break
        return migration_id

    def make_nova_client(self, cloudclient):
        try:
            nc = nova_client.Client(version='2.56',
                                    session=self.cloudclient.session,
                                    region_name='cern')
        except Exception as e:
            logger.error("Failed to make nova client {}".format(e))
            return None
        return nc

    def abort_live_migration(self, cloudclient, server, exec_mode):
        nc = self.make_nova_client(cloudclient)
        migration_id = self.get_migration_id(nc, server)
        try:
            nc.server_migrations.live_migration_abort(server, migration_id)
        except Exception as e:
            logger.error("Failed to abort live migration of {}. {}"
                         .format(server.name, e))
            return False
        logger.info("{} migration aborted.".format(server.name))
        return True

    def live_migration(self, cloudclient, server, hypervisor, exec_mode):
        # ping before live migration starts
        # instance_first_ping_status == True . Ping was reachable
        # instance_first_ping_status == False. Unreachable
        instance_first_ping_status = ping_instance(server.name)

        # if unreachable from beginning. do not probe instance
        if not instance_first_ping_status:
            logger.info("[{} unreachable][do not probe instance]"
                        .format(server.name))

        # start time
        start = time.time()

        if not exec_mode:
            logger.info("[DRYRUN] Live migration on {} : {} started."
                        .format(server.name, server.id))
            return True

        logger.info("Live migration on {} : {} started."
                    .format(server.name, server.id))
        # check if volume is attached to an instance
        if server._info["image"]:
            # if image is attached that means not booted from volume
            logger.info("block migration true")
            try:
                server.live_migrate(host=None, block_migration=True)
            except Exception as e:
                logger.error("Error during block live migration {}".format(e))
                sys.exit(1)
        else:
            # volume is attached set block migration to False
            logger.info("block migration false")
            try:
                server.live_migrate(host=None, block_migration=False)
            except Exception as e:
                logger.error("Error during live migration {}".format(e))
                return False

        INCREMENT = 0
        while MAX_TIMEOUT > INCREMENT:
            # only probe if first ping was success
            if instance_first_ping_status:
                # if probing fail abort live migration
                if not probe_instance_availability(server.name):
                    self.abort_live_migration(cloudclient, server, exec_mode)

            INCREMENT = INCREMENT + 15
            time.sleep(SLEEP_TIME)
            logger.info("{} Live migration progress : {}s"
                        .format(server.name, INCREMENT))

            # get updated server instance
            try:
                ins = cloudclient.get_server(server.id)
            except Exception as e:
                logger.error("Failed to get server instance {}".format(e))
                return False
            # get instance host
            ins_dict = ins.to_dict()

            # check ERROR state of VM
            if ins.status == "ERROR":
                logger.info("VM Migration failed. VM now in ERROR mode")
                return False

            # check if live migration cmd was even successful
            if ins.status != "MIGRATING":
                if hypervisor in \
                    ins_dict['OS-EXT-SRV-ATTR:hypervisor_hostname'] \
                        and ins.status == "ACTIVE":
                    logger.error("live migration failed")
                    return False

            # check if host and status has changed
            if hypervisor not in  \
                ins_dict['OS-EXT-SRV-ATTR:hypervisor_hostname'] \
                    and ins.status == "ACTIVE":
                logger.info("{} migrated to New Host : {}".format(
                            server.name,
                            ins_dict['OS-EXT-SRV-ATTR:hypervisor_hostname']))
                logger.info("{} state : {} \n".format(server.name, ins.status))
                logger.info("{} completed in {}"
                            .format(server.name,
                                    round(time.time() - start, 2)))
                return True
        return False

    def cold_migration(self, cloudclient, server, hypervisor, exec_mode):

        # start time
        start = time.time()

        if not exec_mode:
            logger.info("[DRYRUN] Cold migration on {} : {} started."
                        .format(server.name, server.id))
            return True

        logger.info("Cold migration on %s : %s started.",
                    server.name, server.id)
        try:
            server.migrate()
            logger.info("Server migrate executed, wait for state change")
            time.sleep(SLEEP_TIME)
        except Exception as e:
            logger.error("Error during cold migration {}".format(e))
            return False

        # cold migration checks
        INCREMENT = 0
        while MAX_TIMEOUT > INCREMENT:
            INCREMENT = INCREMENT + 15
            time.sleep(SLEEP_TIME)
            logger.info("{} Cold migration progress : {}s"
                        .format(server.name, INCREMENT))
            # get updated server instance
            try:
                ins = cloudclient.get_server(server.id)
                ins_dict = ins.to_dict()
            except Exception as e:
                logger.error("Failed to get server instance {}".format(e))
                return False

            # check if the state has changed to Error
            if ins.status == "ERROR":
                logger.info("cold migration cmd failed.")
                return False

            if ins_dict["OS-EXT-STS:task_state"] is None \
                    and ins.status == "SHUTOFF":
                logger.error("server migrate cmd failed")
                return False

            # next wait for RESIZE to VERIFY_RESIZE
            if ins.status == "RESIZE" \
                and (ins_dict["OS-EXT-STS:task_state"] == "RESIZE_PREP"
                     or ins_dict["OS-EXT-STS:task_state"] == "RESIZE_MIGRATING"
                     or ins_dict["OS-EXT-STS:task_state"] == "RESIZE_MIGRATED"
                     or ins_dict["OS-EXT-STS:task_state"] == "RESIZE_FINISH"):
                continue

            # if state is VERIFY_RESIZE exit the loop
            if ins.status == "VERIFY_RESIZE" and \
                    ins_dict["OS-EXT-STS:task_state"] is None:
                break

        # perform server.confirm_resize()
        if ins.status == "VERIFY_RESIZE":
            try:
                ins.confirm_resize()
            except Exception as e:
                logger.error("Confirm resize operation failed {}".format(e))
                sys.exit(1)

        INCREMENT = 0
        while INCREMENT < 30:
            time.sleep(SLEEP_TIME)
            INCREMENT = INCREMENT + SLEEP_TIME
            # get updated server instance
            try:
                ins = cloudclient.get_server(server.id)
                ins_dict = ins.to_dict()
            except Exception as e:
                logger.error("Failed to get server instance {}".format(e))
                return False
            # Check if host has changed & VM state is back to SHUTOFF or ACTIVE
            if hypervisor not in  \
                ins_dict["OS-EXT-SRV-ATTR:hypervisor_hostname"] \
                    != hypervisor and (ins.status == "SHUTOFF"
                                       or ins.status == "ACTIVE"):
                logger.info("{} status : {}"
                            .format(server.name, ins.status))
                logger.info("{} migrated to New Host : {} \n"
                            .format(server.name, ins_dict[
                                'OS-EXT-SRV-ATTR:hypervisor_hostname']))
                logger.info("{} completed in {}".format(server.name,
                            round(time.time() - start, 2)))
                return True
        return False

    def hypervisor_has_vms(self, cloudclient, hypervisor, exec_mode):
        # List of servers
        try:
            servers = cloudclient.get_servers_by_hypervisor(hypervisor)
        except Exception as e:
            logger.error("Error in retrieving servers from HV {}".format(e))
            sys.exit(1)
        logger.info("{} has following VMs : {}".format(hypervisor, servers))
        if servers:
            for server in servers:
                # get updated VM state each time
                # because migration takes time and
                # other VM state might change in mean time
                logger.info("initial state of {} : {}"
                            .format(server.name, server.status))
                server_name = server.name
                try:
                    server = cloudclient.get_server(server.id)
                except Exception as e:
                    logger.error("Error in getting server instance {}"
                                 .format(e))
                    sys.exit(1)
                # check if server still exists
                if server is None:
                    logger.info("Server %s no longer exists/found. ",
                                server_name)
                    continue
                logger.info("updated state {} : {}"
                            .format(server.name, server.status))

                # convert server obj to dict to get task state
                server_dict = server.to_dict()
                # check task state
                if server_dict["OS-EXT-STS:task_state"] is None:
                    if server.status == "ACTIVE":
                        res = self.live_migration(cloudclient,
                                                  server,
                                                  hypervisor,
                                                  exec_mode)
                    elif server.status == "SHUTOFF":
                        # do cold migration
                        res = self.cold_migration(cloudclient,
                                                  server,
                                                  hypervisor,
                                                  exec_mode)
                    else:
                        logger.info("Server : %s failed to migrate. \
                        Not in ACTIVE or SHUTOFF status", server.name)
                        res = False
                    # store result if false break
                    if (not res):
                        logger.info("Migration failed for VM : %s",
                                    server.name)
                        self.abort_live_migration(cloudclient,
                                                  server,
                                                  exec_mode)
                        continue
                else:
                    logger.info("Server : %s can't be migrated", server.name)
                    logger.info("Server %s task state not NONE",
                                server.name)
        else:
            logger.warning(
                "Did NOT found VMs in the provided list of hypervisors.")

    def post_migration_VM_Check_in_HV(self,
                                      cloudclient,
                                      hypervisor,
                                      exec_mode):
        if not exec_mode:
            logger.info("[DRYRUN] NO VMs in {} ".format(hypervisor))
            return False
        # List of servers
        try:
            servers = cloudclient.get_servers_by_hypervisor(hypervisor)
        except Exception as e:
            logger.error("Error in retrieving servers from HV {}".format(e))
            sys.exit(1)
        if servers:
            logger.info("{} has following VMs : {}"
                        .format(hypervisor, servers))
            return True
        else:
            logger.info(
                "Did NOT found VMs in the provided in {}".format(hypervisor))
            return False

    def main(self, args=None):
        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true',)
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')
        self.parser.add_argument(
            "--hypervisors", required=True,
            help="Name of hosts to migrate VMs from.")

        args = self.parse_args(args)

        # start time
        start = time.time()

        # execution mode
        if not args.exec_mode:
            logger.info("[DRYRUN] Execution mode.")
        else:
            logger.info("[PERFORM] Execution mode.")

        # process each hypervisor
        for hypervisor in args.hypervisors.split():
            logger.info("Current HV : {}".format(hypervisor))
            # get hypervisor state
            HV_state = self.cloudclient.get_hypervisor_status(
                hypervisor)
            if HV_state == "up":
                self.hypervisor_has_vms(self.cloudclient, hypervisor,
                                        args.exec_mode)
            elif HV_state == "down":
                logger.info("Hypervisor {} is down".format(hypervisor))
            else:
                logger.info("Unknown Hypervisor state")

        # check if HV still has any VMs after migration jobs
        for hypervisor in args.hypervisors.split():
            if self.post_migration_VM_Check_in_HV(self.cloudclient,
                                                  hypervisor,
                                                  args.exec_mode):
                logger.info("{} still has VMs in it. cannot reboot"
                            .format(hypervisor))
                sys.exit(127)
            else:
                logger.info("{} is ready for reboot. No VMs found"
                            .format(hypervisor))

        logger.info('finish in {} second(s)'
                    .format(round(time.time() - start, 2)))


# Needs static method for setup.cfg
def main(args=None):
    MigrationInNodeCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
