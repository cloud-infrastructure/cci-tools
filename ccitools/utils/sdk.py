import openstack.cloud
import openstack.config
import openstack.connection
from openstack.image.v2 import image as _image


class SDKClient(object):
    """SDK client encapsulating operations in the OpenStack cloud.

    This class is designed as a facade to facilitate the interaction with
    all the components of the CERN OpenStack Cloud.
    """

    def __init__(self, cloud='cern', region='cern', interface='public'):
        """Initialize the facade to the Cloud operations.

        :param cloud: cloud to use for the clients
        :parar region_name: region to connect
        """
        self.cloud = self.get_cloud(
            cloud=cloud,
            region=region,
            interface=interface
        )

    def get_cloud(self, cloud='cern', region='cern', interface='public'):
        config = openstack.config.get_cloud_region(
            cloud=cloud,
            region_name=region,
            interface=interface)
        return openstack.connection.Connection(config=config)

    def get_endpoints_per_service(self, service_type, iface='public',
                                  region=None):
        # Retrieve the catalog from the session
        catalog = self.cloud.service_catalog

        # Get the endpoints for a service type
        if catalog:
            endpoints = [entry['endpoints'] for entry in catalog
                         if entry['type'] == service_type].pop()
        else:
            endpoints = []

        if iface:
            endpoints = [ep for ep in endpoints if ep['interface'] == iface]
        if region:
            endpoints = [ep for ep in endpoints if ep['region'] == region]
        return endpoints

    def get_regions_per_service(self, service_type, region=None,
                                iface='public',
                                tag='production'):
        endpoints = self.get_endpoints_per_service(service_type, iface)

        # Retrieve the unique regions with the interface requested
        regions = list(set([endpoint['region'] for endpoint in endpoints]))
        # Return only the regions that are in production
        regions = [r for r in regions if self.has_region_tag(r, tag)]
        if region:
            return [region] if region in regions else []
        return regions

    def is_service_available_in_region(self,
                                       service_type,
                                       region,
                                       session=None,
                                       iface='public'):
        return region in self.get_regions_per_service(
            service_type=service_type,
            session=session,
            iface=iface)

    def get_default_region(self):
        for region in self.cloud.identity.regions():
            if 'default' in region.description.split(','):
                return region.id
        return None

    def has_region_tag(self, region, tag='production'):
        return tag in self.cloud.identity.get_region(
            region).description.split(',')

    def list_regions_with_tag(self, tag='lifecycle'):
        regions = []
        for region in self.cloud.identity.regions():
            if tag in region.description.split(','):
                regions.append(region.id)
        return regions

    def list_projects(self, domain=None, user=None, parent=None, **kwargs):
        return [p for p in self.cloud.identity.projects(**kwargs)]

    def find_project(self, reference):
        return self.cloud.identity.find_project(reference)

    def create_image(self, region=None, **image_attrs):
        """Update properties on an image."""
        for r in self.get_regions_per_service(
                service_type='image', region=region):
            cloud = self.get_cloud(region=r)
            try:
                cloud.image.create_image(**image_attrs)
            except Exception as error:
                print("There was an ERROR: %s", str(error))

    def download_image(self, image, region=None, stream=True):
        """Download an image."""
        data = None
        for r in self.get_regions_per_service(
                service_type='image', region=region):
            cloud = self.get_cloud(region=r)
            data = cloud.image.download_image(image, stream=stream)
        return data

    def update_image(self, image, region=None, **properties):
        """Update the attributes of an image."""
        if not isinstance(image, _image.Image):
            # If we come here with a dict (cloud) - convert dict to real object
            # to properly consume all properties (to calculate the diff).
            # This currently happens from unittests.
            image = _image.Image.existing(**image)
        for r in self.get_regions_per_service(
                service_type='image', region=region):
            cloud = self.get_cloud(region=r)
            cloud.image.update_image(image, **properties)
        return None

    def get_image(self, id, region=None):
        """Get an image."""
        image = None
        for r in self.get_regions_per_service(
                service_type='image', region=region):
            cloud = self.get_cloud(region=r)
            ret = cloud.image.find_image(id)
            if ret:
                image = ret
        return image

    def get_images_by_project(self, project_name, region=None,
                              tag=None, visibility='private'):
        """List all the instance images saved on the tenant."""
        project = self.find_project(project_name)
        return self.get_images_by_project_id(
            project.id, region, tag, visibility)

    def get_images_by_project_id(self, project_id, region=None,
                                 tag=None, visibility=None):
        """List all the instance images saved on the tenant."""
        images = []
        filters = {'owner': project_id}
        if tag:
            filters['tag'] = tag
        if visibility:
            filters['visibility'] = visibility
        for r in self.get_regions_per_service(
                service_type='image', region=region):
            cloud = self.get_cloud(region=r)
            for image in cloud.image.images(**filters):
                images.append(image)
        return images
