import logging

from ccitools.cmd import hypervisor_state_post_reboot
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.cmd.hypervisor_state_post_reboot.ssh_executor')
@mock.patch('time.sleep')
@mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
class TestHypervisorStateCMD(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self, mock_time,
                          mock_exec,
                          mock_crc):
        with self.assertRaises(SystemExit) as cm:
            hypervisor_state_post_reboot.main([])
        self.assertEqual(cm.exception.code, 2)

    def test_dryun_args(self, mock_time,
                        mock_exec,
                        mock_crc):
        hypervisor_state_post_reboot.main([
            '--hypervisors', 'my_host1 my_host2',
            '--dryrun',
            '--uptime', "['my_host1:252, my_host2:1765']",
        ])

    def test_perform_args(self, mock_time,
                          mock_exec,
                          mock_crc):
        mock_exec.return_value = [], None
        hypervisor_state_post_reboot.main([
            '--hypervisors', 'my_host1 my_host2',
            '--perform',
            '--uptime', "['my_host1:252, my_host2:1765']",
        ])


class TestUptime(TestCase):
    def test_process_uptime(self):
        uptime = "['p05792984c75626.cern.ch:9055.0']"
        self.assertIsNone(
            hypervisor_state_post_reboot.HypervisorStateCMD().process_uptime(
                uptime
            )
        )
