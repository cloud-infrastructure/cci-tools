import logging

from ccitools.common import ssh_executor
from ccitools.errors import SSHException
from ccitools.verify import base

# configure logging
logger = logging.getLogger(__name__)


class ServiceWrongAction(base.ActionBase):

    def __init__(self, host, service, delay=120):
        super(ServiceWrongAction, self).__init__(host, delay)
        self.service = service
        self.pattern = (r'.*\/systemd-%s\/gauge-running' % service)

    def try_fix(self):
        """Implement the fix for the exception thrown in collectd."""
        # Try to start the service in the machine
        try:
            logger.info("Starting the %s service", self.service)
            ssh_executor(
                self.host,
                ('systemctl start %s' % self.service)
            )
        except SSHException:
            pass

    def check_alarm_state(self, after_try=False):
        ret = super(ServiceWrongAction, self).check_alarm_state(after_try)
        if ret:
            self.reason = (
                " - %s --> Restart of %s have not cleaned the alarm." % (
                    self.host, self.service)
            )
        return ret


class HttpdAction(ServiceWrongAction):
    def __init__(self, host):
        super(HttpdAction, self).__init__(
            host,
            'httpd')


class HaproxyAction(ServiceWrongAction):
    def __init__(self, host):
        super(HaproxyAction, self).__init__(
            host,
            'haproxy')


class BarbicanAPIAction(ServiceWrongAction):
    def __init__(self, host):
        super(BarbicanAPIAction, self).__init__(
            host,
            'openstack-barbican-api')


class CinderSchedulerAction(ServiceWrongAction):
    def __init__(self, host):
        super(CinderSchedulerAction, self).__init__(
            host,
            'openstack-cinder-scheduler')


class CinderVolumeAction(ServiceWrongAction):
    def __init__(self, host):
        super(CinderVolumeAction, self).__init__(
            host,
            'openstack-cinder_volume')


class EC2APIAction(ServiceWrongAction):
    def __init__(self, host):
        super(EC2APIAction, self).__init__(
            host,
            'openstack-ec2-api')


class GlanceAPIAction(ServiceWrongAction):
    def __init__(self, host):
        super(GlanceAPIAction, self).__init__(
            host,
            'openstack-glance-api')


class HeatEngineAction(ServiceWrongAction):
    def __init__(self, host):
        super(HeatEngineAction, self).__init__(
            host,
            'openstack-heat-engine')


class LibvirtdAction(ServiceWrongAction):
    def __init__(self, host):
        super(LibvirtdAction, self).__init__(
            host,
            'libvirtd')


class MagnumAPIAction(ServiceWrongAction):
    def __init__(self, host):
        super(MagnumAPIAction, self).__init__(
            host,
            'openstack-magnum-api')


class MagnumConductorAction(ServiceWrongAction):
    def __init__(self, host):
        super(MagnumConductorAction, self).__init__(
            host,
            'openstack-magnum-conductor')


class ManilaAPIAction(ServiceWrongAction):
    def __init__(self, host):
        super(ManilaAPIAction, self).__init__(
            host,
            'openstack-manila-api')


class ManilaSchedulerAction(ServiceWrongAction):
    def __init__(self, host):
        super(ManilaSchedulerAction, self).__init__(
            host,
            'openstack-manila-scheduler')


class ManilaShareAction(ServiceWrongAction):
    def __init__(self, host):
        super(ManilaShareAction, self).__init__(
            host,
            'openstack-manila-share')


class MistralAPIAction(ServiceWrongAction):
    def __init__(self, host):
        super(MistralAPIAction, self).__init__(
            host,
            'openstack-mistral-api')


class MistralEngineAction(ServiceWrongAction):
    def __init__(self, host):
        super(MistralEngineAction, self).__init__(
            host,
            'openstack-mistral-engine')


class MistralEventEngineAction(ServiceWrongAction):
    def __init__(self, host):
        super(MistralEventEngineAction, self).__init__(
            host,
            'openstack-mistral-event-engine')


class MistralExecutorAction(ServiceWrongAction):
    def __init__(self, host):
        super(MistralExecutorAction, self).__init__(
            host,
            'openstack-mistral-executor')


class NeutronServerAction(ServiceWrongAction):
    def __init__(self, host):
        super(NeutronServerAction, self).__init__(
            host,
            'neutron-server')


class NovaComputeAction(ServiceWrongAction):
    def __init__(self, host):
        super(NovaComputeAction, self).__init__(
            host,
            'openstack-nova-compute')


class NovaConductorAction(ServiceWrongAction):
    def __init__(self, host):
        super(NovaConductorAction, self).__init__(
            host,
            'openstack-nova-conductor')


class NovaNetworkAction(ServiceWrongAction):
    def __init__(self, host):
        super(NovaNetworkAction, self).__init__(
            host,
            'openstack-nova-network')


class NovaSchedulerAction(ServiceWrongAction):
    def __init__(self, host):
        super(NovaSchedulerAction, self).__init__(
            host,
            'openstack-nova-scheduler')
