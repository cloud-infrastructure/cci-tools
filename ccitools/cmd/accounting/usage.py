#!/usr/bin/python3

import ccitools.conf
import datetime
import logging
import os
import prettytable
import sys
import tempfile


from ccitools.cmd.accounting.handler import QueryHandler
from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools import common
from ccitools.utils.sendmail import MailClient
from influxdb import InfluxDBClient

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class AccountingUsage(BaseCloudCMD):
    def __init__(self):
        super(AccountingUsage, self).__init__(
            description=(
                "Retrieves the usage information from InfluxDB"))

    def fill_metrics(self, influx_client, metrics, influx_date, data, key):
        query_list = []
        key_set = set()
        tables_to_titles = {}

        for metric in metrics:
            logger.info("Processing metric %s", metric)
            query_list.append(CONF.accounting.query.format(
                metrics[metric]['select'],
                metrics[metric]['field'],
                metrics[metric]['from'],
                influx_date,
                '|'.join([r for r in CONF.accounting.regions]),
                ', '.join(metrics[metric]['group'])
            ))
            for k in metrics[metric]['group']:
                key_set.add(k)
            tables_to_titles[metrics[metric]['from']] = metric

        logger.info("Querying information from influxdb")
        stats = QueryHandler(
            influx_client,
            query_list,
            list(key_set),
            tables_to_titles
        ).run()

        logger.info("Filling metric information into data")
        for stat in stats:
            id = stat['keys'][key]
            region = stat['keys']['region']
            if id in data[region]:
                for metric in metrics:
                    entry = metric
                    if 'as' in metrics[metric]:
                        entry = metrics[metric]['as']
                    field = metrics[metric]['field']
                    if (entry in stat and metric in data[region][id]):
                        data[region][id][metric] = stat[entry][field]

    def main(self, args):
        self.parser.add_argument(
            "--subject",
            help="email subject",
            default='[CLOUD] Resource report')
        self.parser.add_argument(
            "--to",
            help="email to",
            default='cloud-infrastructure-resource-report@cern.ch')
        self.parser.add_argument(
            "--body",
            help="Url to the template to be used as email body",
            default=('Hello,<br/><br/>'
                     'Please find attached the report of Cloud resource usage'
                     '<br/><br/>Best regards,'
                     '<br/>Cloud Infrastructure Team'))
        self.parser.add_argument(
            '--date',
            dest='str_date',
            help='Date that will be used by the '
                 'Usage extractor (DD-MM-YYYY format).',
            default=datetime.date.today().strftime('%d-%m-%Y')
        )
        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true',
                               help="If present, sends email")
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')

        args = self.parse_args(args)

        logger.info("Connecting to influxdb")
        influx_client = InfluxDBClient(
            CONF.influxdb.host,
            CONF.influxdb.port,
            CONF.influxdb.user,
            CONF.influxdb.password,
            CONF.influxdb.database,
            ssl=True
        )

        date = datetime.datetime.strptime(args.str_date, '%d-%m-%Y')
        influx_date = date.strftime('\'%Y-%m-%d\'')

        logger.info("Retrieving chargegroups")
        chargegroups = {
            ch['uuid']: {
                'name': ch['name'],
                'org_unit': ch['org_unit'],
                'count': 0,
            } for ch in common.get_chargegroups(
                inactive=True
            )}

        logger.info("Retrieving projects")
        projects = self.cloudclient.list_projects(domain='default')

        if not projects:
            logger.error("No projects retrieved")
            raise SystemExit(1)

        # Initialize dictionary with data per region
        project_data = {region: {} for region in CONF.accounting.regions}

        # Prefill the project dictionary with empty results
        logger.info("Preloading project data")
        for project in projects:
            project_info = {
                'name': project.name,
                'chargegroup_id': project.chargegroup if hasattr(
                    project, 'chargegroup') else '',
                'type': project.type if hasattr(
                    project, 'type') else 'service',
                'chargegroup': '',
                'org_unit': '',
            }
            # Set the chargegroup name and orgunit
            if project_info['chargegroup_id']:
                ch_id = project_info['chargegroup_id']
                project_info['chargegroup'] = (
                    chargegroups[ch_id]['name']
                )
                project_info['org_unit'] = (
                    chargegroups[ch_id]['org_unit']
                )
                chargegroups[ch_id]['count'] += 1
            # Set all usage values to 0 in case they are no reports
            for metric in CONF.accounting.project_metrics:
                project_info[metric] = 0.0

            for region in CONF.accounting.regions:
                project_data[region][project.id] = dict.copy(project_info)

        # Retrieve project stats
        logger.info("Retrieving project stats")
        self.fill_metrics(
            influx_client,
            CONF.accounting.project_metrics,
            influx_date,
            project_data,
            'project_id')

        # Prefill the chargegroup dictionary with empty results
        logger.info("Preloading chargegroup data")
        chg_data = {region: {} for region in CONF.accounting.regions}

        for chg in chargegroups:
            if chargegroups[chg]['count']:
                chg_info = {
                    'name': chargegroups[chg]['name'],
                    'org_unit': chargegroups[chg]['org_unit'],
                    'count': chargegroups[chg]['count'],
                }
                # Set all usage values to 0 in case they are no reports
                for metric in CONF.accounting.chg_metrics:
                    chg_info[metric] = 0.0
                for region in CONF.accounting.regions:
                    chg_data[region][chg] = dict.copy(chg_info)

        # Retrieve chargegroup stats
        logger.info("Retrieving chargegroup stats")
        self.fill_metrics(
            influx_client,
            CONF.accounting.chg_metrics,
            influx_date,
            chg_data,
            'chargegroup')

        # Calculate results for projects
        projects_table = {
            region: prettytable.PrettyTable(
                ["Project Name", "OrgUnit", "Chargegroup"]
                + [v['display']
                   for v in CONF.accounting.project_metrics.values()]
            ) for region in CONF.accounting.regions
        }

        for region in CONF.accounting.regions:
            for project in project_data[region].values():
                projects_table[region].add_row(
                    [project['name'], project['org_unit'],
                     project['chargegroup']]
                    + [project[k]
                       for k in CONF.accounting.project_metrics.keys()]
                )

        # Calculate results for chargegroups
        chargegroups_table = {
            region: prettytable.PrettyTable(
                ["Chargegroup", "OrgUnit", "Projects"]
                + [v['display'] for v in CONF.accounting.chg_metrics.values()])
            for region in CONF.accounting.regions
        }
        for region in CONF.accounting.regions:
            for chg in chg_data[region].values():
                chargegroups_table[region].add_row(
                    [chg['name'], chg['org_unit'], chg['count']]
                    + [chg[k] for k in CONF.accounting.chg_metrics.keys()]
                )

        mailclient = MailClient("localhost")
        if args.exec_mode:
            # Generate temporary directory
            with tempfile.TemporaryDirectory() as tmpdirname:
                paths = []
                for region in CONF.accounting.regions:
                    prjs_file_name = 'projects.{0}.txt'.format(region)
                    prjs_file = os.path.join(tmpdirname, prjs_file_name)
                    with open(prjs_file, 'w') as w:
                        w.write(
                            projects_table[region].get_string(
                                sortby="Project Name"))
                    paths.append(prjs_file)

                    chg_file_name = 'chargegroups_{0}.txt'.format(region)
                    chgs_file = os.path.join(tmpdirname, chg_file_name)
                    with open(chgs_file, 'w') as w:
                        w.write(
                            chargegroups_table[region].get_string(
                                sortby="Chargegroup")
                        )
                    paths.append(chgs_file)

                try:
                    mailclient.send_mail(
                        args.to, args.subject, args.body,
                        "noreply@cern.ch", "", "", "html",
                        files=paths)
                    logger.info("Email sent successfully to %s", args.to)
                except Exception as ex:
                    logger.error("Impossible to send mailto '%s'. "
                                 "Error message: '%s'", args.to, ex)
                logger.info("To: %-20s Subject: \"%s\"", args.to, args.subject)
        else:
            logger.warning("Projects usage: \n%s",
                           projects_table.get_string(sortby="Project Name"))
            logger.warning("Chargegroups usage: \n%s",
                           chargegroups_table.get_string(sortby="Chargegroup"))
        logger.info("Execution finished")


# Needs static method for setup.cfg
def main(args=None):
    AccountingUsage().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)
