import abc

from ccitools import common


class RecordProducerSoap(object):
    """Wrapper to get/update record producers.

    To get a list of the DECORATORS and ELEMENT_IDs and what they represent
    you would need to:

    - Create the record producer via the SNOW form
    - Get the sys_id of the ticket
    - Get the list of records associated with the sys_id of the ticket
    - Then iterate over them. If they have a value, associate
        the `question` field of the record with a `label`. Include the
        order too.
    - If they don't have a value they should be included as a decorator.
    """

    RECORD_PRODUCER_ID = "dfb4225078a27100de14a0934907b597"
    RECORD_PRODUCERS_TABLE = "question_answer_list"

    def __init__(self, instance, transport):
        self.client = common.get_soap_client(
            instance, RecordProducerSoap.RECORD_PRODUCERS_TABLE, transport)

    def get_quota_update_rp(self, request):
        """Get quota update record producer.

        :param request: a `Request` instance
        :returns: a `QuotaUpdateRecordProducer` instance
        """
        return QuotaUpdateRecordProducer(
            request, self._get_record_field_values(request))

    def get_project_creation_rp(self, request):
        """Get project creation record producer.

        :param request: a `Request` instance
        :returns: a `CreateProjectRecordProducer` instance
        """
        return CreateProjectRecordProducer(
            request, self._get_record_field_values(request))

    def get_project_deletion_rp(self, request):
        """Get project deletion record producer.

        :param request: a `Request` instance
        :returns: a `DeleteProjectRecordProducer` instance
        """
        return DeleteProjectRecordProducer(
            request, self._get_record_field_values(request))

    def _convert_RQF(self, request, data, record_project_class):
        """Convert RQF into a specific record producer filling the custom form.

        Convert RQF in a record producer using:

        - record_project_class.LABEL_TO_ELEMENT_ID: indicate to which snow
            sys id correlates with which data key
        - record_project_class.LABEL_TO_ORDER: they are the map
            between a sys_id and where
        - record_project_class.DECORATORS: they are the fields that don't
            include any information in the form, that are used to make
            the visualization in the ServiceNow webpage better. First field
            of the tuple is a `sys_id` and second one is the `order`

        :param request: an existing `Request` instance
        :param data: a dict with the necessary fields that need to be fill
            in the record producer form
        :param record_project_class: a `class` that should have
            `LABEL_TO_ELEMENT_ID`, `DECORATORS` and `LABEL_TO_ORDER`
        :returns: a `RecordProducer` instance
        """
        # remove any unicode characters from the comments field
        try:
            data["comment"] = data["comment"].encode("ascii", "replace")
        except KeyError:
            pass  # ignore if the key does not exist

        # fill the form with the data provided in data
        for key, value in data.items():
            try:
                element_id = record_project_class.LABEL_TO_ELEMENT_ID[key]
                self.client.insert(
                    table_sys_id=request.info.sys_id,
                    table_name=request.info.sys_class_name,
                    question=element_id,
                    value=value,
                    # includes the order to specify where the fields should
                    # appear in the webpage
                    order=record_project_class.LABEL_TO_ORDER[key],
                )

            except KeyError:
                pass  # ignore the keys that are not included in the form

        # complete the record producer information inserting
        # extra decoration in the form
        for element_id, order in record_project_class.DECORATORS:
            self.client.insert(
                table_sys_id=request.info.sys_id,
                table_name=request.info.sys_class_name,
                question=element_id,
                value=None,
                order=order
            )

        # welp, this is a bit hackish
        try:
            description = data["description"]
        except KeyError:
            description = data["comment"]

        # we convert the ticket into a record producer, this means that
        # it will use the information we inserted in the record producer table
        # and will fill the form accordingly with these values
        request.update(u_caller_id=data["username"],
                       description=description,
                       u_record_producer=RecordProducerSoap.RECORD_PRODUCER_ID)

        comment = u"""
User comment:
{0}

Cloud Service Automation (on behalf of the user)
"""

        request.add_comment(comment.format(
            data["comment"].decode('utf-8') if data["comment"] else 'None'))

        return record_project_class(
            request, self._get_record_field_values(request))

    def convert_RQF_to_quota_update(self, request, data):
        """Convert the request to a quota update record producer.

        Convert the request to a quota update record producer. All the
        necessary fields are:

            data = {
                "username": "my_username",
                "project_name": "Cloud Monitoring",
                "comment": "Hello, testing",
                "instances": "new_number_of_instances",
                "cores": "new_number_of_cores",
                "ram": "new_ram",
                "cp1_gigabytes": "new_gb_cp1",
                "cp1_volumes": "new_volumes_cp1",
                "cpio1_gigabytes": "new_gb_cpio1",
                "cpio1_volumes": "new_volumes_cpio1",
                "io1_gigabytes": "new_gb_io1",
                "io1_volumes": "new_volumes_io1",
                "standard_gigabytes": "new_gb_standard",
                "standard_volumes": "new_volumes_standard",
            }

        :param request: a `Request` instance
        :param data: `dict` with values to create the record producer
        :returns: a `RecordProducer` instance
        """
        # this fills the volume type dropdowns with the correct volume
        # types
        data["standard"] = "standard"
        data["cp1"] = "cp1"
        data["cpio1"] = "cpio1"
        data["io1"] = "io1"
        data["visible_cp1"] = "Yes"
        data["visible_cpio1"] = "Yes"
        data["visible_io1"] = "Yes"

        return self._convert_RQF(request, data, QuotaUpdateRecordProducer)

    def convert_RQF_to_project_creation(self, request, data):
        """Convert the request to a project creation record producer.

        Convert the request to a project creation record producer. All the
        necessary fields are:

            data = {
                "accounting_group": "ALICE",
                "comment": "Hello, testing",
                "cores": 5,
                "description": "my description",
                "egroup": "cloud-infrastructure-3rd-level",
                "gigabytes": 5,
                "instances": 5,
                "owner": "lpigueir",
                "project_name": "Openstack test",
                "ram": 10,
                "username": "lpigueir",
                "volumes": 5,
            }

        :param request: a `Request` instance
        :param data: `dict` with values to create the record producer
        :returns: a `RecordProducer` instance
        """
        return self._convert_RQF(request, data, CreateProjectRecordProducer)

    def convert_RQF_to_project_deletion(self, request, data):
        """Convert the request to a project deletion record producer.

        Convert the request to a project deletion record producer. All the
        necessary fields are:

            data = {
                "username": "my_username",
                "project_name": "Openstack Project Name",
                "comment": "My important comment!!!",
            }

        :param request: a `Request` instance
        :param data: `dict` with values to create the record producer
        :returns: a `RecordProducer` instance
        """
        return self._convert_RQF(request, data, DeleteProjectRecordProducer)

    def _get_record_field_values(self, request):
        return self.client.getRecords(table_sys_id=request.info.sys_id)


class RecordProducer(object):
    """Abstract class to create record producers.

    To get from SNOW which IDs matches which field to be able to
    build `LABEL_TO_ELEMENT_ID`, `LABEL_TO_ORDER` and `DECORATORS`, you
    need to create first a ticket via ServiceNow and then
    read the fields that this generated in the record producers table
    """

    __metaclass__ = abc.ABCMeta  # noqa - incompatibility with python 3.x

    # a child of this function should include these four parameters
    # that will indicate how the form is built for each specific
    # record producer
    LABEL_TO_ELEMENT_ID = None
    ELEMENT_ID_TO_LABEL = None
    LABEL_TO_ORDER = None
    DECORATORS = None

    def __init__(self, request, field_values):
        self.request = request

        for record_field in field_values:
            if record_field.question in self.element_id_to_label:
                setattr(self, self.element_id_to_label[record_field.question],
                        record_field.value)

    @abc.abstractproperty
    def element_id_to_label(self):
        raise NotImplementedError


class CreateProjectRecordProducer(RecordProducer):
    """Reference for the create project record producer."""

    LABEL_TO_ELEMENT_ID = {
        'project_name': '5f413770f1a389004baf946ac6346faf',
        'description': '7c917730f1a389004baf946ac6346f4a',
        'owner': '7233bf70f1a389004baf946ac6346f62',
        'egroup': '59d2fb70f1a389004baf946ac6346f4a',
        'chargegroup': '85a3ee331b0c1c50dfdbdc6a9b4bcb0b',
        'chargerole': 'cbc322331b0c1c50dfdbdc6a9b4bcbcf',
        'metadata': '63332e5b1b9b1010dfdbdc6a9b4bcb9c',
        'accounting_group': 'f1450d4f4f77524015d3bc511310c7ff',
        'project_type': '013be4bc4f73de0015d3bc511310c7ef'
    }

    # change keys by values and values by keys
    ELEMENT_ID_TO_LABEL = {y: x for x, y in LABEL_TO_ELEMENT_ID.items()}

    LABEL_TO_ORDER = {
        'project_name': 3,
        'description': 4,
        'owner': 6,
        'egroup': 7,
        'chargegroup': 13,
        'chargerole': 14,
        'metadata': 17,
        'accounting_group': 24,
        'project_type': 27,
    }

    DECORATORS = [
        ('48010e3c115530404bafa32c34fb16fe', 1),  # common_rp_date_validation
        ('b9622e634f895600e3a2119f0310c738', 2),  # project_label
        # ('5f413770f1a389004baf946ac6346faf', 3), project_name
        # ('7c917730f1a389004baf946ac6346f4a', 4), project_description
        ('e4f276a74f895600e3a2119f0310c7e2', 5),  # question_container_1
        # ('7233bf70f1a389004baf946ac6346f62', 6), owner
        # ('59d2fb70f1a389004baf946ac6346f4a', 7), egroup_name
        ('703376274f895600e3a2119f0310c7b1', 8),  # Cont2_end
        ('60491ef31bc81c50dfdbdc6a9b4bcb22', 9),  # accounting_section
        ('30b9d6f31bc81c50dfdbdc6a9b4bcb3b', 10),  # question_container_2
        ('e543e27b1bc81c50dfdbdc6a9b4bcb81', 11),  # accounting_group_label
        ('6f7366731b0c1c50dfdbdc6a9b4bcbad', 12),  # accounting_cont_split
        # ('85a3ee331b0c1c50dfdbdc6a9b4bcb0b', 13), chargegroup
        # ('cbc322331b0c1c50dfdbdc6a9b4bcbcf', 14), chargerole
        ('ddf36afb1bc81c50dfdbdc6a9b4bcba3', 15),  # accounting_cont_end
        ('b1d2a65b1b9b1010dfdbdc6a9b4bcb48', 16),  # metadata_label
        # ('63332e5b1b9b1010dfdbdc6a9b4bcb9c', 17),  # quota_values
        ('8993669b1b9b1010dfdbdc6a9b4bcb49', 18),  # formatter
        ('e2ab3e09e0d3d1004baf32cd13601026', 19),  # more_label
        ('db65f319e057d1004baf32cd13601032', 20),  # comments
        ('7c2f635a4fbd77c08ed9119f0310c706', 21),  # watch_list_classic
        ('b15ae0bc4f73de0015d3bc511310c7be', 22),  # properties_note
        ('593db1114f0ce24015d3bc511310c7a9', 23),  # question_container_3
        # ('f1450d4f4f77524015d3bc511310c7ff', 24), accounting_group
        ('a38df9114f0ce24015d3bc511310c770', 25),  # accounting_group_other
        ('345d3d9d4fc8e24015d3bc511310c70e', 26),  # Properties_accounting
        # ('013be4bc4f73de0015d3bc511310c7ef', 27), project_type
        ('1cb9a07c4f73de0015d3bc511310c7ea', 28)  # Properties_end
    ]

    def __init__(self, request, field_values):
        super(CreateProjectRecordProducer, self).__init__(request,
                                                          field_values)

    @property
    def element_id_to_label(self):
        return CreateProjectRecordProducer.ELEMENT_ID_TO_LABEL


class QuotaUpdateRecordProducer(RecordProducer):
    """Reference for the quota update record producer."""

    LABEL_TO_ELEMENT_ID = {
        'project_name': '27b6a65078a27100de14a0934907b5a4',
        'chargegroup': 'fc2616ff1b881c50dfdbdc6a9b4bcbf6',
        'chargerole': 'bf561aff1b881c50dfdbdc6a9b4bcbef',
        'metadata': '00204e56dbf254d0838f5c88f4961996',
        'comment': 'b02b2e9078a27100de14a0934907b540'
    }

    # change keys by values and values by keys
    ELEMENT_ID_TO_LABEL = {y: x for x, y in LABEL_TO_ELEMENT_ID.items()}

    LABEL_TO_ORDER = {
        'project_name': 3,
        'chargegroup': 9,
        'chargerole': 10,
        'metadata': 13,
        'comment': 16
    }

    DECORATORS = [
        ('48010e3c115530404bafa32c34fb16fe', 1),  # common_rp_date_validation
        ('d3af21794f0c9e00e3a2119f0310c752', 2),  # project_label
        # ('27b6a65078a27100de14a0934907b5a4', 3), project_name
        ('9f56bd484f585e00398ebc511310c75d', 4),  # project_name_note
        ('5854563f1b881c50dfdbdc6a9b4bcb95', 5),  # accounting_section
        ('8d845e3f1b881c50dfdbdc6a9b4bcb38', 6),  # question_container_1
        ('2675de7f1b881c50dfdbdc6a9b4bcb20', 7),  # accounting_group_label
        ('32b59e3f1b881c50dfdbdc6a9b4bcb4a', 8),  # accountinf_cont_split
        # ('fc2616ff1b881c50dfdbdc6a9b4bcbf6', 9), chargegroup
        # ('bf561aff1b881c50dfdbdc6a9b4bcbef', 10), chargerole
        ('e3b4d67f1b881c50dfdbdc6a9b4bcb04', 11),  # accounting_container_end
        ('637fb592dbf254d0838f5c88f4961949', 12),  # metadata_label
        # ('00204e56dbf254d0838f5c88f4961996', 13), quota_values
        ('269d18bd4f4fd74064cc119f0310c7c0', 14),  # Break_end
        ('1fdae29078a27100de14a0934907b542', 15),  # more_label
        # ('b02b2e9078a27100de14a0934907b540, 16), comment
        ('7c2f635a4fbd77c08ed9119f0310c706', 17),  # watch_list_classic
    ]

    def __init__(self, request, field_values):
        super(QuotaUpdateRecordProducer, self).__init__(request,
                                                        field_values)

    @property
    def element_id_to_label(self):
        return QuotaUpdateRecordProducer.ELEMENT_ID_TO_LABEL


class DeleteProjectRecordProducer(RecordProducer):
    """Reference for the delete project record producer."""

    LABEL_TO_ELEMENT_ID = {
        "project_name": "079305bf4fa2b60064cc119f0310c723",
    }

    # change keys by values and values by keys
    ELEMENT_ID_TO_LABEL = {y: x for x, y in LABEL_TO_ELEMENT_ID.items()}

    LABEL_TO_ORDER = {
        "project_name": "200",
    }

    DECORATORS = [
        ('48010e3c115530404bafa32c34fb16fe', 100),
        # ('079305bf4fa2b60064cc119f0310c723', 200), project_name
        ('0ea4ae35e05bd1004baf32cd1360103c', 300)
    ]

    def __init__(self, request, field_values):
        super(DeleteProjectRecordProducer, self).__init__(request,
                                                          field_values)

    @property
    def element_id_to_label(self):
        return DeleteProjectRecordProducer.ELEMENT_ID_TO_LABEL
