import logging
import requests_mock

from ccitools.cmd.jira_ticket_cleanup import main
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase


class TestJiraTicketCleanup(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):
        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)

    @requests_mock.Mocker()
    def test_dryrun_no_tickets(self, mock_req):
        mock_req.get(
            'https://its.cern.ch/jira/rest/api/2/search',
            text=fixtures.JIRA_NO_TICKETS)
        main([
            '--jql', 'fake_query',
            '--dryrun'
        ])

    @requests_mock.Mocker()
    def test_dryrun_returns_404(self, mock_req):
        mock_req.get(
            'https://its.cern.ch/jira/rest/api/2/search',
            text='Not Found', status_code=404)

        with self.assertRaises(SystemExit) as cm:
            main([
                '--jql', 'fake_query',
                '--dryrun'
            ])
        self.assertEqual(cm.exception.code, 1)

    @requests_mock.Mocker()
    def test_dryrun(self, mock_req):
        mock_req.get(
            'https://its.cern.ch/jira/rest/api/2/search',
            text=fixtures.JIRA_TICKETS)

        main([
            '--jql', 'fake_query',
            '--dryrun'
        ])

    @requests_mock.Mocker()
    def test_perform_no_tickets(self, mock_req):
        mock_req.get(
            'https://its.cern.ch/jira/rest/api/2/search',
            text=fixtures.JIRA_NO_TICKETS)
        main([
            '--jql', 'fake_query',
            '--perform'
        ])

    @requests_mock.Mocker()
    def test_perform_returns_404(self, mock_req):
        mock_req.get(
            'https://its.cern.ch/jira/rest/api/2/search',
            text='Not Found', status_code=404)

        with self.assertRaises(SystemExit) as cm:
            main([
                '--jql', 'fake_query',
                '--perform'
            ])
        self.assertEqual(cm.exception.code, 1)

    @requests_mock.Mocker()
    def test_perform(self, mock_req):
        mock_req.get(
            'https://its.cern.ch/jira/rest/api/2/search',
            text=fixtures.JIRA_TICKETS)

        mock_req.get(
            "https://its.cern.ch/jira/rest/api/2/issue/OS-1111/transitions",
            text='OK')

        mock_req.post(
            "https://its.cern.ch/jira/rest/api/2/issue/OS-1111/transitions",
            text='OK')

        main([
            '--jql', 'fake_query',
            '--perform'
        ])

    @requests_mock.Mocker()
    def test_perform_fails_to_close(self, mock_req):
        mock_req.get(
            'https://its.cern.ch/jira/rest/api/2/search',
            text=fixtures.JIRA_TICKETS)

        mock_req.get(
            "https://its.cern.ch/jira/rest/api/2/issue/OS-1111/transitions",
            text='OK')

        mock_req.post(
            "https://its.cern.ch/jira/rest/api/2/issue/OS-1111/transitions",
            text='Forbidden', status_code=403)

        with self.assertRaises(SystemExit) as cm:
            main([
                '--jql', 'fake_query',
                '--perform'
            ])
        self.assertEqual(cm.exception.code, 1)

    @requests_mock.Mocker()
    @mock.patch('ccitools.cmd.jira_ticket_cleanup.'
                'JiraTicketCleanup.transition')
    def test_perform_raises_exception(self, mock_req, mock_transition):
        mock_req.get(
            'https://its.cern.ch/jira/rest/api/2/search',
            text=fixtures.JIRA_TICKETS)

        mock_req.get(
            "https://its.cern.ch/jira/rest/api/2/issue/OS-1111/transitions",
            text='OK')

        mock_transition.side_effect = (
            iter([Exception('error')])
        )

        with self.assertRaises(SystemExit) as cm:
            main([
                '--jql', 'fake_query',
                '--perform'
            ])
        self.assertEqual(cm.exception.code, 1)
