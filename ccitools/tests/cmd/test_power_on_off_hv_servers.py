import logging

from ccitools.cmd import power_on_off_hv_servers
from ccitools.tests.cmd import fixtures
from tenacity import stop_after_attempt
from tenacity import wait_none
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
@mock.patch('ccitools.cmd.power_on_off_hv_servers.MailClient')
class TestPowerOnOffServers(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self, mock_mc, mock_crc):
        with self.assertRaises(SystemExit) as cm:
            power_on_off_hv_servers.main([])
        self.assertEqual(cm.exception.code, 2)

    def test_server_off(self, mock_mc, mock_crc):
        mock_crc.return_value = mock_crc
        mock_mc.return_value = mock_mc

        cmd = power_on_off_hv_servers.PowerOnOffHVServersCMD()
        cmd.wait_for_active.retry.stop = stop_after_attempt(1)
        cmd.wait_for_active.retry.wait = wait_none
        cmd.wait_for_stopped.retry.stop = stop_after_attempt(1)
        cmd.wait_for_stopped.retry.wait = wait_none

        srv_mock = mock.Mock()
        srv_mock.name = 'fake_n'
        mock_crc.get_servers_by_vm_state.return_value = [
            srv_mock
        ]
        mock_crc.get_servers_by_last_action_user.return_value = [
            srv_mock
        ]
        mock_crc.get_servers_by_hypervisor.return_value = [
            srv_mock
        ]

        cmd.main([
            '--action', 'off',
            '--hypervisors', 'fake_hv',
            '--username', 'fake_user',
            '--mailto', 'fake_mail',
        ])

    def test_server_on(self, mock_mc, mock_crc):
        mock_crc.return_value = mock_crc
        mock_mc.return_value = mock_mc

        cmd = power_on_off_hv_servers.PowerOnOffHVServersCMD()
        cmd.wait_for_active.retry.stop = stop_after_attempt(1)
        cmd.wait_for_active.retry.wait = wait_none
        cmd.wait_for_stopped.retry.stop = stop_after_attempt(1)
        cmd.wait_for_stopped.retry.wait = wait_none

        srv_mock = mock.Mock()
        srv_mock.name = 'fake_n'
        mock_crc.get_servers_by_vm_state.return_value = [
            srv_mock
        ]
        mock_crc.get_servers_by_last_action_user.return_value = [
            srv_mock
        ]
        mock_crc.get_servers_by_hypervisor.return_value = [
            srv_mock
        ]

        cmd.main([
            '--action', 'on',
            '--hypervisors', 'fake_hv',
            '--username', 'fake_user',
            '--mailto', 'fake_mail',
        ])

    def test_server_none(self, mock_mc, mock_crc):
        mock_crc.return_value = mock_crc
        mock_mc.return_value = mock_mc
        mock_crc.get_servers_by_last_action_user.return_value = None

        cmd = power_on_off_hv_servers.PowerOnOffHVServersCMD()
        cmd.wait_for_active.retry.stop = stop_after_attempt(1)
        cmd.wait_for_active.retry.wait = wait_none
        cmd.wait_for_stopped.retry.stop = stop_after_attempt(1)
        cmd.wait_for_stopped.retry.wait = wait_none

        cmd.main([
            '--action', 'on',
            '--hypervisors', 'fake_hv',
            '--username', 'fake_user',
            '--mailto', 'fake_mail',
        ])

    def test_server_none_last_action(self, mock_mc, mock_crc):
        mock_crc.return_value = mock_crc
        mock_mc.return_value = mock_mc
        mock_crc.get_servers_by_vm_state.return_value = None
        mock_crc.get_servers_by_last_action_user.return_value = None
        mock_crc.get_servers_by_hypervisor.return_value = None

        cmd = power_on_off_hv_servers.PowerOnOffHVServersCMD()
        cmd.wait_for_active.retry.stop = stop_after_attempt(1)
        cmd.wait_for_active.retry.wait = wait_none
        cmd.wait_for_stopped.retry.stop = stop_after_attempt(1)
        cmd.wait_for_stopped.retry.wait = wait_none

        cmd.main([
            '--action', 'on',
            '--hypervisors', 'fake_hv',
            '--username', 'fake_user',
            '--mailto', 'fake_mail',
        ])

    def test_server_off_full(self, mock_mc, mock_crc):
        mock_crc.return_value = mock_crc
        mock_mc.return_value = mock_mc

        cmd = power_on_off_hv_servers.PowerOnOffHVServersCMD()
        cmd.wait_for_active.retry.stop = stop_after_attempt(1)
        cmd.wait_for_active.retry.wait = wait_none
        cmd.wait_for_stopped.retry.stop = stop_after_attempt(1)
        cmd.wait_for_stopped.retry.wait = wait_none

        srv_mock = mock.Mock()
        srv_mock.name = 'fake_n'
        mock_crc.get_servers_by_vm_state.return_value = [
            srv_mock
        ]
        mock_crc.get_servers_by_last_action_user.return_value = [
            srv_mock
        ]
        mock_crc.get_servers_by_hypervisor.return_value = [
            srv_mock
        ]
        srv_mock.manager.get.return_value = fixtures.SERVER_MANAGER_OUTPUT

        cmd.main([
            '--action', 'off',
            '--hypervisors', 'fake_hv',
            '--username', 'fake_user',
            '--mailto', 'fake_mail',
        ])

    def test_server_on_full(self, mock_mc, mock_crc):
        mock_crc.return_value = mock_crc
        mock_mc.return_value = mock_mc

        cmd = power_on_off_hv_servers.PowerOnOffHVServersCMD()
        cmd.wait_for_active.retry.stop = stop_after_attempt(1)
        cmd.wait_for_active.retry.wait = wait_none
        cmd.wait_for_stopped.retry.stop = stop_after_attempt(1)
        cmd.wait_for_stopped.retry.wait = wait_none

        srv_mock = mock.Mock()
        srv_mock.name = 'fake_n'
        mock_crc.get_servers_by_vm_state.return_value = [
            srv_mock
        ]
        mock_crc.get_servers_by_last_action_user.return_value = [
            srv_mock
        ]
        mock_crc.get_servers_by_hypervisor.return_value = [
            srv_mock
        ]
        srv_mock.manager.get.return_value = fixtures.SERVER_MANAGER_OUTPUT2

        cmd.main([
            '--action', 'on',
            '--hypervisors', 'fake_hv',
            '--username', 'fake_user',
            '--mailto', 'fake_mail',
        ])
