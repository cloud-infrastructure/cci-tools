import logging

from ccitools.cmd.rally_cleanup import main
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.utils.cloud.keystone_session')
@mock.patch('ccitools.utils.cloud.cloud_config')
@mock.patch('ccitools.utils.cloud.CloudRegionClient.get_shares_by_project_id')
@mock.patch('ccitools.utils.cloud.CloudRegionClient.get_servers')
@mock.patch('ccitools.utils.cloud.CloudRegionClient.get_volumes')
@mock.patch('ccitools.utils.cloud.CloudRegionClient.get_keypairs')
@mock.patch('ccitools.utils.cloud.CloudRegionClient.get_loadbalancers')
@mock.patch('ccitools.utils.cloud.CloudRegionClient.delete_servers')
@mock.patch('ccitools.utils.cloud.CloudRegionClient.delete_volumes')
@mock.patch('ccitools.utils.cloud.CloudRegionClient.delete_keypairs')
@mock.patch('ccitools.utils.cloud.CloudRegionClient.delete_loadbalancers')
@mock.patch('ccitools.cmd.rally_cleanup.ServiceNowClient')
class TestRallyCleanup(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self,
                          mock_snow,
                          mock_del_lb,
                          mock_del_kp,
                          mock_del_vol,
                          mock_del_ser,
                          mock_get_lb,
                          mock_get_kp,
                          mock_get_vol,
                          mock_get_serv,
                          mock_get_shares,
                          mock_cc,
                          mock_ks):
        mock_get_serv.return_value = []
        mock_get_vol.return_value = []
        mock_get_kp.return_value = []

        main(['--timeout', '1'])

        mock_snow.assert_called_with(instance='cern')

        mock_get_serv.assert_called_with(
            user_name='svcprobe', tag='cleanup')
        mock_get_vol.assert_called_with(
            user_name='svcprobe', tag='cleanup')
        mock_get_serv.assert_called_with(
            user_name='svcprobe', tag='cleanup')
        mock_get_kp.assert_called_with(
            user_name='svcprobe', cloud='probe',
            tag='cleanup')

    def test_default_args_with_output(self,
                                      mock_snow,
                                      mock_del_lb,
                                      mock_del_kp,
                                      mock_del_vol,
                                      mock_del_ser,
                                      mock_get_lb,
                                      mock_get_kp,
                                      mock_get_vol,
                                      mock_get_serv,
                                      mock_get_shares,
                                      mock_cc,
                                      mock_ks):
        mock_get_serv.side_effect = iter([
            [
                fixtures.SERVER,
                fixtures.BROKEN_SERVER,
                fixtures.BROKEN_SERVER_CREATING,
                fixtures.BROKEN_SERVER_DELETING
            ],
            []
        ])
        mock_get_vol.side_effect = iter([
            [
                fixtures.VOLUME,
                fixtures.BROKEN_VOLUME,
                fixtures.BROKEN_VOLUME_CREATING,
                fixtures.BROKEN_VOLUME_DELETING
            ],
            []
        ])
        mock_get_shares.side_effect = iter([
            [
                fixtures.SHARE,
                fixtures.BROKEN_SHARE,
                fixtures.BROKEN_SHARE_CREATING,
                fixtures.BROKEN_SHARE_DELETING
            ],
            [],
            [],
            [],
            [],
            [],
        ])
        mock_get_kp.side_effect = iter([
            [
                fixtures.KEYPAIR
            ],
            [],
            [],
            [],
            [],
            [],
        ])
        mock_get_lb.side_effect = iter([
            [
                fixtures.LOADBALANCER_DATA,
                fixtures.BROKEN_LOADBALANCER_DATA,
                fixtures.BROKEN_LOADBALANCER_CREATING_DATA,
                fixtures.BROKEN_LOADBALANCER_DELETING_DATA
            ],
            [],
            [],
            [],
            [],
            [],
        ])

        main(['--timeout', '1'])

        mock_snow.assert_called_with(instance='cern')

    def test_default_args_with_output_broke_server(self,
                                                   mock_snow,
                                                   mock_del_lb,
                                                   mock_del_kp,
                                                   mock_del_vol,
                                                   mock_del_ser,
                                                   mock_get_lb,
                                                   mock_get_kp,
                                                   mock_get_vol,
                                                   mock_get_serv,
                                                   mock_get_shares,
                                                   mock_cc,
                                                   mock_ks):
        mock_get_serv.side_effect = iter([
            [
                fixtures.SERVER,
                fixtures.BROKEN_SERVER,
                fixtures.BROKEN_SERVER_CREATING,
                fixtures.BROKEN_SERVER_DELETING
            ],
            [
                fixtures.SERVER,
                fixtures.BROKEN_SERVER,
                fixtures.BROKEN_SERVER_CREATING,
                fixtures.BROKEN_SERVER_DELETING
            ],
            []
        ])
        mock_get_vol.return_value = []
        mock_get_kp.return_value = []

        with self.assertRaises(SystemExit) as context:
            main(['--timeout', '1'])
        self.assertEqual(context.exception.code, 1)

        mock_snow.assert_called_with(instance='cern')

    def test_default_args_with_output_broke_volume(self,
                                                   mock_snow,
                                                   mock_del_lb,
                                                   mock_del_kp,
                                                   mock_del_vol,
                                                   mock_del_ser,
                                                   mock_get_lb,
                                                   mock_get_kp,
                                                   mock_get_vol,
                                                   mock_get_serv,
                                                   mock_get_shares,
                                                   mock_cc,
                                                   mock_ks):
        mock_get_serv.return_value = []
        mock_get_vol.side_effect = iter([
            [
                fixtures.VOLUME,
                fixtures.BROKEN_VOLUME,
                fixtures.BROKEN_VOLUME_CREATING,
                fixtures.BROKEN_VOLUME_DELETING
            ],
            [
                fixtures.VOLUME,
                fixtures.BROKEN_VOLUME,
                fixtures.BROKEN_VOLUME_CREATING,
                fixtures.BROKEN_VOLUME_DELETING
            ],
            []
        ])
        mock_get_kp.return_value = []

        with self.assertRaises(SystemExit) as context:
            main(['--timeout', '1'])
        self.assertEqual(context.exception.code, 1)

        mock_snow.assert_called_with(instance='cern')

    def test_default_args_with_output_broke_share(self,
                                                  mock_snow,
                                                  mock_del_lb,
                                                  mock_del_kp,
                                                  mock_del_vol,
                                                  mock_del_ser,
                                                  mock_get_lb,
                                                  mock_get_kp,
                                                  mock_get_vol,
                                                  mock_get_serv,
                                                  mock_get_shares,
                                                  mock_cc,
                                                  mock_ks):
        mock_get_serv.return_value = []
        mock_get_shares.side_effect = iter([
            [
                fixtures.SHARE,
                fixtures.BROKEN_SHARE,
                fixtures.BROKEN_SHARE_CREATING,
                fixtures.BROKEN_SHARE_DELETING
            ],
            [
                fixtures.SHARE,
                fixtures.BROKEN_SHARE,
                fixtures.BROKEN_SHARE_CREATING,
                fixtures.BROKEN_SHARE_DELETING
            ],
            [],
        ])
        mock_get_kp.return_value = []

        with self.assertRaises(SystemExit) as context:
            main(['--timeout', '1'])
        self.assertEqual(context.exception.code, 1)

        mock_snow.assert_called_with(instance='cern')

    def test_default_args_with_output_broke_loadbalancer(self,
                                                         mock_snow,
                                                         mock_del_lb,
                                                         mock_del_kp,
                                                         mock_del_vol,
                                                         mock_del_ser,
                                                         mock_get_lb,
                                                         mock_get_kp,
                                                         mock_get_vol,
                                                         mock_get_serv,
                                                         mock_get_shares,
                                                         mock_cc,
                                                         mock_ks):
        mock_get_serv.return_value = []
        mock_get_lb.side_effect = iter([
            [
                fixtures.LOADBALANCER_DATA,
                fixtures.BROKEN_LOADBALANCER_DATA,
                fixtures.BROKEN_LOADBALANCER_CREATING_DATA,
                fixtures.BROKEN_LOADBALANCER_DELETING_DATA
            ],
            [
                fixtures.LOADBALANCER_DATA,
                fixtures.BROKEN_LOADBALANCER_DATA,
                fixtures.BROKEN_LOADBALANCER_CREATING_DATA,
                fixtures.BROKEN_LOADBALANCER_DELETING_DATA
            ],
            []
        ])
        mock_get_kp.return_value = []

        with self.assertRaises(SystemExit) as context:
            main(['--timeout', '1'])
        self.assertEqual(context.exception.code, 1)

        mock_snow.assert_called_with(instance='cern')
