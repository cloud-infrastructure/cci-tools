import logging

from ccitools.cmd.send_calendar_invitation import main
from unittest import mock
from unittest import TestCase


class TestSendCalendarInvitation(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 2)

    @mock.patch('ccitools.utils.sendmail.smtplib')
    def test_dry_run_ics(self, mock_smtp):
        main([
            '--organizer', 'user@cern.ch',
            '--attendees', 'att1@cern.ch,att2@cern.ch',
            '--title', 'Fake date',
            '--description', 'Fake description',
            '--date', '01-01-2019 08:00'
        ])

    @mock.patch('ccitools.utils.sendmail.smtplib')
    def test_perform_ics(self, mock_smtp):

        main([
            '--perform',
            '--organizer', 'user@cern.ch',
            '--attendees', 'att1@cern.ch,att2@cern.ch',
            '--title', 'Fake date',
            '--description', 'Fake description',
            '--date', '01-01-2019 08:00'
        ])

        mock_smtp.SMTP.assert_called_with('localhost')
