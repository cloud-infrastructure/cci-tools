#!/usr/bin/python3

import json
import logging
import requests
import sys

from ccitools.cmd import base
from requests_kerberos import HTTPKerberosAuth

# configure logging
logger = logging.getLogger(__name__)


class ReloadNodesFromPuppetDBCMD(base.BaseCMD):
    def __init__(self):
        super(ReloadNodesFromPuppetDBCMD, self).__init__(
            description="Get rundeck nodes from PuppetDB")

    def getFactPuppetDB(self, apiurl, factname, hostgroup):
        url = '%s/facts/%s' % (apiurl, factname)
        query = ('["in", "certname", ["extract", "certname", '
                 '[select-facts", ["and", ["=", "name", "hostgroup"], '
                 '["~", "value", "%s"]]]]]') % hostgroup
        headers = {'Content-Type': 'application/json',
                   'Accept': 'application/json, version=2'}
        payload = {'query': query}
        logging.info("Getting '%s' from '%s', query: '%s'" % (
            factname, url, query))
        r = requests.get(
            url,
            params=payload,
            headers=headers,
            auth=HTTPKerberosAuth(),
            verify="/etc/pki/tls/certs/CERN-bundle.pem",
            timeout=60)
        if r.status_code == requests.codes.ok:
            logging.info("Request code: '%s'" % r.status_code)
            return json.loads(r.text)
        logger.error("The request failed with code '%s'" % r.status_code)
        return None

    def store_nodes(self, apiurl, hostgroup, output):
        """Save the node list in a local file so rundeck can access it."""
        counter = 0
        operatingsystemfacts = self.getFactPuppetDB(
            apiurl,
            "operatingsystem",
            hostgroup)

        hostgroupfacts = self.getFactPuppetDB(apiurl, "hostgroup", hostgroup)
        if operatingsystemfacts and hostgroupfacts:
            logging.info("Saving node list in '%s'..." % output)
            with open("%s" % output, 'w') as file:
                for operatingsystem in operatingsystemfacts:
                    machine = operatingsystem['certname']

                    counter = counter + 1
                    file.write("%s:\n" % (machine))
                    file.write("    hostname: %s\n" % (machine))
                    file.write("    username: root\n")
                    file.write("    tags: ")

                    for hostgroup in hostgroupfacts:
                        if (machine == hostgroup['certname']):
                            file.write("    hostgroup=%s\n" % (
                                hostgroup['value']))
                    file.write("    osName: %s\n" % (
                        operatingsystem['value']))
            logging.info("Node list saved successfully")
        else:
            logging.error("Fact list empty. Check PuppetDB connection params")
        return counter

    def main(self, args):
        self.parser.add_argument(
            "--apiurl",
            help=("PuppetDB API url (https://<SERVER>:<PORT>/<API VERSION>)"),
            required=True)
        self.parser.add_argument(
            "--hostgroup",
            help="Foreman hostgroup",
            required=True)
        self.parser.add_argument(
            "--output",
            help="Path where the node list will be stored",
            required=True)

        args = self.parse_args(args)

        counter = self.store_nodes(args.apiurl, args.hostgroup, args.output)
        logger.info("%s machines added to: %s" % (counter, args.output))


# Needs static method for setup.cfg
def main(args=None):
    ReloadNodesFromPuppetDBCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)
