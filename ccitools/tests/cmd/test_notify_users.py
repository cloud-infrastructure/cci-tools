import logging

from ccitools.cmd.notify_users import main
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase


class TestNotifyUsers(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.notify_users.urllib.request')
    @mock.patch('ccitools.cmd.notify_users.XldapClient')
    @mock.patch('ccitools.cmd.notify_users.MailClient')
    @mock.patch('ccitools.cmd.notify_users.Template')
    def test_default_args(
        self,
        mock_tem,
        mock_mc,
        mock_xld,
        mock_url,
        mock_crc
    ):

        mock_cl = mock_crc.return_value

        # 1. test default args

        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)

        # 2. test proper data perform

        m = fixtures.FAKE_INSTANCE_OUTPUT
        mock_url.urlopen.return_value.getcode.return_value = 200
        mock_tem.return_value.readTemplate.return_value = 'fake_te'
        mock_cl.get_servers_by_names.return_value = m
        mock_cl.get_servers_by_hypervisors.return_value = m
        mock_cl.get_servers_by_projects.return_value = m
        mock_cl.get_project.return_value.name = 'fake_s'

        main([
            '--subject', 'fake_mail',
            '--body', 'fak_b',
            '--date', '20-06-2019 00:00',
            '--duration', '5',
            '--perform',
            'owners',
            '--projects', '{\
                                "vms": "vms_f",\
                                "project": "fake_p\
                                "user": "fake_u"\
                            "}',
        ])

        # 3. proper data dryrun

        main([
            '--subject', 'fake_mail',
            '--body', 'fak_b',
            '--date', '20-06-2019 00:00',
            '--duration', '5',
            '--dryrun',
            'owners',
            '--projects', '{\
                                "vms": "vms_f",\
                                "project": "fake_p\
                                "user": "fake_u"\
                            "}',
        ])

        # 4. proper data but no date

        main([
            '--subject', 'fake_mail',
            '--body', 'fak_b',
            '--duration', '5',
            '--dryrun',
            'owners',
            '--projects', '{\
                                "vms": "vms_f",\
                                "project": "fake_p\
                                "user": "fake_u"\
                            "}',
        ])

        # 5. exception perform

        mock_mc.return_value.send_mail.side_effect = iter([Exception])

        main([
            '--subject', 'fake_mail',
            '--body', 'fak_b',
            '--date', '20-06-2019 00:00',
            '--duration', '5',
            '--perform',
            'owners',
            '--projects', '{\
                                "vms": "vms_f",\
                                "project": "fake_p\
                                "user": "fake_u"\
                            "}',
        ])

        # 6. Exception get_vm_owner

        mock_xld.return_value.get_vm_owner.side_effect = iter([Exception])

        main([
            '--subject', 'fake_mail',
            '--body', 'fak_b',
            '--date', '20-06-2019 00:00',
            '--duration', '5',
            '--perform',
            'owners',
            '--projects', '{\
                                "vms": "vms_f",\
                                "project": "fake_p\
                                "user": "fake_u"\
                            "}',
        ])

        # 7. raise exception code not 200

        mock_url.urlopen.return_value.getcode.return_value = 404

        with self.assertRaises(Exception) as cm:
            main([
                '--subject', 'fake_mail',
                '--body', 'fak_b',
                '--date', '20-06-2019 00:00',
                '--duration', '5',
                '--perform',
                'owners',
                '--projects', '{\
                                    "vms": "vms_f",\
                                    "project": "fake_p\
                                    "user": "fake_u"\
                                "}',
            ])
        self.assertEqual(Exception, Exception)

        # 8. proper data members owners_project false

        mock_url.urlopen.return_value.getcode.return_value = 200

        main([
            '--subject', 'fake_mail',
            '--body', 'fak_b',
            '--date', '20-06-2019 00:00',
            '--duration', '5',
            '--perform',
            'members',
            '--projects', '{\
                                "vms": "vms_f",\
                                "project": "fake_p\
                                "user": "fake_u"\
                            "}',
        ])

        # 9. proper data members

        mock_xld.return_value.get_vm_owner.side_effect = iter(['fake_s'])
        mock_url.urlopen.return_value.getcode.return_value = 200
        mock_cl.get_project_members.return_value = ["fake1", "fake1"]
        mock_cl.find_project.return_value.name = 'fake2'

        main([
            '--subject', 'fake_mail',
            '--body', 'fak_b',
            '--date', '20-06-2019 00:00',
            '--duration', '5',
            '--perform',
            'members',
            '--projects', '{\
                                "vms": "vms_f",\
                                "project": "fake_p\
                                "user": "fake_u"\
                            "}',
        ])
