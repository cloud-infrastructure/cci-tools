import configparser
import logging
import time

from aitools.trustedbag import TrustedBagClient

logger = logging.getLogger(__name__)

TEIGI_TIMEOUT = 1800
TEIGI_RETRY_INTERVAL = 15


class AIClient(object):
    def __init__(self):
        config = configparser.ConfigParser()
        config.read_file(open('/etc/ai/ai.conf'))
        self.tbagclient = TrustedBagClient(
            host=config.get("tbag", "tbag_hostname"),
            port=config.get("tbag", "tbag_port"),
            timeout=config.get("tbag", "tbag_timeout"))

    def get_teigi_key(self, args, index):
        hostgroup = args.hostgroup
        teigi_key = "%s_%s" % (args.teigi_tag, index)
        timeout = time.time() + TEIGI_TIMEOUT
        logger.info("Getting '%s' key from TEIGI..." % teigi_key)
        while time.time() < timeout:
            try:
                return self.tbagclient.get_key(hostgroup, "hostgroup",
                                               teigi_key)['secret']
            except Exception as ex:
                logger.error("Cannot connect to TEIGI ('%s'). "
                             "Waiting %s seconds before retrying..."
                             % (ex, TEIGI_RETRY_INTERVAL))
                time.sleep(TEIGI_RETRY_INTERVAL)

        error_msg = "Impossible to get '%s' from TEIGI after %s " \
                    "seconds waiting" % (teigi_key, TEIGI_TIMEOUT)
        raise Exception(error_msg)

    def update_teigi_key(self, args, index, value):
        hostgroup = args.hostgroup
        teigi_key = "%s_%s" % (args.teigi_tag, index)
        timeout = time.time() + TEIGI_TIMEOUT
        logger.info("Updating '%s' key on TEIGI..." % teigi_key)
        while time.time() < timeout:
            try:
                return self.tbagclient.add_key(hostgroup, "hostgroup",
                                               teigi_key, value)
            except Exception as ex:
                logger.error("Cannot connect to TEIGI ('%s'). "
                             "Waiting %s seconds before retrying..."
                             % (ex, TEIGI_RETRY_INTERVAL))
                time.sleep(TEIGI_RETRY_INTERVAL)

        error_msg = "Impossible to UPDATE '%s' TEIGI after %s " \
                    "seconds waiting" % (teigi_key, TEIGI_TIMEOUT)
        raise Exception(error_msg)
