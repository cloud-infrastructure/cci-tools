import logging

# configure logging
logger = logging.getLogger(__name__)


class Phase(object):
    def __init__(self, description, func=None):
        self.description = description
        self.status = " -- "
        self.details = "Not executed"
        self.func = func

    def run(self, args, state):
        if self.func:
            self.func(args, self, state)

    def fail(self, reason):
        self.status = "FAIL"
        self.details = reason

    def ok(self, details):
        self.status = " OK "
        self.details = details

    def __str__(self):
        """Return string representation of the phase."""
        return "[{0}]: {1} ({2})".format(
            self.status,
            self.description,
            self.details)


class Phases(object):
    def __init__(self, phases):
        self.phases = phases

    def execute(self, start_from, args, state):
        for num, phase in enumerate(self.phases, start=1):
            if num >= int(start_from):
                logger.info('Run  step [%s] %s', num, phase.description)
                phase.run(args, state)
            else:
                logger.info('Skip step [%s] %s', num, phase.description)

    def __str__(self):
        """Return string representation of the project creation."""
        str = ""
        for num, phase in enumerate(self.phases, start=1):
            str += "Step {0}: {1}\n".format(num, phase)
        return str
