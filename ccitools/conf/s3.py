from oslo_config import cfg


host = cfg.StrOpt(
    'host',
    default='s3.cern.ch',
    help="Host to use for s3")

port = cfg.IntOpt(
    'port',
    default=443,
    help="Port to use for s3")

access_key = cfg.StrOpt(
    'access_key',
    default='access_key',
    help="access key for s3")

secret_key = cfg.StrOpt(
    'secret_key',
    default='secret_key',
    help='secret key for s3'
)

aws_signature = cfg.StrOpt(
    'aws_signature',
    default='AWS2',
    help='signature to use to communicate with the api'
)

is_secure = cfg.BoolOpt(
    'is_secure',
    default=True,
    help='validate SSL certificates during request'
)


GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    host,
    port,
    access_key,
    secret_key,
    aws_signature,
    is_secure,
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
