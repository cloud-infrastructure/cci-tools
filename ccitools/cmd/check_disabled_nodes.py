#!/usr/bin/python3

import ccitools.conf
import dateutil.parser
import logging
import prettytable
import sys

from ccitools.cmd.base.misc import BaseCloudForemanCMD
from ccitools import common
from ccitools.utils.sendmail import MailClient

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class CheckDisabledNodesCMD(BaseCloudForemanCMD):

    def __init__(self):
        super(CheckDisabledNodesCMD, self).__init__(
            description="Checks Nodes disabled in puppet owned by cloud team")

    def check_disabled_nodes(self, cloud, foreman, mailto):
        disabled_down_table = prettytable.PrettyTable(["Cell", "Host",
                                                       "Operating System",
                                                       "Updated at",
                                                       "Status", "State",
                                                       "Reason"])
        disabled_up_table = prettytable.PrettyTable(["Cell", "Host",
                                                     "Operating System",
                                                     "Updated at",
                                                     "Status", "State",
                                                     "Reason"])
        enabled_down_table = prettytable.PrettyTable(["Cell", "Host",
                                                      "Operating System",
                                                      "Updated at",
                                                      "Status", "State",
                                                      "Reason"])
        affected_hosts = []

        aggregates = cloud.get_nova_aggregates()

        for service in cloud.get_nova_services_list('nova-compute'):
            if (service.status.lower() == "disabled"
                    or service.state.lower() == "down"):
                try:
                    hostname = common.normalize_fqdn(service.host)

                    os = foreman.gethost(hostname)['operatingsystem_name']

                    aggrs = list(set(
                        [a.name for a in aggregates if hostname in a.hosts]))
                    region = aggrs.pop() if aggrs else '-'

                    date = dateutil.parser.parse(
                        service.updated_at.strftime('%d/%m/%Y at %H:%M'))

                    affected_hosts.append(hostname)

                    if service.status.lower() == "disabled" \
                            and service.state.lower() == "down":
                        disabled_down_table.add_row([region, hostname, os,
                                                     date,
                                                     service.status,
                                                     service.state,
                                                     service.disabled_reason])
                    elif service.status.lower() == "disabled" \
                            and service.state.lower() == "up":
                        disabled_up_table.add_row([region, hostname, os,
                                                   date,
                                                   service.status,
                                                   service.state,
                                                   service.disabled_reason])
                    else:
                        enabled_down_table.add_row([region, hostname, os,
                                                    date,
                                                    service.status,
                                                    service.state,
                                                    service.disabled_reason])
                except Exception as ex:
                    logger.error(ex)

        disabled_down = disabled_down_table.get_string(sortby="Cell")
        disabled_up = disabled_up_table.get_string(sortby="Cell")
        enabled_down = enabled_down_table.get_string(sortby="Cell")

        logger.info("Hosts DISABLED and DOWN: \n%s", disabled_down)
        logger.info("Hosts DISABLED and UP: \n%s", disabled_up)
        logger.info("Hosts ENABLED and DOWN: \n%s", enabled_down)

        mail_body = (
            'Dear all,<br/><br/>This is the list of all the <b>{0}</b> '
            'compute nodes that are <b>disabled</b> or <b>down</b> on '
            'our infrastructure.<br/>Please verify them, especially if '
            'there is no REASON specified.<br/><br/>'
            '<b>Hosts DISABLED and DOWN:'
            '</b><pre style="font-family:monospace;">{1}</pre><br/>'
            '<b>Hosts DISABLED and UP:</b>'
            '<pre style="font-family:monospace;">{2}</pre><br/>'
            '<b>Hosts ENABLED and DOWN:</b>'
            '<pre style="font-family:monospace;">{3}</pre><br/>').format(
                len(affected_hosts),
                disabled_down,
                disabled_up,
                enabled_down)

        mailclient = MailClient()
        mailclient.send_mail(
            mailto,
            " List of disabled compute nodes",
            mail_body,
            'svc.rdeck@cern.ch',
            '',
            '',
            'html'
        )

    def main(self, args=None):
        self.parser.add_argument("--mailto", required=True,
                                 help="Receiver of the report")
        args = self.parse_args(args)

        self.check_disabled_nodes(
            self.cloudclient,
            self.foreman,
            args.mailto
        )


# Needs static method for setup.cfg
def main(args=None):
    CheckDisabledNodesCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
