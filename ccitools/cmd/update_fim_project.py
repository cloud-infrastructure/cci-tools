#!/usr/bin/python3

import ccitools.conf
import logging
import sys

from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools.utils.fim import FIMClient


# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class UpdateFIMProjectCMD(BaseCloudCMD):
    def __init__(self):
        super(UpdateFIMProjectCMD, self).__init__(
            description="Update project(s) ownerships in FIM")

    def fim_update_project_name(self, fimclient, project_name, new_name):
        logger.debug("Updating Name of Projects via FIM...")

        return_code = fimclient.set_project_name(project_name, new_name)
        if return_code == 0:
            logger.info("Project '%s' updated successfully in FIM",
                        project_name)
        else:
            logger.error("Error trying to update project from FIM")
            raise Exception("Error trying to update project from FIM")

    def fim_update_project_description(self, fimclient, project_name, desc):
        logger.debug("Updating Description of Projects via FIM...")

        return_code = fimclient.set_project_description(project_name, desc)
        if return_code == 0:
            logger.info("Project '%s' updated successfully in FIM",
                        project_name)
        else:
            logger.error("Error trying to update project from FIM")
            raise Exception("Error trying to update project from FIM")

    def fim_update_project_owner(self, fimclient, project_name, owner):
        logger.debug("Updating Owner of Projects via FIM...")

        return_code = fimclient.set_project_owner(project_name, owner)
        if return_code == 0:
            logger.info("Project '%s' updated successfully in FIM",
                        project_name)
        else:
            logger.error("Error trying to update project from FIM")
            raise Exception("Error trying to update project from FIM")

    def fim_update_project_status(self, fimclient, project_name, status):
        logger.debug("Updating Status of Projects via FIM...")

        return_code = fimclient.set_project_status(project_name, status)
        if return_code == 0:
            logger.info("Project '%s' updated successfully in FIM",
                        project_name)
        else:
            logger.error("Error trying to update project from FIM")
            raise Exception("Error trying to update project from FIM")

    def retrieve_projects(self, args, cloud):
        if args.project_name:
            return [cloud.find_project(args.project_name)]

        elif args.owner:
            return cloud.get_projects_by_role(
                user=args.owner,
                rolename=args.role
            )

        return []

    def update_projects(self, args, projects, fimclient):
        for project in projects:
            if (project.id not in args.exclude_id
                    and project.type not in args.exclude_type):

                if args.set_name is not None:
                    logger.info("Changing project '%s' to '%s'",
                                project.name,
                                args.set_name)
                    if args.execute:
                        self.fim_update_project_name(
                            fimclient, project.name, args.set_name)

                if args.set_description is not None:
                    logger.info("Changing project '%s' description to '%s'",
                                project.name,
                                args.set_description)
                    if args.execute:
                        self.fim_update_project_description(
                            fimclient, project.name, args.set_description)

                if args.set_owner is not None:
                    logger.info("Changing project '%s' to be owned by '%s'",
                                project.name,
                                args.set_owner)
                    if args.execute:
                        self.fim_update_project_owner(
                            fimclient, project.name, args.set_owner)

                if args.status is not None:
                    logger.info("Changing project '%s' status to '%s'",
                                project.name,
                                args.status)
                    if args.execute:
                        self.fim_update_project_status(
                            fimclient, project.name, args.status)

    def main(self, args=None):
        self.parser.add_argument(
            "--role",
            default='owner',
            help="Name of the role to search for")

        self.parser.add_argument(
            '--exclude-id',
            action='append',
            help='Exclude identifier from project list',
            default=[])

        self.parser.add_argument(
            '--exclude-type',
            action='append',
            help='Exclude a type from project list',
            default=['personal'])

        filter = self.parser.add_mutually_exclusive_group()
        filter.add_argument(
            "--project-name",
            default='',
            help="Name of the project to fetch the project"
        )
        filter.add_argument(
            "--owner",
            default='',
            help="Name of current owner to fetch projects")

        self.parser.add_argument(
            '--execute',
            action='store_true',
            help='execute the operation')

        self.parser.add_argument(
            "--set-name",
            default=None,
            help="Value of the name to set")

        self.parser.add_argument(
            "--set-description",
            default=None,
            help="Value for the description to set")

        self.parser.add_argument(
            "--set-owner",
            default=None,
            help="Value of the owner to set")

        status = self.parser.add_mutually_exclusive_group()
        status.add_argument(
            '--set-enabled',
            dest='status',
            action='store_true')
        status.add_argument(
            '--set-disabled',
            dest='status',
            action='store_false')
        self.parser.set_defaults(status=None)

        args = self.parse_args(args)

        projects = self.retrieve_projects(args, self.cloudclient)
        self.update_projects(args, projects, FIMClient(CONF.fim.webservice))


# Needs static method for setup.cfg
def main(args=None):
    try:
        UpdateFIMProjectCMD().main(args)
    except Exception as e:
        logger.exception(e)
        sys.exit(1)


if __name__ == "__main__":
    main()
