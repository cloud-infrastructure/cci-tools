import logging

from ccitools.verify.loadhigh import LoadHighAction
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.verify.base.ssh_executor')
class TestLoadHighAction(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.client = LoadHighAction(host='fake.domain')

    def test_try_fix(self, mock_common):
        self.client.try_fix()

    def test_check_alarm_state(self, mock_common):
        mock_common.return_value = [], None
        self.assertFalse(self.client.check_alarm_state())

        mock_common.return_value = (
            ['fake.domain/another_alarm'], None)
        self.assertFalse(self.client.check_alarm_state())

        mock_common.return_value = (
            ['fake.domain/load/load-relative'], None)
        self.assertTrue(self.client.check_alarm_state())
        self.assertEqual(
            ' - fake.domain\n',
            self.client.get_reason())
