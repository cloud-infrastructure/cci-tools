import logging

from ccitools.cmd import notify_anomalous_vm
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase


class TestNotifyAnomalousVm(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.cloud_config')
    def test_default_args(self, mock_cc, mock_ks):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value

        cmd = notify_anomalous_vm.NotifyAnomalousVmCMD()

        with self.assertRaises(SystemExit) as cm:
            cmd.main([])
            mock_cc.OpenStackConfig.assert_called_with()
            mock_ks.Session.assert_called_with(
                auth=mock_cloud.get_auth.return_value)
        self.assertEqual(cm.exception.code, 2)

    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.cmd.notify_anomalous_vm.ServiceNowClient')
    @mock.patch('ccitools.cmd.notify_anomalous_vm.MailClient')
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.notify_anomalous_vm.XldapClient')
    def test_notify_anomalous_vm(self, mock_crc, mock_snc, mock_cc,
                                 mock_ks, mock_xld, mock_mc):
        mock_client = mock_crc.return_value

        m = fixtures.FAKE_INSTANCE_OUTPUT
        mock_client.get_servers_by_name.return_value = m

        cmd = notify_anomalous_vm.NotifyAnomalousVmCMD()
        cmd.main([
            '--instance', 'cern',
            '--vm', 'fake',
            '--issue', 'fake_issue',
        ])

    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.cmd.notify_anomalous_vm.ServiceNowClient')
    @mock.patch('ccitools.cmd.notify_anomalous_vm.MailClient')
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.notify_anomalous_vm.XldapClient')
    def test_notify_anomalous_vm_perform(self, mock_crc, mock_snc, mock_cc,
                                         mock_ks, mock_xld, mock_mc):
        mock_client = mock_crc.return_value

        m = fixtures.FAKE_INSTANCE_OUTPUT4

        mock_client.get_servers_by_name.return_value = m

        cmd = notify_anomalous_vm.NotifyAnomalousVmCMD()
        cmd.main([
            '--instance', 'fake',
            '--vm', 'fake',
            '--issue', 'fake_issue',
            '--perform'
        ])

    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.cmd.notify_anomalous_vm.ServiceNowClient')
    @mock.patch('ccitools.cmd.notify_anomalous_vm.MailClient')
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.notify_anomalous_vm.XldapClient')
    def test_notify_anomalous_vm_dryrun(self, mock_crc, mock_snc, mock_cc,
                                        mock_ks, mock_xld, mock_mc):
        mock_client = mock_crc.return_value

        m = fixtures.FAKE_INSTANCE_OUTPUT4

        mock_client.get_servers_by_name.return_value = m

        cmd = notify_anomalous_vm.NotifyAnomalousVmCMD()
        cmd.main([
            '--instance', 'fake',
            '--vm', 'fake',
            '--issue', 'fake_issue',
            '--dryrun'
        ])
