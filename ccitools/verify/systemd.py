import logging

from ccitools.common import ssh_executor
from ccitools.errors import SSHException
from ccitools.verify import base

# configure logging
logger = logging.getLogger(__name__)


class SystemDNeedReloadAction(base.ActionBase):

    def __init__(self, host, delay=120):
        super(SystemDNeedReloadAction, self).__init__(host, delay)
        self.pattern = (r'.*\/systemd-needreload\/boolean-NeedDaemonReload')

    def try_fix(self):
        """Implement the fix for the exception thrown in collectd."""
        # Try to start the service in the machine
        try:
            logger.info("Reloading systemd daemon")
            ssh_executor(
                self.host,
                'systemctl daemon-reload'
            )
        except SSHException:
            pass

    def check_alarm_state(self, after_try=False):
        ret = super(SystemDNeedReloadAction, self).check_alarm_state(after_try)
        if ret:
            self.reason = " - Reload of systemd has not cleaned the alarm."
        return ret


class SystemDStateAction(base.ActionBase):

    def __init__(self, host, delay=120):
        super(SystemDStateAction, self).__init__(host, delay)
        self.pattern = (r'.*\/systemd-systemd_state\/boolean-running')

    def try_fix(self):
        # There is no fix for this alarm, ignoring it.
        pass
