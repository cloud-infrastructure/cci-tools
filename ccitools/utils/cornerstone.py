import ccitools.conf
import logging

from cornerstoneclient.rest.v2 import ClientRestV2

logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class CornerstoneClient(object):

    def __init__(self):
        self.client = ClientRestV2(
            auth='oidc',
            params={
                'id': CONF.cornerstone.client_id,
                'secret': CONF.cornerstone.client_secret,
                'audience': CONF.cornerstone.audience,
                'token_url': CONF.cornerstone.token_url,
            },
            url=CONF.cornerstone.endpoint
        )

    def create_project(self, id, name, description,
                       status, type, owner, administrators):
        """Create a new OpenStack Project.

        :param id: Project ID
        :param name: Project name
        :param description: Project description
        :param status: status of the project
        :param type: type of the project
        :param owner: Project owner
        :param administrator: Project administrators
        """
        self.client.project_create(
            id=id,
            name=name,
            description=description,
            status=status,
            type=type,
            owner=owner,
            administrator=administrators
        )

    def update_project(self, id, name=None, description=None,
                       status=None, type=None, owner=None,
                       administrators=None):
        """Update an existing OpenStack Project.

        :param id: Project ID
        :param name: New Project name
        :param description: New Project description
        :param status: New status for the project
        :param type: new type for the project
        :param owner: new Project owner
        :param administrators: new Project administrator
        """
        self.client.project_update(
            id=id,
            name=name,
            description=description,
            status=status,
            type=type,
            owner=owner,
            administrator=administrators
        )

    def delete_project(self, id):
        """Delete an existing OpenStack Project.

        :param id: Project id
        """
        self.client.project_delete(id=id)
