import logging
import requests_mock

from ccitools.cmd.service_metrics import main
from unittest import mock
from unittest import TestCase


@requests_mock.Mocker()
@mock.patch('ccitools.cmd.service_metrics.InfluxDBClient')
class TestServiceMetrics(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_service_metrics_error(self, mock_req, mock_influx):

        mock_req.post(
            'http://monit-metrics:10012/',
            text='OK',
            status_code=200
        )

        main([])

        mock_influx.assert_called_with(
            host='dbod-ccinflux.cern.ch',
            port=8087,
            username='fake_user',
            password='fake_password',
            database='dblogger',
            ssl=True
        )

    def test_service_metrics_error_post(self, mock_req, mock_influx):

        mock_req.post(
            'http://monit-metrics:10012/',
            text='Error',
            status_code=404
        )

        with self.assertRaises(SystemExit) as cm:
            main([])
            self.assertEqual(cm.exception.code, -1)

        mock_influx.assert_called_with(
            host='dbod-ccinflux.cern.ch',
            port=8087,
            username='fake_user',
            password='fake_password',
            database='dblogger',
            ssl=True
        )

    def test_service_metrics(self, mock_req, mock_influx):
        mock_query = mock_influx.return_value.query.return_value
        mock_query.get_points.side_effect = iter([
            [
                {'last': 1}
            ],
            [
                {'last': 1}
            ],
            [
                {'sum': 1},
            ],
            [
                {'last': 1}
            ],
            [
                {'last': 1}
            ]
        ])

        mock_req.post(
            'http://monit-metrics:10012/',
            text='OK',
            status_code=200
        )

        main([])

        mock_influx.assert_called_with(
            host='dbod-ccinflux.cern.ch',
            port=8087,
            username='fake_user',
            password='fake_password',
            database='dblogger',
            ssl=True
        )

    def test_service_metrics_empty(self, mock_req, mock_influx):
        mock_query = mock_influx.return_value.query.return_value
        mock_query.get_points.side_effect = iter([
            # 1
            [
                {'last': 0},
            ],
            [
                {'last': 1},
            ],
            [
                {'sum': 1},
            ],
            [
                {'last': 1},
            ],
            [
                {'last': 1},
            ],
            # 2
            [
                {'last': 1},
            ],
            [
                {'last': 0},
            ],
            [
                {'sum': 1},
            ],
            [
                {'last': 1},
            ],
            [
                {'last': 1},
            ],
            # 3
            [
                {'last': 1},
            ],
            [
                {'last': 1},
            ],
            [
                {'sum': 0},
            ],
            [
                {'last': 1},
            ],
            [
                {'last': 1},
            ],
            # 4
            [
                {'last': 1},
            ],
            [
                {'last': 1},
            ],
            [
                {'sum': 1},
            ],
            [
                {'last': 0},
            ],
            [
                {'last': 1},
            ],
            # 5
            [
                {'last': 1},
            ],
            [
                {'last': 1},
            ],
            [
                {'sum': 1},
            ],
            [
                {'last': 1},
            ],
            [
                {'last': 0},
            ],
        ])

        mock_req.post(
            'http://monit-metrics:10012/',
            text='OK',
            status_code=200
        )

        for i in range(5):
            with self.assertRaises(SystemExit) as cm:
                main([])
                mock_influx.assert_called_with(
                    host='dbod-ccinflux.cern.ch',
                    port=8087,
                    username='fake_user',
                    password='fake_password',
                    database='dblogger',
                    ssl=True
                )
            self.assertEqual(cm.exception.code, -1)
