#!/usr/bin/python3

import ccitools.conf
import getpass
import logging
import socket
import sys
import tenacity

from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools.cmd.common import Phase
from ccitools.cmd.common import Phases
from ccitools.utils.cornerstone import CornerstoneClient
from ccitools.utils.fim import FIMClient
from ccitools.utils.resources import ResourcesClient
from ccitools.utils.servicenowv2 import ServiceNowClient
from ccitools.utils.snow.ticket import RequestState


# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


FIM_TIMEOUT = 3600
FIM_RETRY_INTERVAL = 60


class ResourcesError(Exception):
    pass


class ExistenceError(Exception):
    pass


class DeletionPhases(Phases):
    def __init__(self):
        self.project_content = Phase("Check content of project")
        self.project_fim = Phase("Project deletion call to FIM/MIM")
        self.project_deletion = Phase("Cornerstone synchronization")

        super().__init__([
            self.project_content,
            self.project_fim,
            self.project_deletion
        ])


class DeleteProjectCMD(BaseCloudCMD):
    def __init__(self):
        super(DeleteProjectCMD, self).__init__(
            description="Deletes an existing OpenStack "
                        "project based on different sources")

    def get_project(self, cloud, region, project_name):
        logger.info("Check '%s' project existence...", project_name)
        project = self.cloudclient.find_project(
            project_name,
            cloud=cloud,
            region=region
        )
        logger.info("Project '%s' found with id '%s'",
                    project_name, project.id)
        return project

    def check_project_content(self, cloud, project_name, phase):
        logger.info("Check content of '%s' project...", project_name)
        error_list = []

        volume_snapshots_list = self.cloudclient.get_volume_snapshots(
            project_name=project_name)

        volumes_list = self.cloudclient.get_volumes(project_name=project_name)

        instance_snapshots_list = self.cloudclient.get_images_by_project(
            project_name=project_name)

        server_list = self.cloudclient.get_servers(project_name=project_name)

        shares_list = self.cloudclient.get_shares_by_project(
            project_name=project_name)

        if volumes_list:
            error_list.append("Volumes")
        if volume_snapshots_list:
            error_list.append("Volume snapshots")
        if instance_snapshots_list:
            error_list.append("Instance snapshots")
        if server_list:
            error_list.append("Instances")
        if shares_list:
            error_list.append("Shares")

        if error_list:
            list_msg = '\n - '.join(error_list)
            error_msg = (
                "Project '{0}' could not be deleted since it contains "
                "the following resources:\n\n - {1}\n\n").format(
                    project_name, list_msg)
            logger.error(error_msg)
            phase_error_msg = "Project '%s' contains " \
                              "resources in use" % project_name
            phase.fail(phase_error_msg)
            raise ResourcesError(error_msg)
        else:
            info_msg = "Project '%s' is empty. " \
                       "Proceeding to deletion..." % project_name
            phase.ok(info_msg)
            logger.info(info_msg)

    def delete_project(self, cloud, region, project_name, project_id,
                       caller, engine_mode, safe_exec_mode, phase):
        logger.info("Deleting Project via FIM...")

        self.cloudclient.delete_project_fim_properties(project_id)
        info_msg = "'fim-skip' and 'fim-lock' fields removed from project"
        logger.info(info_msg)

        if engine_mode:
            # Deleting project on new Resources Portal
            project = self.cloudclient.find_project(
                project_name,
                cloud=cloud,
                region=region
            )
            CornerstoneClient().delete_project(
                id=project.id,
            )

            ResourcesClient().delete_project(
                id=project.id
            )

            info_msg = "Project '%s' deleted successfully from MIM" % (
                project_name)
            phase.ok(info_msg)
            logger.info(info_msg)

        else:
            fimclient = FIMClient(CONF.fim.webservice)
            return_code = fimclient.delete_project(project_name)

            if return_code == 0:
                info_msg = "Project '%s' deleted successfully from FIM" % (
                    project_name)
                phase.ok(info_msg)
                logger.info(info_msg)
            else:
                logger.error("Error trying to delete project from FIM")
                raise Exception("Error trying to delete project from FIM")

    @tenacity.retry(stop=tenacity.stop_after_delay(FIM_TIMEOUT),
                    wait=tenacity.wait_fixed(FIM_RETRY_INTERVAL),
                    before_sleep=tenacity.before_sleep_log(
                        logger,
                        logging.WARNING))
    def wait_for_project(self, cloud, region, target_project):
        try:
            self.cloudclient.find_project(
                target_project,
                cloud=cloud,
                region=region
            )
            raise tenacity.TryAgain
        except Exception:  # nosec
            # Project deleted skip active wait
            pass

    def check_for_project_deletion(self, cloud, region, project_name, phase):
        logger.info("Waiting for project deletion in OpenStack...")
        try:
            self.wait_for_project(cloud, region, project_name)
        except Exception:
            error_msg = "Project '%s' could not be deleted. " \
                        "Please check FIM logs" % project_name
            phase.fail(error_msg)
            logger.error(error_msg)
            raise ExistenceError(error_msg)
        else:
            info_msg = "Project '%s' has been deleted " % (project_name)
            phase.ok(info_msg)
            logger.info(info_msg)

    def delete_main(self, cloud, region, data, engine_mode, safe_exec_mode,
                    check_resources, phases_status, caller=None,
                    snowclient=None):
        """Delete a new OpenStack project based on the information provided.

        :snowclient: servicenow client instance
        :ticket: servicenow ticket number
        :cloud: cloud client
        """
        project = self.get_project(cloud, region, data.project_name)
        if check_resources:
            self.check_project_content(cloud, data.project_name,
                                       phases_status.project_content)
        else:
            logger.warning(
                "Ignoring any found resource for project deletion...")
        self.delete_project(cloud, region, data.project_name,
                            project.id, caller, engine_mode,
                            safe_exec_mode, phases_status.project_fim)
        self.check_for_project_deletion(
            cloud, region, data.project_name, phases_status.project_deletion)

    def delete_from_snow(self, args, phases_status):
        snowclient = ServiceNowClient(instance=args.instance)
        record_producer = snowclient.get_project_deletion_rp(
            args.ticket_number)
        caller = snowclient.user.get_user_info_by_sys_id(
            record_producer.request.info.u_caller_id)
        resolver = snowclient.user.get_user_info_by_user_name(
            args.resolver)

        try:
            self.delete_main(args.cloud, args.region_name, record_producer,
                             args.engine_mode,
                             args.safe_exec_mode, args.check_resources,
                             phases_status, caller)
        except ResourcesError as e:
            # FIXME(luis): it takes some time to enable the project, so for
            # some reason you get a 401 when doing SOAP requests again in SNOW
            # (due to timeouts, most likely). That's why I need to force again
            # to get a new session, reinitializing the object and the
            # tickets/record producers. It's worth to check with David from
            # the SNOW team why this is happening
            snowclient = ServiceNowClient(instance=args.instance)
            request = snowclient.ticket.get_ticket(args.ticket_number)

            ticket_comment = "Dear %s,\n\n%sPlease, make sure that " \
                             "your project has no resources in use.\n\n" \
                             "Best regards,\nCloud Infrastructure Team" % \
                             (caller.first_name, e)
            request.add_comment(ticket_comment)
            request.change_state(RequestState.WAITING_FOR_USER)
            request.change_assignee(resolver.name)
            request.save()
            sys.exit(1)
        else:
            # FIXME(luis): it takes some time to enable the project, so for
            # some reason you get a 401 when doing SOAP requests again in SNOW
            # (due to timeouts, most likely). That's why I need to force again
            # to get a new session, reinitializing the object and the
            # tickets/record producers. It's worth to check with David from
            # the SNOW team why this is happening

            snowclient = ServiceNowClient(instance=args.instance)
            request = snowclient.ticket.get_ticket(args.ticket_number)

            pname = record_producer.project_name
            ticket_comment = """Dear %s,\n\n""" \
                             """Your project '%s' has been deleted.\n\n""" \
                             """Best regards,\n""" \
                             """Cloud Infrastructure Team""" % (
                                 caller.first_name, pname)
            request.resolve(ticket_comment,
                            args.resolver)
            request.save()
        finally:
            work_note = "%s\n\n%s" % (
                args.execution_reference, str(phases_status))
            print("\n%s" % work_note)

    def delete_from_input(self, args, phases_status):
        try:
            self.delete_main(args.cloud, args.region, args, args.engine_mode,
                             args.safe_exec_mode,
                             args.check_resources, phases_status)
        finally:
            work_note = "%s\n\n%s" % (
                args.execution_reference, str(phases_status))
            print("\n%s" % work_note)

    def main(self, args=None):
        subparsers = self.parser.add_subparsers(metavar='<subcommand>')

        engine = self.parser.add_mutually_exclusive_group()
        engine.add_argument('--mim',
                            dest="engine_mode",
                            action='store_true')
        engine.add_argument('--fim',
                            dest="engine_mode",
                            action='store_false')

        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--default',
                               dest="safe_exec_mode",
                               action='store_true')
        behaviour.add_argument('--force',
                               dest="safe_exec_mode",
                               action='store_false')

        resources = self.parser.add_mutually_exclusive_group()
        resources.add_argument(
            '--check',
            help="Check existence of project resources "
                 " when deleting the project.",
            dest="check_resources",
            action='store_true')
        resources.add_argument(
            '--ignore',
            help="Ignore existence of project resources "
                 " when deleting the project.",
            dest="check_resources",
            action='store_false')

        self.parser.add_argument(
            "--execution-reference",
            help="adds a execution reference to the work notes",
            default="Executed by '%s' on '%s'" % (
                getpass.getuser(), socket.getfqdn()))

        parser_snow = subparsers.add_parser(
            'from-snow',
            help="Delete a project based on the "
                 "information provided "
                 "via snow ticket.")
        parser_snow.add_argument("--instance",
                                 default="cern",
                                 help="Service now instance")
        parser_snow.add_argument("--ticket-number",
                                 required=True)
        parser_snow.add_argument("--resolver",
                                 help="User who will resolve the ticket")
        parser_snow.set_defaults(func=self.delete_from_snow)

        parser_input = subparsers.add_parser(
            'from-input',
            help="Delete a project based on the "
                 "information provided as arguments.")
        parser_input.add_argument(
            "--project-name",
            required=True,
            help="Complete name of the project. "
                 "Example: Build service, Web development, etc")
        parser_input.set_defaults(func=self.delete_from_input)

        args = self.parse_args(args)

        try:
            call = args.func
        except AttributeError:
            self.parser.error("Too few arguments")

        call(args, DeletionPhases())


# Needs static method for setup.cfg
def main(args=None):
    DeleteProjectCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
