import logging

from ccitools.cleanup import base
from ccitools import common
from datetime import datetime

# configure logging
logger = logging.getLogger(__name__)
date_format = "%Y-%m-%dT%H:%M:%SZ"


class ServerResourceCleanup(base.ResourceCleanup):
    def __init__(self, cloud, quiet):
        super(ServerResourceCleanup, self).__init__(
            'SERVERS', cloud, quiet)

    def get_pending(self):
        servers_pending = []
        logger.info("Looking for leftover servers ...")
        for server in self.cloud.get_servers(
                user_name='svcprobe', tag='cleanup'):
            if base.RALLY_REGEX.match(server.name):
                creation = datetime.strptime(
                    server.created, date_format)
                if (datetime.now() - creation).days >= 1:
                    servers_pending.append(server)
        return servers_pending

    def delete_leftovers_before(self):
        if self.leftovers:
            self.cloud.delete_servers(self.leftovers)

    def delete_leftovers_after(self):
        pass

    def get_broken(self):
        broken_servers = []
        for server in self.cloud.get_servers(
                user_name='svcprobe', tag='cleanup'):
            if server in self.leftovers:
                broken_servers.append(server)
        return broken_servers

    def format_table(self):
        server_fields = [
            ('ID', 'id'),
            ('Name', 'name'),
            ('Created', 'created'),
            ('Project ID', 'tenant_id'),
            ('Host ID', 'hostId'),
            ('Status', 'status'),
            ('Task State', 'OS-EXT-STS:task_state'),
            ('Updated', 'updated'),
        ]
        servers_table = common.get_resources_info_table(
            self.leftovers, server_fields)

        return servers_table
