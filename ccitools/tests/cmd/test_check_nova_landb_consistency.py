import logging

from ccitools.cmd.check_nova_landb_consistency import main
from unittest import mock
from unittest import TestCase


class TestCheckNovaLandbConsistency(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch('ccitools.cmd.check_nova_landb_consistency.LanDB')
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    def test_dryrun(self, mock_cloud, mock_landb):

        main([])

        mock_landb.assert_called_with(
            username='neutron',
            password='fake_password',
            host='network.cern.ch',
            port=443,
            protocol='https',
            version=6
        )
