#!/usr/bin/python3

import json
import logging
import prettytable
import subprocess  # nosec
import sys
import tenacity

from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools.cmd.base.foreman import BaseForemanCMD
from ccitools.common import ping
from ccitools.common import ssh_executor
from ccitools.utils.sendmail import MailClient
from ccitools.utils.servicenowv2 import ServiceNowClient

# configure logging
logger = logging.getLogger(__name__)

SNOW_TIMEOUT = 600
SNOW_RETRY_INTERVAL = 60


class HealthReportHvCMD(BaseForemanCMD, BaseCloudCMD):
    def __init__(self):
        super(HealthReportHvCMD, self).__init__(
            description="Health report for HV without GNI ticket")
        self.parser.add_argument(
            "--instance", default="cern", help="Service-Now instance")
        self.parser.add_argument(
            "--mail-to", required=True,
            help="User or egroup the report will be sent to")
        self.parser.add_argument(
            "--mail-from", default='noreply@cern.ch', help="mail sender")
        self.parser.add_argument(
            "--pattern", default="cern.ch", help="Hostname filter pattern")

    @tenacity.retry(stop=tenacity.stop_after_delay(SNOW_TIMEOUT),
                    wait=tenacity.wait_fixed(SNOW_RETRY_INTERVAL))
    def check_tickets(self, hostname, snowclient):
        tickets = []
        try:
            query = ("active=true^stateNOT IN7,6^"
                     "u_configuration_items_listLIKE{0}").format(hostname)
            logger.info("Getting snow tickets"
                        "related to %s", hostname)
            tickets = snowclient.ticket.raw_query_incidents(query)

            # tickets related?
            # If This HV has open ticket, ignore.
        except Exception:
            logger.info("Cannot connect to snowclient")
            try:
                raise tenacity.TryAgain
            except Exception:
                logger.error("No more retries left")
        return tickets

    def get_failing_hypervisors(self, snowclient, mail_to, mail_from, pattern):
        # Dictionary to store hostnames and problems
        fail_dict = {}
        cmd = "hostname"
        mail_body = 'All HV OK'

        # We need detailed list to filter QEMU type
        hypervisor_list = [

            hv.hypervisor_hostname for hv in (
                self.cloudclient.get_hypervisors_list(
                    pattern, detailed=True)) if hv.hypervisor_type == 'QEMU'
        ]

        # Iterate over list that may contain duplicates
        for hostname in set(hypervisor_list):
            ssh_status = '?'
            ssh_success = False
            try:
                ssh_status, err = ssh_executor(hostname, cmd,
                                               connect_timeout=120,
                                               banner_timeout=120,
                                               keep_alive_interval=2)
                if not ssh_status and not err:
                    ssh_status = 'SSH_ERROR'
                else:
                    ssh_status = "Machine is working fine"
                    ssh_success = True

            except Exception:
                logger.info("Cannot connect to %s. Trying to ping it...",
                            hostname)
                if ping(hostname):
                    logger.info("Host is pingable")
                    ssh_status = "Exception-Impossible to SSH(ownership?)"
                else:
                    logger.info("Host seems to be OFF")
                    ssh_status = "Exception-Host seems to be OFF(not pingable)"

            if not ssh_success and not self.check_tickets(hostname,
                                                          snowclient):
                # HV is not accessible by SSH and it doesn't have any ticket in
                fail_dict[hostname] = ssh_status

        if fail_dict:
            mail_body = """Dear all,
                        <br/>
                        <br/>
                        List of HV failing to connect without a ticket
                        <p>host problem list: </p><br/>
                        """
            # Build a table -> Host -- Problem
            pt = prettytable.PrettyTable(
                ["Host", "Problem", "Foreman group", "Roger alarms"])
            pt.format = True
            pt.hrules = True
            pt.vrules = True
            for host, problem in fail_dict.items():
                # Next code should work but to avoid crashes I add try/pass
                # Due this code takes 3-8 hours to run
                # Obtain foreman data
                try:
                    host_g = self.foreman.gethost(host)
                except Exception:
                    host_g = {'hostgroup_title': "No data"}
                # Obtain Roger status
                try:
                    result = subprocess.run(  # nosec
                        ['roger', 'show', host],
                        stdout=subprocess.PIPE)
                    text = result.stdout
                    # Give format to text
                    f_t = json.loads(text)
                    rg = f_t[0]['app_alarmed']
                except Exception:
                    rg = 'No data'
                # Add data to table.
                pt.add_row([host, problem, host_g['hostgroup_title'], rg])

            mail_body += pt.get_html_string()

        logger.info(mail_body)

        # send report
        mailclient = MailClient()
        mailclient.send_mail(mail_to, "Health report for HV",
                             mail_body, mail_from, '', '', 'html')

    def main(self, args=None):
        args = self.parse_args(args)

        snowclient = ServiceNowClient(instance=args.instance)

        self.get_failing_hypervisors(
            snowclient, args.mail_to, args.mail_from, args.pattern)


# Needs static method for setup.cfg
def main(args=None):
    HealthReportHvCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logging.error(e)
        sys.exit(1)
