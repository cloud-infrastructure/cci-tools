from oslo_config import cfg


accounting_url = cfg.StrOpt(
    'accounting_url',
    default=('https://accounting-receiver.cern.ch/v3/fe'),
    help="accounting url")

unitcost_url = cfg.StrOpt(
    'unitcost_url',
    default=('https://accounting-receiver.cern.ch/v3/unit_cost'),
    help="unitcost url")

verify = cfg.BoolOpt(
    'verify',
    default=False,
    help='Verify SSL connection')

apikeys = cfg.DictOpt(
    'apikeys',
    default={
        'Cloud': '',
        'Ceph': ''
    },
    help="API keys to access accounting service")

query = cfg.StrOpt(
    'query',
    default=('SELECT {0} '
             'FROM {1} '
             'WHERE time >= {3} - 24h AND time < {3} '
             'GROUP BY {2}')
)

services = cfg.DictOpt(
    'services',
    default={
        'Cloud': {
            'from': 'Cloud Infrastructure',
            'doc': 'https://clouddocs.web.cern.ch/details/accounting',
            'metrics': {
                'cores_usage': {
                    'select': 'mean(usage) AS usage',
                    'table': ('cores_usage_per_chargegroup_and_'
                              'chargerole_and_type'),
                    'group': ['chargegroup', 'chargerole', 'type'],
                    'name': 'Cores',
                    'aggr': 'avg',
                    'qos': 'type',
                    'factor': 1,
                    'cost': False
                }
            }
        },
        'Ceph': {
            'from': 'Ceph',
            'doc': 'https://clouddocs.web.cern.ch/details/accounting',
            'multi': False,
            'metrics': {
                'volumes_size': {
                    'select': 'mean(usage) AS usage',
                    'table': ('volumes_gb_quota_per_type_'
                              'per_chargegroup_and_chargerole'),
                    'group': ['chargegroup', 'chargerole', 'type'],
                    'name': 'VolumeSizeBytes',
                    'aggr': 'avg',
                    'qos': 'type',
                    'factor': 1073741824,
                    'cost': True,
                    'function': 'get_volume_types',
                    'cost_attr': 'description',
                    'cost_key': 'unit_value'
                },
                'shares_size': {
                    'select': 'mean(usage) AS usage',
                    'table': ('fileshares_gb_quota_per_type_'
                              'per_chargegroup_and_chargerole'),
                    'group': ['chargegroup', 'chargerole', 'type'],
                    'name': 'FileShareSizeBytes',
                    'aggr': 'avg',
                    'qos': 'type',
                    'factor': 1073741824,
                    'cost': True,
                    'function': 'get_share_types',
                    'cost_attr': 'description',
                    'cost_key': 'unit_value'
                }
            }
        }
    }
)

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    accounting_url,
    unitcost_url,
    verify,
    apikeys,
    query,
    services
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
