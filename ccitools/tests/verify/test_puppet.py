import logging

from ccitools.verify.puppet import PuppetAgentAction
from ccitools.verify.puppet import PuppetAgentCannotCompileAction
from ccitools.verify.puppet import PuppetAgentRunErrorsAction
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.verify.puppet.ssh_executor')
@mock.patch('ccitools.verify.base.ssh_executor')
class TestPuppetAgentAction(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.client = PuppetAgentAction(host='fake.domain')

    def test_try_fix(self, mock_common, mock_puppet):
        self.client.try_fix()

    def test_check_alarm_state(self, mock_common, mock_puppet):
        mock_common.return_value = [], None
        mock_puppet.return_value = [], None
        self.assertFalse(self.client.check_alarm_state())

        mock_common.return_value = (
            ['fake.domain/another_alarm'], None)
        mock_puppet.return_value = [], None
        self.assertFalse(self.client.check_alarm_state())

        mock_common.return_value = (
            ['fake.domain/puppet/puppet_time'], None)
        mock_puppet.return_value = [], None
        self.assertTrue(self.client.check_alarm_state())
        self.assertEqual(
            " - fake.domain\n",
            self.client.get_reason())

        mock_common.return_value = (
            ['fake.domain/puppet/puppet_time'], None)
        mock_puppet.return_value = (
            ['{"disabled_message":"Testing"}'], None)

        self.assertTrue(self.client.check_alarm_state())
        self.assertEqual(
            " - fake.domain --> Puppet was manually disabled."
            " Reason: 'Testing'\n",
            self.client.get_reason())


@mock.patch('ccitools.verify.puppet.ssh_executor')
@mock.patch('ccitools.verify.base.ssh_executor')
class TestPuppetAgentRunErrorsAction(TestCase):

    def setUp(self):
        self.client = PuppetAgentRunErrorsAction(host='fake.domain')

    def test_try_fix(self, mock_common, mock_puppet):
        self.client.try_fix()

    def test_check_alarm_state(self, mock_common, mock_puppet):
        mock_common.return_value = [], None
        mock_puppet.return_value = [], None
        self.assertFalse(self.client.check_alarm_state())

        mock_common.return_value = (
            ['fake.domain/another_alarm'], None)
        mock_puppet.return_value = [], None
        self.assertFalse(self.client.check_alarm_state())

        mock_common.return_value = (
            ['fake.domain/puppet/resources-failed'], None)
        mock_puppet.return_value = [], None
        self.assertTrue(self.client.check_alarm_state())
        self.assertEqual(
            " - fake.domain\n",
            self.client.get_reason())

        mock_common.return_value = (
            ['fake.domain/puppet/resources-failed'], None)
        mock_puppet.return_value = (
            ['{"disabled_message":"Testing"}'], None)

        self.assertTrue(self.client.check_alarm_state())
        self.assertEqual(
            " - fake.domain --> Puppet was manually disabled."
            " Reason: 'Testing'\n",
            self.client.get_reason())


@mock.patch('ccitools.verify.puppet.ssh_executor')
@mock.patch('ccitools.verify.base.ssh_executor')
class TestPuppetAgentCannotCompileAction(TestCase):

    def setUp(self):
        self.client = PuppetAgentCannotCompileAction(host='fake.domain')

    def test_try_fix(self, mock_common, mock_puppet):
        self.client.try_fix()

    def test_check_alarm_state(self, mock_common, mock_puppet):
        mock_common.return_value = [], None
        mock_puppet.return_value = [], None
        self.assertFalse(self.client.check_alarm_state())

        mock_common.return_value = (
            ['fake.domain/another_alarm'], None)
        mock_puppet.return_value = [], None
        self.assertFalse(self.client.check_alarm_state())

        mock_common.return_value = (
            ['fake.domain/puppet/boolean-compiled'], None)
        mock_puppet.return_value = [], None
        self.assertTrue(self.client.check_alarm_state())
        self.assertEqual(
            " - fake.domain\n",
            self.client.get_reason())

        mock_common.return_value = (
            ['fake.domain/puppet/boolean-compiled'], None)
        mock_puppet.return_value = (
            ['{"disabled_message":"Testing"}'], None)

        self.assertTrue(self.client.check_alarm_state())
        self.assertEqual(
            " - fake.domain --> Puppet was manually disabled."
            " Reason: 'Testing'\n",
            self.client.get_reason())
