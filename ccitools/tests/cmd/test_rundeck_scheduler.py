import logging
import requests

from ccitools.cmd.rundeck_scheduler import main
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase


class TestRundeckScheduler(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 2)

    @mock.patch('ccitools.cmd.base.rundeck.auth_get_sso_cookie')
    @mock.patch('ccitools.utils.rundeck.requests')
    def test_dry_run_old_date(self, mock_requests, mock_cookie):
        with self.assertRaises(SystemExit) as cm:
            main([
                '--auth', 'sso',
                '--job-id', '1111111',
                '--datetime', '2000-01-01T00:00:00+0200',
                '--user', 'fake_user'
            ])
        self.assertEqual(cm.exception.code, 1)

    @mock.patch('ccitools.cmd.base.rundeck.auth_get_sso_cookie')
    @mock.patch('ccitools.utils.rundeck.requests')
    def test_dry_run(self, mock_requests, mock_cookie):
        main([
            '--auth', 'sso',
            '--job-id', '1111111',
            '--datetime', '2030-01-01T00:00:00+0200',
            '--user', 'fake_user'
        ])

    @mock.patch('ccitools.cmd.base.rundeck.auth_get_sso_cookie')
    @mock.patch('ccitools.utils.rundeck.requests')
    def test_dry_run_with_params(self, mock_requests, mock_cookie):
        main([
            '--auth', 'sso',
            '--job-id', '1111111',
            '--datetime', '2030-01-01T00:00:00+0200',
            '--user', 'fake_user',
            '--parameters', "{'myopt1':'value'}"
        ])

    @mock.patch('ccitools.cmd.base.rundeck.auth_get_sso_cookie')
    @mock.patch('ccitools.utils.rundeck.requests')
    def test_perform_with_params(self, mock_requests, mock_cookie):
        mock_response = mock_requests.post.return_value
        type(mock_response).status_code = mock.PropertyMock(
            return_value=requests.codes.ok)
        type(mock_response).text = mock.PropertyMock(
            return_value=fixtures.RUNDECK_SCHEDULER_OUTPUT)
        type(mock_requests.codes).ok = mock.PropertyMock(
            return_value=requests.codes.ok)

        main([
            '--auth', 'sso',
            '--job-id', '1111111',
            '--datetime', '2030-01-01T00:00:00+0200',
            '--user', 'fake_user',
            '--perform'
        ])
