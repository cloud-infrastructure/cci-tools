#!/usr/bin/python3

import logging
import prettytable
import sys

from ccitools.cmd.base import BaseCMD
from ccitools.utils.servicenowv2 import ServiceNowClient

# configure logging
logger = logging.getLogger(__name__)


class DumpTicketCMD(BaseCMD):
    def __init__(self):
        super(DumpTicketCMD, self).__init__(
            description="Script to dump a ticket ServiceNow")

    def dump_ticket(self, args):
        snowclient = ServiceNowClient(instance=args.instance)

        # get ticket information
        ticket = snowclient.ticket.get_ticket(args.ticket_number)
        if ticket is None:
            raise Exception('No ticket found')

        caller = snowclient.user.get_user_info_by_sys_id(
            ticket.info['u_caller_id']).name
        functional_element = snowclient.fe.fe_client.get(
            ticket.info['u_functional_element']).name
        assigned_to = snowclient.user.get_user_info_by_sys_id(
            ticket.info['assigned_to']).name

        # build table and print
        t = prettytable.PrettyTable(["Field", "Value"])
        t.add_row(["Ticket:", f"{args.ticket_number}"])
        t.add_row(["Title:", f"{ticket.info['short_description']}"])
        t.add_row(["Caller:", f"{caller}"])
        t.add_row(["Functional Element:", f"{functional_element}"])
        t.add_row(["Assigned To:", f"{assigned_to}"])

        t.border = True
        t.header = False
        t.align["Field"] = 'r'
        t.align["Value"] = 'l'
        print(t)

    def main(self, args=None):

        self.parser.add_argument("--ticket-number", required=True)
        self.parser.add_argument(
            "--instance", default="cern", help="Service now instance")

        args = self.parse_args(args)

        self.dump_ticket(args)


# Needs static method for setup.cfg
def main(args=None):
    try:
        DumpTicketCMD().main(args)
    except Exception as e:
        logging.exception(e)
        sys.exit(-1)


if __name__ == "__main__":
    main()
