import ccitools.conf
import json
import logging
import re
import requests
import socket
import sys
import tenacity

from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools.utils.sendmail import MailClient
from ccitools.utils.servicenowv2 import ServiceNowClient

# configure logging
logger = logging.getLogger(__name__)

CONF = ccitools.conf.CONF

SNOW_TIMEOUT = 600
SNOW_RETRY_INTERVAL = 60
TICKETS_THRESHOLD = 5


class BmcConnectivityCheckCMD(BaseCloudCMD):
    def __init__(self):
        super(BmcConnectivityCheckCMD, self).__init__(
            description="Ironic BMC connectivity check")
        self.parser.add_argument(
            "--instance", default="cern", help="Service-Now instance")
        self.parser.add_argument(
            "--mail-to", default="cloud-infrastructure-3rd-level@cern.ch",
            help="User or egroup the report will be sent to")
        self.parser.add_argument(
            "--mail-from", default='noreply@cern.ch', help="mail sender")
        self.error_nodes = {}

    def query_es_logs(self):
        """Query logs from Elastic search.

        Search for the logs from the last week indicating
        the nodes with ipmi connectivity issues. This log
        occures only once after the issue is found.
        """
        server = CONF.es.server
        user = CONF.es.user
        password = CONF.es.password
        query_string = "metadata.hostgroup: /cloud_baremetal\\/all_in_one\\" \
            "/gva_baremetal_001\\/.*/ AND \"During sync_power_state, could " \
            "not get power state for node\" AND \"attempt 10 of 10\""
        url = server + '/monit_private_openstack_logs_generic-*/_search'
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        query = json.dumps({
            "_source": ["data.raw"],
            "size": 500,
            "query": {
                "bool": {
                    "must": [
                        {
                            "query_string": {
                                "query": query_string,
                                "analyze_wildcard": True
                            }
                        },
                        {
                            "match_phrase": {
                                "data.source": {
                                    "query": "ironic.conductor"
                                }
                            }
                        },
                        {
                            "range": {
                                "metadata.timestamp": {
                                    "format": "strict_date_optional_time",
                                    "gte": "now-7d"
                                }
                            }
                        }
                    ]
                }
            }})
        response = requests.get(url, auth=(
            user, password), data=query, headers=headers,
            verify="/etc/pki/tls/certs/"
            "CERN_Root_Certification_Authority_2.pem",
            timeout=300)
        return json.loads(response.text)['hits']['hits']

    def parse_logs(self, es_logs):
        """Go through all logs and call a function for uuid extraction."""
        errors_list = json.loads(es_logs)
        nodes = {
            uuid for uuid in map(
                self.extract_node_uuid, errors_list) if uuid is not None}
        return nodes

    def extract_node_uuid(self, error_log):
        """Extract uuid from kibana log.

        From the Kibana log it will extract the uuid of the broken node.
        It may contain letters, digits and '-'.
        """
        raw_log = error_log['_source']['data']['raw']
        match_obj = re.match(
            r'During sync_power_state, could not get power'
            r' state for node ([\w-]+), attempt 10 of 10.',
            raw_log, re.M
        )
        if match_obj:
            return match_obj.group(1)
        logger.error("Can not retrieve node uuid from the log")
        self.error_nodes[raw_log] = "Can not retrieve node uuid from the log"
        return None

    def get_node_name_by_uuid(self, uuid):
        """Return node's name if its power state is still None.

        If power is not None, it means that the node was fixed
        because it's able to get power state using IPMI.
        """
        node = self.cloudclient.get_baremetal_node_by_id(uuid)
        try:
            if not node.power_state:
                return node.name
        except AttributeError:
            logger.error("Can not retrieve node's name by uuid: %s", uuid)
            self.error_nodes[uuid] = "Can not get node's name"
            return

    def get_broken_nodes_names(self):
        """Retrieve names of nodes with ipmi connectivity issues.

        Query ES for logs. Extract uuid of nodes and map them to names.
        """
        es_logs = json.dumps(self.query_es_logs())
        nodes_uuid = self.parse_logs(es_logs)
        nodes_names = {
            node_name for node_name in map(
                self.get_node_name_by_uuid, nodes_uuid)
            if node_name is not None}
        if nodes_names:
            logger.info("Broken nodes are: %s", nodes_names)
        else:
            logger.info("No broken nodes")
        return nodes_names

    def send_email(self, nodes_and_tickets, mail_to, mail_from):
        """Send email with the list of broken nodes.

        If there are any errors during retrieving nodes' information
        it will be added to the email.
        """
        mail_body = """<style>
                            table, th, td {
                            border:1px solid black;
                            border-collapse: collapse;
                            width: 100%;
                            table-layout: fixed}
                        </style>
                            Dear Ironic members,<br/><br/>"""
        if isinstance(nodes_and_tickets, set):
            mail_body += ('The amount of the nodes broken last week is'
                          f'more than a threshold = {TICKETS_THRESHOLD}'
                          '<br/>'
                          'Suspicious... Maybe there was an accident?'
                          '<br/>'
                          '<br/>'
                          'The nodes are:')
            for node in nodes_and_tickets:
                mail_body += f"<br/>{node}"
        elif nodes_and_tickets:
            mail_body += """
                            Please, find a list of nodes with IPMI
                            connectivity problem since the last week
                            and the tickets associated with them:
                            <br/>
                            <br/>
                            <table>
                                <tr>
                                    <th>Node name</th>
                                    <th>Ticket</th>
                                </tr>
                            """
            for node, ticket in nodes_and_tickets.items():
                mail_body += f"<tr><td>{node}</td><td>{ticket}</td></tr>"
            mail_body += "</table>"
        else:
            mail_body += """congratulations,
                             no new nodes with IPMI issues were
                              found since the last week.
                            <br/>
                        """
        if self.error_nodes:
            mail_body += """<br/>
                            <br/>
                            Encounted issues
                             while retrieving nodes information:
                            <br/>
                            <br/>
                            <table>
                                <tr>
                                    <th>Node uuid</th>
                                    <th>Error</th>
                                </tr>
                         """
            for node, error in self.error_nodes.items():
                mail_body += f"<tr><td>{node}</td><td>{error}</td></tr>"
            mail_body += "</table>"
        mail_body += "<br/><br/><br/>Have a great week!"
        logger.info("Sending email")
        mailclient = MailClient()
        mailclient.send_mail(mail_to, "Nodes with BMC connectivity issues",
                             mail_body, mail_from, '', '', 'html')

    def get_device_name(self, node_name):
        """Get device name assosiated with provided node name."""
        try:
            host_ip = socket.gethostbyname(node_name)
        except socket.gaierror:
            logger.error("Can not get node's ip: %s", node_name)
            self.error_nodes[node_name] = "Can not get node's ip"
            return node_name, None
        try:
            device_name, _, _ = socket.gethostbyaddr(host_ip)
            return node_name, re.sub(".cern.ch", "", device_name)
        except socket.gaierror:
            logger.error("Can not get node's host name: %s", host_ip)
            self.error_nodes[node_name] = "Can not get node's host name"
            return node_name, None

    def create_ticket(self, snowclient, host):
        """Open a snow ticket to Cloud 3rd Level."""
        logger.info("Opening a SNOW ticket for %s", host)
        short_description = ("BMC isn't pingable on the node {0}").format(host)
        description = ("""Dear colleagues,
                         BMC on the {0} node is not pingable.

                         Could you, please, take a look?

                         Best regards,
                         Cloud Infrastructure""").format(host)
        incident = snowclient.ticket.create_INC(
            short_description=short_description,
            description=description,
            comments=description,
            functional_element=CONF.bmc_access.functional_element,
            assignment_group=CONF.bmc_access.assignment_group
        )
        logger.info("Created incident number: %s", incident.info.number)
        incident.save()
        return incident.info.number

    @tenacity.retry(stop=tenacity.stop_after_delay(SNOW_TIMEOUT),
                    wait=tenacity.wait_fixed(SNOW_RETRY_INTERVAL))
    def check_for_ticket(self, hostname, snowclient):
        """Check if there are already open tickets regarding that device name.

        If not, create a new one.
        """
        tickets = []
        try:
            query = ('descriptionLIKE{0}^active=true^'
                     'stateNOT IN7,6').format(hostname)
            logger.info("Getting snow tickets "
                        "related to %s", hostname)
            tickets = snowclient.ticket.raw_query_incidents(query)
            if not tickets:
                new_ticket = self.create_ticket(snowclient, hostname)
                return "New ticket created " + new_ticket
            logger.info("Snow tickets already exist for %s", hostname)
            tickets_names = [ticket['number'] for ticket in tickets]
            return tickets_names
        except Exception:
            logger.info("Can not connect to snowclient")
            try:
                raise tenacity.TryAgain
            except Exception:
                logger.error("No more retries left")

    def get_tickets_and_names(self, nodes_names, snowclient):
        """Get devices names and check tickets.

        Go through the nodes, get their device name,
        check if tickets already exist, otherwise open a new one.
        """
        logger.info("Getting the device names")
        device_names = {
            node: device_name for node, device_name in map(
                self.get_device_name, nodes_names) if device_name is not None}
        nodes_and_tickets = {node:
                             self.check_for_ticket(device_name, snowclient)
                             for node, device_name in device_names.items()}
        return nodes_and_tickets

    def main(self, args=None):
        """Get broken nodes and send email."""
        args = self.parse_args(args)

        snowclient = ServiceNowClient(instance=args.instance)
        nodes_names = self.get_broken_nodes_names()
        nodes_and_tickets = {}
        if len(nodes_names) > TICKETS_THRESHOLD:
            nodes_and_tickets = nodes_names
        elif nodes_names:
            nodes_and_tickets = self.get_tickets_and_names(nodes_names,
                                                           snowclient)
        self.send_email(
            nodes_and_tickets, args.mail_to, args.mail_from)


def main(args=None):
    BmcConnectivityCheckCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
