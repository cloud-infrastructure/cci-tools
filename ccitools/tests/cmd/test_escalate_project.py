import logging

from ccitools.cmd.escalate_project import main
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.utils.cloud.cloud_config')
@mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
@mock.patch('ccitools.cmd.escalate_project.ServiceNowClient')
@mock.patch('ccitools.cmd.escalate_project.FIMClient')
class TestEscalateProject(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self, mock_fim, mock_snc, mock_cloud, mock_cc):
        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)

    def test_valid_owner_valid_dryrun(self, mock_fim, mock_snc, mock_cloud,
                                      mock_cc):
        mock_fim.return_value = mock_fim
        mock_fim.set_project_status.return_value = 0
        mock_fim.is_valid_owner.return_value = True

        mock_snc.return_value = mock_snc
        mock_snc.get_project_creation_rp.return_value = mock.Mock(
            request=mock.Mock(info=mock.Mock(u_caller_id='fake_id'))
        )
        mock_snc.user.get_user_info_by_sys_id.return_value = mock.Mock(
            u_preferred_first_name='fake_user_i'
        )

        main(
            [
                '--ticket-number', 'RQFXXXX',
                '--instance', 'cern',
                '--dryrun',
                '--fe', 'fake_felemt',
                '--assignment-group', 'fake_group',
            ]
        )

        # 3. Test no valid owner and valid group
    def test_invalid_owner_valid_dryrun(self, mock_fim, mock_snc, mock_cloud,
                                        mock_cc):
        mock_fim.return_value = mock_fim
        mock_fim.set_project_status.return_value = 0
        mock_fim.is_valid_owner.return_value = False

        mock_snc.return_value = mock_snc
        mock_snc.get_project_creation_rp.return_value = mock.Mock(
            request=mock.Mock(info=mock.Mock(u_caller_id='fake_id'))
        )
        mock_snc.user.get_user_info_by_sys_id.return_value = mock.Mock(
            u_preferred_first_name='fake_user_i'
        )

        with self.assertRaises(Exception):
            main(
                [
                    '--ticket-number', 'RQFXXXX',
                    '--instance', 'cern',
                    '--dryrun',
                    '--fe', 'fake_felemt',
                    '--assignment-group', 'fake_group',
                ]
            )
        self.assertEqual(Exception, Exception)

    def test_invalid_dryrun(self, mock_fim, mock_snc, mock_cloud, mock_cc):
        mock_cloud.return_value.is_group.return_value = False

        mock_fim.return_value = mock_fim
        mock_fim.set_project_status.return_value = 0
        mock_fim.is_valid_owner.return_value = True

        mock_snc.return_value = mock_snc
        mock_snc.get_project_creation_rp.return_value = mock.Mock(
            request=mock.Mock(info=mock.Mock(u_caller_id='fake_id'))
        )
        mock_snc.user.get_user_info_by_sys_id.return_value = mock.Mock(
            u_preferred_first_name='fake_user_i'
        )

        with self.assertRaises(Exception):
            main(
                [
                    '--ticket-number', 'RQFXXXX',
                    '--instance', 'cern',
                    '--dryrun',
                    '--fe', 'fake_felemt',
                    '--assignment-group', 'fake_group',
                ]
            )
        self.assertEqual(Exception, Exception)

    def test_valid_owner_valid_perform(self, mock_fim, mock_snc, mock_cloud,
                                       mock_cc):
        mock_cloud.return_value.is_group.return_value = True

        mock_fim.return_value = mock_fim
        mock_fim.set_project_status.return_value = 0
        mock_fim.is_valid_owner.return_value = True

        mock_snc.return_value = mock_snc
        mock_snc.get_project_creation_rp.return_value = mock.Mock(
            info=mock.Mock(chargegroup='cms')
        )
        mock_snc.user.get_user_info_by_sys_id.return_value = mock.Mock(
            u_preferred_first_name='fake_user_i'
        )

        main(
            [
                '--ticket-number', 'RQFXXXX',
                '--instance', 'cern',
                '--perform',
                '--fe', 'fake_felemt',
                '--assignment-group', 'fake_group',
            ]
        )

    def test_valid_data_invalid_group(self, mock_fim, mock_snc, mock_cloud,
                                      mock_cc):
        mock_cloud.return_value.is_group.return_value = True

        mock_fim.return_value = mock_fim
        mock_fim.set_project_status.return_value = 0
        mock_fim.is_valid_owner.return_value = True

        mock_snc.return_value = mock_snc
        mock_snc.get_project_creation_rp.return_value = mock.Mock(
            info=mock.Mock(chargegroup='fake_group')
        )
        mock_snc.user.get_user_info_by_sys_id.return_value = mock.Mock(
            u_preferred_first_name='fake_user_i'
        )

        main(
            [
                '--ticket-number', 'RQFXXXX',
                '--instance', 'cern',
                '--perform',
                '--fe', 'fake_felemt',
                '--assignment-group', 'fake_group',
            ]
        )
