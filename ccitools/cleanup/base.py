import abc
import logging
import re

# configure logging
logger = logging.getLogger(__name__)

RALLY_REGEX = re.compile('^rally-')
RALLY_REGEX_KEYPAIR = re.compile('^c_rally_')


class ResourceCleanup(object, metaclass=abc.ABCMeta):

    def __init__(self, name, cloud, quiet):
        self.name = name
        self.cloud = cloud
        self.quiet = quiet
        self.leftovers = self.get_pending()

    def refresh(self):
        if not self.quiet:
            self.leftovers = self.get_broken()
        else:
            self.leftovers = []

    @abc.abstractmethod
    def get_pending(self):
        """Retrieve the pending resource for that specific type."""

    @abc.abstractmethod
    def delete_leftovers_before(self):
        """Delete the pending resource before the sleep."""

    @abc.abstractmethod
    def delete_leftovers_after(self):
        """Delete the pending resource after the sleep."""

    @abc.abstractmethod
    def get_broken(self):
        """Check the state of the resource after the deletion."""

    @abc.abstractmethod
    def format_table(self):
        """Retrieve the pending resource for that specific type."""
