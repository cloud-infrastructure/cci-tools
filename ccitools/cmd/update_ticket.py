#!/usr/bin/python3

import logging
import sys

from ccitools.cmd import base
from ccitools.errors import CciSnowTicketError
from ccitools.utils.servicenowv2 import ServiceNowClient

logger = logging.getLogger(__name__)


class UpdateSNOWTicketCMD(base.BaseCMD):

    def __init__(self):
        super(UpdateSNOWTicketCMD, self).__init__(
            description="Update a ServiceNow ticket")

    def update_main(self, instance, ticket_number, comment, worknote):
        snowclient = ServiceNowClient(instance=instance)

        ticket = snowclient.ticket.get_ticket(ticket_number)
        try:
            if comment:
                ticket.add_comment(comment)
                logger.info(
                    "'%s' updated with the following comment: '%s'",
                    ticket_number, comment)
            if worknote:
                ticket.add_work_note(worknote)
                logger.info(
                    "'%s' updated with the following worknote: '%s'",
                    ticket_number, worknote)
        except CciSnowTicketError as msg:
            logger.warning(str(msg))
        ticket.save()

    def main(self, args=None):
        self.parser.add_argument("--ticket-number", required=True)
        self.parser.add_argument("--instance", default="cern",
                                 help="Service now instance")
        self.parser.add_argument("--comment", dest='comment')
        self.parser.add_argument("--worknote", dest='worknote')

        args = self.parse_args(args)
        if not (args.comment or args.worknote):
            self.parser.error('Nothing to do. Add --comment or --worknote')

        self.update_main(args.instance,
                         args.ticket_number,
                         args.comment,
                         args.worknote)


# Needs static method for setup.cfg
def main(args=None):
    UpdateSNOWTicketCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
