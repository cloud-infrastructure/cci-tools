#!/usr/bin/python3

import ccitools.conf
import json
import logging
import prettytable
import sys

from ccitools.cmd.base.rundeck import BaseRundeckCMD
from ccitools.common import normalize_hostname

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class RundeckFilterExecutionCMD(BaseRundeckCMD):

    def __init__(self):
        super(RundeckFilterExecutionCMD, self).__init__(
            description="Filter rundeck executions for given a host")

    def get_data_from_execution(self, execution, key_list):
        """Get data from the 'execution'.

        Args:
            execution (dict): job execution
            key_list (list): keys where to get the values from.
        Return:
            execution (string): values extracted from the execution dict
        """
        for key in key_list:
            execution = execution[key]
        return execution

    def normalize_hostname_list(self, hostname_list):
        """Normalize list of hostnames.

        Args:
            hostname_list (list): list of hostnames
        Return:
            hosts (list): normalized list of hostnames
        """
        hosts = []
        for hostname in hostname_list:
            hosts.append(normalize_hostname(hostname))
        return hosts

    def generate_table(self, filtered_executions, host, columns):
        """Generate table with job executions of a given host.

        Args:
            filtered_executions (list): list of executions that contain
                the specified hostname
            host (string): host name to filter by
            columns (string): table columns to display
        """
        config_keys = CONF.rundeck.execution_fields
        columns = columns.split(',')
        table = prettytable.PrettyTable(columns)
        table.border = True
        table.header = True
        table.format = True
        # HTML style to apply to the table
        attrs = {'class': 'table table-bordered'}
        execution_keys = []
        for col in columns:
            table.align[col] = 'c'
            if col in config_keys:
                execution_keys.append(config_keys[col])

        for execution in filtered_executions:
            row = []
            # We check the host column apart because we already
            # have the filtered host name and otherwise, we would need to
            # access the fields in the execution and check the three
            # different names for the host (hosts, instances and hostname)
            if 'Host' in columns:
                row.append(host)
            for key_list in execution_keys:
                row.append(self.get_data_from_execution(execution, key_list))
            table.add_row(row)
        # Print table in HTML
        print(table.get_html_string(attributes=attrs))

    def filter_executions(self, rdeckclient, project, host, interval,
                          columns):
        """Filter executions that contain a given host.

        Args:
            rdeckclient (_type_): Rundeck API Client
            project (string): project name where to get executions
            host (string): host name to filter by
            interval (string): period of time to filter
            columns (string): table columns to display
        """
        try:
            query = {'recentFilter': interval}
            executions = json.loads(rdeckclient.project_executions(
                project, query))['executions']
            filtered_executions = []
            host = normalize_hostname(host)
            for ex in executions:
                # In the jobs options we name the host as:
                # Hosts, instances and hostname.
                # Therefore we have to check the three cases.
                if 'hosts' in ex['job']['options']:
                    if host in self.normalize_hostname_list(
                        ex['job']['options']['hosts'].split()
                    ):
                        ex['job']['options']['hosts'] = host
                        filtered_executions.append(ex)
                elif 'instances' in ex['job']['options']:
                    if host in self.normalize_hostname_list(
                        ex['job']['options']['instances'].split()
                    ):
                        ex['job']['options']['instances'] = host
                        filtered_executions.append(ex)
                elif 'hostname' in ex['job']['options']:
                    if host in self.normalize_hostname_list(
                        ex['job']['options']['hostname'].split()
                    ):
                        ex['job']['options']['hostname'] = host
                        filtered_executions.append(ex)

            if filtered_executions:
                self.generate_table(filtered_executions, host, columns)
            else:
                logger.info("No executions found for host '%s'" % host)
        except Exception as ex:
            logger.error("Exception: %s" % ex)

    def main(self, args=None):
        self.parser.add_argument("--project", required=True,
                                 help="Project from which we want \
                                       to obtain the executions")
        self.parser.add_argument("--host_filter", required=True,
                                 help="Host to filter by")
        self.parser.add_argument("--interval", required=True,
                                 help="Format 'XY' where X is an integer, \
                                       and 'Y' 'h' (hour),'d' (day), \
                                       w' (week),'m' (month), \
                                       or 'y' (year). \
                                       Returns job executions that completed \
                                       within a period of time.")
        self.parser.add_argument("--columns", required=True,
                                 help="Table columns to display")

        args = self.parse_args(args)

        self.filter_executions(
            self.rundeckcli,
            args.project,
            args.host_filter,
            args.interval,
            args.columns
        )


# Needs static method for setup.cfg
def main(args=None):
    RundeckFilterExecutionCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
