import logging

from ccitools.cmd.base.cloud import BaseCloudCMD
from unittest import mock
from unittest import TestCase


class TestBaseCloudCMD(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch('ccitools.utils.cloud.cloud_config')
    def test_default_args(self, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value

        BaseCloudCMD('fake').parse_args([])

        mock_config.get_one_cloud.assert_called_with(
            cloud='cern',
            region_name=None,
            argparse=None
        )

    @mock.patch('ccitools.utils.cloud.cloud_config')
    def test_default_args_raise_IOError(self, mock_cc):
        mock_cc.OpenStackConfig.side_effect = (
            iter([IOError])
        )

        with self.assertRaises(IOError):
            BaseCloudCMD('fake').parse_args([])

        mock_cc.OpenStackConfig.assert_called_with()

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    def test_default_args_auth_succeed_no_cloud(self, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value

        BaseCloudCMD('fake').parse_args([])

        mock_config.get_one_cloud.assert_called_with(
            cloud='cern',
            region_name=None,
            argparse=None
        )

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    def test_default_args_auth_succeed_with_cloud(self, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value

        BaseCloudCMD('fake').parse_args(['--os-cloud', 'fake'])

        mock_config.get_one_cloud.assert_called_with(
            cloud='fake',
            region_name=None,
            argparse=None
        )

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    def test_default_args_auth_succeed_with_cloud_region(self, mock_ks,
                                                         mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value

        BaseCloudCMD('fake').parse_args(
            ['--os-cloud', 'fake', '--os-region-name', 'fake_region'])

        mock_config.get_one_cloud.assert_called_with(
            cloud='fake',
            region_name='fake_region',
            argparse=None
        )

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    def test_default_args_with_debug(self, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value

        BaseCloudCMD('fake').parse_args([
            '--os-cloud', 'fake', '-d'
        ])

        mock_config.get_one_cloud.assert_called_with(
            cloud='fake',
            region_name=None,
            argparse=None
        )

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    def test_default_args_with_verbose_level_1(self, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value

        BaseCloudCMD('fake').parse_args([
            '--os-cloud', 'fake', '-v'
        ])

        mock_config.get_one_cloud.assert_called_with(
            cloud='fake',
            region_name=None,
            argparse=None
        )

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    def test_default_args_with_verbose_level_2(self, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value

        BaseCloudCMD('fake').parse_args([
            '--os-cloud', 'fake', '-vv'
        ])

        mock_config.get_one_cloud.assert_called_with(
            cloud='fake',
            region_name=None,
            argparse=None
        )

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    def test_default_args_with_verbose_level_3(self, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value

        BaseCloudCMD('fake').parse_args([
            '--os-cloud', 'fake', '-vvv'
        ])

        mock_config.get_one_cloud.assert_called_with(
            cloud='fake',
            region_name=None,
            argparse=None
        )
