import logging

from ccitools.verify.yum import BrokenReposAction
from ccitools.verify.yum import DistroSyncAction
from ccitools.verify.yum import UnfinishedTransactionsAction
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.verify.yum.ssh_executor')
@mock.patch('ccitools.verify.base.ssh_executor')
class TestDistroSyncAction(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.client = DistroSyncAction(host='fake.domain')

    def test_try_fix(self, mock_common, mock_yum):
        self.client.try_fix()

        mock_yum.assert_has_calls(
            [
                mock.call(
                    'fake.domain',
                    '/usr/sbin/logrotate -f /etc/logrotate.conf'
                ),
                mock.call(
                    'fake.domain',
                    '/usr/bin/sh /usr/local/sbin/distro_sync.sh'
                ),
            ]
        )

    def test_check_alarm_state(self, mock_common, mock_yum):
        mock_common.return_value = [], None
        self.assertFalse(self.client.check_alarm_state())

        mock_common.return_value = (
            ['fake.domain/another_alarm'], None)
        self.assertFalse(self.client.check_alarm_state())

        mock_common.return_value = (
            ['fake.domain/tail-distro_sync/count-yum_error'], None)
        self.assertTrue(self.client.check_alarm_state())
        self.assertEqual(
            ' - fake.domain\n',
            self.client.get_reason())


@mock.patch('ccitools.verify.yum.ssh_executor')
@mock.patch('ccitools.verify.base.ssh_executor')
class TestUnfinishedTransactionsAction(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.client = UnfinishedTransactionsAction(host='fake.domain')

    def test_try_fix(self, mock_common, mock_yum):
        self.client.try_fix()

        mock_yum.assert_has_calls(
            [
                mock.call(
                    'fake.domain',
                    '/usr/sbin/yum-complete-transaction -y'
                )
            ]
        )

    def test_check_alarm_state(self, mock_common, mock_yum):
        mock_common.return_value = [], None
        self.assertFalse(self.client.check_alarm_state())

        mock_common.return_value = (
            ['fake.domain/another_alarm'], None)
        self.assertFalse(self.client.check_alarm_state())

        mock_common.return_value = (
            ['fake.domain/yum-unfinished_transactions/gauge'], None)
        self.assertTrue(self.client.check_alarm_state())
        self.assertEqual(
            ' - fake.domain\n',
            self.client.get_reason())


@mock.patch('ccitools.verify.yum.ssh_executor')
@mock.patch('ccitools.verify.base.ssh_executor')
class TestBrokenReposAction(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.client = BrokenReposAction(host='fake.domain')

    def test_try_fix(self, mock_common, mock_yum):
        self.client.try_fix()

    def test_check_alarm_state(self, mock_common, mock_yum):
        mock_common.return_value = [], None
        self.assertFalse(self.client.check_alarm_state())

        mock_common.return_value = (
            ['fake.domain/another_alarm'], None)
        self.assertFalse(self.client.check_alarm_state())

        mock_common.return_value = (
            ['fake.domain/yum-broken_repos/gauge'], None)
        self.assertTrue(self.client.check_alarm_state())
        self.assertEqual(
            ' - fake.domain\n',
            self.client.get_reason())
