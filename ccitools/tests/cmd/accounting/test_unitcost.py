import logging
import requests_mock

from ccitools.cmd.accounting.unitcost import main
from ccitools.tests.cmd import fixtures
from ccitools.tests import fixtures as base_fixtures
from ccitools.tests.utils import fixtures as utils_fixtures
from unittest import mock
from unittest import TestCase


class TestAccountingUnitCost(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @requests_mock.Mocker()
    def test_dryrun_empty(self, mock_cloud, mock_req):
        mock_cli = mock_cloud.return_value

        mock_cli.get_volume_types.return_value = []
        mock_cli.get_share_types.return_value = []

        main(['--dryrun'])

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @requests_mock.Mocker()
    def test_dryrun_non_callable(self, mock_cloud, mock_req):
        mock_cli = mock_cloud.return_value

        mock_cli.get_volume_types = mock.NonCallableMock()
        mock_cli.get_share_types = mock.NonCallableMock()

        main(['--dryrun'])

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @requests_mock.Mocker()
    def test_dryrun(self, mock_cloud, mock_req):
        mock_cli = mock_cloud.return_value

        mock_cli.get_volume_types.return_value = [
            base_fixtures.Resource(utils_fixtures.VOLUME_TYPE_PUBLIC),
            base_fixtures.Resource(utils_fixtures.VOLUME_TYPE_PRIVATE),
            base_fixtures.Resource(utils_fixtures.VOLUME_TYPE_NO_DESCRIPTION),
            base_fixtures.Resource(
                utils_fixtures.VOLUME_TYPE_EMPTY_DESCRIPTION)
        ]
        mock_cli.get_share_types.return_value = [
            base_fixtures.Resource(utils_fixtures.SHARE_TYPE_PUBLIC),
            base_fixtures.Resource(utils_fixtures.SHARE_TYPE_PRIVATE),
            base_fixtures.Resource(utils_fixtures.SHARE_TYPE_NO_DESCRIPTION),
            base_fixtures.Resource(utils_fixtures.SHARE_TYPE_EMPTY_DESCRIPTION)
        ]

        main(['--dryrun'])

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @requests_mock.Mocker()
    def test_default_args(self, mock_cloud, mock_req):
        mock_cli = mock_cloud.return_value

        mock_cli.get_volume_types.return_value = [
            base_fixtures.Resource(utils_fixtures.VOLUME_TYPE_PUBLIC),
            base_fixtures.Resource(utils_fixtures.VOLUME_TYPE_PRIVATE),
            base_fixtures.Resource(utils_fixtures.VOLUME_TYPE_NO_DESCRIPTION),
            base_fixtures.Resource(
                utils_fixtures.VOLUME_TYPE_EMPTY_DESCRIPTION)
        ]
        mock_cli.get_share_types.return_value = [
            base_fixtures.Resource(utils_fixtures.SHARE_TYPE_PUBLIC),
            base_fixtures.Resource(utils_fixtures.SHARE_TYPE_PRIVATE),
            base_fixtures.Resource(utils_fixtures.SHARE_TYPE_NO_DESCRIPTION),
            base_fixtures.Resource(utils_fixtures.SHARE_TYPE_EMPTY_DESCRIPTION)
        ]

        mock_req.post(
            'https://accounting-receiver.cern.ch/v3/unit_cost',
            text=fixtures.ACCOUNTING_RECEIVER_RESPONSE,
            status_code=200
        )

        main([])

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @requests_mock.Mocker()
    def test_default_args_error(self, mock_cloud, mock_req):
        mock_cli = mock_cloud.return_value

        mock_cli.get_volume_types.return_value = [
            base_fixtures.Resource(utils_fixtures.VOLUME_TYPE_PUBLIC),
            base_fixtures.Resource(utils_fixtures.VOLUME_TYPE_PRIVATE),
            base_fixtures.Resource(utils_fixtures.VOLUME_TYPE_NO_DESCRIPTION),
            base_fixtures.Resource(
                utils_fixtures.VOLUME_TYPE_EMPTY_DESCRIPTION)
        ]
        mock_cli.get_share_types.return_value = [
            base_fixtures.Resource(utils_fixtures.SHARE_TYPE_PUBLIC),
            base_fixtures.Resource(utils_fixtures.SHARE_TYPE_PRIVATE),
            base_fixtures.Resource(utils_fixtures.SHARE_TYPE_NO_DESCRIPTION),
            base_fixtures.Resource(utils_fixtures.SHARE_TYPE_EMPTY_DESCRIPTION)
        ]

        mock_req.post(
            'https://accounting-receiver.cern.ch/v3/unit_cost',
            text=fixtures.ACCOUNTING_RECEIVER_RESPONSE,
            status_code=400
        )

        main([])
