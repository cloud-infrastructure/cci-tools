#!/usr/bin/python3

import logging
import sys


from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools import common

# configure logging
logger = logging.getLogger(__name__)


class AccountingSync(BaseCloudCMD):

    def __init__(self):
        super(AccountingSync, self).__init__(
            description=("Sync chargegroups on personal projects"))

    def main(self, args):
        args = self.parse_args(args)
        user_mapping = {
            k: v['charge_group'] for k, v in common.get_user_mapping().items()
        }
        logger.info("Retrieved {0} users".format(len(user_mapping)))

        chargegroups = {
            ch['name']: ch['uuid'] for ch in common.get_chargegroups()}
        logger.info("Retrieved {0} chargegroups".format(len(chargegroups)))

        personal_projects = self.cloudclient.list_projects(
            tags_any=['expiration'])
        logger.info("Retrieved {0} personal projects".format(
            len(personal_projects)))

        self.sync_chargegroups(user_mapping, chargegroups, personal_projects)

    def sync_chargegroups(self, user_mapping, chargegroups, personal_projects):
        """Update the information in Keystone with the chargegroup data.

        It will iterate over all the personal projects, and if the
        charge group doesn't match the current information on the accounting
        service will be updated to match it

        :args user_mapping: Users and the chargegroup that must be set
        :args chargegroups: Mapping to resolve chargegroup names to uuids
        :args personal_projects: List of personal projects
        """
        for project in personal_projects:
            chargegroup = (project.chargegroup
                           if hasattr(project, 'chargegroup')
                           else None)
            username = project.name.split()[1]

            try:
                new_chargegroup = chargegroups[user_mapping[username]]
            except KeyError:
                logger.info("This user called {0} is not found. "
                            "Openstack project is: {1}".format(username,
                                                               project.name))
                # In case we don't find it we keep the previous chargegroup
                new_chargegroup = chargegroup

            if new_chargegroup != chargegroup:
                # In this case, we need to the update the project information
                logger.info("Updating {0} from {1} to {2}".format(
                    project.name,
                    chargegroup,
                    new_chargegroup))

                self.cloudclient.update_project(
                    project=project,
                    chargegroup=new_chargegroup)


# Needs static method for setup.cfg
def main(args=None):
    AccountingSync().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)
