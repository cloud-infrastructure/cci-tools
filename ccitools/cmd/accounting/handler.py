import ccitools.conf
import types

from collections.abc import MutableMapping
from collections import defaultdict

CONF = ccitools.conf.CONF


def merge_dicts(dict_list):
    """Merge a list of dictionaries recursively.

    Args:
        dict_list: list of dictionaries to be merged.

    Return:
        Result dictionary after merging all the dicts. in dict_list.
    """
    def _recursive_merge(d1, d2):
        for k, v in d1.items():
            if k in d2:
                if all(isinstance(e, MutableMapping) for e in (v, d2[k])):
                    d2[k] = _recursive_merge(v, d2[k])
        d3 = d1.copy()
        d3.update(d2)
        return d3

    out = defaultdict(lambda: defaultdict(dict))
    for elem in dict_list:
        for key, subdict in elem.items():
            out[key] = dict(_recursive_merge(out[key], subdict))
    return dict(out)


def run_multiple_queries(self):
    """Run the list of queries, merges their outputs and flattens its result.

    Returns:
        Output of the list of queries.
    """
    results = [self.run_query(query, self.keys) for query in self.queries]
    merged_result = merge_dicts(results)
    return self.flatten_result(merged_result)


def run_single_query(self):
    """Run the query and flatten its output.

    Returns:
        Output of the query.
    """
    result = self.run_query(self.queries, self.keys)
    return self.flatten_result(result)


def format_multiple_keys(query_result, key_list, tables_to_titles):
    """Convert multiple keys from influx.

    Turns the output of the query to InfluxDB into a dictionary in order to be
    written into a JSON file. It deletes the 'time' attribute. Example:

    TABLES_TO_TITLES: {'table1': 't1', ...}

    query_result:
        {
            ('table1', ['v1_key1', v1_key2]): [{'attr1': 'v1', 'time': X},
                                               {'attr1': 'v2', 'time': Y}],
            ('table1', ['v2_key1', v2_key2]): ...,
            ...
        }

    output:
        {
            ('v1_key1', v1_key2): {'t1': [{'attr1': 'v1'}, {'attr1': 'v2'}]},
            ('v2_key1', v2_key2): {'t1': [...]},
            ...
        }

    Args:
        query_result: result of the InfluxDB query
        key_name: name of the key

    Returns:
        Dictionary with the result of the query.
    """
    out = defaultdict(dict)
    for (table, keys), values_generator in query_result.items():
        key = tuple(keys[k] for k in key_list)
        title = tables_to_titles[table]
        for values in values_generator:
            del values['time']
            out[key][title] = values
    return dict(out)


def format_single_key(query_result, key_name, tables_to_titles):
    """Convert influx output into dictionary.

    Turns the output of the query to InfluxDB into a dictionary in order to be
    written into a JSON file. It deletes the 'time' attribute. Example:

    TABLES_TO_TITLES: {'table1': 't1', ...}

    query_result:
        {
            ('table1', ['v1_key1']): [{'attr1': 'v1', 'time': X},
                                      {'attr1': 'v2', 'time': Y}],
            ('table1', ['v2_key1']): ...,
            ...
        }

    output:
        {
            'v1_key1': {'t1': [{'attr1': 'v1'}, {'attr1': 'v2'}]},
            'v2_key1': {'t1': [...]},
            ...
        }

    Args:
        query_result: result of the InfluxDB query
        key_name: name of the key

    Returns:
        Dictionary with the result of the query.
    """
    out = defaultdict(dict)
    for (table, keys), values_generator in query_result.items():
        key = keys[key_name]
        title = tables_to_titles[table]
        for values in values_generator:
            del values['time']
            out[key][title] = values
    return dict(out)


def flatten_multiple_keys(self, result):
    """Flatten the dictionary by multiple keys.

    Turns the dictionary of dictionaries for each key
    combination into a list of dictionaries. Example:

    keys: ['key1', 'key2']
    result:
        {
            ('a', 1): { 'attr1': 'v1', 'attr2': 'v1' },
            ('a', 2): { 'attr1': 'v2', 'attr2': 'v2' },
            ...
        }
    output:
        [
            {'keys': {'key1': 'a', 'key2': 1}, 'attr1': 'v1', 'attr2': 'v1'},
            {'keys': {'key1': 'a', 'key2': 2}, 'attr1': 'v2', 'attr2': 'v2'},
            ...
        ]

    Args:
        self
        result: dictionary of dictionaries for each key.

    Returns:
        List of dictionaries with the key inside it.
    """
    out = []
    for keys, values in result.items():
        values['keys'] = {}
        for index, key in enumerate(keys):
            key_name = self.keys[index]
            values['keys'][key_name] = key
        out.append(values)
    return out


def flatten_single_key(self, result):
    """Flatten the dictionary by key.

    Turns the dictionary of dictionaries for each key into
    a list of dictionaries. Example:

    keys: 'key1'
    result:
        {
            'a': { 'attr1': 'v1', 'attr2': 'v1' },
            'b': { 'attr1': 'v2', 'attr2': 'v2' },
            ...
        }
    output:
        [
            {'key1': 'a', 'attr1': 'v1', 'attr2': 'v1'},
            {'key1': 'b', 'attr1': 'v2', 'attr2': 'v2'},
            ...
        ]


    Args:
        self
        result: dictionary of dictionaries for each key.

    Returns:
        List of dictionaries with the key inside it.
    """
    out = []
    for key, values in result.items():
        values[self.keys] = key
        out.append(values)
    return out


class QueryHandler(object):
    """Run the queries.

    Attributes:
        influx_client: Client to make the queries.
        query_list: List of queries.
        key_list: List of key attributes.

    """

    def __init__(self, influx_client, query_list, key_list, tables_to_titles):
        self.influx = influx_client
        self.tables_to_titles = tables_to_titles

        if len(key_list) > 1:
            self.keys = key_list
            self.format = format_multiple_keys
            self.flatten_result = types.MethodType(flatten_multiple_keys, self)
        else:
            self.keys = key_list[0]
            self.format = format_single_key
            self.flatten_result = types.MethodType(flatten_single_key, self)

        if len(query_list) > 1:
            self.queries = query_list
            self.run = types.MethodType(run_multiple_queries, self)
        else:
            self.queries = query_list[0]
            self.run = types.MethodType(run_single_query, self)

    def run_query(self, query, key):
        """Run a query and formats its output.

        Args:
            query: InfluxDB query.
            key: Key attributes.

        Returns:
            Formatted output of the query.
        """
        query_result = self.influx.query(query)
        return self.format(query_result, key, self.tables_to_titles)
