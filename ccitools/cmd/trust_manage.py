#!/usr/bin/python3

import ccitools.conf
import datetime
import logging
import sqlalchemy
import sqlalchemy.orm
import stevedore
import sys

from ccitools.cmd import base
from ccitools.common import env
from ccitools.utils.cloud import CloudRegionClient
from itertools import compress
from keystoneclient.v3 import client as keystone_client
from osc_lib import utils

# configure logging
logger = logging.getLogger(__name__)
Base = sqlalchemy.orm.declarative_base()
CONF = ccitools.conf.CONF

# formatters
_formatter_namespace = 'cliff.formatter.list'
_formatter_default = 'table'


def _load_formatter_plugins():
    # Here so tests can override
    return stevedore.ExtensionManager(
        _formatter_namespace,
        invoke_on_load=True, )


_formatter_plugins = _load_formatter_plugins()


class Trust(Base):
    __tablename__ = 'trust'
    id = sqlalchemy.Column(sqlalchemy.String, primary_key=True)
    trustor_user_id = sqlalchemy.Column(sqlalchemy.String)
    trustee_user_id = sqlalchemy.Column(sqlalchemy.String)
    project_id = sqlalchemy.Column(sqlalchemy.String)
    deleted_at = sqlalchemy.Column(sqlalchemy.DateTime)


class User(Base):
    __tablename__ = 'user'
    id = sqlalchemy.Column(sqlalchemy.String(64), primary_key=True)
    created_at = sqlalchemy.Column(sqlalchemy.DateTime)


class TrustManagement(base.BaseCMD):

    def __init__(self):
        super(TrustManagement, self).__init__(
            description="Manage trusts inside Keystone Database")

    def _create_session(self, connection_str):
        engine = sqlalchemy.create_engine(connection_str)
        Session = sqlalchemy.orm.sessionmaker(bind=engine)
        return Session()

    def _fetch_query(self, session, args):
        query = session.query(Trust).filter_by(deleted_at=None)
        if args.trust_id:
            query = query.filter_by(
                id=args.trust_id)
        if args.trustor:
            query = query.filter_by(
                trustor_user_id=args.trustor)
        if args.trustee:
            query = query.filter_by(
                trustee_user_id=args.trustee)
        if args.project:
            query = query.filter_by(
                project_id=args.project)
        return query

    def list_trusts(self, connection_str, args):
        session = self._create_session(connection_str)
        try:
            query = self._fetch_query(session, args)
            if query:
                trusts = [t for t in query]
                self.print_trusts(args, trusts)
        except Exception as ex:
            logger.error("There was a problem: %s" % ex)
        finally:
            session.close()

    def print_trusts(self, args, trusts):
        formatter = _formatter_plugins[args.formatter].obj
        # Only consider columns that have desired elements
        columns = []
        for c in trusts[0].__dict__.keys():
            if c not in ['_info', '_sa_instance_state']:
                columns.append(c)

        data = (utils.get_item_properties(s, columns, formatters={})
                for s in trusts)
        columns_to_include, selector = self._generate_columns_and_selector(
            args, columns)
        if selector:
            data = (list(compress(row, selector)) for row in data)
        formatter.emit_list(columns_to_include, data, sys.stdout, args)

    def _generate_columns_and_selector(self, args, column_names):
        if not args.columns:
            columns_to_include = column_names
            selector = None
        else:
            columns_to_include = [c for c in column_names if c in args.columns]
            if not columns_to_include:
                raise ValueError(
                    'No recognized column names in %s' % str(args.columns))
            # Set up argument to compress()
            selector = [(c in columns_to_include) for c in column_names]
        return columns_to_include, selector

    def update_trusts(self, connection_str, args):
        # Prevent update of trusts
        if (not args.trust_id and not args.trustor and not args.trustee
                and not args.project):
            logger.error("Tyring to update all trusts with the"
                         "same trustor. Exiting...")
            return

        session = self._create_session(connection_str)
        try:
            query = self._fetch_query(session, args)
            if query:
                if args.dry_run:
                    logger.info("[DRY RUN] Updating trustor '%s' to '%s'",
                                args.trustor,
                                args.new_trustor)
                else:
                    logger.info("Updating trustor '%s' to '%s'",
                                args.trustor,
                                args.new_trustor)
                    query.update({'trustor_user_id': args.new_trustor})
                    session.commit()
        except Exception as ex:
            logger.error("There was a problem: %s" % ex)

    def purge_trusts(self, connection_str, args):
        client = CloudRegionClient(cloud=args.cloud)
        kc = keystone_client.Client(session=client.session)
        session = self._create_session(connection_str)

        try:
            query = session.query(User).join(
                Trust, User.id == Trust.trustee_user_id
            ).filter(
                Trust.trustor_user_id == args.trustor
            )
            if args.after:
                query = query.filter(
                    User.created_at >= args.after
                )
            if args.before:
                query = query.filter(
                    User.created_at <= args.before
                )
            for exclude in args.exclude_user:
                query = query.filter(User.id != exclude)
            query = query.order_by(User.created_at)

            for u in query:
                logger.info("Deleting user %s created at %s",
                            u.id, u.created_at)
                if args.execute:
                    kc.users.delete(u.id)
        except Exception as ex:
            logger.error("There was a problem: %s" % ex)
        finally:
            session.close()

    def main(self, args):
        # Calculate default for before
        default_before = datetime.datetime.now() - datetime.timedelta(hours=4)

        self.parser.add_argument(
            "--trust-id",
            help="Trust ID of the trust")

        self.parser.add_argument(
            "--trustor",
            help="Trustor that creates the trust")

        self.parser.add_argument(
            "--trustee",
            help="Trustee that uses the trust")

        self.parser.add_argument(
            "--project",
            help="Project ID for the trust")

        subparsers = self.parser.add_subparsers(metavar='<subcommand>')

        parser_list = subparsers.add_parser(
            'list',
            help="Lists the trusts with the filters "
                 "passed as parameters.")
        parser_list.set_defaults(func=self.list_trusts)

        parser_purge = subparsers.add_parser(
            'purge',
            help="Remove trusts that are no longer used")
        parser_purge.add_argument(
            "--after",
            default='',
            help="Date and time of the trusts to be purged.")
        parser_purge.add_argument(
            "--before",
            default=default_before.strftime('%Y-%m-%d %H:%M:%S'),
            help="Date and time of the trusts to be purged.")
        parser_purge.add_argument(
            '--exclude-user',
            action='append',
            help='Exclude a user from user list',
            default=['33612461b088461f87e6e004c6273036', 'svcheat'])
        parser_purge.add_argument(
            '--os-cloud',
            metavar='<cloud-config-name>',
            dest='cloud',
            default=env('OS_CLOUD', default='cern'),
            help='Cloud name in clouds.yaml (Env: OS_CLOUD)', )
        parser_purge.add_argument(
            '--execute',
            action='store_true',
            help='execute the operation')
        parser_purge.set_defaults(func=self.purge_trusts)

        parser_update = subparsers.add_parser(
            'update',
            help="Updates the trusts with the filters "
                 "passed as parameters.")
        parser_update.add_argument(
            '--dry-run',
            default=False,
            action='store_true',
            help="Do not execute the commands, "
                 "only print them")
        parser_update.add_argument(
            "--new-trustor",
            required=True,
            help="New Trustor that replaces the original one in the trust")

        parser_update.set_defaults(func=self.update_trusts)

        formatter_group = self.parser.add_argument_group(
            title='output formatters',
            description='output formatter options', )
        formatter_choices = sorted(_formatter_plugins.names())
        formatter_default = _formatter_default
        if formatter_default not in formatter_choices:
            formatter_default = formatter_choices[0]
        formatter_group.add_argument(
            '-f',
            '--format',
            dest='formatter',
            action='store',
            choices=formatter_choices,
            default=formatter_default,
            help='the output format, defaults to %s' % formatter_default, )
        formatter_group.add_argument(
            '-c',
            '--column',
            action='append',
            default=[],
            dest='columns',
            metavar='COLUMN',
            help='specify the column(s) to include, can be repeated', )
        for formatter in _formatter_plugins:
            formatter.obj.add_argument_group(self.parser)

        args = self.parse_args(args)

        try:
            call = args.func
        except AttributeError:
            self.parser.error("Too few arguments")

        call(CONF.keystone.connection, args)


# Needs static method for setup.cfg
def main(args=None):
    TrustManagement().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)
