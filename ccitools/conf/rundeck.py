from oslo_config import cfg


alias = cfg.StrOpt(
    'alias',
    default='cci-rundeck.cern.ch',
    help="alias for rundeck")

api_token = cfg.StrOpt(
    'api_token',
    default='secret_token',
    help="secret token for rundeck")

api_version = cfg.IntOpt(
    'api_version',
    default=20,
    help='API version for rundeck')

connection = cfg.StrOpt(
    'connection',
    default='mysql+pymysql://user:pass@127.0.0.1:3306/rundeck',
    help="mysql connection url")

port = cfg.StrOpt(
    'port',
    default='443',
    help='port for rundeck')

verify = cfg.BoolOpt(
    'verify',
    default=False,
    help='verify SSL connection')

interventions = cfg.ListOpt(
    'interventions',
    default=[
        'c58c7515-5938-42aa-8b89-32f194c62bd0',
        '99ba923e-7b5b-4b3f-bb37-3c0d4240ebe8',
        '30b36b76-ed8f-4d7a-a740-c0ad0b25c155'
    ],
    help="UUIDs for the interventions to look for in interventions manager")

execution_fields = cfg.DictOpt(
    'execution_fields',
    default={
        "Job": ['job', 'name'],
        "Status": ['status'],
        "Started": ['date-started', 'date'],
        "Finished": ['date-ended', 'date'],
        "Username": ['user'],
        "Details": ['permalink']
    }
)

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    alias,
    api_token,
    api_version,
    connection,
    port,
    verify,
    interventions,
    execution_fields
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
