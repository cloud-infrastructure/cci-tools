#!/usr/bin/python3

import logging
import os
import sys

from ccitools.cmd.base.cloud import BaseSDKCMD
import ccitools.conf
from glanceclient import exc as glance_exceptions
from oslo_serialization import jsonutils
from oslo_utils import uuidutils

# configure logging
logger = logging.getLogger(__name__)

CONF = ccitools.conf.CONF

IMAGE_ALREADY_PRESENT_MESSAGE = ('The image %s is already present on '
                                 'the target, but our check for it did '
                                 'not find it. This indicates that we '
                                 'do not have permissions to see all '
                                 'the images on the target server.')


def _human_readable_size(num, suffix='B'):
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
        if abs(num) < 1024.0:
            return "%3.1f %s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f %s%s" % (num, 'Yi', suffix)


def _remove_empty_values(d):
    return {k: v for k, v in d.items() if v}


def _remove_unwanted(source, args):
    for key in args.dontreplicate:
        if key in source:
            logger.debug(
                'Stripping %s from source metadata', key)
            del source[key]
    for key in args.removeproperties:
        if key in source['properties']:
            logger.debug(
                'Stripping %s from source properties', key)
            del source['properties'][key]
    return source


def _dict_diff(a, b):
    # Only things the source has which the target lacks matter
    if set(a.keys()) - set(b.keys()):
        logger.info('metadata diff -- source has extra keys: %s',
                    ' '.join(set(a.keys()) - set(b.keys())))
        return True

    for key in a:
        if type(a[key]) is not type(b[key]):
            logger.info("Data type differs for key %s", key)
            return True
        # In case we have a dict, we must check recursively
        if isinstance(a[key], dict):
            logger.debug("Recursively checking dict %s", key)
            if _dict_diff(a[key], b[key]):
                return True
            continue
        else:
            if str(a[key]) != str(b[key]):
                logger.info(
                    'metadata diff -- value differs for key '
                    '%s: source "%s" vs target "%s"',
                    key,
                    a[key],
                    b[key])
                return True

    return False


class ReplicatorImagesCMD(BaseSDKCMD):
    def __init__(self):
        super(ReplicatorImagesCMD, self).__init__(
            description="Replicate Images between regions")

    def is_image_present(self, image_uuid, region):
        return self.client.get_image(
            image_uuid, region=region)

    def get_projects_to_sync(self):
        return self.client.list_projects(
            any_tags=['imagesync'])

    def replication_size(self, args):
        total_size = 0
        count = 0
        logger.info("Retrieving projects")
        projects = self.get_projects_to_sync()
        logger.info("Found %d projects", len(projects))

        for project in projects:
            logger.info('Processing %s', project.name)
            for image in self.client.get_images_by_project_id(
                    region=args.source,
                    project_id=project.id,
                    visibility='all',
                    tag=['imagesync']):
                logger.info(
                    '[%s] Considering image: %s',
                    project.name, image.id)
                if image['status'] == 'active':
                    total_size += int(image['size'])
                    count += 1

            logger.info(
                '[%s] Total size is %d bytes '
                '(%s) across %d images',
                project.name,
                total_size,
                _human_readable_size(total_size),
                count)

    def replication_dump(self, args):
        logger.info("Retrieving projects")
        projects = self.get_projects_to_sync()
        logger.info("Found %d projects", len(projects))

        for project in projects:
            logger.info('Processing %s', project.name)
            for image in self.client.get_images_by_project_id(
                    region=args.source,
                    project_id=project.id,
                    visibility='all',
                    tag=['imagesync']):
                logger.info(
                    '[%s] Considering: %s (%s) (%d bytes)',
                    project.name,
                    image['id'],
                    image.get('name', '--unnamed--'),
                    image['size'])

                data_path = os.path.join(args.path, project.id)
                if not os.path.exists(data_path):
                    os.makedirs(data_path)
                meta_file_name = os.path.join(data_path, image['id'])
                data_file_name = meta_file_name + '.img'

                logger.info(
                    '[%s] Storing: %s (%s) (%d bytes) in %s',
                    project.name,
                    image['id'],
                    image.get('name', '--unnamed--'),
                    image['size'],
                    data_file_name)

                # Dump glance information
                with open(meta_file_name, 'w', encoding='utf-8') as f:
                    f.write(jsonutils.dumps(_remove_empty_values(image)))

                if image['status'] == 'active' and not args.metaonly:
                    # Now fetch the image. The metadata returned in headers
                    # here is the same as that which we got from the
                    # detailed images request earlier, so we can ignore it
                    # here. Note that we also only dump active images.
                    logger.debug('Image %s is active', image['id'])
                    image_response = self.client.download_image(
                        image['id'],
                        args.source)
                    with open(data_file_name, 'wb') as f:
                        for chunk in image_response:
                            f.write(chunk)

    def replication_load(self, args):
        logger.info("Retrieving projects")
        projects = self.get_projects_to_sync()
        logger.info("Found %d projects", len(projects))
        updated = []

        for project in projects:
            logger.info('Processing %s', project.name)
            data_path = os.path.join(args.path, project.id)

            entities = [s for s in os.listdir(data_path)
                        if not s.endswith('.img')]
            for ent in entities:
                if uuidutils.is_uuid_like(ent):
                    image_uuid = ent
                logger.info('[%s] Considering: %s', project.name, image_uuid)

                meta_file_name = os.path.join(data_path, image_uuid)
                data_file_name = meta_file_name + '.img'
                with open(meta_file_name) as meta_file:
                    meta = jsonutils.loads(meta_file.read())

                # Remove keys which don't make sense for replication
                for key in args.dontreplicate:
                    if key in meta:
                        logger.debug('Stripping %s from saved metadata', key)
                        del meta[key]

                if self.is_image_present(image_uuid, args.target):
                    # NOTE(mikal): Perhaps we just need to update the
                    # metadata? Note that we don't attempt to change an
                    # image file once it has been uploaded.
                    logger.debug('Image %s already present', image_uuid)
                    headers = _remove_empty_values(
                        self.client.get_image(image_uuid, args.target))
                    stripped = _remove_unwanted(headers, args)
                    if _dict_diff(meta, stripped):
                        logger.info('[%s] Image %s metadata has changed',
                                    project.name, image_uuid)
                        if args.dryrun:
                            logger.info('Dryrun: Would update image id='
                                        '%s\nSource: %s\n\nTarget" %s' %
                                        (image_uuid, stripped, meta))
                        else:
                            self.client.update_image(
                                image_uuid, args.target, meta)
                            updated.append(meta['id'])
                else:
                    if not os.path.exists(data_file_name):
                        logger.debug(
                            '%s dump is missing image data, skipping',
                            image_uuid)
                        continue
                    stripped = _remove_unwanted(meta, args)
                    # Upload the image itself
                    with open(os.path.join(data_file_name), 'rb') as img_file:
                        if args.dryrun:
                            logger.info('Dryrun: Would create image id='
                                        '%s with properties %s' %
                                        (image_uuid, stripped))
                        else:
                            try:
                                kwargs = stripped
                                kwargs['data'] = img_file
                                self.client.create_image(
                                    args.target, **kwargs)
                            except glance_exceptions.HTTPConflict:
                                logger.error(IMAGE_ALREADY_PRESENT_MESSAGE,
                                             image_uuid)
        return updated

    def replication_livecopy(self, args):
        logger.info("Will replicate a max of %d images in this run",
                    int(args.maxnum))
        logger.info("Retrieving projects")
        projects = self.get_projects_to_sync()
        logger.info("Found %d projects", len(projects))
        for project in projects:
            logger.info("    - %s", project.name)
        updated = []
        nprocessed = 0
        for project in projects:
            if args.project != project.name and args.project != 'all':
                logger.info('Skipping %s', project.name)
                continue
            logger.info('Processing %s', project.name)
            for image in self.client.get_images_by_project_id(
                    region=args.source,
                    project_id=project.id,
                    visibility='all',
                    tag=['imagesync']):
                logger.info('Considering %s', image['id'])
                source = _remove_unwanted(_remove_empty_values(image), args)
                if self.is_image_present(image['id'], args.target):
                    # NOTE(mikal): Perhaps we just need to update the
                    # metadata? Note that we don't attempt to change an
                    # image file once it has been uploaded.
                    target = self.client.get_image(image['id'], args.target)
                    headers = _remove_empty_values(target)
                    if headers['status'] == 'active':
                        stripped = _remove_unwanted(headers, args)
                        if _dict_diff(source, stripped):
                            logger.info('[%s] Image %s metadata has changed',
                                        project.name, image['id'])
                            if args.dryrun:
                                logger.info('Dryrun: Would patch id=%s: '
                                            '\nSource: %s\n Target: %s\n"' %
                                            (image['id'], source, stripped))
                            else:
                                self.client.update_image(target,
                                                         args.target, source)
                                updated.append(image['id'])

                elif image['status'] == 'active':
                    if nprocessed >= int(args.maxnum):
                        logging.info("Skip: max number of images reached.")
                        continue
                    nprocessed = nprocessed + 1
                    logger.info('[%s] Image %s (%s) (%d bytes) '
                                'is being synced',
                                project.name,
                                image.get('name', '--unnamed--'),
                                image['id'],
                                image['size'])
                    if not args.metaonly:
                        headers = _remove_empty_values(
                            self.client.get_image(image['id'], args.source))
                        headers['data'] = self.client.download_image(
                            image['id'], args.source, stream=False)
                        #  We need to remove some read-only fields
                        # from the headers now
                        stripped = _remove_unwanted(headers, args)
                        # set the image ID
                        stripped['id'] = image['id']
                        if args.dryrun:
                            logger.info('Dryrun: Would create image id=%s'
                                        ' with properties %s' %
                                        (image['id'], stripped))
                        else:
                            try:
                                self.client.create_image(
                                    region=args.target,
                                    **stripped)
                                updated.append(image['id'])
                            except glance_exceptions.HTTPConflict:
                                logger.error(IMAGE_ALREADY_PRESENT_MESSAGE,
                                             image['id'])
                else:
                    logger.info('Image %d status is not active.', image['id'])

        return updated

    def replication_compare(self, args):
        logger.info("Retrieving projects")
        projects = self.get_projects_to_sync()
        logger.info("Found %d projects", len(projects))
        differences = {}

        for project in projects:
            logger.info('Processing %s', project.name)
            for image in self.client.get_images_by_project_id(
                    region=args.source,
                    project_id=project.id,
                    visibility='all',
                    tag=['imagesync']):
                source = _remove_empty_values(image)
                if self.is_image_present(image['id'], args.target):
                    headers = self.client.get_image(
                        image['id'], args.target)
                    for key in args.dontreplicate:
                        if key in source:
                            logger.debug(
                                'Stripping %s from source metadata', key)
                            del source[key]
                        if key in headers:
                            logger.debug(
                                'Stripping %s from target metadata', key)
                            del headers[key]
                    for key in args.removeproperties:
                        if key in source.properties:
                            logger.debug(
                                'Stripping %s from source metadata', key)
                            del source.properties[key]
                        if key in headers.properties:
                            logger.debug(
                                'Stripping %s from source properties', key)
                            del headers.properties[key]
                    for key in source:
                        if source[key] != headers[key]:
                            logger.warning(
                                '[%s] %s: field %s differs '
                                '(source is %s, destination is %s)',
                                project.name,
                                image['id'],
                                key,
                                source[key],
                                headers.get(key, 'undefined'))
                            differences[image['id']] = 'diff'
                        else:
                            logger.debug('%s is identical', image['id'])

                elif image['status'] == 'active':
                    logger.warning(
                        ' [%s] Image %s ("%s") '
                        'entirely missing from the destination',
                        project.name,
                        image['id'],
                        image.get('name', '--unnamed'))
                    differences[image['id']] = 'missing'

        return differences

    def main(self, args=None):

        self.parser.add_argument(
            '--dryrun',
            action='store_true',
            help='read-only mode')

        self.parser.add_argument(
            '--chunksize',
            default=65536,
            help="Amount of data to transfer per HTTP write.")
        self.parser.add_argument(
            '--dontreplicate',
            default=[
                'created_at',
                'date',
                'deleted_at',
                'location',
                'updated_at',
                'direct_url',
                'file',
                'schema',
                'checksum',
                'region',
                'region_name',
                'virtual_size',
                'status',
                'size',
            ],
            nargs="*",
            help="List of fields to not replicate.")
        self.parser.add_argument(
            '--removeproperties',
            default=[
                'stores',
                'direct_url'],
            nargs="*",
            help="List of properties to not replicate.")
        self.parser.add_argument(
            '--metaonly',
            default=False,
            help="Only replicate metadata, not images.")

        subparsers = self.parser.add_subparsers(metavar='<subcommand>')

        parser_compare = subparsers.add_parser(
            'compare',
            help="Compare the contents of fromserver with those of toserver")
        parser_compare.add_argument(
            '--source',
            required=True,
            help='Source region to get the images')
        parser_compare.add_argument(
            '--target',
            required=True,
            help='Target region to set the images')
        parser_compare.set_defaults(func=self.replication_compare)

        parser_dump = subparsers.add_parser(
            'dump',
            help="Dump the contents of a glance instance to local disk")
        parser_dump.add_argument(
            '--source',
            required=True,
            help='Source region to get the images')
        parser_dump.add_argument(
            '--path',
            required=True,
            help='File path to dump the images')
        parser_dump.set_defaults(func=self.replication_dump)

        parser_livecopy = subparsers.add_parser(
            'livecopy',
            help="Load the contents of one glance instance into another.")
        parser_livecopy.add_argument(
            '--source',
            required=True,
            help='Source region to get the images')
        parser_livecopy.add_argument(
            '--target',
            required=True,
            help='Target region to set the images')
        parser_livecopy.add_argument(
            '--maxnum',
            required=False,
            default=9999,
            help='maximum number of images to process in this run')
        parser_livecopy.add_argument(
            '--project',
            required=False,
            default='all',
            help='Project name. Default: all')
        parser_livecopy.set_defaults(func=self.replication_livecopy)

        parser_load = subparsers.add_parser(
            'load',
            help="Load the contents of a local directory into glance.")
        parser_load.add_argument(
            '--target',
            required=True,
            help='Target region to set the images')
        parser_load.add_argument(
            '--path',
            required=True,
            help='File path to load the images')
        parser_load.set_defaults(func=self.replication_load)

        parser_size = subparsers.add_parser(
            'size',
            help="Determine the size of a glance instance if dumped to disk.")
        parser_size.add_argument(
            '--source',
            required=True,
            help='Source region to get the images')
        parser_size.set_defaults(func=self.replication_size)

        args = self.parse_args(args)

        if args.dryrun:
            logger.info(
                "--- THIS IS A DRYRUN. NO CHANGES WILL BE MADE ---")

        try:
            call = args.func
        except AttributeError:
            self.parser.error("Too few arguments")

        call(args)


def main(args=None):
    ReplicatorImagesCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
