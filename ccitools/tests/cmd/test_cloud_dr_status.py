import logging

from ccitools.cmd.cloud_dr_status import main
from ccitools.tests.cmd import fixtures
from glanceclient.common.exceptions import HTTPNotFound
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.utils.cloud.cloud_config')
@mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
class TestCloudDRStatus(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch('ccitools.cmd.cloud_dr_status.cinder_client')
    @mock.patch('ccitools.cmd.cloud_dr_status.glance_client')
    @mock.patch('ccitools.cmd.cloud_dr_status.nova_client')
    def test_no_servers_no_images(self, mock_nc, mock_gc, mock_cic, mock_crc,
                                  mock_cc):
        mock_cl = mock_crc.return_value
        mock_nl = mock_nc.Client.return_value
        mock_gl = mock_gc.Client.return_value
        mock_ci = mock_cic.Client.return_value

        mock_cl.list_projects.return_value = []
        mock_nl.servers.list.return_value = []

        main(['-c', 'name'])

        mock_crc.assert_called_with(
            cloud='cern',
            region_name=None
        )
        mock_nc.Client.assert_called_with(
            version='2.1',
            session=mock_cl.session
        )
        mock_gc.Client.assert_called_with(
            session=mock_cl.session
        )
        mock_cic.Client.assert_called_with(
            session=mock_cl.session
        )

        mock_cl.list_projects.assert_called_with()
        mock_nl.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'name': '^cci-'
            }
        )
        mock_gl.images.get.assert_not_called()
        mock_ci.volumes.get.assert_not_called()

    @mock.patch('ccitools.cmd.cloud_dr_status.cinder_client')
    @mock.patch('ccitools.cmd.cloud_dr_status.glance_client')
    @mock.patch('ccitools.cmd.cloud_dr_status.nova_client')
    def test_server_bfv_no_metadata(self, mock_nc, mock_gc, mock_cic, mock_crc,
                                    mock_cc):
        mock_cl = mock_crc.return_value
        mock_cl = mock_crc.return_value
        mock_nl = mock_nc.Client.return_value
        mock_gl = mock_gc.Client.return_value
        mock_ci = mock_cic.Client.return_value

        mock_cl.list_projects.return_value = fixtures.DR_PROJECTS
        mock_nl.servers.list.return_value = fixtures.DR_SERVERS_BFV
        mock_ci.volumes.get.return_value = fixtures.DR_VOLUME

        main(['--regex', '^test'])

        mock_crc.assert_called_with(
            cloud='cern',
            region_name=None
        )
        mock_nc.Client.assert_called_with(
            version='2.1',
            session=mock_cl.session
        )
        mock_gc.Client.assert_called_with(
            session=mock_cl.session
        )
        mock_cic.Client.assert_called_with(
            session=mock_cl.session
        )

        mock_cl.list_projects.assert_called_with()
        mock_nl.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'name': '^test'
            }
        )
        mock_gl.images.get.assert_not_called()
        mock_ci.volumes.get.assert_called_with(
            'fake_volume_id'
        )

    @mock.patch('ccitools.cmd.cloud_dr_status.cinder_client')
    @mock.patch('ccitools.cmd.cloud_dr_status.glance_client')
    @mock.patch('ccitools.cmd.cloud_dr_status.nova_client')
    def test_server_bfv_metadata(self, mock_nc, mock_gc, mock_cic, mock_crc,
                                 mock_cc):
        mock_cl = mock_crc.return_value
        mock_cl = mock_crc.return_value
        mock_nl = mock_nc.Client.return_value
        mock_gl = mock_gc.Client.return_value
        mock_ci = mock_cic.Client.return_value

        mock_cl.list_projects.return_value = fixtures.DR_PROJECTS
        mock_nl.servers.list.return_value = fixtures.DR_SERVERS_BFV
        mock_ci.volumes.get.return_value = fixtures.DR_VOLUME_METADATA

        main(['--regex', '^test'])

        mock_crc.assert_called_with(
            cloud='cern',
            region_name=None
        )
        mock_nc.Client.assert_called_with(
            version='2.1',
            session=mock_cl.session
        )
        mock_gc.Client.assert_called_with(
            session=mock_cl.session
        )
        mock_cic.Client.assert_called_with(
            session=mock_cl.session
        )

        mock_cl.list_projects.assert_called_with()
        mock_nl.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'name': '^test'
            }
        )
        mock_gl.images.get.assert_not_called()
        mock_ci.volumes.get.assert_called_with(
            'fake_volume_id'
        )

    @mock.patch('ccitools.cmd.cloud_dr_status.cinder_client')
    @mock.patch('ccitools.cmd.cloud_dr_status.glance_client')
    @mock.patch('ccitools.cmd.cloud_dr_status.nova_client')
    def test_server_bfi(self, mock_nc, mock_gc, mock_cic, mock_crc,
                        mock_cc):
        mock_cl = mock_crc.return_value
        mock_nl = mock_nc.Client.return_value
        mock_gl = mock_gc.Client.return_value
        mock_ci = mock_cic.Client.return_value

        mock_cl.list_projects.return_value = fixtures.DR_PROJECTS
        mock_nl.servers.list.return_value = fixtures.DR_SERVERS_BFI
        mock_gl.images.get.return_value = fixtures.DR_IMAGE

        main(['--regex', '^test'])

        mock_crc.assert_called_with(
            cloud='cern',
            region_name=None
        )
        mock_nc.Client.assert_called_with(
            version='2.1',
            session=mock_cl.session
        )
        mock_gc.Client.assert_called_with(
            session=mock_cl.session
        )
        mock_cic.Client.assert_called_with(
            session=mock_cl.session
        )

        mock_cl.list_projects.assert_called_with()
        mock_nl.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'name': '^test'
            }
        )
        mock_gl.images.get.called_with(
            'fake_image_id'
        )
        mock_ci.volumes.get.assert_not_called()

    @mock.patch('ccitools.cmd.cloud_dr_status.cinder_client')
    @mock.patch('ccitools.cmd.cloud_dr_status.glance_client')
    @mock.patch('ccitools.cmd.cloud_dr_status.nova_client')
    def test_server_bfi_raise(self, mock_nc, mock_gc, mock_cic, mock_crc,
                              mock_cc):
        mock_cl = mock_crc.return_value
        mock_nl = mock_nc.Client.return_value
        mock_gl = mock_gc.Client.return_value
        mock_ci = mock_cic.Client.return_value

        mock_cl.list_projects.return_value = fixtures.DR_PROJECTS
        mock_nl.servers.list.return_value = fixtures.DR_SERVERS_BFI
        mock_gl.images.get.side_effect = HTTPNotFound

        main(['--regex', '^test'])

        mock_crc.assert_called_with(
            cloud='cern',
            region_name=None
        )
        mock_nc.Client.assert_called_with(
            version='2.1',
            session=mock_cl.session
        )
        mock_gc.Client.assert_called_with(
            session=mock_cl.session
        )
        mock_cic.Client.assert_called_with(
            session=mock_cl.session
        )

        mock_cl.list_projects.assert_called_with()
        mock_nl.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'name': '^test'
            }
        )
        mock_gl.images.get.called_with(
            'fake_image_id'
        )
        mock_ci.volumes.get.assert_not_called()
