import logging

from unittest import mock
from unittest import TestCase


class TestCheckNodeForemanCMD(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.aitools_mock = mock.MagicMock()
        modules = {
            'aitools': self.aitools_mock,
            'aitools.foreman': self.aitools_mock.foreman,
        }
        self.patcher = mock.patch.dict('sys.modules', modules)
        self.patcher.start()
        self.addCleanup(self.patcher.stop)

    def test_default_args(self):
        from ccitools.cmd.check_node_foreman import main
        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)

    def test_hostgroup_in_client(self):
        from ccitools.cmd.check_node_foreman import main
        mock_foreman = self.aitools_mock.foreman.ForemanClient().gethost
        mock_foreman.return_value = {
            'hostgroup': {
                'title': 'true_hostgroup'
            }
        }

        main(
            [
                '--nodes', 'fake_node',
                '--hostgroup', 'true_hostgroup',
            ]
        )

    def test_hostgroup_not_in_client(self):
        from ccitools.cmd.check_node_foreman import main
        mock_foreman = self.aitools_mock.foreman.ForemanClient().gethost
        mock_foreman.return_value = {
            'hostgroup': {
                'title': 'fake_fake'
            }
        }

        with self.assertRaises(SystemExit) as cm:
            main(
                [
                    '--nodes', 'fake_node',
                    '--hostgroup', 'fake_hostgroup',
                ]
            )
        self.assertEqual(cm.exception.code, 1)

    def test_no_hostgroup(self):
        from ccitools.cmd.check_node_foreman import main
        mock_foreman = self.aitools_mock.foreman.ForemanClient().gethost
        mock_foreman.return_value = {
            'hostgroup': {
                'title': 'fake_fake'
            }
        }

        main(
            [
                '--nodes', 'fake_node',
                '--hostgroup', '',
            ]
        )

    def test_exception(self):
        from ccitools.cmd.check_node_foreman import main
        # 5. Testing exception
        mock_foreman = self.aitools_mock.foreman.ForemanClient().gethost
        mock_foreman.side_effect = iter([Exception('ooops')])
        with self.assertRaises(SystemExit) as cm:
            with self.assertRaises(Exception):
                main(
                    [
                        '--nodes', 'fake_node',
                    ]
                )
        self.assertEqual(cm.exception.code, 1)
