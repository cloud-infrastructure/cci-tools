#!/usr/bin/python3

from __future__ import print_function

import logging
import re
import subprocess  # nosec
import sys
import tenacity

from ccitools.cmd import base
from socket import gethostbyname

# configure logging
logger = logging.getLogger(__name__)


class WrongProvisionStateException(Exception):
    pass


class ProvisionStateFailureException(Exception):
    pass


class ForemanCredentialsSync(base.BaseCMD):
    def __init__(self):
        super(ForemanCredentialsSync, self).__init__(
            description=("Synchronize credentials between Foreman and Ironic"))

    def get_ipmi_creds(self, host):
        try:
            command = (
                "ai-ipmi get-creds %s 2>&1 | "
                "grep -E '(Username|Password)'| "
                "awk '{printf \"%s \", $2}'" % (host, "%s")
            )
            ipmi_username, ipmi_password = subprocess.check_output(
                command, shell=True).split()  # nosec
            logger.info(
                "IPMI credentials for machine '%s' grabbed successfuly", host)
            return ipmi_username, ipmi_password
        except subprocess.CalledProcessError as e:
            logger.error(
                "Cannot get IPMI credentials for machine '%s': %s",
                host,
                str(e))
            raise e

    def generate_ironic_name(self, host, ipmi_username, ipmi_password):
        try:
            command = (
                "ipmitool fru -H %s-ipmi.cern.ch -U %s -P %s | "
                "grep -E '(Asset|Product Serial)' | head -n2 | "
                "cut -d':' -f 2|tr -d '\n'" % (
                    host,
                    ipmi_username,
                    ipmi_password)
            )
            serial, asset = subprocess.check_output(
                command, shell=True).split()  # nosec
            logger.info(
                "Generated '%s' name for machine '%s'",
                (asset + "-" + serial).lower(),
                host)
            return (asset + "-" + serial).lower()
        except subprocess.CalledProcessError as e:
            logger.error(
                "Cannot get serial or asset tag for machine '%s': %s",
                host,
                str(e))
            raise e

    def create_ironic_node(self,
                           ironic_name,
                           ipmi_address,
                           ipmi_username,
                           ipmi_password):
        cmd = (
            "openstack baremetal node create "
            "--driver cern_pxe_ipmitool "
            "--driver-info ipmi_port=623 "
            "--driver-info ipmi_username=%s "
            "--driver-info ipmi_password=%s "
            "--driver-info ipmi_address=%s "
            "--driver-info "
            "deploy_kernel=d50eeaee-b64d-4f63-abe2-40e15e80791a "
            "--driver-info "
            "deploy_ramdisk=d50eeaee-b64d-4f63-abe2-40e15e80791a "
            "--name %s"
        )

        try:
            logger.info(
                "Creating ironic node '%s' with IPMI IP '%s' ...",
                ironic_name,
                ipmi_address)
            out = subprocess.check_output(cmd % (
                ipmi_username,
                ipmi_password,
                ipmi_address,
                ironic_name), shell=True)  # nosec
            logger.debug(out)
        except subprocess.CalledProcessError as e:
            logger.error(
                "Cannot create ironic node '%s': %s",
                ipmi_username,
                ironic_name,
                str(e))
            raise e

    def enable_console(self, ironic_name):
        try:
            logger.info("Enabling console for ironic node '%s' ...",
                        ironic_name)
            command = (
                "openstack baremetal node console enable %s" % ironic_name)
            out = subprocess.check_output(command, shell=True)  # nosec
            logger.debug(out)
        except subprocess.CalledProcessError as e:
            logger.error(
                "Cannot enable console for ironic node '%s': %s",
                ironic_name,
                str(e))
            raise e

    def set_provision_state(self, ironic_name, state):
        try:
            logger.info("Putting '%s' into '%s' state ...",
                        ironic_name, state)
            command = (
                "openstack baremetal node %s %s" % (state, ironic_name))
            subprocess.check_output(command, shell=True).split()  # nosec
        except subprocess.CalledProcessError as e:
            logger.error(
                "Cannot set ironic node '%s' to state '%s': %s",
                ironic_name,
                state,
                str(e))
            raise e

    @tenacity.retry(stop=tenacity.stop_after_attempt(60),
                    wait=tenacity.wait_fixed(60),
                    retry=tenacity.retry_if_exception_type(
        WrongProvisionStateException))
    def wait_for_provision_state(self, ironic_name, target_state):
        """Wait for desired provision state.

        This function will retry checking for a desired provision state for a
        given ironic node. Construction of the decorator will make it check
        the state every 60 seconds, retrying max 60 times as long as current
        state is different than a desired state.
        """
        try:
            logger.info("Waiting for '%s' to go into '%s' state ...",
                        ironic_name,
                        target_state)
            command = (
                "openstack baremetal node show %s | "
                "grep ' provision_state ' | "
                "awk '{ print $4 }'" % ironic_name
            )
            out = subprocess.check_output(command, shell=True).strip()  # nosec
            logger.debug("Detected state for '%s': '%s'", ironic_name, out)
            if out != target_state:
                if "failed" in out:
                    raise ProvisionStateFailureException
                raise WrongProvisionStateException
            else:
                return True
        except subprocess.CalledProcessError:
            raise WrongProvisionStateException

    def delete_foreman_entry(self, host):
        try:
            logger.info("Removing host '%s' from Foreman ..." % host)
            out = subprocess.check_output(
                "ai-foreman delhost %s" % host + ".cern.ch",
                shell=True)  # nosec
            logger.info(out)
        except subprocess.CalledProcessError as e:
            logger.error(
                "Cannot remove host '%s' from Foreman: %s", host, str(e))
            raise e

    def move_foreman_entry(self, host, hostgroup):
        try:
            logger.info("Moving host '%s' in Foreman ..." % host)
            command = (
                "ai-foreman -f %s updatehost -c %s" % (
                    host + ".cern.ch", hostgroup)
            )
            out = subprocess.check_output(command, shell=True)  # nosec
            logger.info(out)
        except subprocess.CalledProcessError as e:
            logger.error(
                "Cannot move host '%s' between hostgroups: %s",
                host,
                str(e))
            raise e

    def get_hosts_from_hostgroup(self, hostgroup):
        try:
            logger.info("Getting list of hosts in '%s' hostgroup ...",
                        hostgroup)
            command = (
                "ai-foreman -g %s --no-header showhost | "
                "awk '{ print $2 }'" % hostgroup
            )
            out = subprocess.check_output(command, shell=True).split()  # nosec
            logger.info(out)
            return out
        except subprocess.CalledProcessError as e:
            logger.error("Cannot list hostgroup '%s': %s",
                         hostgroup, str(e))
            raise e

    def main(self, args=None):
        try:
            self.parser.add_argument(
                '--hosts',
                default=None,
                help='comma separated list of hosts')
            self.parser.add_argument(
                '--hostgroup',
                default=None,
                help='hostgroup containing nodes to be enrolled')

            self.parser.add_argument(
                '--dryrun',
                action='store_true',
                help='read-only mode')
            self.parser.add_argument(
                '--parallel',
                action='store_true',
                help='parallel mode mode')
            args = self.parse_args(args)

            if args.dryrun:
                logger.info(
                    "--- THIS IS A DRYRUN. NO CHANGES WILL BE MADE ---")
        except Exception as e:
            logger.error("Wrong command line arguments (%s)" % str(e))
            sys.exit(2)

        hosts = []
        if args.hosts != "" and args.hosts is not None:
            hosts = args.hosts.split(",")
            logger.info(hosts)
        if args.hostgroup != "" and args.hostgroup is not None:
            hosts = self.get_hosts_from_hostgroup(args.hostgroup)

        for host in hosts:
            host = re.sub('.cern.ch$', '', host)
            if not args.dryrun:
                try:
                    ipmi_username, ipmi_password = self.get_ipmi_creds(host)
                    ironic_name = self.generate_ironic_name(
                        host, ipmi_username, ipmi_password)
                    ipmi_address = gethostbyname(host + "-ipmi")

                    self.create_ironic_node(
                        ironic_name,
                        ipmi_address,
                        ipmi_username,
                        ipmi_password)
                    self.enable_console(ironic_name)
                    self.set_provision_state(ironic_name, "manage")
                    self.set_provision_state(ironic_name, "inspect")

                    if not args.parallel:
                        try:
                            self.wait_for_provision_state(
                                ironic_name, "manageable")
                            self.set_provision_state(ironic_name, "provide")
                        except WrongProvisionStateException:
                            logger.error("Timeout while inspecting node '%s'",
                                         ironic_name)
                        except ProvisionStateFailureException:
                            logger.error("Failure while inspecting node '%s'",
                                         ironic_name)
                    else:
                        try:
                            self.wait_for_provision_state(
                                ironic_name, "available")
                        except WrongProvisionStateException:
                            logger.error("Timeout while providing node '%s'",
                                         ironic_name)
                        except ProvisionStateFailureException:
                            logger.error("Failure while providing node '%s'",
                                         ironic_name)

                    self.move_foreman_entry(host, "cloud_baremetal/phy_box")
                    self.delete_foreman_entry(host)
                    logger.info(
                        "Host '%s' successfuly enrolled as '%s'",
                        host,
                        ironic_name)
                except Exception:  # nosec
                    continue
            if args.dryrun:
                ipmi_username, ipmi_password = self.get_ipmi_creds(host)
                self.generate_ironic_name(host, ipmi_username, ipmi_password)
                gethostbyname(host + "-ipmi")


# Needs static method for setup.cfg
def main(args=None):
    ForemanCredentialsSync().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)
