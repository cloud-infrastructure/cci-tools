import ccitools.conf

from aitools.pdb import PdbClient
from ccitools.cmd.base.cloud import BaseCloudCMD

CONF = ccitools.conf.CONF


class BasePuppetDBCMD(BaseCloudCMD):
    def __init__(self, description):
        super(BasePuppetDBCMD, self).__init__(description=description)

        self.pdb = PdbClient(
            host=CONF.puppetdb.host,
            port=CONF.puppetdb.port,
            timeout=CONF.puppetdb.timeout)
