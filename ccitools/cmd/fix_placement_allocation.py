#!/usr/bin/python3

import json
import logging
import re
import requests
import sys

from ccitools.cmd.base.cloud import BaseCloudCMD
import ccitools.conf

# configure logging
logger = logging.getLogger(__name__)

CONF = ccitools.conf.CONF

REGEXP = (r"Instance ([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}"
          "-[0-9a-f]{4}-[0-9a-f]{12}) has been moved to "
          "another host.*")


class FixPlacementAllocationCMD(BaseCloudCMD):
    def __init__(self):
        super(FixPlacementAllocationCMD, self).__init__(
            description="Fix Placement Allocations")
        self.error_nodes = {}

    def query_es_logs(self):
        """Query logs from Elastic search.

        Search for the logs from the last week indicating
        the nodes with ipmi connectivity issues. This log
        occures only once after the issue is found.
        """
        server = CONF.es.server
        user = CONF.es.user
        password = CONF.es.password
        query_string = "metadata.toplevel_hostgroup: cloud_compute "\
            "AND \"has been moved to another host\""
        url = server + '/monit_private_openstack_logs_generic-*/_search'
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        query = json.dumps({
            "_source": ["data.raw"],
            "size": 1000,
            "query": {
                "bool": {
                    "must": [
                        {
                            "query_string": {
                                "query": query_string,
                                "analyze_wildcard": True
                            }
                        },
                        {
                            "match_phrase": {
                                "data.source": {
                                    "query": "nova.compute"
                                }
                            }
                        },
                        {
                            "range": {
                                "metadata.timestamp": {
                                    "format": "strict_date_optional_time",
                                    "gte": "now-1d"
                                }
                            }
                        }
                    ]
                }
            }})
        response = requests.get(url, auth=(
            user, password), data=query, headers=headers,
            verify="/etc/pki/tls/certs/"
            "CERN_Root_Certification_Authority_2.pem",
            timeout=120)
        return json.loads(response.text)['hits']['hits']

    def fix_allocation_of_instance(self, dryrun, instance_uuid):
        # Find the instance information to identify the host
        logger.info('Checking if instance %s exists', instance_uuid)
        instance = self.cloudclient.get_server(instance_uuid)

        # skip if the machine doesn't exist
        if not instance:
            logger.info('Instance %s does not exists skipping...',
                        instance_uuid)
            return

        # skip if it's currently being migrated
        if instance.status == 'MIGRATING':
            logger.info('Instance %s is being migrated skipping...',
                        instance_uuid)
            return

        host = getattr(instance, 'OS-EXT-SRV-ATTR:host')
        logger.info('Retrieving resource provider for host %s', host)
        rp = self.cloudclient.get_resource_provider(host)['uuid']
        logger.info('Instance %s should be in the following '
                    'resource_provider %s', instance_uuid, rp)

        logger.info('Checking allocations for instance %s')
        # Checking how many allocations we have on this resource
        entries = self.cloudclient.get_allocations(instance_uuid)

        for region, entry in entries.items():
            if (not entry
                    or 'allocations' not in entry
                    or not entry['allocations']):
                logger.info('There are no allocations for instance %s, '
                            'on region %s. skipping...',
                            instance_uuid, region)
                continue

        # trigger the fix if there are many or the one does not
        # match the real one
        if (rp not in entry['allocations'].keys()
                or len(entry['allocations']) > 1):
            # Gets the values from the first of the allocations
            # Sets the allocation to the existing hypervisor
            resources = list(entry['allocations'].values())[0][
                'resources']

            if dryrun:
                logger.info(
                    "DRYRUN: Set the allocation to:"
                    "instance_uuid=%s, rp=%s, vcpu=%s, "
                    "memory_mb=%s, disk_gb=%s, project_id=%s, "
                    "user_id=%s, ",
                    instance_uuid,
                    rp,
                    resources['VCPU'],
                    resources['MEMORY_MB'],
                    resources['DISK_GB'],
                    entry['project_id'],
                    entry['user_id'],
                    region)
            else:
                self.cloudclient.set_allocation(
                    instance_uuid,
                    rp,
                    resources['VCPU'],
                    resources['MEMORY_MB'],
                    resources['DISK_GB'],
                    entry['project_id'],
                    entry['user_id'],
                    region)

    def main(self, args=None):
        """Get broken nodes and send email."""
        self.parser.add_argument(
            '--dryrun',
            action='store_true',
            help='read-only mode')
        args = self.parse_args(args)

        if args.dryrun:
            logger.info(
                "--- THIS IS A DRYRUN. NO CHANGES WILL BE MADE ---")

        logger.info("Querying logs from opensearch...")
        messages = self.query_es_logs()

        logger.info('Processing messages')
        for message in messages:
            match = re.match(REGEXP, message['_source']['data']['raw'])
            if match and match[0] and match[1]:
                instance_uuid = match[1]
                self.fix_allocation_of_instance(args.dryrun, instance_uuid)


def main(args=None):
    FixPlacementAllocationCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
