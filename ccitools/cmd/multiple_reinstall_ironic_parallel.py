#!/usr/bin/python3

import ccitools.conf
import json
import logging
import sys
import time

from ccitools.cmd.base.rundeck import BaseRundeckCMD


# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class MultipleReinstallIronicParallel(BaseRundeckCMD):
    def __init__(self):
        super(MultipleReinstallIronicParallel, self).__init__(
            description=("Reinstall Ironic nodes in parallel using the API"))

    def main_reinstall(self, rdeckclient, args):
        hostnames = args.hosts.split()
        logger.info("Starting '%s' pararell jobs to reinstall "
                    "the following Ironic servers: %s",
                    len(hostnames),
                    ', '.join(hostnames))
        logger.info("Hosts will be reinstalled in batches of '%s' "
                    "with a delay between them of '%s' seconds",
                    args.parallel,
                    args.delay)
        startat = 0
        while (startat <= len(hostnames)):
            hosts_batch = hostnames[startat:startat + int(args.parallel)]
            if hosts_batch:
                for hostname in hosts_batch:
                    logger.info("Starting reinstallation of '%s'...", hostname)
                    output = rdeckclient.run_job_by_id(
                        args.job_id,
                        parameters={
                            'hostname': hostname,
                            'os': args.os,
                            'force_raid': args.force_raid,
                            'additional_puppet_run':
                                args.additional_puppet_run,
                            'additional_reboot': args.additional_reboot,
                            'check_empty': args.check_empty,
                            'force_host': args.force_host})
                    json_out = json.loads(output)
                    logger.info("Follow the execution here: %s",
                                json_out['permalink'])

                startat += int(args.parallel)
                if startat < len(hostnames):
                    logger.info("Sleeping for '%s' seconds before launching "
                                "the next batch of reinstallations",
                                args.delay)
                    time.sleep(args.delay)
                else:
                    logger.info("No more hosts to reinstall :)")
                    break

    def main(self, args=None):
        self.parser.add_argument(
            '--delay',
            type=int,
            default=0,
            help='Delay between executions')
        self.parser.add_argument(
            '--parallel',
            type=int,
            default=2,
            help='Parallel execution')
        self.parser.add_argument(
            '--job-id',
            help='Job ID')
        self.parser.add_argument(
            '--hosts',
            required=True,
            help='List of servers')
        self.parser.add_argument(
            '--os',
            required=True,
            help='Operating system')
        self.parser.add_argument(
            '--force_raid',
            required=True,
            help='Raid config')
        self.parser.add_argument(
            '--additional_puppet_run',
            required=True,
            help='Additional puppet run')
        self.parser.add_argument(
            '--additional_reboot',
            required=True,
            help='Additional reboot')
        self.parser.add_argument(
            '--check_empty',
            required=True,
            help='Check if the server has allocated machines')
        self.parser.add_argument(
            '--force_host',
            required=True,
            help='Force the scheduler to use the same node on reinstallation')

        args = self.parse_args(args)

        self.main_reinstall(self.rundeckcli, args)


# Needs static method for setup.cfg
def main(args=None):
    MultipleReinstallIronicParallel().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)
