import logging

from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.utils.cloud.cloud_config')
@mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
class TestCheckCloudVMOwners(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.aitools_mock = mock.MagicMock()
        modules = {
            'aitools': self.aitools_mock,
            'aitools.pdb': self.aitools_mock.pdb,
        }
        self.patcher = mock.patch.dict('sys.modules', modules)
        self.patcher.start()
        self.addCleanup(self.patcher.stop)

    def test_fake_machine(self, mock_cloud, mock_cc):
        from ccitools.cmd.check_cloud_vm_owners import main
        mock_pdb = self.aitools_mock.pdb.PdbClient.return_value
        mock_pdb.raw_request.return_value = (
            200,
            [
                {
                    'certname': 'fake_name',
                    'value': 'true',
                }
            ]
        )

        main([])

    def test_machine_with_proper_data(self, mock_cloud, mock_cc):
        from ccitools.cmd.check_cloud_vm_owners import main
        mock_pdb = self.aitools_mock.pdb.PdbClient.return_value
        mock_pdb.raw_request.side_effect = iter([
            (
                0,
                [
                    {
                        "certname": "name1.cern.ch",
                        "environment": "production",
                        "name": "is_virtual",
                        'value': 'true',
                    },
                    {
                        "certname": "name2.cern.ch",
                        "environment": "production",
                        "name": "is_virtual",
                        'value': 'false',
                    }
                ]
            ),
            (
                0,
                [
                    {
                        "certname": "name1.cern.ch",
                        "environment": "production",
                        "name": "hostgroup_0",
                        "value": "cloud_infrastructure"
                    },
                    {
                        "certname": "name2.cern.ch",
                        "environment": "production",
                        "name": "hostgroup_0",
                        "value": "cloud_identity"
                    }
                ]
            ),
            (
                0,
                [
                    {
                        "certname": "name1.cern.ch",
                        "environment": "production",
                        "name": "landb_responsible_name",
                        "value": "E-GROUP CLOUD-INFRASTRUCTURE-IDENTITY-ADMIN"
                    },
                    {
                        "certname": "name2.cern.ch",
                        "environment": "production",
                        "name": "landb_responsible_name",
                        "value": "E-GROUP CLOUD-INFRASTRUCTURE-CORE-ADMIN"
                    }
                ]
            )
        ])

        main([])

    def test_machine_wrong_responsible(self, mock_cloud, mock_cc):
        from ccitools.cmd.check_cloud_vm_owners import main
        mock_pdb = self.aitools_mock.pdb.PdbClient.return_value
        mock_cloud.return_value.get_servers_by_name.return_value.metadata = {
            "landb-mainuser": 'cloud-infrastructure-identity-admin'
        }

        mock_pdb.raw_request.side_effect = iter([
            (
                0,
                [
                    {
                        "certname": "name1.cern.ch",
                        "environment": "production",
                        "name": "is_virtual",
                        'value': 'true',
                    }
                ]
            ),
            (
                0,
                [
                    {
                        "certname": "name1.cern.ch",
                        "environment": "production",
                        "name": "hostgroup_0",
                        "value": "cloud_workflow"
                    }
                ]
            ),
            (
                0,
                [
                    {
                        "certname": "name1.cern.ch",
                        "environment": "production",
                        "name": "landb_responsible_name",
                        "value": "E-GROUP CLOUD-INFRASTRUCTURE-IDENTITY-ADMIN"
                    }
                ]
            )
        ])

        main([])

    def test_fake_machine_right_responsible(self, mock_cloud, mock_cc):
        from ccitools.cmd.check_cloud_vm_owners import main
        mock_pdb = self.aitools_mock.pdb.PdbClient.return_value
        mock_cloud.return_value.get_servers_by_name.return_value.metadata = {
            "landb-mainuser": 'cloud-infrastructure-identity-admin'
        }

        mock_pdb.raw_request.side_effect = iter([
            (
                0,
                [
                    {
                        "certname": "fake_name",
                        "environment": "pro_fake",
                        "name": "is_fake",
                        'value': 'true',
                    }
                ]
            ),
            (
                0,
                [
                    {
                        "certname": "name0_fake",
                        "environment": "fake_env",
                        "name": "fakegroup_0",
                        "value": "cloud_identity"
                    }
                ]
            ),
            (
                0,
                [
                    {
                        "certname": "name1_fake",
                        "environment": "pro_fake",
                        "name": "name_fake",
                        "value": "E-GROUP CLOUD-INFRASTRUCTURE-IDENTITY-ADMIN"
                    }
                ]
            )
        ])

        main([])

    def test_fake_machine_wrong_responsible(self, mock_cloud, mock_cc):
        from ccitools.cmd.check_cloud_vm_owners import main
        mock_pdb = self.aitools_mock.pdb.PdbClient.return_value
        mock_cloud.return_value.get_servers_by_name.return_value.metadata = {
            "landb-mainuser": 'cloud-infrastructure-identity-admin'
        }

        mock_pdb.raw_request.side_effect = iter([
            (
                0,
                [
                    {
                        "certname": "fake_name",
                        "environment": "production",
                        "name": "is_virtual",
                        'value': 'true',
                    }
                ]
            ),
            (
                0,
                [
                    {
                        "certname": "fake_name",
                        "environment": "production",
                        "name": "hostgroup0",
                        "value": "cloud_identity"
                    }
                ]
            ),
            (
                0,
                [
                    {
                        "certname": "fake_name",
                        "environment": "production",
                        "name": "landb_responsible_name",
                        "value": "E-GROUP CLOUD-INFRASTRUCTURE-CORE-ADMIN"
                    }
                ]
            )
        ])

        main([])

    def test_exception_on_get_servers_by_name(self, mock_cloud, mock_cc):
        from ccitools.cmd.check_cloud_vm_owners import main
        mock_pdb = self.aitools_mock.pdb.PdbClient.return_value
        mock_cloud.return_value.get_servers_by_name.side_effect = iter([
            Exception('fake')
        ])

        mock_pdb.raw_request.side_effect = iter([
            (
                0,
                [
                    {
                        "certname": "fake_name",
                        "environment": "production",
                        "name": "is_virtual",
                        'value': 'true',
                    }
                ]
            ),
            (
                0,
                [
                    {
                        "certname": "fake_name",
                        "environment": "production",
                        "name": "hostgroup0",
                        "value": "cloud_identity"
                    }
                ]
            ),
            (
                0,
                [
                    {
                        "certname": "fake_name",
                        "environment": "production",
                        "name": "landb_responsible_name",
                        "value": "E-GROUP CLOUD-INFRASTRUCTURE-CORE-ADMIN"
                    }
                ]
            )
        ])

        main([])
