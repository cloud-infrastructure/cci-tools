from oslo_config import cfg

service_metrics_producer = cfg.StrOpt(
    'service_metrics_producer',
    default='cloud',
    help="Producer indentifier registered in MONIT to publish service metrics")

service_metrics_endpoint = cfg.StrOpt(
    'service_metrics_endpoint',
    default='http://monit-metrics.cern.ch:10012',
    help="Producer indentifier registered in MONIT to publish service metrics")


GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    service_metrics_producer,
    service_metrics_endpoint
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
