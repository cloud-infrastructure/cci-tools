import logging
import re

from ccitools.common import ping
from ccitools.common import ssh_executor
from ccitools.errors import SSHException
from ccitools.verify import base

# configure logging
logger = logging.getLogger(__name__)


class NoContactAction(base.ActionBase):

    def try_fix(self):
        """Implement the fix for the exception thrown in collectd."""
        pass

    def check_alarm_state(self, after_try=False):
        logger.info("Running ping to check aliveness")
        if not ping(self.host):
            return True

        logger.info("Running host in the machine")
        try:

            out, err = ssh_executor(
                self.host,
                'hostname'
            )

            for line in out:
                if re.search(self.host, line):
                    return False

        except SSHException:
            pass

        self.reason = "Node {0} is down".format(self.host)
        return True
