#!/usr/bin/python3

import ccitools.conf
import logging
import requests
import sys
import yaml


from ccitools.cmd.base.cloud import BaseCloudCMD

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class AccountingUnitCost(BaseCloudCMD):
    def __init__(self):
        super(AccountingUnitCost, self).__init__(
            description=(
                "Uploads Unit Cost information into Accounting APIs"))

    def main(self, args):
        self.parser.add_argument(
            '--dryrun',
            dest='dryrun',
            help='Do not send the calculated datapoints',
            action='store_true',
            default=False
        )

        args = self.parse_args(args)

        for svc in CONF.dbreporter.services:
            logger.info("Processing service %s", svc)
            data = []

            for metric in CONF.dbreporter.services[svc]['metrics']:
                logger.info("Generate unit cost entries for metric %s on "
                            "service %s", metric, svc)
                metric_cfg = CONF.dbreporter.services[svc]['metrics'][metric]

                if not metric_cfg['cost']:
                    logger.info('Per cost publish for metric not enabled %s, '
                                'skipping...',
                                metric)
                    continue

                # Generate unit cost entries
                unitcost_data = {
                    "MessageFormatVersion": 3,
                    "FromChargeGroup": CONF.dbreporter.services[svc]['from'],
                    "MetricName": metric_cfg['name'],
                    "CostingDoc": CONF.dbreporter.services[svc]['doc'],
                    "data": []
                }

                if not hasattr(self.cloudclient,
                               metric_cfg['function']):
                    logger.error(
                        "The service %s metric %s function %s does not exist",
                        svc,
                        metric,
                        metric_cfg['function']
                    )
                    continue

                if not callable(
                    getattr(self.cloudclient,
                            metric_cfg['function'])):
                    logger.error(
                        "The service %s metric %s function %s is not callable",
                        svc,
                        metric,
                        metric_cfg['function']
                    )
                    continue

                for type in getattr(self.cloudclient, metric_cfg['function'])(
                        region='cern', show_all=True):
                    if not hasattr(type, metric_cfg['cost_attr']):
                        logger.error(
                            "The service %s metric %s, type %s, attr %s does "
                            "not exist",
                            svc,
                            metric,
                            type.name,
                            metric_cfg['cost_attr']
                        )
                        continue

                    attr = getattr(type, metric_cfg['cost_attr'])

                    if not attr:
                        logger.error(
                            "The service %s metric %s, type %s, "
                            "attr %s is empty",
                            svc,
                            metric,
                            type.name,
                            metric_cfg['cost_attr']
                        )
                        continue

                    if metric_cfg['cost_key']:
                        attr = yaml.safe_load(attr)
                        cost = attr[metric_cfg['cost_key']]
                    else:
                        cost = attr

                    if type.name and cost:
                        unitcost_data['data'].append({
                            "QualityOfService": type.name,
                            "CHF": float(cost)
                        })

                if not unitcost_data['data']:
                    logger.info('No data to send for unitcost on '
                                'service %s metric %s, skipping...',
                                svc, metric)
                elif 'dryrun' in metric_cfg and metric_cfg['dryrun']:
                    logger.info('Skip sending data for service %s '
                                'metric %s', svc, metric)
                else:
                    data.append(unitcost_data)

            if not data:
                logger.info('No data to send for unitcost on service %s '
                            'skipping...', svc)
                continue

            logger.debug('Send unit cost data for service %s', svc)
            headers = {
                "Content-Type": "application/json",
                "API-Key": CONF.dbreporter.apikeys[svc]
            }

            if len(data) == 1:
                data = data[0]

            if args.dryrun or ('dryrun' in CONF.dbreporter.services[svc]
                               and CONF.dbreporter.services[svc]['dryrun']):
                logger.info('Dryrun for unit cost for service %s, skipping...',
                            svc)
                continue

            try:
                response = requests.post(
                    CONF.dbreporter.unitcost_url,
                    json=data,
                    headers=headers,
                    verify=CONF.dbreporter.verify,
                    timeout=60)
                response.raise_for_status()
            except requests.exceptions.HTTPError:
                logger.error('Error while storing data')

            if response.status_code not in [200]:
                logger.error(
                    'Status code: %s. Message: %s',
                    response.status_code,
                    response.text)

        logger.info("Execution finished")


# Needs static method for setup.cfg
def main(args=None):
    AccountingUnitCost().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)
