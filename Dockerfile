FROM gitlab-registry.cern.ch/linuxsupport/cs8-base
ARG COMMIT_SHA
LABEL maintainer="Jose Castro Leon <Jose.Castro.Leon@cern.ch>"
RUN dnf install -y gcc git python3-devel openldap-devel krb5-libs krb5-devel
RUN git clone https://gitlab.cern.ch/cloud-infrastructure/cci-tools.git && cd cci-tools && git checkout -b build $COMMIT_SHA
RUN python3 -m pip install --upgrade pip && python3 -m pip install -r /cci-tools/requirements.txt && python3 -m pip install -e /cci-tools/.

RUN chmod 755 /cci-tools/ccitools/cmd/*py
ENV PYTHONPATH=/cci-tools
ENV PATH=/cci-tools/ccitools/cmd:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
