import logging
import requests_mock

from ccitools.cmd.fix_placement_allocation import main
import ccitools.conf
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase

CONF = ccitools.conf.CONF


@mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
@mock.patch('ccitools.utils.cloud.cloud_config')
@requests_mock.Mocker()
class TestFixPlacementAllocation(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self, mock_cc, mock_crc, mock_req):
        mock_req.get(
            CONF.es.server + '/monit_private_openstack_logs_generic-*/_search',
            text=fixtures.PLACEMENT_ALLOCATION)
        mock_crc.return_value = mock_crc
        mock_crc.get_server.return_value = None

        main([])

        mock_crc.get_server.assert_called_with(
            'd92a5cdf-d8d0-4ac3-8911-ca595e44b38c')

    def test_dry_run(self, mock_cc, mock_crc, mock_req):
        mock_req.get(
            CONF.es.server + '/monit_private_openstack_logs_generic-*/_search',
            text=fixtures.PLACEMENT_ALLOCATION)
        mock_crc.return_value = mock_crc
        mock_crc.get_server.return_value = None

        main(['--dryrun'])

        mock_crc.get_server.assert_called_with(
            'd92a5cdf-d8d0-4ac3-8911-ca595e44b38c')

    def test_server_migrating(self, mock_cc, mock_crc, mock_req):
        mock_req.get(
            CONF.es.server + '/monit_private_openstack_logs_generic-*/_search',
            text=fixtures.PLACEMENT_ALLOCATION)
        mock_crc.return_value = mock_crc
        mock_crc.get_server.return_value = fixtures.SERVER_MIGRATING

        main(['--dryrun'])

        mock_crc.get_server.assert_called_with(
            'd92a5cdf-d8d0-4ac3-8911-ca595e44b38c')
        mock_crc.get_resource_provider.assert_not_called()

    def test_server_allocations_correct(self, mock_cc, mock_crc, mock_req):
        mock_req.get(
            CONF.es.server + '/monit_private_openstack_logs_generic-*/_search',
            text=fixtures.PLACEMENT_ALLOCATION)
        mock_crc.return_value = mock_crc
        mock_crc.get_server.return_value = fixtures.SERVER
        mock_crc.get_resource_provider.return_value = {
            'uuid': 'b203eae5-7596-483b-8265-2203f990fb18'}
        mock_crc.get_allocations.return_value = fixtures.ALLOCATIONS

        main([])

        mock_crc.get_server.assert_called_with(
            'd92a5cdf-d8d0-4ac3-8911-ca595e44b38c')
        mock_crc.get_resource_provider.assert_called_with(
            'hypervisor1')
        mock_crc.get_allocations.assert_called_with(
            'd92a5cdf-d8d0-4ac3-8911-ca595e44b38c')

    def test_server_allocations_fix_dryrun(self, mock_cc, mock_crc, mock_req):
        mock_req.get(
            CONF.es.server + '/monit_private_openstack_logs_generic-*/_search',
            text=fixtures.PLACEMENT_ALLOCATION)
        mock_crc.return_value = mock_crc
        mock_crc.get_server.return_value = fixtures.SERVER
        mock_crc.get_resource_provider.return_value = {
            'uuid': 'a203eae5-7596-483b-8265-2203f990fb18'}
        mock_crc.get_allocations.return_value = fixtures.ALLOCATIONS

        main(['--dryrun'])

        mock_crc.get_server.assert_called_with(
            'd92a5cdf-d8d0-4ac3-8911-ca595e44b38c')
        mock_crc.get_resource_provider.assert_called_with(
            'hypervisor1')
        mock_crc.get_allocations.assert_called_with(
            'd92a5cdf-d8d0-4ac3-8911-ca595e44b38c')
        mock_crc.set_allocation.assert_not_called()

    def test_server_allocations_fix(self, mock_cc, mock_crc, mock_req):
        mock_req.get(
            CONF.es.server + '/monit_private_openstack_logs_generic-*/_search',
            text=fixtures.PLACEMENT_ALLOCATION)
        mock_crc.return_value = mock_crc
        mock_crc.get_server.return_value = fixtures.SERVER
        mock_crc.get_resource_provider.return_value = {
            'uuid': 'a203eae5-7596-483b-8265-2203f990fb18'}
        mock_crc.get_allocations.return_value = fixtures.ALLOCATIONS

        main([])

        mock_crc.get_server.assert_called_with(
            'd92a5cdf-d8d0-4ac3-8911-ca595e44b38c')
        mock_crc.get_resource_provider.assert_called_with(
            'hypervisor1')
        mock_crc.get_allocations.assert_called_with(
            'd92a5cdf-d8d0-4ac3-8911-ca595e44b38c')
        mock_crc.set_allocation.assert_called_with(
            'd92a5cdf-d8d0-4ac3-8911-ca595e44b38c',
            'a203eae5-7596-483b-8265-2203f990fb18',
            4,
            7500,
            40,
            'c34de133-1e4c-4a7f-b7b1-97fadb07ad61',
            'svcprobe',
            'regionone'
        )
