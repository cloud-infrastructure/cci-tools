import logging

from unittest import mock
from unittest import TestCase


@mock.patch('aitools.foreman.ForemanClient')
@mock.patch('ccitools.utils.servicenowv2.ServiceNowClient')
@mock.patch('ccitools.utils.sendmail.MailClient')
@mock.patch('ccitools.common.ssh_executor')
@mock.patch('ccitools.common.ping')
class TestHealthReportPuppet(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

        self.aitools_mock = mock.MagicMock()
        modules = {
            'aitools': self.aitools_mock,
            'aitools.foreman': self.aitools_mock.foreman,
        }
        self.patcher = mock.patch.dict('sys.modules', modules)
        self.patcher.start()
        self.addCleanup(self.patcher.stop)

    def test_default_args(self, mock_ping, mock_ssh, mock_mc, mock_snc,
                          mock_fc):
        from ccitools.cmd.health_report_puppet import main

        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)
        mock_snc.assert_not_called()

    def test_data_no_cloud_nor_spare(self, mock_ping, mock_ssh, mock_mc,
                                     mock_snc, mock_fc):
        from ccitools.cmd.health_report_puppet import main

        mock_fc.return_value.search_query.return_value = [{
            'name': 'fake_n.cern.ch',
            'hostgroup_title': 'fake_hn'
        }]

        main([
            '--threshold', '24',
            '--instance', 'cern',
            '--mail-to', 'fake_user',
        ])

        mock_snc.assert_called_with(instance='cern')
        mock_ssh.assert_not_called()

    def test_data_cloud_and_error(self, mock_ping, mock_ssh, mock_mc,
                                  mock_snc, mock_fc):
        from ccitools.cmd.health_report_puppet import main

        mock_fc.return_value.search_query.return_value = [{
            'name': 'fake_n.cern.ch',
            'hostgroup_title': 'cloud',
            'last_report': '20-07-2019 00:00.667794+00:00'
        }]

        mock_ssh.side_effect = iter([(None, None)])

        main([
            '--threshold', '24',
            '--instance', 'cern',
            '--mail-to', 'fake_user',
        ])

        mock_snc.assert_called_with(instance='cern')
        mock_ssh.assert_called_with(
            'fake_n',
            'test -f /opt/puppetlabs/puppet/cache/state/agent_disabled.lock'
            ' && cat /opt/puppetlabs/puppet/cache/state/agent_disabled.lock',
            connect_timeout=5,
            keep_alive_interval=2
        )
        mock_ping.assert_not_called()
        mock_snc.return_value.ticket.raw_query_incidents.assert_called_with(
            'active=true^stateNOT IN7,6^u_configuration_items_listLIKEfake_n'
        )

    def test_data_cloud_and_disabled(self, mock_ping, mock_ssh, mock_mc,
                                     mock_snc, mock_fc):
        from ccitools.cmd.health_report_puppet import main

        mock_fc.return_value.search_query.return_value = [{
            'name': 'fake_n.cern.ch',
            'hostgroup_title': 'cloud',
            'last_report': '20-07-2019 00:00.667794+00:00'
        }]

        mock_ssh.side_effect = iter([('{"disabled_message":"Testing"}', None)])

        main([
            '--threshold', '24',
            '--instance', 'cern',
            '--mail-to', 'fake_user',
        ])

        mock_snc.assert_called_with(instance='cern')
        mock_ssh.assert_called_with(
            'fake_n',
            'test -f /opt/puppetlabs/puppet/cache/state/agent_disabled.lock'
            ' && cat /opt/puppetlabs/puppet/cache/state/agent_disabled.lock',
            connect_timeout=5,
            keep_alive_interval=2
        )
        mock_ping.assert_not_called()
        mock_snc.return_value.ticket.raw_query_incidents.assert_called_with(
            'active=true^stateNOT IN7,6^u_configuration_items_listLIKEfake_n'
        )

    def test_data_cloud_and_pingable(self, mock_ping, mock_ssh, mock_mc,
                                     mock_snc, mock_fc):
        from ccitools.cmd.health_report_puppet import main

        mock_fc.return_value.search_query.return_value = [{
            'name': 'fake_n.cern.ch',
            'hostgroup_title': 'cloud',
            'last_report': '20-07-2019 00:00.667794+00:00'
        }]
        mock_ssh.side_effect = iter([Exception])
        mock_ping.return_value = True

        main([
            '--threshold', '24',
            '--instance', 'cern',
            '--mail-to', 'fake_user',
        ])

        mock_snc.assert_called_with(instance='cern')
        mock_ssh.assert_called_with(
            'fake_n',
            'test -f /opt/puppetlabs/puppet/cache/state/agent_disabled.lock'
            ' && cat /opt/puppetlabs/puppet/cache/state/agent_disabled.lock',
            connect_timeout=5,
            keep_alive_interval=2
        )
        mock_ping.assert_called_with('fake_n')
        mock_snc.return_value.ticket.raw_query_incidents.assert_called_with(
            'active=true^stateNOT IN7,6^u_configuration_items_listLIKEfake_n'
        )

    def test_data_cloud_and_unpingable(self, mock_ping, mock_ssh, mock_mc,
                                       mock_snc, mock_fc):
        from ccitools.cmd.health_report_puppet import main

        mock_fc.return_value.search_query.return_value = [{
            'name': 'fake_n.cern.ch',
            'hostgroup_title': 'cloud',
            'last_report': '20-07-2019 00:00.667794+00:00'
        }]
        mock_ssh.side_effect = iter([Exception])
        mock_ping.return_value = False

        main([
            '--threshold', '24',
            '--instance', 'cern',
            '--mail-to', 'fake_user',
        ])

        mock_snc.assert_called_with(instance='cern')
        mock_ssh.assert_called_with(
            'fake_n',
            'test -f /opt/puppetlabs/puppet/cache/state/agent_disabled.lock'
            ' && cat /opt/puppetlabs/puppet/cache/state/agent_disabled.lock',
            connect_timeout=5,
            keep_alive_interval=2
        )
        mock_ping.assert_called_with('fake_n')
        mock_snc.return_value.ticket.raw_query_incidents.assert_called_with(
            'active=true^stateNOT IN7,6^u_configuration_items_listLIKEfake_n'
        )
