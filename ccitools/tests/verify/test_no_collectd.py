import logging

from ccitools.errors import SSHException
from ccitools.verify.no_collectd import NoCollectdAction
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.verify.no_collectd.ssh_executor')
class TestNoCollectdAction(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.client = NoCollectdAction(host='fake.domain')

    def test_try_fix(self, mock_service):
        self.client.try_fix()
        mock_service.assert_called_with(
            'fake.domain',
            ('systemctl restart collectd')
        )

    def test_try_fix_raise(self, mock_service):
        mock_service.side_effect = iter([SSHException()])
        self.client.try_fix()

    def test_check_alarm_state(self, mock_common):
        mock_common.return_value = [], None
        self.assertTrue(self.client.check_alarm_state())

        mock_common.return_value = ['running'], None
        self.assertFalse(self.client.check_alarm_state())

        mock_common.side_effect = iter([SSHException('Fake')])
        self.assertTrue(self.client.check_alarm_state())
