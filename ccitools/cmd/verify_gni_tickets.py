#!/usr/bin/python3

import getpass
import logging
import re
import socket

from ccitools.cmd import base
from ccitools import common
from ccitools.utils.servicenowv2 import ServiceNowClient
from stevedore import driver

logger = logging.getLogger(__name__)


class VerifyGNITicketsCMD(base.BaseCMD):

    def __init__(self):
        super(VerifyGNITicketsCMD, self).__init__(
            description="Identify and close automatically "
                        "GNI tickets")

    def check_host_exists(self, host):
        try:
            logger.info("Checking existance of server %s...", host)
            address = socket.gethostbyname(host)
            logger.info("Found host %s with IPv4 address %s", host, address)
            return True

        except socket.gaierror:
            logger.error("Machine %s does not exist.", host)
            return False

        except Exception:
            logger.warning("Unknown error trying to get machine %s from DNS. "
                           "Let's assume it exists.", host)
            return True

    def main_verify_gni_tickets(self, assignee, group, user, instance,
                                reference):

        snowclient = ServiceNowClient(instance=instance)

        ag_id = snowclient.assignment_group.get_assignment_group_by_name(
            group).sys_id
        user_id = snowclient.user.get_user_info_by_user_name(user).sys_id
        query = ("assignment_group={gid}"
                 "^active=true^state NOT IN4,3,7,6"
                 "^sys_created_by=gniweb"
                 "^assigned_toISEMPTY^ORassigned_to={uid}").format(
                     gid=ag_id, uid=user_id)

        for raw_ticket in snowclient.ticket.raw_query_incidents(query):

            KO_hosts = []
            message = ""
            alarm_type = raw_ticket.u_alarms_list
            hosts = raw_ticket.u_configuration_items_list.split(',')
            # Grafana alerts have always the producer in the description of
            # the ticket
            match = re.findall(r'Producer: [\w-]*', raw_ticket.description)
            producer = match[0].split(' ')[-1] if match else 'none'

            if producer != 'monit-grafana':
                logger.info("Getting info from: https://%s.service-now.com"
                            "?id=ticket&n=%s",
                            instance, raw_ticket.number)

                logger.info("Event reported on '%s' (%s) has been associated "
                            "with %s different hosts (%s)", raw_ticket.number,
                            alarm_type, len(hosts), ', '.join(hosts))

                for host in hosts:
                    try:
                        host = common.normalize_fqdn(host)
                        logger.info("Checking exceptions on '%s'", host)
                        if self.check_host_exists(host):
                            mgr = driver.DriverManager(
                                namespace='ccitools.verify_actions',
                                name=alarm_type,
                                invoke_on_load=True,
                                invoke_args=(host,),
                            )
                            if mgr.driver.check_alarm_state(False):
                                logger.info(
                                    "Alarm still present on '%s'", host)

                                # Try to fix the error
                                mgr.driver.try_fix()

                                if mgr.driver.check_alarm_state(True):
                                    reason = mgr.driver.get_reason()
                                    logger.info(reason)
                                    message += reason
                                    KO_hosts.append(host)
                            else:
                                logger.info(
                                    "collectdctl did not find any alarm"
                                    " '%s' for '%s'", alarm_type, host)

                    except Exception as ex:
                        logger.error(ex)
                        message += " - %s\n" % ex
                        KO_hosts.append(host)

                ######################################################
                # Hacking: The following line recreates
                # the SNOW client session for every ticket of the list.
                # The reason to this is that the checking proccess on
                # the hosts may take too much time, which makes the
                # SNOW client session stop before finishing the script,
                # and therefore failing in the execution of the job.
                # More information at OS-5470
                ######################################################
                logger.info("Recreating session for SNOW client...")
                snowclient = ServiceNowClient(instance=instance)
                ticket = snowclient.ticket.get_ticket(raw_ticket.number)

                if len(KO_hosts) == 0:
                    comments = ticket.get_comments()
                    resolve = True
                    if comments:
                        for comment in comments:
                            if comment['sys_created_by'] not in [
                                    'gniweb', 'svcrdeck']:
                                resolve = False
                    if resolve:
                        logger.info(
                            "There are no hosts still affected from '%s'."
                            " Resolving ticket %s...", alarm_type,
                            ticket.info.number)
                        ticket.resolve("No hosts affected anymore (%s)."
                                       "\nClosing." % reference, "svcrdeck")
                    else:
                        logger.info("Discussion detected on the ticket. "
                                    "I'll leave the ticket opened :)")
                else:
                    if assignee == 'svcrdeck':
                        logger.info(
                            "There are still affected hosts but I'm just a"
                            " computer and I cannot decide what to do. "
                            "I'll leave the ticket opened :)")
                    else:
                        logger.info("There are still affected hosts. "
                                    "Adding worknote to ticket %s...",
                                    ticket.info.number)
                        ticket.add_work_note("List of hosts still affected:"
                                             "\n%s\n%s" % (
                                                 message, reference))
                        ticket.change_assignee(assignee)
                ticket.save()
                logger.info("Done\n")
            else:
                logger.info("Cannot perform actions on GNI ticket"
                            "with host field set to 'UNKNOWN'")

    def main(self, args=None):
        self.parser.add_argument("--assignee", default="",
                                 help="User to assign the tickets "
                                 "to after processing them")
        self.parser.add_argument("--instance", default="cern",
                                 help="Service now instance")
        self.parser.add_argument("--user",
                                 default="svcrdeck",
                                 help="Rundeck account")
        self.parser.add_argument("--group",
                                 default="Cloud Infrastructure 3rd "
                                 "Line Support",
                                 help="Assignment Group")
        self.parser.add_argument("--reference",
                                 help="Execution reference to be added"
                                 " to the ticket",
                                 default="Executed by '%s' on '%s'" % (
                                     getpass.getuser(), socket.getfqdn()))

        args = self.parse_args(args)

        self.main_verify_gni_tickets(
            assignee=args.assignee,
            group=args.group,
            user=args.user,
            instance=args.instance,
            reference=args.reference)


# Needs static method for setup.cfg
def main(args=None):
    VerifyGNITicketsCMD().main(args)


if __name__ == "__main__":
    main()
