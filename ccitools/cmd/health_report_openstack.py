#!/usr/bin/python3

import ccitools.conf
import datetime
import logging
import os
import prettytable
import pytz
import signal
import socket
import subprocess  # nosec
import sys
import time

from ccitools.cmd.base.cloud import BaseCloudCMD
from dateutil.parser import parse

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


TABLE_FIELDS = ["vm", "hypervisor", "status", "task", "state",
                "since", "user_id", "deletable", "reason", "error"]
TABLE_DEFAULT = ["vm", "status", "task", "since",
                 "user_id", "deletable", "reason"]
SORT_DEFAULT = "since"


class HealthReportCMD(BaseCloudCMD):
    def __init__(self):
        super(HealthReportCMD, self).__init__(
            description="Command-line interface to generate "
                        "standard reports of the cloud")

    def wait(self, process, timeout):
        start = datetime.datetime.now()
        while process.poll() is None:
            time.sleep(0.1)
            now = datetime.datetime.now()
            if (now - start).seconds > timeout:
                os.kill(process.pid, signal.SIGKILL)
                os.waitpid(-1, os.WNOHANG)
                return True, process.stdout.read(), process.stderr.read()
        return False, process.stdout.read(), process.stderr.read()

    def check(self, vm, test_ssh):
        """Check the status of a virtual machine for deletion.

        :vm: instance object containing the VM details
        """
        if not getattr(vm, 'OS-EXT-SRV-ATTR:host'):
            return True, "No hypervisor assigned"
        if len(vm.addresses) == 0:
            return True, "No addresses assigned to the instance"

        try:
            socket.gethostbyname(vm.name)
        except socket.gaierror:
            return True, "DNS cannot resolve VM name"

        if test_ssh:
            try:
                logger.debug("Connecting via ssh to %s to check %s",
                             getattr(vm, 'OS-EXT-SRV-ATTR:host'), vm.name)
                popen_args = ['ssh', "-o", "StrictHostKeyChecking=no",
                              "-o", "UserKnownHostsFile=/dev/null",
                              "-o", "ConnectTimeout=5",
                              "root@%s" % (getattr(
                                  vm, 'OS-EXT-SRV-ATTR:host')),
                              "virsh list --all --uuid"]
                process = subprocess.Popen(  # nosec
                    popen_args,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE)
                timed_out, out, err = self.wait(process, 1)

                if timed_out:
                    raise Exception(
                        "Virsh via ssh timed_out, check hypervisor")
                elif process.returncode != 0:
                    raise Exception(err)

                if vm.id in out:
                    return False, "It's running in the hypervisor"
                else:
                    return True, "It's not running in the hypervisor"
            except Exception as ex:
                logger.warning("Cannot SSH into '%s' to check '%s': %s",
                               getattr(vm, 'OS-EXT-SRV-ATTR:host'), vm.name,
                               str(ex))
                return False, "Cannot ssh hypervisor. Cred? Hyper-V?"
        else:
            return False, "All OK & forced to skip SSH step"

    def __relevant(self, vm, update_timedelta, deletable):
        """Return a boolean representing whether the VM is relevant."""
        if vm.status == 'BUILD' and (
            update_timedelta.days == 0
            and update_timedelta.seconds < 3600
        ):
            return False
        else:
            return True

    def vms(self, args, cloud):
        """Create a new report of the machines in error state.

        Checking for each VM:
        1. Is it scheduled to one hypervisor?
        2. Has the instance network information?
        3. Is it running in the assigned hypervisor?

        :cloud: cloud client
        """
        table = prettytable.PrettyTable(TABLE_FIELDS)

        error = cloud.get_servers(status='error')
        logger.info("%s machines found in ERROR state in all tenants",
                    len(error))
        build = cloud.get_servers(status='build')
        logger.info("%s machines found in BUILD state in all tenants",
                    len(build))
        total = error + build

        for vm in total:
            deletable, reason = self.check(vm, args.no_ssh)
            timedelta_update = (pytz.utc.localize(datetime.datetime.now())
                                - parse(vm.updated))
            fault_message = '-'
            if hasattr(vm, 'fault'):
                fault_message = vm.fault['message']
            if self.__relevant(vm, timedelta_update, deletable):
                table.add_row([vm.name,
                               getattr(vm, 'OS-EXT-SRV-ATTR:host'),
                               vm.status,
                               getattr(vm, 'OS-EXT-STS:task_state'),
                               getattr(vm, 'OS-EXT-STS:vm_state'),
                               timedelta_update.days,
                               vm.user_id,
                               deletable,
                               reason,
                               fault_message])

        table.border = True
        table.align["Field"] = 'r'
        table.align["Value"] = 'l'

        fields = args.fields
        if logger.getEffectiveLevel() == logging.DEBUG:
            fields = TABLE_FIELDS

        print(table.get_string(sortby=args.sort_by,
                               fields=fields,
                               reversesort=True))

    def volumes(self, args, cloud):
        """Implement 'volumes' subcommand.

        It queries the Cloud looking for volumes in these cases:
           1. In error state.
           2. Marked 'in-use' but attached to non existent instances.
           3. Marked as 'available' but with attachments linked.
        """
        error = cloud.get_volumes(status='error')
        in_use = cloud.get_volumes(status='in-use')
        available = cloud.get_volumes(status='available')

        table = prettytable.PrettyTable(["Volume", "Status", "Highlights"])
        for vol in error:
            table.add_row([vol.id, vol.status, "In ERROR state"])

        for vol in in_use:
            error_msg = None
            vm = cloud.get_server(vol.attachments[0]['server_id'])

            if not vm:
                error_msg = "Volume attached to non existent instance"
                table.add_row([vol.id, vol.status, error_msg])

        for vol in available:
            if vol.attachments != []:
                table.add_row([vol.id, vol.status,
                               "Volume available but contains "
                               "attachment details"])

        table.border = True
        table.align["Field"] = 'r'
        table.align["Value"] = 'l'
        print(table)

    def main(self, args=None):
        subparsers = self.parser.add_subparsers(metavar='<subcommand>')

        parser_vms = subparsers.add_parser(
            'vms', help="Print VMs in unhealthy "
                        "state and guess which one "
                        "is safe to delete.")
        parser_vms.add_argument(
            '--fields', nargs='+', metavar='FIELD',
            help='List of fields to show',
            default=TABLE_DEFAULT, choices=TABLE_FIELDS)
        parser_vms.add_argument(
            '--no-ssh', action='store_false')
        parser_vms.add_argument(
            '--sort-by', metavar='FIELD',
            help='Sort the table by the given field',
            default=SORT_DEFAULT, choices=TABLE_FIELDS)
        parser_vms.set_defaults(func=self.vms)

        parser_volumes = subparsers.add_parser(
            'volumes', help="Print volumes in unhealthy state")
        parser_volumes.set_defaults(func=self.volumes)

        args = self.parse_args(args)

        try:
            call = args.func
        except AttributeError:
            self.parser.error("Too few arguments")

        call(args, self.cloudclient)


# Needs static method for setup.cfg
def main(args=None):
    HealthReportCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
