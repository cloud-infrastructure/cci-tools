from ccitools import common
from zeep.client import Client


class FIMClient(object):

    def __init__(self, url):
        transport = common.get_transport(url, fim=True)
        self.client = Client(url, transport=transport).service

    def create_project(self, name, description, owner, shared=True):
        """Create a new OpenStack Project.

        :param name: Project name
        :param description: Project description
        :param owner: Project owner
        :param shared: boolean
        """
        return self.client.CreateProject(
            name=name,
            description=description,
            owner=owner,
            shared=shared)

    def delete_project(self, name):
        """Delete an existing OpenStack Project.

        :param name: Project name
        """
        return self.client.DeleteProject(
            name=name)

    def set_project_name(self, name, new_name):
        """Set Name in FIM for a project.

        :param name: Project name
        :param new_name: new name of the project
        """
        return self.client.SetProjectName(
            name=name,
            newName=new_name)

    def set_project_description(self, name, desc):
        """Set Name in FIM for a project.

        :param name: Project name
        :param desc: new description of the project
        """
        return self.client.SetProjectDescription(
            name=name,
            newDescription=desc)

    def set_project_owner(self, name, owner):
        """Set Owner in FIM for a project.

        :param name: Project name
        :param owner: new owner of the project
        """
        return self.client.SetProjectOwner(
            name=name,
            newOwner=owner)

    def set_project_status(self, name, enabled):
        """Set Name in FIM for a project.

        :param name: Project name
        :param enabled: new status of the project
        """
        return self.client.SetProjectStatus(
            name=name,
            enabled=enabled)

    def is_valid_owner(self, username):
        """Check if user is a valid OpenStack Project owner.

        :param username: User name
        """
        return self.client.ValidateOwner(
            username)


class FIMCodes(object):
    SUCCESS = 0
    ERROR_INVALID_PARAMS = 1
    ERROR_NOT_SUSCRIBED = 2
    ERROR_PROJECT_EXISTS = 3
    INTERNAL_ERROR = -1
