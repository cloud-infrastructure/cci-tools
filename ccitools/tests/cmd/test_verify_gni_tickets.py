import logging
import socket

from ccitools.cmd.verify_gni_tickets import main
from ccitools.cmd.verify_gni_tickets import VerifyGNITicketsCMD
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase


class TestVerifyGNITickets(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch("ccitools.cmd.verify_gni_tickets.socket.gethostbyname",
                return_value='0.0.0.0')
    def test_check_host_exists(self, mock_sock):
        self.assertTrue(
            VerifyGNITicketsCMD().check_host_exists('fake'))

    @mock.patch("ccitools.cmd.verify_gni_tickets.socket.gethostbyname",
                side_effect=iter([socket.gaierror('')]))
    def test_check_host_exists_raise_error(self, mock_sock):
        self.assertFalse(
            VerifyGNITicketsCMD().check_host_exists('fake'))

    @mock.patch("ccitools.cmd.verify_gni_tickets.socket.gethostbyname",
                side_effect=iter([Exception("Fake")]))
    def test_check_host_exists_raise_exception(self, mock_sock):
        self.assertTrue(
            VerifyGNITicketsCMD().check_host_exists('fake'))

    @mock.patch('ccitools.cmd.verify_gni_tickets.VerifyGNITicketsCMD'
                '.check_host_exists', return_value=False)
    @mock.patch('ccitools.cmd.verify_gni_tickets.ServiceNowClient')
    def test_main_fake_action_no_args(self, mock_snow, mock_check):
        mockedSC = mock_snow.return_value

        mockedSC.ticket.raw_query_incidents.return_value = (
            fixtures.TICKETS_EMPTY
        )
        mockedTk = mockedSC.ticket.get_ticket.return_value

        mockedTk.get_comments.return_value = (
            fixtures.COMMENTS_GNIWEB
        )

        main([])

        mock_snow.assert_called_with(instance='cern')
        (mockedSC.assignment_group
         .get_assignment_group_by_name.assert_called_with(
             'Cloud Infrastructure 3rd Line Support'))
        mock_check.assert_called_with('fake.cern.ch')
        mockedSC.ticket.get_ticket.assert_called_with('INCXXXXXXX')

    @mock.patch("ccitools.cmd.verify_gni_tickets.socket.gethostbyname",
                side_effect=iter([Exception("Fake")]))
    @mock.patch('ccitools.cmd.verify_gni_tickets.ServiceNowClient')
    def test_main_fake_action_nonexisting_host(self, mock_snow, mock_check):
        mockedSC = mock_snow.return_value

        mockedSC.ticket.raw_query_incidents.return_value = (
            fixtures.TICKETS_EMPTY
        )
        mockedTk = mockedSC.ticket.get_ticket.return_value

        mockedTk.get_comments.return_value = (
            fixtures.COMMENTS_GNIWEB
        )

        main([])

        mock_snow.assert_called_with(instance='cern')
        (mockedSC.assignment_group
         .get_assignment_group_by_name.assert_called_with(
             'Cloud Infrastructure 3rd Line Support'))
        mock_check.assert_called_with('fake.cern.ch')
        mockedSC.ticket.get_ticket.assert_called_with('INCXXXXXXX')

    @mock.patch('ccitools.cmd.verify_gni_tickets.driver')
    @mock.patch("ccitools.cmd.verify_gni_tickets.socket.gethostbyname",
                return_value='0.0.0.0')
    @mock.patch('ccitools.cmd.verify_gni_tickets.ServiceNowClient')
    def test_main_fake_action_fqdn(self, mock_snow, mock_check, mock_driver):
        mockedSC = mock_snow.return_value

        mockedSC.ticket.raw_query_incidents.return_value = (
            fixtures.TICKETS_FQDN
        )
        mockedTk = mockedSC.ticket.get_ticket.return_value

        mockedTk.get_comments.return_value = (
            fixtures.COMMENTS_GNIWEB
        )

        main([])

        mock_snow.assert_called_with(instance='cern')
        (mockedSC.assignment_group
         .get_assignment_group_by_name.assert_called_with(
             'Cloud Infrastructure 3rd Line Support'))
        mock_check.assert_called_with('fake.cern.ch')
        mockedSC.ticket.get_ticket.assert_called_with('INCXXXXXXX')

        # Test_main_fake_action_args

        mockedSC = mock_snow.return_value

        mockedSC.ticket.raw_query_incidents.return_value = (
            fixtures.TICKETS_EMPTY
        )
        mockedTk = mockedSC.ticket.get_ticket.return_value

        mockedTk.get_comments.return_value = (
            fixtures.COMMENTS_GNIWEB
        )

        main([
            '--assignee', 'svcrdeck',
            '--instance', 'fake',
            '--group', 'fakegroup',
            '--reference', 'fakereference',
        ])

        mock_snow.assert_called_with(instance='fake')
        (mockedSC.assignment_group
         .get_assignment_group_by_name.assert_called_with(
             'fakegroup'))

        mock_check.assert_called_with('fake.cern.ch')
        mockedSC.ticket.get_ticket.assert_called_with('INCXXXXXXX')

        # test_main_action_no_alarm

        mockedSC = mock_snow.return_value
        mockedSC.ticket.raw_query_incidents.return_value = (
            fixtures.TICKETS_EMPTY
        )

        mockedTk = mockedSC.ticket.get_ticket.return_value
        mockedTk.get_comments.return_value = (
            fixtures.COMMENTS_GNIWEB
        )

        mockMgr = mock_driver.DriverManager.return_value
        mockMgr.driver.check_alarm_state.return_value = False

        main([])

        mock_snow.assert_called_with(instance='cern')
        (mockedSC.assignment_group
         .get_assignment_group_by_name.assert_called_with(
             'Cloud Infrastructure 3rd Line Support'))

        mock_check.assert_called_with('fake.cern.ch')
        mockedSC.ticket.get_ticket.assert_called_with('INCXXXXXXX')
        mock_driver.DriverManager.assert_called_with(
            namespace='ccitools.verify_actions',
            name='fake_alarm',
            invoke_on_load=True,
            invoke_args=('fake.cern.ch',)
        )
        mockMgr.driver.check_alarm_state.assert_called_with(False)

        # Test_main_action_fix_alarm

        mockedSC = mock_snow.return_value
        mockedSC.ticket.raw_query_incidents.return_value = (
            fixtures.TICKETS_EMPTY
        )

        mockedTk = mockedSC.ticket.get_ticket.return_value
        mockedTk.get_comments.return_value = (
            fixtures.COMMENTS_GNIWEB
        )

        mockMgr = mock_driver.DriverManager.return_value
        mockMgr.driver.check_alarm_state.side_effect = iter([
            True,
            False
        ])

        main([])

        mock_snow.assert_called_with(instance='cern')
        (mockedSC.assignment_group
         .get_assignment_group_by_name.assert_called_with(
             'Cloud Infrastructure 3rd Line Support'))

        mock_check.assert_called_with('fake.cern.ch')
        mockedSC.ticket.get_ticket.assert_called_with('INCXXXXXXX')
        mock_driver.DriverManager.assert_called_with(
            namespace='ccitools.verify_actions',
            name='fake_alarm',
            invoke_on_load=True,
            invoke_args=('fake.cern.ch',)
        )
        mockMgr.driver.check_alarm_state.assert_called_with(True)
        mockMgr.driver.try_fix.assert_called_with()

        # Test_main_action_fix_alarm_no_comments

        mockedSC = mock_snow.return_value
        mockedSC.ticket.raw_query_incidents.return_value = (
            fixtures.TICKETS_EMPTY
        )

        mockedTk = mockedSC.ticket.get_ticket.return_value
        mockedTk.get_comments.return_value = (
            fixtures.COMMENTS_EMPTY
        )

        mockMgr = mock_driver.DriverManager.return_value
        mockMgr.driver.check_alarm_state.side_effect = iter([
            True,
            False
        ])

        main([])

        mock_snow.assert_called_with(instance='cern')
        (mockedSC.assignment_group
         .get_assignment_group_by_name.assert_called_with(
             'Cloud Infrastructure 3rd Line Support'))

        mock_check.assert_called_with('fake.cern.ch')
        mockedSC.ticket.get_ticket.assert_called_with('INCXXXXXXX')
        mock_driver.DriverManager.assert_called_with(
            namespace='ccitools.verify_actions',
            name='fake_alarm',
            invoke_on_load=True,
            invoke_args=('fake.cern.ch',)
        )
        mockMgr.driver.check_alarm_state.assert_called_with(True)
        mockMgr.driver.try_fix.assert_called_with()

        # Test_main_action_fix_alarm_multiple_comments

        mockedSC = mock_snow.return_value
        mockedSC.ticket.raw_query_incidents.return_value = (
            fixtures.TICKETS_EMPTY
        )

        mockedTk = mockedSC.ticket.get_ticket.return_value
        mockedTk.get_comments.return_value = (
            fixtures.COMMENTS_MULTIPLE
        )

        mockMgr = mock_driver.DriverManager.return_value
        mockMgr.driver.check_alarm_state.side_effect = iter([
            True,
            False
        ])

        main([])

        mock_snow.assert_called_with(instance='cern')
        (mockedSC.assignment_group
         .get_assignment_group_by_name.assert_called_with(
             'Cloud Infrastructure 3rd Line Support'))

        mock_check.assert_called_with('fake.cern.ch')
        mockedSC.ticket.get_ticket.assert_called_with('INCXXXXXXX')
        mock_driver.DriverManager.assert_called_with(
            namespace='ccitools.verify_actions',
            name='fake_alarm',
            invoke_on_load=True,
            invoke_args=('fake.cern.ch',)
        )
        mockMgr.driver.check_alarm_state.assert_called_with(True)
        mockMgr.driver.try_fix.assert_called_with()

        # Test_main_action_unfixable_alarm

        mockedSC = mock_snow.return_value
        mockedSC.ticket.raw_query_incidents.return_value = (
            fixtures.TICKETS_EMPTY
        )

        mockedTk = mockedSC.ticket.get_ticket.return_value
        mockedTk.get_comments.return_value = (
            fixtures.COMMENTS_GNIWEB
        )

        mockMgr = mock_driver.DriverManager.return_value
        mockMgr.driver.check_alarm_state.side_effect = iter([
            True,
            True
        ])

        main([])

        mock_snow.assert_called_with(instance='cern')
        (mockedSC.assignment_group
         .get_assignment_group_by_name.assert_called_with(
             'Cloud Infrastructure 3rd Line Support'))

        mock_check.assert_called_with('fake.cern.ch')
        mockedSC.ticket.get_ticket.assert_called_with('INCXXXXXXX')
        mock_driver.DriverManager.assert_called_with(
            namespace='ccitools.verify_actions',
            name='fake_alarm',
            invoke_on_load=True,
            invoke_args=('fake.cern.ch',)
        )

        mockMgr.driver.check_alarm_state.assert_called_with(True)
        mockMgr.driver.try_fix.assert_called_with()
        mockMgr.driver.get_reason.assert_called_with()
