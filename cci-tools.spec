Name:       cci-tools
Version:    0.8.21
Release:    2%{?dist}
Summary:    Tools for the CERN Cloud Infrastructure team
Source0:    %{name}-%{version}.tar.gz
BuildArch:  noarch
Group:      CERN/Utilities
License:    ASL 2.0
URL:        https://gitlab.cern.ch/cloud-infrastructure/cci-tools/

BuildRequires:  python3-devel
BuildRequires:  python3-pbr
BuildRequires:  python3-setuptools
BuildRequires:  python3-oslo-config

Requires:   auth-get-sso-cookie
Requires:   python3-cinderclient
Requires:   python3-dateutil
Requires:   python3-boto
Requires:   python3-ldap
Requires:   python3-cornerstoneclient
Requires:   python3-gssapi
Requires:   python3-ironicclient
Requires:   python3-keystoneclient
Requires:   python3-magnumclient
Requires:   python3-manilaclient
Requires:   python3-novaclient
Requires:   python3-neutronclient
Requires:   python3-octaviaclient
Requires:   python3-openstackclient
Requires:   python3-oslo-config
Requires:   python3-os-client-config
Requires:   python3-osc-placement
Requires:   python3-paramiko
Requires:   python3-prettytable
Requires:   python3-scp
Requires:   python3-PyMySQL
Requires:   python3-pytz
Requires:   python3-requests
Requires:   python3-requests-kerberos
Requires:   python3-requests-oauthlib
Requires:   python3-six
Requires:   python3-sqlalchemy
Requires:   python3-swiftclient
Requires:   python3-tenacity
Requires:   python3-tooz
Requires:   python3-radosgw-admin
Requires:   python3-zeep
Requires:   python-landbclient
Requires:   python3-stomppy
Requires:   python3-gitlab

%description
Set of Python tools to be used by the Cloud Infrastructure Team

%prep
%autosetup -p1 -n %{name}-%{version}

%build
%py3_build

%install
%py3_install
%{__install} -d -m 755 %{buildroot}%{_sysconfdir}/ccitools
%{__install} -p -D -m 640 etc/ccitools.conf.sample %{buildroot}%{_sysconfdir}/ccitools/ccitools.conf
%{__install} -p -D -m 640 etc/chargegroups.json %{buildroot}%{_sysconfdir}/ccitools/chargegroups.json

%files
%defattr (-, root, root)
%{_bindir}/cci-*
%{python3_sitelib}/ccitools/
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/accounting/dbreporter.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/accounting/unitcost.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/accounting/sync.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/accounting/usage.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/assign_resource_class_ironic.py 
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/assignment_cleanup.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/check_cloud_vm_owners.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/check_date_format.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/check_disabled_nodes.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/check_node_foreman.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/check_scheduled_rundeck_jobs.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/check_nova_landb_consistency.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/clean_cattle_vms.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/clean_probe_allocation.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/cloud_dr_status.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/create_project.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/delete_hosted_vms.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/delete_project.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/dump_ticket.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/dump_record_producer.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/enable_project.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/enroll_physical_nodes.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/escalate_project.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/export_rundeck_definitions.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/export_rundeck_projects.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/fix_placement_allocation.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/foreman_credentials_sync.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/get_uptime.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/health_report_openstack.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/health_report_puppet.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/hypervisor_has_vms.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/hypervisor_state_post_reboot.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/interventions_manager.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/iterator_project.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/jira_ticket_cleanup.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/lock_unlock_hv_servers.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/locked_instances.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/manage_neutron_agent.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/manage_nova_compute.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/megabus_producer.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/migration_hostgroup.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/migration.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/multiple_recreate_parallel.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/multiple_reinstall_ironic_parallel.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/notify_anomalous_vm.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/notify_intervention.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/notify_report.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/notify_users.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/process_report.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/power_on_off_hv_servers.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/rally_cleanup.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/recreate_instance.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/reload_nodes_from_puppetdb.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/report_gitlab_mrs.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/reset_fernet_keys.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/rotate_fernet_keys.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/rundeck_filter_execution.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/rundeck_scheduler.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/s3_quota.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/send_calendar_invitation.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/sendmail.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/service_metrics.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/snowflake_reminder.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/sync_assignments.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/sync_images.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/takeover_scheduled_rundeck_jobs.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/trust_manage.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/update_fim_project.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/update_quota.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/update_ticket.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/upgrade_report.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/verify_gni_tickets.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/wait_until_machine_is_sshable.py
%attr(0755, root, root) %{python3_sitelib}/ccitools/cmd/weekly_ssh_probe_compute_nodes.py
%{python3_sitelib}/*.egg-info
%dir %attr(0755, root, root) %{_sysconfdir}/ccitools
%config(noreplace) %attr(0644, root, root) %{_sysconfdir}/ccitools/ccitools.conf
%config(noreplace) %attr(0644, root, root) %{_sysconfdir}/ccitools/chargegroups.json

%changelog
* Fri Mar 07 2025 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.21-2
- Improve notify report command to be more flexible with the attribute selection

* Thu Mar 06 2025 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.21-1
- Add commands to process reports and send notifications for those

* Tue Feb 11 2025 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.20-2
- Bugfixes in enable_project and delete_project

* Fri Feb 07 2025 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.20-1
- Add more changes for new resources portal integration
- Use id instead of primaryAccountId for users
- (origin/OS-18522) Add tag support in loadbalancers search
- Cleanup keypairs per region
- Allow to search on cleanup tags for rally cleanup
- Fix issue while printing loadbalancers
- Redo get_loadbalancers and delete_loadbalancers to loop on all regions
- Revert "[OS-18522] Make cloud LB functions honor region argument"
- [OS-18522] Make cloud LB functions honor region argument

* Wed Jan 29 2025 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.19-4
- Add filter for cells in report

* Tue Jan 28 2025 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.19-3
- Replace rundeck@noreply by noreply@cern.ch

* Thu Jan 16 2025 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.19-2
- Add scenarios for enable and delete and improve test coverage

* Tue Jan 14 2025 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.19-1
- Initial implementation of glance_sync
- Integrate with new resources portal
- Fix tests for cloud commands
- Add dependencies for requests-kerberos and requests-oauthlib

* Fri Oct 25 2024 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.18-1
- Add cci-dump-ticket command to validate snow tickets

* Mon Oct 21 2024 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.8.17-1
- Add S3 quota tag <region>_s3_quota

* Mon Sep 30 2024 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.16-1
- Fix used values on accounting report

* Fri Aug 09 2024 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.8.15-1
- Publish metrics to MONIT related to resource request workflows (!422)

* Thu Aug 08 2024 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.14-4
- Target a cloud=region for temp swift session
- Return default quota if s3 user is not initialised

* Wed Aug 07 2024 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.14-3
- Set default to 0 l7policies
- Fix octavia default quota result

* Wed Aug 07 2024 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.14-2
- Fix loadbalancer project usage
- Add missing self to date_parser function in intervention manager
- Update assign_resource_class_ironic.py

* Wed Jul 31 2024 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.8.14-1
- Fix get_regions_per_service to report not one but all (!415)
- Make addremove_* ops iterate regions (!416)
- Skip octavia quota if not available in region (!417)
- Add support for multiple Rados GW (!414)
- Report default octavia quota/usage on regions without service (!419)

* Thu Jul 18 2024 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.13-2
- Check producer in description for monit-grafana tickets

* Wed Jul 17 2024 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.13-1
- Use regions in accounting usage command
- GNI ticket cleanup also takes into account tickets assigned to rundeck account

* Mon Jul 08 2024 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.12-1
- Set some defaults for neutron current usage
- Force tenacity <= 8.2.3
- Remove backwards compatible loadbalancer for older quota projects

* Mon Jun 24 2024 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.11-2
- Use quota dictionary which neutronclient expects

* Mon Jun 24 2024 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.8.11-1
- Add neutronclient to project iterator
- Add region name to base cmd and session initialisation

* Fri Jun 21 2024 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.10-2
- Last minute fix to fetch octavia quota

* Thu Jun 20 2024 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.10-1
- Cache locally chargegroups

* Wed Jun 19 2024 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.9-1
- Add quota management for loadbalancer and network components

* Thu May 2 2024 Maryna Savchenko <maryna.savchenko@cern.ch> - 0.8.8-2
- BMC check job send email to cloud 3rd level

* Thu Mar 14 2024 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.8.8-1
- [OS-17522] Add handling of cinder-backup quotas

* Wed Mar 06 2024 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.8.7-1
- Fix handling of migrations without dates in upgrade report

* Thu Feb 29 2024 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.8.6-1
- Don't initialise Octavia quota in PDC

* Mon Feb 12 2024 Daniel Failing <daniel.failing@cern.ch> - 0.8.5-2
- Octavia quota add defaults as a factor of loadbalancer for other quota types

* Tue Feb 06 2024 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.5-1
- Octavia quota add defaults as a factor of loadbalancer for other quota types
- Disable storage services quota retrieval on pdc region

* Wed Jan 17 2024 Spyridon Trigazis <spyridon.trigazis@cern.ch> - 0.8.4-1
- Remove reference to sdn1quota

* Mon Dec 04 2023 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.3-1
- Add custom transport to digest callback in resources portal after move to new sso

* Wed Nov 15 2023 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.2-1
- Add filter to retrieve quotas per region
- Add tool to synchronize acls accross regions (one way)

* Mon Nov 13 2023 Daniel Failing <daniel.failing@cern.ch> - 0.8.1-4
- Build for zed

* Tue Nov 07 2023 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.1-3
- Adapt merge quotas based on regions
- Remove floating ips that are not supported at the moment
- Fix intervention manager to handle scheduled executions

* Fri Nov 03 2023 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.1-2
- Use volumev3 to do operations with cinder volumes

* Fri Nov 03 2023 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.1-1
- Build only on el9
- Use octavia client for loadbalancer quota and usage
- Rename helper functions
- Adapt tox to generate the config file
- Adapt output to octavia quota output
- Adapt codebase for multiple regions

* Mon Oct 9 2023 Maryna Savchenko <maryna.savchenko@cern.ch> - 0.8.0-2
- Adding script for generating ironic resource class

* Thu Sep 28 2023 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.8.0-1
- Yoga only version of ccitools
- Ignore non-existing projects in usage calculation
- Add date_parser test && fix for ambiguous case
- Fix exporting of rundeck definitions after upgrade to 4.2.16

* Wed Sep 13 2023 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.89-1
- Add report to look for MRs in the cci groups

* Mon Sep 11 2023 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.88-7
- Remove isoformat from dates parsed by rundeck_date_parser
- Change accounting usage mail to html

* Wed Aug 30 2023 Domingo Rivera Barros <driverab@cern.ch> - 0.7.88-6
- OS-16879 BMC: assign the ticket to the repair team

* Thu Aug 10 2023 Domingo Rivera Barros <driverab@cern.ch> - 0.7.88-5
- Remove all_tenants parameter not supported by Octavia
- Add missing cloud override

* Thu Aug 10 2023 Domingo Rivera Barros <driverab@cern.ch> - 0.7.88-4
- Use cloud probe context to get and delete keypairs

* Tue Aug 08 2023 Domingo Rivera Barros <driverab@cern.ch> - 0.7.88-3
- Add command to fetch usage from influxdb

* Tue Aug 08 2023 Domingo Rivera Barros <driverab@cern.ch> - 0.7.88-2
- Use tag cci-utils for victoria

* Tue Aug 08 2023 Domingo Rivera Barros <driverab@cern.ch> - 0.7.88-1
- Add tag for victoria and 8s
- Replace stompest by stomppy
- Replace retry by tenacity.retry
- Remove unused cernservicexml
- Replacing arrow by dateutil
- Rebuild for 9 and yoga

* Tue Aug 01 2023 Domingo Rivera Barros <driverab@cern.ch> - 0.7.87-3
- BMC_check: threshold for the max tickets+check all active tickets
- Fixes in cleanup keypairs and lbs
- Add tests for Keypair and Loadbalancer cleanup
- OS-16821: Add loadbalancers to Rally cleanup script
- OS-16304: Cleanup old keypairs created by rally

* Fri Jul 07 2023 Domingo Rivera Barros <driverab@cern.ch> - 0.7.87-2
- Add function to check if user has a given role in a project

* Mon Jul 03 2023 Domingo Rivera Barros <driverab@cern.ch> - 0.7.87-1
- Delete br html tags not needed anymore
- Make allocation cleanup region aware
- Allow cleanup of allocations

* Fri May 26 2023 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.7.86-1
- Generate update report using an initial campaign date rather than fix time

* Mon May 15 2023 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.7.85-1
- Dereference foreman alias to use alias endpoint
- Add cci-upgrade-report script

* Thu May 04 2023 Domingo Rivera Barros <driverab@cern.ch> - 0.7.84-3
- Add scripts to clean up migration_failed or orphaned allocations
- Add back coverage report into the pipeline

* Fri Apr 28 2023 Maryna Savchenko <maryna.savchenko@cern.ch> - 0.7.84-2
- Ironic IPMI check improvement: open tickets for the cloud team
- OS-15869: Open a ticker for CF in case of BMC connectivity issues

* Tue Mar 21 2023 Domingo Rivera Barros <driverab@cern.ch> - 0.7.84-1
- Increase enable project timeout to 3h
- OS-16624: Add job to reinstall Ironic nodes in parallel

* Wed Mar 15 2023 Domingo Rivera Barros <driverab@cern.ch> - 0.7.83-6
- Add list of regions to be ignored while fetching quota information

* Fri Mar 10 2023 Domingo Rivera Barros <driverab@cern.ch> - 0.7.83-5
- Add cern_get_sso_cookie needed for fim

* Thu Mar 09 2023 Domingo Rivera Barros <driverab@cern.ch> - 0.7.83-4
- Full replacement of cern-get-sso-cookie by auth-get-sso-cookie

* Tue Mar 07 2023 Domingo Rivera Barros <driverab@cern.ch> - 0.7.83-3
- Remove extra calls to get_shares as there is no cleanup on shares through magnum
- Remove magnum tests in rally_cleanup
- Remove magnum cleanup for rally in cci-tools

* Wed Feb 15 2023 Domingo Rivera Barros <driverab@cern.ch> - 0.7.83-2
- Increase fim_timeout and fim_retry_interval
- Move build to wallaby release

* Wed Feb 08 2023 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.83-1
- Rebuild for rhel8 and alma8
- Add suds into requirements, needed by landbclient
- Add io2/io3 as valid volume types for recreation

* Wed Nov 23 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.82-5
- Empty quotas by default in get_project_quota

* Tue Nov 15 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.82-4
- Delete unused variable

* Fri Nov 11 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.82-3
- Fix megabus

* Thu Nov 03 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.82-2
- Minor fixes megabus producer

* Mon Oct 24 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.82-1
- Increase retry timeout and interval
- OS-16011: Send notification to megabus when an intervention is cancelled

* Thu Sep 22 2022 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.81-5
- Add ide as boot device for volumes

* Thu Sep 22 2022 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.81-4
- Fix issue with machines using ide as boot disk for volumes

* Tue Sep 20 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.81-3
- Add retries to get_soap_client function
- Increase timeout to create snapshot to 2 days

* Mon Sep 19 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.81-2
- Apply temporary fix for the image min_disk bigger than flavor disk size
- Fix get_preserve that is not showing the real value of the entry

* Fri Sep 09 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.81-1
- Mock tests to be run outside CERN network while accessing LanDB
- Move preserve port logic after and fine tune it a bit
- Fix bug when adding to watchlist
- Add handling of no_collectd alarm

* Wed Aug 10 2022 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.7.80-4
- Updated mail subject for recreation notification emails

* Tue Aug 02 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.80-3
- Fix in recreate_instance

* Mon Aug 01 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.80-2
- OS-16104: Precheck existence of landb-mainuser and landb-responsible in bunny-hop

* Fri Jul 29 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.80-1
- Add script to create a snowflake rota reminder

* Thu Jul 28 2022 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.79-7
- Fetch the new VM after creation within project context
- Proper handle multiple aliases

* Wed Jul 27 2022 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.79-6
- Delete the landb-alias property if it's a loadbalanced dns alias to prevent issues while creation

* Wed Jul 20 2022 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.79-5
- Add missing section mail in configuration

* Mon Jul 18 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.79-4
- Add mail to configuration

* Mon Jul 11 2022 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.79-3
- Add path information to allow us to release locks on demand

* Fri Jul 08 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.79-2
- Add kazoo in requirements.txt

* Fri Jul 08 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.79-1
- Add distributed locking to protect race conditions on host and/or project
- OS-16049: Add script rundeck-filter-execution
- Modify verify_gni_tickets to avoid closing of Grafana alarms

* Fri Jul 01 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.78-1
- Save incident
- Add role coordinator for experiments on project creation
- Do not set landb properties if they are already set in the project metadata
- Move exclusive lock internally and use a different timeout for wait for exclusive lock

* Fri Jun 24 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.77-1
- Add some more logging fixes

* Fri Jun 24 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.76-1
- Move exclusive lock to whole procedure
- Several fixes on test coverage
- Add exclusive lock for recreations that can be activated from the command line
- Minor fix in error message
- Ensure that the volumes are not flagged to be deleted on termination
- Wait until the machine has been deleted to continue with the creation
- Fix logic on skip_volume_snapshot
- Unset creation properties after landb-skip
- Skip creation for volume snapshot in recreation campaign
- Add argument to force the job execution
- Fix bug getting ticket data

* Tue Jun 21 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.75-1
- Add coordinator egroup to watchlist on quota update

* Wed Jun 15 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.74-1
- Add check nova landb consistency out of landbclient operations
- OS-15935: Check if there is any open ticket notifying anomaly in the VM

* Thu Jun 09 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.73-1
- OS-15935: Support egroups as vm responsible
- OS-15935: Edit message
- OS-15935: Move INC URL to config

* Thu May 19 2022 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.72-1
- Last minute fix on nova db manipulation
- Add extra handling to set preserve_on_delete to False on recreated machines

* Wed May 18 2022 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.71-1
- Extra fixes for preserve, protect and notify on recreate commands

* Wed May 18 2022 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.70-1
- Bring back ipv6 on the create port and fix issue with cell unmapping

* Tue May 17 2022 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.69-1
- Add pipeline to create docker images

* Mon May 16 2022 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.68-1
- Enhancements on recreate instance

* Mon May 16 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.67-1
- Fix bug when getting servicenow caller

* Fri May 13 2022 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.66-1
- Add dockerfile to build container image
- Add ipv6 and device_owner in port creation

* Thu May 12 2022 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.65-1
- Fix for wait while sched in recreate_vm
- Handle port and nics properties on create_server
- Save old property in case of recreating a project mapped already to a cell
- Set cern-landbskipdevice, ignore az when landb-skip
- Fix issues found when debugging recreate command on nova-network to neutron cells
- Add role for port creation on dedicated project
- Use dict for create_port body
- Fix tests on create_port
- Fix issue with port creation in recreate command

* Mon May 09 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.64-1
- Fix pep8 errors
- OS-15973: Add tests for new alarms
- OS-15973: Add action for systemd_state alarm
- OS-15973: Add action for yum_broken_repos alarm
- OS-15973: Add action for puppet_agent_cannot_compile alarm

* Mon Apr 25 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.63-2
- Fixes in notify_anomalous_vm

* Mon Apr 25 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.63-1
- OS-15935: Improvements in job to notify anomalous VMs

* Fri Apr 22 2022 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.62-1
- Default recreate command to use same specs as source machine for recreation

* Mon Apr 04 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.61-1
- OS-15808: Fix set_project_property
- Fix bug on recreate_instance regarding current tenant of instance

* Wed Mar 30 2022 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.60-1
- Create ticket for CF in case a BMC interface is not reachable
- Remove duplicate executions in intervention list

* Fri Mar 18 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.59-1
- OS-15597: Fixes in notify anomalous vms job

* Thu Mar 17 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.58-1
- OS-15597: Add job to notify users about Anomalous VMs behaviour

* Wed Mar 16 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.57-1
- Fix bug in add_project_property and remove_project_property

* Mon Mar 07 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.56-1
- OS-15679: Fix cells mapping project property

* Fri Feb 25 2022 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.55-1
- Add landb-skip in recreate_instance
- Add wait while scheduling after project_mapping in recreate_instance

* Wed Feb 16 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.54-1
- Remove extra parameter from remove property call
- Add coverage for create_port function in cloud utils
- Adding create_port flow in recreate vm
- Add extra params to help in nova-network to neutron migration

* Fri Feb 04 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.53-1
- Fix flavor mapping

* Thu Feb 03 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.52-1
- Query usage via loadbalancer get and floating ip get
- OS-15463: Apply ram quota update when value is 0

* Thu Jan 27 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.51-2
- Allow mapping of multiple flavors in create_project

* Fri Jan 21 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.51-1
- OS-15348: Add mapping to flavors for project creation job

* Thu Jan 13 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.50-2
- Fix issues while testing on the field

* Wed Jan 12 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.50-1
- Move more hardcoded settings into configuration
- Move extensions to configuration
- Move hardcoded cleanup flows to extensions
- Split business logic into specific classes for each resource type

* Mon Jan 10 2022 Domingo Rivera Barros <driverab@cern.ch> - 0.7.49-1
- OS-15325: Add handling of systemd_needreload GNI alarm

* Thu Dec 09 2021 Domingo Rivera Barros <driverab@cern.ch> - 0.7.48-2
- Several bugfixes on recreate command

* Wed Dec 08 2021 Domingo Rivera Barros <driverab@cern.ch> - 0.7.48-1
- Adapt cell mapping for any kind of cell in create_project
- Add volume checks

* Thu Dec 02 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.47-1
- Increase timeouts on recreate instance for really big instances
- Add retries while fetching the template from gitlab

* Thu Nov 18 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.46-1
- Change name of tag
- Tag projects mapped to baremetal cells

* Tue Nov 16 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.45-1
- Enable share usage reporting for Ceph Service

* Tue Nov 16 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.44-3
- Fix bug on multiple types for unitcost

* Tue Nov 16 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.44-2
- Refactor for multimetric flow and single post per service
- Simplify configuration setting for apikeys

* Mon Nov 15 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.44-1
- Implement command to report unitcost dynamically
- Remove artifacts for transition to global rp
- Improve metadata of python package

* Mon Nov 08 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.43-2
- Disable multi metric until accounting service allows it
- Fix compatibility issues running test framework on py3.10
- Add missing metric in multi metric setup

* Mon Nov 08 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.43-1
- Fix issues with volume creation in recreate command
- Add mapping to baremetal cells in project creation from snow
- Enable FileShare metrics
- Add support for multi metric push

* Fri Oct 29 2021 Domingo Rivera Barros <driverab@cern.ch> - 0.7.42-1
- Add args map_baremetal and baremetal_cell

* Thu Oct 21 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.41-1
- Review network speed action

* Tue Oct 19 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.40-4
- Add quota and flavor checks to the precheck phase
- Include full hostgroup path in weekly report

* Wed Oct 13 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.40-3
- Use hostgroup title after foreman api change
- Change logic for puppet agent GNI errors

* Tue Oct 12 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.40-2
- Change fields in report for ssh_probe

* Fri Oct 08 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.40-1
- Add hv filter type in ssh_probe command
- Send db reports splitted by type
- Remove rundeck project/command mapping

* Wed Oct 06 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.39-1
- Improve weekly report
- wait until machine is stopped to SHUTOFF

* Mon Oct 04 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.38-1
- Set chargerole to default on newly created projects

* Fri Oct 01 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.37-2
- Remove tuple from override headers
- Add inactive chargegroups if present
- Change to json data in the post request

* Tue Sep 28 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.37-1
- Report average of last 24 hours

* Tue Sep 28 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.36-4
- Remove tuple from override headers
- Add inactive chargegroups if present
- Change to json data in the post request

* Tue Sep 28 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.36-3
- Do not publish volumes and shares in dbreporter

* Tue Sep 28 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.36-2
- Make configurable the ssl validation
- Improve error message while posting data
- Send volume and fileshare metrics in dbreporter
- Adapt accounting url and disable ssl verification
- Misc fixes on dbreporter accounting producer

* Tue Sep 28 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.36-1
- Refactor dbreporter to use v3 service costing API

* Fri Sep 24 2021 Domingo Rivera Barros    <driverab@cern.ch> - 0.7.35-1
- Remove accounting group from ccitools
- Add tag to intermediate images in bunny hop process
- Retry rally cleanup
- Transfer volumes or images based on destination project

* Wed Sep 22 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.34-1
- Do not throw an exception if the user is not allowed to fetch it in Nova, Cinder nor Manila
- Parametrize intervention uuids
- Update config file sample

* Tue Aug 31 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.33-3
- Fix bug on cinder quota volume checks

* Tue Aug 31 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.33-2
- Fix issues with recreate instance command

* Tue Aug 24 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.33-1
- Update ServiceNow ticket url

* Fri Jul 16 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.32-3
- Add cern-activedirectory flag to avoid compute object deletion

* Fri Jul 16 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.32-2
- Push the default for the trust purge to 4 hours in the past

* Fri Jul 16 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.32-1
- Change default cloud in trust_manage purge
- Add defaults for trust purge so it can be added into rundeck

* Tue Jul 13 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.31-2
- Fix issue with project creation in FIM
- Add after and before to help parallelized trust purge

* Mon Jul 12 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.31-1
- Add purge command to remove orphaned trusts

* Thu Jul 01 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.30-2
- Use newOwner attribute for the SOAP call

* Thu Jul 01 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.30-1
- Add command to update projects in bulk on FIM

* Thu Jun 24 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.29-1
- Complete implementation of recreate instance command

* Wed Jun 16 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.28-4
- Refactor imports on tests
- Unify compute version calls
- Improve test coverage on hypervisor list
- Remove unused utils class for snow
- fix for migrating unreachable instances

* Mon May 31 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.28-3
- Allow to show all properties available on the filtered entries

* Wed May 26 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.28-2
- Fix bug on accounting synchronization

* Wed May 26 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.28-1
- Add accounting synchronization with chargegroups script

* Fri May 07 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.27-1
- Print region name for magnum clusters and templates

* Fri Apr 30 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.26-3
- Round s3 size to reflect more appropiately s3 usage

* Tue Apr 20 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.26-2
- Add calculations to retrieve s3 usage

* Tue Apr 20 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.26-1
- Filter out None CC and BCC addresses on sendmail
- Do not break if there is an exception on the neutron service

* Fri Apr 16 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.25-2
- Improve get_chargegroups function
- Fix issues with no accounting_group property

* Wed Apr 14 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.25-1
- Add logic to handle chargegroups from names in project creation

* Wed Apr 14 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.24-2
- Add banner timeout on weekly probe

* Tue Apr 13 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.24-1
- Rebuild for Centos 8 stream

* Tue Apr 13 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.23-1
- Centralize quota retrieval and allow impersonation

* Mon Apr 12 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.22-5
- Fix issues with urllib on python3
- Decode bytestring from urlopen.read()

* Mon Apr 12 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.22-4
- Fix encoding on megabus messages

* Fri Apr 09 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.22-3
- Remove functional tests
- Remove xldap usage from escalate project
- Drop kerberized usage of xldap
- Remove xldapclient from updatequota

* Fri Apr 09 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.22-2
- Remove iteritems from all dict iterations

* Thu Apr 08 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.22-1
- Abort migration feature

* Thu Apr 08 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.21-1
- Move to python3 and centos 8 only

* Fri Mar 19 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.20-2
- Fix typo in interventions manager

* Wed Mar 17 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.20-1
- Fix UnboundLocalError: local variable 'res' referenced before assignment
- Fix ussue while creating machine in nova using microversion > 2.36

* Fri Mar 12 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.19-1
- Add logging and the possibility to exclude job IDs in interventions manager

* Fri Feb 19 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.18-1
- Add check date format
- Fix requirements on oslo.config
- Update migration.py removed logger.error(sys.exc_info()) as it's not needed and cause misleading error logs.

* Mon Jan 25 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.17-3
- Performance improvement fixes on weekly ssh probe

* Mon Jan 25 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.17-2
- Increase timeout in weekly ssh probe

* Tue Jan 19 2021 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.17-1
- Fix CI intermittent issue while testing
- Remove br from user mail messages
- Centralize quota retrieval

* Thu Dec 10 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.16-1
- Clean magnum clusters in second project for magnum_t
- Timeout on migrations changed from 2hours to 4 hours

* Tue Dec 08 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.15-2
- Fix link to serviceportal in project enable and quota update

* Wed Dec 02 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.15-1
- Add support to define default landb mainuser and/or responsible in ccitools

* Wed Nov 25 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.14-3
- Enforce quota enabled on s3

* Wed Nov 25 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.14-2
- Add tag sdn1quota on projects with network quota

* Tue Nov 24 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.14-1
- Remove unused xldap calls
- Apply compatibility fixes for xldap methods on python3

* Wed Nov 18 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.13-5
- Retry a maximum of 10 times show in weekly ssh probe

* Thu Nov 12 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.13-4
- Increase timeout and manage snowclient exception

* Wed Nov 11 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.13-3
- Update date parameter if present in postpone intervention jobs
- Fix logging in cci-interventions-manager
- Add remediation task for network_speed_wrong gni tickets

* Tue Nov 10 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.13-2
- Fix expiration of megabus notifications

* Thu Nov 05 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.13-1
- Add weekly_ssh_probe_compute_nodes command

* Tue Oct 27 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.12-8
- Make comment in snow tickets python3 friendly

* Mon Oct 26 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.12-7
- OS-12377: user input hostname or FQDN

* Fri Oct 16 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.12-6
- Fix issue when applying share quota when processing a partial quota update
- Fix template deletion in rally cleanup

* Fri Oct 16 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.12-5
- Add command cci-jira-ticket-cleanup

* Fri Oct 16 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.12-4
- Handle cases in which there is a partial update on the quota
- Several fixes in update_quota and dump_record_producer after metadata change

* Tue Oct 13 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.12-3
- Ignore types in which the project has no quota

* Tue Oct 13 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.12-2
- Allow to iterate over all share types and volume types
- Exclude __DEFAULT__ volume type from get volume type results
- Use show_all parameter on get_volume_types
- Remove duplicates when fetching volumes, volume snapshots, volume types, shares and share types

* Mon Oct 12 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.12-1
- Remove duplicates when fetching VMs
- Fix update_quota to use new record producers

* Tue Oct 06 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.11-3
- Handle case of record producer without metadata field

* Wed Sep 30 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.11-2
- Convert quotas values into integer

* Mon Sep 28 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.11-1
- Add jira stale issue job to cci-tools
- Rely on public types instead of hardcoded ones

* Wed Sep 23 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.10-2
- Remove u_cloned_from_ticket that are not to be used in record producers

* Wed Sep 23 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.10-1
- Refactor to add snow metadata
- Remove xsls metrics command

* Mon Sep 21 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.9-2
- Remove timestamp in metrics send with service_metrics command

* Fri Sep 18 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.9-1
- Implement service_metrics command

* Tue Sep 15 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.8-8
- Enable project before creation of s3 data in radosgw

* Tue Sep 08 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.8-7
- Fix bug in project creation with s3 quota

* Tue Sep 08 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.8-6
- Handle multiple regions in magnum rally cleanup

* Fri Aug 14 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.8-5
- Specify a reason for no_contact alarms
- Changed migration from parallel to sequential

* Fri Aug 07 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.8-4
- Fix bug on recreate instance when renaming it

* Wed Jul 29 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.8-3
- Fix bug in dbreporter

* Mon Jul 27 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.8-2
- Handle creation of users in radosgw when there is quota for s3

* Mon Jul 27 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.8-1
- Add byte information in accounting data sent to S3

* Fri Jul 24 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.7-1
- Add migration tools to allow host evacuation

* Mon Jul 20 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.6-8
- Fix loadbalancer sub-resources default quotas
- Use direct ping instead of local ssh connection

* Fri Jul 03 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.6-7
- Change calendar invitation to UTC to avoid issues with kopano

* Fri Jul 03 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.6-6
- OS-11799: create_project, wait until fim synchronization has finished

* Wed Jul 01 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.6-5
- Fix issues on recreate instance

* Wed Jun 24 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.6-4
- Wait until clusters are deleted to delete cluster templates
- OS-11746: Set metadata after VM recreation
- Change to cinderclient.v3
- OS-11748: Transfer volumes attached during recreation
- Use tenacity for retries on deletion before recreation
- Simplify cloudclient usage in recreate
- OS-11747: Allow to rename machines during recreation
- Differentiate failure in case of no vms created in the last hour
- Get extra information from volume metadata in cloud_dr_status
- Calculate all boot parameters with current credentials and only change before deletion
- Fix bug on multiple occurrences while fetching images
- Add script for cloud_dr_status
- Harden quota operations when RP changes

* Fri Jun 19 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.6-3
- Fix bug with cloudregionclient

* Fri Jun 19 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.6-2
- Use a scoped session in magnumclient
- Remove openstack configuration file as it is not used anymore
- Refactor cloudregionclient to allow session retrieval
- Fix bug in timezone for python2

* Thu Jun 18 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.6-1
- Add support for vault-100 and vault-500 volume types
- Add cleanup for leftovers from Rally Magnum tests
- Fix recreation of machines on different projects
- Fix issue with network quota and extract method to add/remove endpoint groups
- Catch keystone NotFound exception while searching for endpointgroup membership

* Tue May 19 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.5-4
- Fix packaging issues with python-ldap on el7

* Tue May 19 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.5-3
- Fix packaging issues on el7

* Thu May 14 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.5-2
- Rebuild compatible with el7 and el8

* Wed May 13 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.5-1
- Set load balancer quotas
- Fix bug on history parameter in cci-interventions-manager
- Only set chargegroup and chargerole if they are not null
- Remove defaults for chargegroup and chargerole
- Do not crash if the record producer does not have chargegroup or chargerole
- Allow dump record producer to manage missing chargegroup and chargerole attributes
- Remove print messages from some scripts
- Fix bug while loading ccitools configuration
- Simplify configuration loading

* Fri Apr 17 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.4-3
- Fix record producers identifier ordering

* Thu Apr 16 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.4-2
- Add identifiers from new snow RPs that contain chargegroup and chargerole fields
- OS-11064: Add chargegroup and chargerole to creation/escalate/dump commands
- Flavors retrieval does not pick public flavors

* Tue Apr 07 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.4-1
- Disable network quotas until sdn1 service made public
- Populate chargegroup and chargerole in dbreporter
- List all flavors when recreating a machine

* Fri Apr 03 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.3-17
- Replace cern-get-sso-cookie by auth_get_sso_cookie
- Skip updating quota if we are reusing the volume (BFV)
- Fix check_enough_quota command after changes in cinder update quota
- Add missing python-glanceclient dependency
- Do not use stestr version 3 as it dropped support of python3

* Fri Mar 06 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.3-16
- Fix issue on check_enough_quota command

* Tue Mar 03 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.3-15
- Allow to recreate BFV and keep volumes attached
- Change behavior on production/development regions
- Add support for yum_unfinished_transactions
- Fix verbose logging on standard commands
- Allow rundeck to close tickets with comments from itself
- Add missing self into compatible flavor call for recreate
- Handle swap full alarms

* Tue Feb 04 2020 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.3-14
- Fail XSLS script if there is no data
- Escape characters in notify intervention
- Add support to configure a flavor map to allow recreation with a change on the flavor
- Fix recreation scripts
- Improve rally cleanup on delete servers and volumes
- Add extra logging while in active waiting
- Remove project name as it clashes with session arguments

* Fri Nov 22 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.3-13
- Allow to not initialize the clients needed by hzrequestpanel

* Thu Nov 21 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.3-12
- Fix bug in s3 stats

* Thu Nov 21 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.3-11
- Add methods to reset state of VMs and volumes when they are in transient state
- Add improvements in s3 quota command
- Rename health_report to health_report_openstack
- Remove openstack commands from rally cleanup
- Fix issues when reinstalling

* Wed Nov 20 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.3-10
- Remove duplicate parse_args in interventions manager

* Tue Nov 19 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.3-9
- Fix command for accounting db reports
- Add extra configuration settings for accounting db reports
- Fix pep8 issues

* Tue Nov 19 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.3-8
- Several fixes for loading configuration properly
- Import of accounting db reporting commands

* Thu Nov 14 2019 Raul Villar Ramos <raul.villar.ramos@cern.ch> - 0.7.3-7
- Adapt update_quota to new base class
- Adapt power_on_off_hv_servers to new base class
- Adapt notify_users to new base class
- Adapt notify_intervention to new base class
- Adapt enable_project to new base class
- Adapt megabus_producer to new base class
- Adapt hypervisor_has_vms to new base class
- Adapt lock_unlock_hv_servers to new base class
- Adapt health_report to new base class
- Adapt delete_project to new base class
- Adapt create_project to new base class
- Adapt check_enough_quota to new base class

* Fri Oct 18 2019 Raul Villar Ramos <raul.villar.ramos@cern.ch> - 0.7.3-6
- Adapt rundeck_scheduler to new base class
- Adapt takeover_scheduled_rundeck_jobs to new base class
- Fix bug on ssh_executor that only retries n-1 times
- Fix code after refactor of rgw client
- Add s3 size used in the quota command
- Add command to manage S3 Quota
- Improve test coverage in iterator_project
- Improve test coverage in base cloud class
- Adapt multiple_recreate_parallel to new base class
- Move inside radosgw and cornerstone client
- move rundeck new base class intervention_manager
- Move export rundeck projects to new base class
- Handle another type of no_contact exception
- Move check_node_foreman to new base class
- Refactor tests and move foreman commands to new base class
- Move check_cloud_vm_owners to puppetdb base class
- Rename cloudforeman class
- Move export rundeck definitions to new rundeck base class
- Refactor base classes to prepare migration of commands
- removing unused code
- change s3 port configuration option to integer
- Add new configuration mode
- Fix issue with port in updated version of boto
- Improve test coverage in trust manage command
- Review trust_manage command
- Improve role assignment cleanup

* Tue Oct 01 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.3-5
- Move acl_cleanup to options in cci-assignment-cleanup

* Tue Oct 01 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.3-4
- Add acl_cleanup to the list of commands
- Move hardcoded info to config and fix tests on check_cloud_owners
- Add missing dependencies
- Fix ICS in send calendar invitation

* Thu Sep 26 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.3-3
- Fix bug with region name

* Thu Sep 26 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.3-2
- Rename fields for network quota on sdn1 region

* Thu Sep 26 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.3-1
- Add neutron quota management for sdn1 region

* Tue Sep 24 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.3-0
- Move manage neutron_agent to BaseCloudCMD
- Move manage nova compute to BaseCloudCMD
- Add test class for BaseCloudCMD
- Remove duplicated tests after merge
- Move project_iterator to BaseCloudCMD
- Move assignment_cleanup to BaseCloudCMD
- Add CloudCMD class with refactored code
- Add configuration framework

* Mon Sep 23 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.2-11
- Add readme entries for adding a new command
- Improve test coverage
- Adapt Record Producers for loadbalancers and floating ips
- Add locked instances tool in cci-tools
- Add small tool to manage trusts in keystone database
- Change delete project command to use tenacity
- Replace active wait in project creation with tenacity framework
- Increase retry interval in enable_project to 5 minutes

* Wed Sep 11 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.2-10
- Remove wigner volume types
- Improve test coverage
- Remove spark script for checking hypervisor reboots
- Wait until the s3 user has been created before continuing with the quota
- Disable arrow warnings
- Remove unnecessary argument logging

* Fri Aug 30 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.2-9
- Improve test coverage and add helper classes
- Fix bug with intervention manager
- Add recreate script for migration through recreation job
- Add missing script for checking quota for migration through recreation jobs

* Mon Aug 19 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.2-8
- Add scripts to manage nova-compute nodes and neutron agents
- Fix bug in assignment cleanup

* Fri Aug 16 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.2-7
- Add class to close puppet_agent_has_run_errors

* Fri Aug 16 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.2-6
- Fix bugs in interventions-manager
- Fix rundeck export executions
- Do not wait for execution of rally jobs when there is nothing to clean up
- Fix assignment cleanup procedure

* Mon Aug 12 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.2-5
- Add more classes for actions
- Fix a bug on clean cattle vms
- Fix issue in cci-interventions-manager

* Fri Aug 09 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.2-4
- Add loadhigh alarm handling

* Fri Aug 09 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.2-3
- Add actions to handle service collect alarms

* Fri Aug 09 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.2-2
- Add more hardcoded scripts from rundeck
- Fix for assignment cleanup
- Add scripts for ironic

* Thu Jul 25 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.2-1
- Major rewrite of code
- Add testing framework and test implementation
- Addition of script for enable/disable compute nodes
- Script to enable/disable neutron agents ported

* Mon May 06 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.1-13
- Consolidate logging format
- Fix endpoint filtering for batch region

* Thu Mar 28 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.1-12
- Add force attribute in delete_servers function

* Fri Mar 22 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.1-11
- Fix issue with missing delete_servers function

* Tue Mar 12 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.1-10
- Fix issues in adding tags on s3 quota

* Tue Mar 12 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.1-9
- Fix issues in cci-create-project

* Mon Mar 11 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.1-8
- Fix issues in apply quota request

* Mon Mar 11 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.1-7
- Fix bug on cci-notify-intervention
- Add back get_project_owner call

* Mon Mar 11 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.1-6
- [OS-8777] Fix crash while searching interventions with no associated jobs

* Mon Mar 11 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.1-5
- Add missing volume type list info for hzrequest panel

* Fri Mar 08 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.1-4
- Some more fixes found on the QA environment

* Fri Mar 08 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.1-3
- Some fixes found on the QA environment

* Fri Mar 08 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.1-2
- Add region endpoint group management on project creation

* Wed Mar 06 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.1-1
- Add removal of quotas for project creation

* Fri Mar 01 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.7.0-1
- Refactor of the code to support regions
- Fix more scripts that were not handled correctly
- [OS-8695]  Make cci-create-project aware of regions
- [OS-8694] Make cci-update-quota aware of regions
- [OS-8686] Make cci-delete-project aware of regions
- [OS-8693] Make cci-check-cloud-owners aware of regions
- [OS-8694] Make cci-rally-cleanup aware of regions
- [OS-8691] Make cci-megabus-producer aware of regions
- [OS-8692] Make cci-deleted-hosted-vms aware of regions
- [OS-8689] Make cci-clean-cattle-vms aware of regions
- [OS-8687] Make cci-power-on-off-hv-serves to be aware of regions
- [OS-8690] Make cci-check-disabled-nodes aware of regions
- Improve log on get servers by hypervisor

* Wed Feb 27 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.6.11-1
- [OS-8682] Make cci-lock-unlock-hv-servers to be aware of regions
- [OS-8681] Make cci-hypervisor-has-vms to be aware of regions
- [OS-8684] Make cci-notify-users to be aware of regions
- [OS-8683] Make cci-notify-intervention to be aware of regions

* Thu Feb 14 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.6.10-5
- [OS-8566] Fix Notify users step if SNOW ticket other than OTG provided

* Fri Feb 08 2019 Daniel Abad <d.abad@cern.ch> - 0.6.10-4
- [OS-8453] set blank otg and reference if unassigned
- [fix] raise exception to make job fail if egroup not found

* Thu Feb 07 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.6.10-3
- Fix issue on setting tag on projects with existing s3 quota
- [OS-8443] Add response text if rundeck call fails

* Mon Jan 28 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.6.10-2
- [OS-7409] Add missing logging into S3 quota update
- [OS-7629] Remove choices from megabus producer script

* Tue Jan 15 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.6.10-1
- Tag the projects with s3 quota with the tag s3quota

* Fri Jan 11 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.6.9-3
- Remove temporary disable S3 quota

* Fri Jan 11 2019 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.6.9-2
- Temporarily disable setting S3 quota

* Mon Dec 10 2018 Jose Castro Leon <jose.castro.leon@cern.ch> - 0.6.9-1
- [OS-7409] Integrate S3 service into service offering

* Mon Dec 10 2018 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.6.8-1
- [OS-8223] Remove comment for collectd SNOW tickets

* Mon Oct 08 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.6.7-1
- [OS-7605] Rundeck should not "leak" Snow INC- or RQF-numbers to end-users

* Mon Oct 01 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.6.6-1
- [OS-7645] improve report of Rundeck rally cleanup
- [OS-7621] Rundeck job for Rally cleanup does not add all comments

* Tue Sep 25 2018 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.6.5-1
- [OS-7061] Ignore collectd SNOW tickets in verify-gni-tickets job

* Tue Sep 04 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.6.4-1
- [OS-6709] Create Snow ticket when Rundeck job cannot cleanup rally VMs
- [OS-7107] Remove cci-wait-puppet-runs scripts

* Fri Aug 31 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.6.3-1
- [OS-6960] Fix gni rundeck script for nocontact + physical machines
- Refactor Rally cleanup job

* Fri Jul 27 2018 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.6.2-1
- Add manila args to manual project creation

* Thu Jul 26 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.6.1-2
- [OS-7147] Add support for region_name in openstack clients

* Mon Jul 23 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.6.1-1
- [OS-6942] Add manila quotas to project creation/updates

* Fri Jul 20 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.6.0-1
- [OS-7103] Delete *only* batch VMs after executions of "Destructive intervention job"
- [common] Fix ssh_executor retry mechanism
- [OS-7076] Change intervention emails not to show reference unless it's OTG
- [OS-7114] Increase enable project timeout
- [OS-7035] Make cci-intervention fail when nova is down

* Wed Jul 11 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.7-3
- [OS-7053] Make Search interventions job show everything

* Tue Jul 10 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.7-2
- [OS-7047] Fix behaviour check

* Tue Jul 10 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.7-1
- [OS-7047] Check behaviour only if provided
- [OS-7047] Include behaviour option for lock-unlock vms script

* Tue Jul 10 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.6-3
- Add python-paramiko package

* Mon Jul 09 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.6-2
- [OS-6641] Refactor ssh_executor in cci-tools

* Mon Jul 02 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.6-1
- [OS-6823] New script to check if host host any VMs on it

* Wed Jun 13 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.5-1
- [OS-6623] Add historical option to cci-intervention-manager
- [OS-6699] Support list of execution_IDs for intervention manager

* Tue May 29 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.4-2
- Revert nova override endpoint

* Tue May 29 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.4-1
- [OS-6726] Remove 'dryrun' executions from interventions manager listing
- [OS-6663] Make check interventions job resilient against uppercase + .cern.ch
- Fix alarms with uppercase characters stored with lowercase in SNOW

* Fri May 18 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.3-2
- [OS-6687] Make interventions manager fail when no interventions found

* Thu May 17 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.3-1
- [OS-6639] Refactor generic notify users job
- [OS-6600] Create job "Reschedule intervention"

* Tue May 15 2018 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.5.2-1
- Revert "[OS-6476] Calculate VM numbers relying on child DBs for XSLS"

* Tue May 15 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.1-2
- Include cancel intervention UUID to be ignored

* Fri May 11 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.1-1
- [OS-6314] Make CLI command to handle job interventions in RD

* Tue May 08 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.4.17-3
- Fix check_resources for project deletion

* Fri May 04 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.4.17-2
- [OS-6427] RD job 'Project delete' should delete resources (VMs, images)

* Wed May 02 2018 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.17-1
- Override nova endpoint for admin operations

* Wed May 02 2018 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.16-1
- [OS-6476] Calculate VM numbers relying on child DBs for XSLS

* Fri Apr 27 2018 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.15-1
- [OS-6371] Fix formatting quota update message

* Wed Mar 28 2018 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.14-1
- Hotfix: Fix cookie collisions between parallel jobs

* Tue Mar 20 2018 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.13-1
- [OS-6290] Make comment unicode by default

* Fri Mar 16 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.4.12-1
- [OS-6065] Improve minor server intervention
- [OS-6069] Inform users when scheduling for wrong date
- [OS-6246] add project name to intervention announcement

* Mon Feb 12 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.4.11-1
- [OS-5957] Make "Minor server intervention" more resilient to execution against wrong nodes

* Tue Jan 23 2018 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.10-1
- [OS-5889] Set volume snapshot quotas when creating project
- [OS-5886] Stop reassignation of project creation tickets to Cloud

* Mon Jan 22 2018 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.9-1
- [OS-5689] Resolve GNI tickets with svcrdeck user

* Thu Jan 18 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.4.8-2
- [OS-5811] Move Check disabled compute nodes
- [OS-5755] Add 'behaviour' option to rundeck scheduler

* Thu Jan 11 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.4.8-1
- [OS-5778] Move Check cloud_* VMs owners
- [OS-5777] Move Send calendar invitation
- [OS-5776] Move Generic install compute node
- [OS-5775] Move Update Foreman settings
- [OS-5771] Move Generic remove compute node
- [OS-5659] Move XSLS availability and metrics job
- [OS-5616] Move Megabus producer job
- [OS-5125] Map cci-tools with rundeck jobs

* Thu Dec 14 2017 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.7-1
- [OS-5667] Delete old files for servicenow v1
- [OS-5485] Adapt to new record producer with more fields

* Tue Dec 05 2017 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.4.6-2
- [scripts] Fix cci-update-ticket

* Tue Dec 05 2017 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.4.6-1
- [OS-5555] Fix cci-health-report to meet new flake8 criteria
- [OS-5343] cci-tools showing warning logs
- [OS-5397] cci-rally-cleanup should do normal deletes (and maybe try with force if it doesn't work?)
- [OS-5470] Refresh snow client with 401 in GNI job
- [OS-5520] Don't check quotas before update quota project in creation/update jobs
- [OS-5398] Enable compute node usually fails when there is a machine in error state
- [OS-5484] Allow verifying multiple egroups
- [OS-5073] Make cci-verify-gni-tickets, cci-update-quota and any others use servicenowv2
- [scripts] Move escalate project job to cci-tools
- [scripts] Move health report for Puppet machines job to cci-tools

* Mon Oct 16 2017 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.4.5-1
- [OS-5330] sendmail not sending emails when no ics attached
- [OS-5347] Fix logging errors power on-off and lock-unlock scripts
- [OS-5325] Scheduled jobs are executed by svcrdeck

* Mon Oct 09 2017 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.4.4-1
- [OS-5195] Remove reset state from servers in rally cleanup
- [rundeckclient] Verify Rundeck cert in RundeckClient to avoid SSLError
- [OS-5114] New ccitools scripts not displaying logs
- [OS-4981] Improve sysadmin procedure when doing minor interventions

* Thu Sep 21 2017 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.3-1
- Fix update-quota until update to snowclientv2

* Tue Sep 19 2017 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.2-1
- [OS-5039] Make dump-project-request generic for all the record producers
- [OS-5039] Remove dependencies from aitools
- [OS-5039] Use own function to check validities of UUIDs
- [OS-5039] Remove windows check in gni-verify-tickets
- [OS-5039] Use password session in script when using CloudClient
- [OS-5039] Include python-zeep, python-click and python-tenacity and remove
    kerberos dependency

* Tue Sep 05 2017 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.1-1
- [OS-4847] Refactor snowclient to minimize ticket changes
- [OS-4709] Use svcrdeck service account to do SNOW requests
- [OS-4868] Improve workflow when enabling project via HW resources

* Fri Aug 11 2017 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.3.11-2
- [OS-4935] Fix exception when no ticket description

* Thu Aug 3 2017 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.3.11-1
- [OS-4848] Remove unnecessary fields from "Rundeck - Project Deletion"
- [OS-4871] Rundeck picks wrong name when addressing user
- [OS-4868] Rework enable project for HW resources
- [OS-4816] Improve volume deletion for Rally Cleanup job
- [OS-4779] Fix logging problem with rundeck jobs
- [OS-4733] Add Jose's scripts in cci-tools and rundeck
- [snowclient] Fixed bug that generated project_deletion and project_creation tickets with description filled with comment's content
- [cci-assignment-cleanup] Added return code for rundeck job

* Tue Jul 11 2017 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.3.10-2
- [OS-4680] Regular cleanup of old Rally jobs
- [cloud] Add methods to retrieve servers and volumes from a given user
- [cloud] Add methods to display tables with detailed inforation about servers and volumes from
- [OS-4736] Improve Rundeck script for check of un-assigned GNI tickets when VM doesn't exist anymore
- [cloud] Add get_server to retrieve servers by name
- [common] Include "-o ConnectTimeout=10 -o ServerAliveInterval=20 -o NumberOfPasswordPrompts=0" as default options for ssh_executor a given user

* Mon Jul 10 2017  Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.3.10-1
- [OS-4713] Remove permissions checks in Rundeck code for quota update
- Add scripts for cleanup of role assignments and iterate over projects
- Fix ticket resolution bug on project deletion job

* Fri Jun 23 2017  Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.3.9-2
- Fix 'department' field bug
- [snowclient] Add ticket resolution to cci-delete-project

* Thu Jun 22 2017  Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.3.9-1
- [snowclient] Add get_ticket_comments and get_openstack_user to servicenow client
- [OS-4124] Edit 'Check un-assigned GNI tickets' so it does not always resolve tickets
- [OS-4644] Revise project creation from SNOW for HW resources

* Mon Jun 12 2017 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.3.8-1
- Add session parameter in cloudclient
- Make all the code flake8 compliant
- [OS-4591] Print backtraes in cci-tools
- [OS-4615] Create Project Deletion extra methods for Horizon
- [OS-4306] Calendar reminders when notifying about interventions affecting virtual machines

* Mon May 15 2017 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.3.7-1
- Fix issue when RAM quota has -1
- [OS-4596] Use nova list instead of hypervisor APIs
- Add delete project function mapping for SNOW form
- [snowclient] Add create_project_deletion to servicenow client

* Thu Jan 12 2017 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3.6-1
- [cloud] Add glance client

* Tue Dec 6 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3.5.1-1
- [xldap] get_primary_account supports primary, service, and secondary accounts

* Tue Dec 6 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3.5-1
- [xldap] Add get_primary_account

* Wed Nov 30 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3.3-1
- [cloudclient] Fix matching condition on get_hypervisor_by_name

* Mon Nov 28 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3.2-1
- [cloudclient] Fix condition on get_hypervisor_by_name

* Tue Nov 15 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3.1-1
- [cci-notify-intervention] Expose mailfrom mailcc & mailbcc to user

* Thu Oct 27 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3-1
- [rudneckclient] Add missing json import
- [cloudclient] Fix cases where searching for an obj on openstack returns all matching elements
- [cci-notify-intervention] Also fix those cases

* Wed Oct 12 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.2-1
- [cloudclient] Changes to make ccitools work with latest aitools openstack auth version

* Thu Oct 6 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.1.1-1
- [snowclient] Add email to ticket watchlist

* Mon Aug 8 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.1-1
- [xldap] Add function to get_starting_with_egroup_members
- [snowclient] Add restriction to only update tickets assigned to groups the user belongs to
- [snowclient] Add new param 'resolver' to resolve_ticket so tickets get resolved/assigned by this user

* Tue Jul 19 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.50-2
- [cci-update-quota] Small update closing comment

* Tue Jul 19 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.50-1
- [cci-update-quota] Update resolving ticket message
- [xldap] Add couple of functions to get users and egroups names
- [snowclient] Add create_project_creation to servicenow client

* Wed Jun 22 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.49-1
- [snowclient] Add create INC
- [cci-create-project] Many changes to support project properties; accounting_group & type

* Mon May 2 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.48-1
- [cci-notify-intervention] Store VM responsible in lowercase to avoid case sensitive duplicates
- [cci-update-quota] Add comment before scalate quota update request

* Thu Apr 14 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.47-1
- [cloud lib] Verify quota update and bug fixes
- [cci-create-project] Remove HEP and enable project from summary table

* Wed Mar 30 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.46-1
- [cci_project_creation] Improve error messages
- [rundeck lib] SSL verify enabled by default
- [rundeck lib] takeover_schedule improvements
- [ldap lib] Fix cases where xldap query may result empty

* Mon Feb 29 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.45-1
- [xldap lib] Fix slowness when using is_existing_egroup function

* Wed Feb 17 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.44-1
- [cci-update-quota] Fix crash when volume_quota empty

* Fri Feb 5 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.43-1
- [servicenow lib] Add Create request and create_quota_update
- [servicenow lib] Code cleanups
- [cci-update-quota] Addapt script to new lib code

* Fri Jan 8 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.42-1
- [rudneck] Change takeover API endpoint (v14 and up)
- [cci-notify-intervention] Address cases where user does not exist
- [cci-update-quota] Get rid of HEP flavors config
- [cci-update-quota] Add users name to closing message
- [cci-update-quota] Get caller from u_caller_id instead of from sys_created_by
- [cci-update-quota] Fix IDs new 7th volume type

* Mon Nov 30 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.41-1
- [cci-create-project] Move instance argument where makes more sense
- [cci-notify-intervention] Fix empty user list

* Thu Nov 26 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.40-1
- [rundeck lib] Fix for takeover schedule
- [cci-tools scripts] Add argument 'instance' to all scripts
- [servicenow lib] Fix for those cases when Rundeck updates a ticket and there is no previous assignee
- [servicenow lib] Remove leading and trailing spaces when reading input from record producer

* Tue Nov 17 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.39-1
- [general lib] Logging handeling improvements
- [cci-update-quota] Fix "Apply project quota request" shows 0 instead of current value bug
- [rundeck lib] Add run execution by job id
- [rundeck lib] Allow custom headers in functions

* Fri Oct 16 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.38-1
- [servicenow lib] QuotaUpdateRequestRP more error resilient
- [fim lib] Add enable_project function

* Wed Oct 14 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.37-1
- [common lib] Check return code when ssh executing
- [cci-notify-intervention] Add vmslist variable
- [servicenow lib] Add new RP fields reflecting the new volume types (wig-cp1, wig-cpio1)

* Sun Oct 4 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.36-1
- [common lib] Add optional params to ssh_executor

* Mon Sep 28 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.35-1
- [servicenow lib] Make servicenow lib compatible with SNOW tables latest changes
- [servicenow lib] Small fixes

* Fri Sep 25 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.34-1
- [cci-notify-intervention] Fix problem with project IDs and names
- [rundeck lib] Fix typo
- [common lib] Add new function ssh_executor

* Wed Sep 23 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.33-1
- [cci-notify-users] ldap server address from an external variable
- [cci-update-quota] ldap server address from an external variable
- [common lib] cern_sso_get_cookie define cookie path as param

* Tue Sep 22 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.32-1
- [cci-notify-intervention] Better exceptions handling
- [cci-notify-intervention] Add mail-content-type option
- [cci-notify-intervention] ldap server address from an external variable

* Tue Sep 22 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.31-1
- [cci-notify-intervention] Set Nova as source of truth for VM's owner/responsible
- [cci-notify-users] Deprecated in favor of cci-notify-intervention
- [python-landbclient] Removed package dependency

* Wed Sep 16 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.30-1
- New improved version of cci-notify-intervention.
- Add python-landbclient dependency

* Wed Sep 9 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.29-1
- Fix cern_get_sso_cookie common function
- cloud library: Add search project by ID
- cci-notify-users: Projects can be referenced using names or IDs
- cci-create-project: Improve output messages

* Tue Sep 1 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.28-1
- Add cern_get_sso_cookie function to cci-tools common library
- Fix typo on cci-create-project

* Thu Aug 13 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.27-1
- Add support for volumes types quota updates

* Thu Aug 13 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.26-1
- Add FIMClient Library
- Update cci-create-project to use FIMClient
- Add default/force exec modes to cci-create-project
- Update ServiceNow Library to selfassign ticket before updating it

* Thu Aug 6 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.25-1
- Add takeover_schedule to Rundeck API Client
- Remove automatic health_check non compatible with SSO auth

* Tue Jul 14 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.24-1
- Support latest updates on "Request for shared Cloud Service Project" RP
- Improve servicenow library code
- Add get tickets by hostname to servicenow library
- Adapt scripts to latest library changes

* Mon Jun 29 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.23-1
- cci-update-quota: Monospace font for quota summary on snow tickets

* Fri Jun 5 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.22-1
- Improve Rundeck API library to support SSO endpoint auth

* Thu May 28 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.21-1
- cci-notify-intervention: Add OTG substitution, sort summary by vmname and show starting hour.
- Add dryrun/perform behaviour to cci-update-quota

* Thu May 07 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.20-1
- cci-notify-intervention: Add option to notify main user of the VM (not only owners).

* Wed May 06 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.19-1
- Add initial cci-notify-intervention script (NSC source, Hyperv oriented)
- xldap: add get_security_groups_members function.

* Thu Apr 30 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.18-1
- Fix cornerstone return code and message
- Add egroup name validation

* Wed Apr 29 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.17-1
- Fix listing servers in hypervisor when no servers are found
- Add logging to sendmail client
- Fix issue when vm owner was not found

* Fri Apr 17 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.16-1
- Refactor the XldapClient & add unit tests.
- Add new variables (date, hour and duration) to cci-notify-users script.

* Thu Apr 02 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.15-1
- Fix raise exception and add print messages.

* Wed Apr 01 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.14-1
- Improvements to cci-update-quota script.
- New method to set state 'waiting for user' in ServiceNow
- New method in the cloud client to check flavour access.

* Wed Mar 25 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.13-1
- Add cci-update-quota script
- Add cci-notify-users script
- Fix SnowClient functionality interacting with tickets.
- Fix XldapClient get_egroup_email function.
- Add get_hypervisor_by_name to CloudClient

* Fri Mar 06 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.12-1
- New functions in CloudClient to get info about nova services

* Fri Mar 06 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.11-1
- Add generic writing method & additional snow funcitonality

* Fri Mar 06 2015 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.10-1
- (cci-health-report) Add task & user_id column and make them default.
- (cci-health-report) Rename 'Last Update' column to 'Since'
- (cci-health-report) Refactor code

* Thu Feb 26 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.9-2
- Fix typo on snowclient

* Thu Feb 26 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.9-1
- Add resolve ticket and change FE to snowclient
- Cornerstone endpoint is read from config file
- Add export job definitions and projects to rundeck API client

* Wed Feb 11 2015 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.8-1
- (danielfr) Add rundeck api client
- (lfernand) Add subcommands to cci-create-project (from-input/from-snow)

* Fri Feb 06 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> 0.0.7-1
- Fix egroup checking for project creation

* Thu Feb 05 2015 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.6-1
- Add cci-health-report to monitor vms & volume status

* Thu Feb 05 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> 0.0.5-1
- Fix cci-project-create to call Cornerstone

* Thu Jan 29 2015 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.4-1
- (lfernand/danielfr) Add cci-create-project script
- (danielfr) Add get_egroup_email to xldap client

* Wed Jan 21 2015 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.3-1
- Add CloudClient class to interoperate with the CERN cloud
- (danielfr) Add validations for servicenow integration
- (danielfr) Add emails utilities

* Wed Dec 17 2014 danielfr <danielfr@cern.ch> - 0.0.2-1
- Add xldap client
- Add method to write work_note in ServiceNow request

* Mon Dec 08 2014 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.1-3
- Add RPM changelog

* Mon Dec 08 2014 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.1-2
- Bump version (Koji fixes)

* Fri Dec 05 2014 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.1-1
- [cci-dump-project-request] Initial version
- [cci-update-ticket] Initial version
- Add 'servicenow' module

* Tue Dec 02 2014 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.0
- Initial version
