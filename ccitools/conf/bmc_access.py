from oslo_config import cfg

functional_element = cfg.StrOpt(
    'functional_element',
    default='Itprocos vendor',
    help="functional element to create the ticket in case of error")

assignment_group = cfg.StrOpt(
    'assignment_group',
    default='Itprocos vendor 2nd Line Support',
    help="assignment group to create the ticket in case of error")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    functional_element,
    assignment_group
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
