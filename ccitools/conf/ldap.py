from oslo_config import cfg


server = cfg.StrOpt(
    'server',
    default='ldap://agiledc.cern.ch',
    help="ldap server")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    server,
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
