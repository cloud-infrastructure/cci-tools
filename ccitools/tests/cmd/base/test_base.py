import logging

from ccitools.cmd.base import BaseCMD
from unittest import TestCase


class TestBaseCMD(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):
        args = BaseCMD('fake').parse_args([])
        self.assertEqual(0, args.verbose_level)
        self.assertFalse(args.debug)

    def test_verbose_level_1(self):
        args = BaseCMD('fake').parse_args(['-v'])
        self.assertEqual(1, args.verbose_level)
        self.assertFalse(args.debug)

    def test_verbose_level_2(self):
        args = BaseCMD('fake').parse_args(['-vv'])
        self.assertEqual(2, args.verbose_level)
        self.assertFalse(args.debug)

    def test_verbose_level_3(self):
        args = BaseCMD('fake').parse_args(['-vvv'])
        self.assertEqual(3, args.verbose_level)
        self.assertFalse(args.debug)

    def test_debug_args(self):
        args = BaseCMD('fake').parse_args(['-d'])
        self.assertEqual(0, args.verbose_level)
        self.assertTrue(args.debug)
