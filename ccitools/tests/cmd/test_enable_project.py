import logging

from ccitools.cmd import enable_project
from tenacity import stop_after_attempt
from tenacity import wait_none
from unittest import mock
from unittest import TestCase


class TestEnableProject(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch('ccitools.utils.cloud.cloud_config')
    def test_default_args(self, mock_cc):
        with self.assertRaises(SystemExit) as cm:
            enable_project.main([])
        self.assertEqual(cm.exception.code, 2)

    @mock.patch('ccitools.cmd.enable_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.enable_project.FIMClient')
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    def test_project_enabled_from_values(self, mock_crc, mock_fim,
                                         mock_snc):
        mock_fim.return_value = mock_fim
        mock_fim.set_project_status.return_value = 0
        mock_crc.return_value = mock_crc
        mock_crc.find_project.return_value = mock.Mock(
            enabled=True
        )

        enable_project.main([
            '--enable-project', 'True',
            'from-values',
            '--project-name', 'fake_name',
        ])

    @mock.patch('ccitools.cmd.enable_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.enable_project.FIMClient')
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    def test_project_enabled_is_disabled_from_values(self,
                                                     mock_crc,
                                                     mock_fim,
                                                     mock_snc):
        # Test project enabled is disabled from values
        # and code not = FIMCodes.SUCCESS.

        mock_fim.return_value = mock_fim
        mock_fim.set_project_status.return_value = 10
        mock_crc.return_value = mock_crc
        mock_crc.find_project.return_value = mock.Mock(
            enabled=False
        )

        with self.assertRaises(Exception):
            enable_project.main([
                '--enable-project', 'True',
                'from-values',
                '--project-name', 'fake_name',
            ])
        self.assertEqual(Exception, Exception)

    @mock.patch('ccitools.cmd.enable_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.enable_project.FIMClient')
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    def test_project_disabled(self, mock_crc, mock_fim,
                              mock_snc):
        mock_fim.return_value = mock_fim
        mock_fim.set_project_status.return_value = 0
        mock_crc.return_value = mock_crc
        mock_crc.find_project.return_value = mock.Mock(
            enabled=False
        )

        enable_project.main([
            '--enable-project', 'False',
            'from-values',
            '--project-name', 'fake_name',
        ])

    @mock.patch('ccitools.cmd.enable_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.enable_project.FIMClient')
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    def test_project_enable_from_snow(self, mock_crc, mock_fim, mock_snc):
        mock_fim.return_value = mock_fim
        mock_fim.set_project_status.return_value = 0
        mock_crc.return_value = mock_crc
        mock_crc.find_project.return_value = mock.Mock(
            enabled=True
        )

        mock_snc.return_value = mock_snc
        mock_snc.get_project_creation_rp.return_value = mock.Mock(
            request=mock.Mock(info=mock.Mock(u_caller_id='fake_id'))
        )
        mock_snc.user.get_user_info_by_sys_id.return_value = mock.Mock(
            u_preferred_first_name='fake_user_i'
        )

        enable_project.main(
            [
                '--enable-project', 'True',
                'from-snow',
                '--ticket-number', 'RQFXXXX',
                '--instance', 'cern',
                '--resolver', 'fake_user',
            ]
        )

    @mock.patch('ccitools.cmd.enable_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.enable_project.FIMClient')
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    def test_project_disable_from_snow(self, mock_crc, mock_fim, mock_snc):
        mock_fim.return_value = mock_fim
        mock_fim.set_project_status.return_value = 0
        mock_crc.return_value = mock_crc
        mock_crc.find_project.return_value = mock.Mock(
            enabled=True
        )

        mock_snc.return_value = mock_snc
        mock_snc.get_project_creation_rp.return_value = mock.Mock(
            request=mock.Mock(info=mock.Mock(u_caller_id='fake_id'))
        )
        mock_snc.user.get_user_info_by_sys_id.return_value = mock.Mock(
            u_preferred_first_name='fake_user_i'
        )

        enable_project.main(
            [
                '--enable-project', 'False',
                'from-snow',
                '--ticket-number', 'RQFXXXX',
                '--instance', 'cern',
                '--resolver', 'fake_user',
            ]
        )

        enable_project.main(
            [
                '--enable-project', 'False',
                'from-snow',
                '--ticket-number', 'RQFXXXX',
                '--resolver', '',
            ]
        )

    @mock.patch('ccitools.cmd.enable_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.enable_project.FIMClient')
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    def test_wait_for_project_fast(self, mock_crc, mock_fim, mock_snc):
        mock_fim.return_value = mock_fim
        mock_fim.set_project_status.return_value = 0
        mock_crc.return_value = mock_crc
        mock_crc.find_project.side_effect = iter([
            mock.Mock(enabled=False),
            mock.Mock(enabled=True)
        ])

        cmd = enable_project.EnableProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(10)

        cmd.main([
            '--enable-project', 'True',
            'from-values',
            '--project-name', 'fake_name',
        ])

    @mock.patch('ccitools.cmd.enable_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.enable_project.CornerstoneClient')
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    def test_project_enable_from_snow_in_resources(self, mock_crc,
                                                   mock_corn, mock_snc):
        mock_corn.return_value = mock_corn
        mock_crc.return_value = mock_crc
        mock_crc.find_project.return_value = mock.Mock(
            id='fake_id',
            enabled=True
        )

        mock_snc.return_value = mock_snc
        mock_snc.get_project_creation_rp.return_value = mock.Mock(
            request=mock.Mock(info=mock.Mock(u_caller_id='fake_id'))
        )
        mock_snc.user.get_user_info_by_sys_id.return_value = mock.Mock(
            u_preferred_first_name='fake_user_i'
        )

        enable_project.main(
            [
                '--enable-project', 'True',
                '--mim',
                'from-snow',
                '--ticket-number', 'RQFXXXX',
                '--instance', 'cern',
                '--resolver', 'fake_user',
            ]
        )

        mock_corn.update_project.assert_called_with(
            id='fake_id',
            status='active'
        )
