#!/usr/bin/python3

import ccitools.conf
import logging
import sys

from ccitools.cmd.base.rundeck import BaseRundeckCMD
from ccitools.errors import RundeckAPIError

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class TakeoverScheduledRundeckJobsCMD(BaseRundeckCMD):
    def __init__(self):
        super(TakeoverScheduledRundeckJobsCMD, self).__init__(
            description="Rundeck takeover for scheduled jobs")

    def main(self, args):

        args = self.parse_args(args)

        try:
            logger.info("Taking over all scheduled jobs...")
            logger.info(self.rundeckcli.takeover_schedule())
        except RundeckAPIError as err:
            logger.error(err)


# Needs static method for setup.cfg
def main(args=None):
    TakeoverScheduledRundeckJobsCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)
