import logging

from ccitools.cmd.hypervisor_has_vms import main
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
class TestHypervisorHasVMs(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self, mock_crc):

        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)

    def test_proper_data(self, mock_crc):
        mock_crc.return_value = mock_crc
        mock_crc.get_servers_by_hypervisor.return_value = (
            [fixtures.SERVER]
        )

        main([
            '--ignore',
            '--hypervisors', 'h1 h2',
        ])

    def test_proper_data_no_ignore(self, mock_crc):
        mock_crc.return_value = mock_crc
        mock_crc.get_servers_by_hypervisor.return_value = (
            [fixtures.SERVER]
        )

        with self.assertRaises(SystemExit) as cm:
            main([
                '--hypervisors', 'h1 h2',
            ])
        self.assertEqual(cm.exception.code, 1)

    def test_no_servers(self, mock_crc):
        mock_crc.return_value = mock_crc
        mock_crc.get_servers_by_hypervisor.return_value = (
            None
        )
        main([
            '--ignore',
            '--hypervisors', 'h1 h2',
        ])
