from oslo_config import cfg


host = cfg.StrOpt(
    'host',
    default='dbod-ccinflux.cern.ch',
    help="Host to use for influxdb")

port = cfg.IntOpt(
    'port',
    default=8087,
    help="Port to use for influxdb")

database = cfg.StrOpt(
    'database',
    default='dblogger',
    help="Database for influxdb")

user = cfg.StrOpt(
    'user',
    default='fake_user',
    help="Defalt user for influxdb")

password = cfg.StrOpt(
    'password',
    default='fake_password',
    help="Defalt password for influxdb")


GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    host,
    port,
    database,
    user,
    password,
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
