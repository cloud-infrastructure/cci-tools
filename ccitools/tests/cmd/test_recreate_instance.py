import logging
import requests_mock
import tempfile

from ccitools.cmd import recreate_instance
from tenacity import stop_after_attempt
from tenacity import wait_none
from unittest import mock
from unittest import TestCase


class TestRecreateInstance(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):
        with self.assertRaises(SystemExit) as cm:
            recreate_instance.main([])
        self.assertEqual(cm.exception.code, 2)

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_fail_add_account(self, mock_crc, mock_landb):
        instance_mock = mock.MagicMock(
            id="1",
            tenant_id="fake",
        )
        instance_mock.name = "my_test_vm"

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        mock_client.grant_role_in_project.side_effect = Exception()

        with self.assertRaises(SystemExit) as cm:
            recreate_instance.main([
                '--vm', 'my_test_vm'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm'
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_stop_raises_exception(self, mock_crc, mock_landb):
        instance_mock = mock.MagicMock(
            id="1",
            tenant_id="fake",
            locked=False,
        )
        instance_mock.name = "my_test_vm"
        instance_mock.stop.side_effect = Exception()

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'

        with self.assertRaises(SystemExit) as cm:
            recreate_instance.main([
                '--vm', 'my_test_vm'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm'
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )
        instance_mock.stop.assert_called_with()

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_fail_to_stop(self, mock_crc, mock_landb):
        instance_mock = mock.MagicMock(
            id="1",
            tenant_id="fake",
            status='ACTIVE'
        )
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.return_value = instance_mock

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--vm', 'my_test_vm'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm'
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )
        instance_mock.stop.assert_called_with()

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_snapshot_bfi_raises(self, mock_crc, mock_landb):
        instance_mock = mock.MagicMock(
            id="1",
            tenant_id="fake",
            image='my_fake_image',
            status='SHUTOFF'
        )
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.return_value = instance_mock

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        instance_mock.create_image.side_effect = Exception()

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--vm', 'my_test_vm'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm',
            all=False,
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_snapshot_bfv_raises(self, mock_crc, mock_landb):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": '',
            "os-extended-volumes:volumes_attached": [
                {
                    "id": "fake_volume_id_1",
                },
                {
                    "id": "fake_volume_id_2",
                }
            ],
            "status": 'SHUTOFF'
        }
        instance_mock = mock.MagicMock(**attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.return_value = instance_mock

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        mock_client.get_volumes.return_value = [
            mock.MagicMock(
                id="fake_volume_id_2",
                attachments=[{'device': '/dev/vdb'}]),
            mock.MagicMock(
                id="fake_volume_id_1",
                attachments=[{'device': '/dev/vda'}])
        ]
        mock_client.create_volume_snapshot.side_effect = Exception()

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--vm', 'my_test_vm'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm',
            all=False
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_fail_to_snapshot_bfi(self, mock_crc, mock_landb):
        instance_mock = mock.MagicMock(
            id="1",
            tenant_id="fake",
            image='my_fake_image',
            status='SHUTOFF'
        )
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.return_value = instance_mock

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        image_mock = mock.MagicMock(status='queued')
        image_mock.name = 'my_test_vm_snap_'
        mock_client.get_images_by_id.return_value = image_mock

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_image_active.retry.wait = wait_none()
        cmd.wait_until_image_active.retry.stop = stop_after_attempt(1)

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--vm', 'my_test_vm'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm',
            all=False
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_fail_to_snapshot_bfv(self, mock_crc, mock_landb):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": '',
            "os-extended-volumes:volumes_attached": [
                {
                    "id": "fake_volume_id_1",
                },
                {
                    "id": "fake_volume_id_2",
                }
            ],
            "status": 'SHUTOFF'
        }
        instance_mock = mock.MagicMock(**attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.return_value = instance_mock

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        mock_client.get_volumes.return_value = [
            mock.MagicMock(
                id="fake_volume_id_2",
                attachments=[{'device': '/dev/vdb'}]),
            mock.MagicMock(
                id="fake_volume_id_1",
                attachments=[{'device': '/dev/vda'}])
        ]
        snapshot_mock = mock.MagicMock(status='creating')
        snapshot_mock.name = 'my_test_vm_snap_'
        snapshot_mock.manager.get.return_value = snapshot_mock
        mock_client.create_volume_snapshot.return_value = snapshot_mock

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_volume_snapshot_available.retry.wait = wait_none()
        cmd.wait_until_volume_snapshot_available.retry.stop = (
            stop_after_attempt(1))

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--vm', 'my_test_vm'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm',
            all=False
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_bfi_vm_no_creation(self, mock_crc, mock_landb):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": 'my_fake_image',
            "status": 'SHUTOFF',
            "OS-EXT-SRV-ATTR:host": "host",
        }
        instance_mock = mock.MagicMock(**attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.return_value = instance_mock

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        image_mock = mock.MagicMock(status='active')
        image_mock.name = 'my_test_vm_snap_'
        mock_client.get_image.return_value = image_mock

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_image_active.retry.wait = wait_none()
        cmd.wait_until_image_active.retry.stop = stop_after_attempt(1)

        cmd.main([
            '--vm', 'my_test_vm',
            '--recreate-from', 'none',
        ])

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm',
            all=False
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_bfv_vm_no_creation(self, mock_crc, mock_landb):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": '',
            'status': 'SHUTOFF',
            "os-extended-volumes:volumes_attached": [
                {
                    "id": "fake_volume_id_1",
                },
                {
                    "id": "fake_volume_id_2",
                }

            ]
        }
        instance_mock = mock.MagicMock(**attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.return_value = instance_mock

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        mock_client.get_volumes.return_value = [
            mock.MagicMock(
                id="fake_volume_id_2",
                attachments=[{'device': '/dev/vdb'}]),
            mock.MagicMock(
                id="fake_volume_id_1",
                attachments=[{'device': '/dev/vda'}])
        ]
        snapshot_mock = mock.MagicMock(status='available')
        snapshot_mock.name = 'my_test_vm_snap_'
        snapshot_mock.manager.get.return_value = snapshot_mock
        mock_client.create_volume_snapshot.return_value = (
            snapshot_mock
        )

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_volume_snapshot_available.retry.wait = wait_none()
        cmd.wait_until_volume_snapshot_available.retry.stop = (
            stop_after_attempt(1))

        cmd.main([
            '--vm', 'my_test_vm',
            '--recreate-from', 'none',
        ])

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm',
            all=False
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )

    @mock.patch('ccitools.cmd.recreate_instance.common')
    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    @mock.patch('ccitools.cmd.recreate_instance.tempfile')
    @requests_mock.Mocker()
    def test_recreate_bfi_vm_sysprep_no_creation(self, mock_temp, mock_crc,
                                                 mock_landb,
                                                 mock_common, mock_req):
        fd, path = tempfile.mkstemp()
        mock_temp.mkstemp.return_value = (fd, path)
        mock_req.get(recreate_instance.APPLIANCE_URL,
                     headers={'content-length': '10'},
                     content=b"EMPTY_FILE")

        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": "my_fake_image",
            "status": "SHUTOFF",
            "OS-EXT-SRV-ATTR:host": "fake_host",
            "OS-EXT-SRV-ATTR:instance_name": "instance-0000001"
        }
        instance_mock = mock.MagicMock(**attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.return_value = instance_mock

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        image_mock = mock.MagicMock(status='active')
        image_mock.name = 'my_test_vm_snap_'
        mock_client.get_image.return_value = image_mock

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_image_active.retry.wait = wait_none()
        cmd.wait_until_image_active.retry.stop = stop_after_attempt(1)

        cmd.main([
            '--vm', 'my_test_vm',
            '--recreate-from', 'none',
            '--sysprep', 'True',
        ])

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm',
            all=False
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )
        mock_common.scp_file.assert_called_with(
            'fake_host',
            path,
            path
        )
        mock_common.ssh_executor.assert_has_calls([
            mock.call(
                'fake_host',
                'tar xvfJ {0} -C /tmp/ && rm {0}'.format(path)
            ),
            mock.call(
                'fake_host',
                ('LIBGUESTFS_PATH=/tmp/appliance/virt-sysprep '
                 '--operations net-hwaddr,udev-persistent-net,customize '
                 '--delete \'/etc/krb5.keytab\' '
                 '--delete \'/var/lib/cern-private-cloud-addons/state\' '
                 '-d instance-0000001')
            ),
        ])

    @mock.patch('ccitools.cmd.recreate_instance.common')
    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    @mock.patch('ccitools.cmd.recreate_instance.tempfile')
    @requests_mock.Mocker()
    def test_recreate_bfi_vm_sysprep_snapshot_raises(
            self, mock_temp, mock_crc, mock_landb,
            mock_common, mock_req):
        fd, path = tempfile.mkstemp()
        mock_temp.mkstemp.return_value = (fd, path)
        mock_req.get(recreate_instance.APPLIANCE_URL,
                     headers={'content-length': '10'},
                     content=b"EMPTY_FILE")

        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": "my_fake_image",
            "status": "SHUTOFF",
            "OS-EXT-SRV-ATTR:host": "fake_host",
            "OS-EXT-SRV-ATTR:instance_name": "instance-0000001"
        }
        instance_mock = mock.MagicMock(**attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.return_value = instance_mock

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        image_mock = mock.MagicMock(status='active')
        image_mock.name = 'my_test_vm_snap_'
        sysprep_snapshot_mock = mock.MagicMock(status='active')
        sysprep_snapshot_mock.name = 'my_test_vm_base_'
        mock_client.get_images_by_id.side_effect = iter([
            image_mock,
            sysprep_snapshot_mock
        ])
        instance_mock.create_image.side_effect = Exception()

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_image_active.retry.wait = wait_none()
        cmd.wait_until_image_active.retry.stop = stop_after_attempt(1)

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--vm', 'my_test_vm',
                '--sysprep', 'True',
                '--sysprep-snapshot', 'True',
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm',
            all=False
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )

    @mock.patch('ccitools.cmd.recreate_instance.common')
    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    @mock.patch('ccitools.cmd.recreate_instance.tempfile')
    @requests_mock.Mocker()
    def test_recreate_bfi_vm_sysprep_snapshot_no_creation(
            self, mock_temp, mock_crc, mock_landb,
            mock_common, mock_req):
        fd, path = tempfile.mkstemp()
        mock_temp.mkstemp.return_value = (fd, path)
        mock_req.get(recreate_instance.APPLIANCE_URL,
                     headers={'content-length': '10'},
                     content=b"EMPTY_FILE")

        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": "my_fake_image",
            "status": "SHUTOFF",
            "OS-EXT-SRV-ATTR:host": "fake_host",
            "OS-EXT-SRV-ATTR:instance_name": "instance-0000001"
        }
        instance_mock = mock.MagicMock(**attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.return_value = instance_mock

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        image_mock = mock.MagicMock(status='active')
        image_mock.name = 'my_test_vm_snap_'
        sysprep_snapshot_mock = mock.MagicMock(status='active')
        sysprep_snapshot_mock.name = 'my_test_vm_base_'
        mock_client.get_image.side_effect = iter([
            image_mock,
            sysprep_snapshot_mock,
            sysprep_snapshot_mock
        ])

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_image_active.retry.wait = wait_none()
        cmd.wait_until_image_active.retry.stop = stop_after_attempt(1)

        cmd.main([
            '--vm', 'my_test_vm',
            '--recreate-from', 'none',
            '--sysprep', 'True',
            '--sysprep-snapshot', 'True',
        ])

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm',
            all=False
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )
        mock_common.scp_file.assert_called_with(
            'fake_host',
            path,
            path
        )
        mock_common.ssh_executor.assert_has_calls([
            mock.call(
                'fake_host',
                'tar xvfJ {0} -C /tmp/ && rm {0}'.format(path)
            ),
            mock.call(
                'fake_host',
                ('LIBGUESTFS_PATH=/tmp/appliance/virt-sysprep '
                 '--operations net-hwaddr,udev-persistent-net,customize '
                 '--delete \'/etc/krb5.keytab\' '
                 '--delete \'/var/lib/cern-private-cloud-addons/state\' '
                 '-d instance-0000001')
            ),
        ])

    @mock.patch('ccitools.cmd.recreate_instance.common')
    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    @mock.patch('ccitools.cmd.recreate_instance.tempfile')
    @requests_mock.Mocker()
    def test_recreate_bfv_vm_sysprep_snapshot_no_creation(
            self, mock_temp, mock_crc, mock_landb,
            mock_common, mock_req):
        fd, path = tempfile.mkstemp()
        mock_temp.mkstemp.return_value = (fd, path)
        mock_req.get(recreate_instance.APPLIANCE_URL,
                     headers={'content-length': '10'},
                     content=b"EMPTY_FILE")
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": '',
            'status': 'SHUTOFF',
            "os-extended-volumes:volumes_attached": [
                {
                    "id": "fake_volume_id_1",
                },
                {
                    "id": "fake_volume_id_2",
                }
            ],
            "OS-EXT-SRV-ATTR:host": "fake_host",
            "OS-EXT-SRV-ATTR:instance_name": "instance-0000001"
        }
        instance_mock = mock.MagicMock(**attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.return_value = instance_mock

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        mock_client.get_volumes.return_value = [
            mock.MagicMock(
                id="fake_volume_id_2",
                attachments=[{'device': '/dev/vdb'}]),
            mock.MagicMock(
                id="fake_volume_id_1",
                attachments=[{'device': '/dev/vda'}])
        ]
        sysprep_snapshot_mock = mock.MagicMock(status='available')
        sysprep_snapshot_mock.name = 'my_test_vm_base_'
        sysprep_snapshot_mock.manager.get.return_value = (
            sysprep_snapshot_mock)

        snapshot_mock = mock.MagicMock(status='available')
        snapshot_mock.name = 'my_test_vm_snap_'
        snapshot_mock.manager.get.return_value = snapshot_mock
        mock_client.create_volume_snapshot.side_effect = iter([
            sysprep_snapshot_mock,
            snapshot_mock
        ])

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_volume_snapshot_available.retry.wait = wait_none()
        cmd.wait_until_volume_snapshot_available.retry.stop = (
            stop_after_attempt(1))

        cmd.main([
            '--vm', 'my_test_vm',
            '--recreate-from', 'none',
            '--sysprep', 'True',
            '--sysprep-snapshot', 'True',
            '--sysprep-params', '--operations noop',
        ])

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm',
            all=False
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )
        mock_common.scp_file.assert_called_with(
            'fake_host',
            path,
            path
        )
        mock_common.ssh_executor.assert_has_calls([
            mock.call(
                'fake_host',
                'tar xvfJ {0} -C /tmp/ && rm {0}'.format(path)
            ),
            mock.call(
                'fake_host',
                ('LIBGUESTFS_PATH=/tmp/appliance/virt-sysprep '
                 '--operations noop -d instance-0000001')
            ),
        ])

    @mock.patch('ccitools.cmd.recreate_instance.common')
    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    @mock.patch('ccitools.cmd.recreate_instance.tempfile')
    @requests_mock.Mocker()
    def test_recreate_bfv_vm_sysprep_no_creation(self, mock_temp, mock_crc,
                                                 mock_landb,
                                                 mock_common, mock_req):
        fd, path = tempfile.mkstemp()
        mock_temp.mkstemp.return_value = (fd, path)
        mock_req.get(recreate_instance.APPLIANCE_URL,
                     headers={'content-length': '10'},
                     content=b"EMPTY_FILE")
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": '',
            'status': 'SHUTOFF',
            "os-extended-volumes:volumes_attached": [
                {
                    "id": "fake_volume_id_1",
                },
                {
                    "id": "fake_volume_id_2",
                }
            ],
            "OS-EXT-SRV-ATTR:host": "fake_host",
            "OS-EXT-SRV-ATTR:instance_name": "instance-0000001"
        }
        instance_mock = mock.MagicMock(**attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.return_value = instance_mock

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        mock_client.get_volumes.return_value = [
            mock.MagicMock(
                id="fake_volume_id_2",
                attachments=[{'device': '/dev/vdb'}]),
            mock.MagicMock(
                id="fake_volume_id_1",
                attachments=[{'device': '/dev/vda'}])
        ]
        snapshot_mock = mock.MagicMock(status='available')
        snapshot_mock.name = 'my_test_vm_snap_'
        snapshot_mock.manager.get.return_value = snapshot_mock
        mock_client.create_volume_snapshot.return_value = (
            snapshot_mock
        )

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_volume_snapshot_available.retry.wait = wait_none()
        cmd.wait_until_volume_snapshot_available.retry.stop = (
            stop_after_attempt(1))

        cmd.main([
            '--vm', 'my_test_vm',
            '--recreate-from', 'none',
            '--sysprep', 'True',
            '--sysprep-params', '--operations noop'
        ])

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm',
            all=False
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )
        mock_common.scp_file.assert_called_with(
            'fake_host',
            path,
            path
        )
        mock_common.ssh_executor.assert_has_calls([
            mock.call(
                'fake_host',
                'tar xvfJ {0} -C /tmp/ && rm {0}'.format(path)
            ),
            mock.call(
                'fake_host',
                ('LIBGUESTFS_PATH=/tmp/appliance/virt-sysprep '
                 '--operations noop -d instance-0000001')
            ),
        ])

    @mock.patch('ccitools.cmd.recreate_instance.socket')
    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_from_image_bfi_vm_delete_raises_exception(
            self, mock_crc, mock_landb, mock_socket):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": "my_fake_image",
            "status": "SHUTOFF",
            "flavor": {
                "original_name": "m2.small",
            },
            "metadata": {},
            "user_id": 'fake_user',
            "OS-EXT-AZ:availability_zone": "cern-geneva-a",
            "os-extended-volumes:volumes_attached": []
        }
        instance_mock = mock.MagicMock(**attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.return_value = instance_mock
        flavor_mock = mock.MagicMock()
        flavor_mock.name = 'm2.small'
        instance_mock.manager._list.side_effect = iter([
            [flavor_mock],
            []
        ])
        instance_mock.delete.side_effect = Exception()

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        image_mock = mock.MagicMock(status='active')
        image_mock.name = 'my_test_vm_snap_'
        mock_client.get_images_by_id.return_value = image_mock
        mock_client.get_images_by_name.return_value = [image_mock]
        mock_socket.return_value.gethostbyname.return_value = '127.0.0.1'

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_image_active.retry.wait = wait_none()
        cmd.wait_until_image_active.retry.stop = stop_after_attempt(1)

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--vm', 'my_test_vm',
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm',
            all=False,
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )

    @mock.patch('ccitools.cmd.recreate_instance.socket')
    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_from_image_bfi_vm_fail_to_delete(
            self, mock_crc, mock_landb, mock_socket):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": "my_fake_image",
            "status": "SHUTOFF",
            "flavor": {
                "original_name": "m2.small",
            },
            "metadata": {},
            "user_id": 'fake_user',
            "OS-EXT-AZ:availability_zone": "cern-geneva-a",
            "os-extended-volumes:volumes_attached": []
        }
        instance_mock = mock.MagicMock(**attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.return_value = instance_mock
        flavor_mock = mock.MagicMock()
        flavor_mock.name = 'm2.small'
        instance_mock.manager._list.side_effect = iter([
            [flavor_mock],
            []
        ])

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        image_mock = mock.MagicMock(status='active')
        image_mock.name = 'my_test_vm_snap_'
        mock_client.get_images_by_id.return_value = image_mock
        mock_client.get_images_by_name.return_value = [image_mock]
        mock_socket.return_value.gethostbyname.return_value = '127.0.0.1'

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_image_active.retry.wait = wait_none()
        cmd.wait_until_image_active.retry.stop = stop_after_attempt(1)
        cmd.wait_until_vm_is_gone.retry.wait = wait_none()
        cmd.wait_until_vm_is_gone.retry.stop = stop_after_attempt(1)

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--vm', 'my_test_vm',
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm',
            all=False
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_from_image_bfi_create_raises_exception(
            self, mock_crc, mock_landb):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": "my_fake_image",
            "flavor": {
                "original_name": "m2.small",
            },
            "locked": False,
            "metadata": {},
            "user_id": 'fake_user',
            "OS-EXT-AZ:availability_zone": "cern-geneva-a",
            "os-extended-volumes:volumes_attached": []
        }
        instance_mock = mock.MagicMock(
            status='SHUTOFF',
            **attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.side_effect = iter([
            mock.MagicMock(status='SHUTOFF', **attrs),
            mock.MagicMock(status='ACTIVE', **attrs)
        ])
        flavor_mock = mock.MagicMock()
        flavor_mock.name = 'm2.small'
        instance_mock.manager._list.side_effect = iter([
            [flavor_mock],
            []
        ])

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        image_mock = mock.MagicMock(status='active')
        image_mock.name = 'my_test_vm_snap_'
        mock_client.get_images_by_id.return_value = image_mock
        mock_client.get_images_by_name.return_value = [image_mock]
        mock_client.create_server.side_effect = Exception()

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_image_active.retry.wait = wait_none()
        cmd.wait_until_image_active.retry.stop = stop_after_attempt(1)
        cmd.wait_until_vm_is_gone.retry.wait = wait_none()
        cmd.wait_until_vm_is_gone.retry.stop = stop_after_attempt(1)

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--vm', 'my_test_vm',
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm',
            all=False
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_from_image_bfi_create_fails(
            self, mock_crc, mock_landb):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": "my_fake_image",
            "flavor": {
                "original_name": "m2.small",
            },
            "metadata": {},
            "user_id": 'fake_user',
            "OS-EXT-AZ:availability_zone": "cern-geneva-a",
            "os-extended-volumes:volumes_attached": []
        }
        instance_mock = mock.MagicMock(
            status='SHUTOFF',
            **attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.side_effect = instance_mock
        flavor_mock = mock.MagicMock()
        flavor_mock.name = 'm2.small'
        instance_mock.manager._list.side_effect = iter([
            [flavor_mock],
            []
        ])

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        image_mock = mock.MagicMock(status='active')
        image_mock.name = 'my_test_vm_snap_'
        mock_client.get_images_by_id.return_value = image_mock
        mock_client.get_images_by_name.return_value = [image_mock]

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_image_active.retry.wait = wait_none()
        cmd.wait_until_image_active.retry.stop = stop_after_attempt(1)
        cmd.wait_until_vm_is_gone.retry.wait = wait_none()
        cmd.wait_until_vm_is_gone.retry.stop = stop_after_attempt(1)

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--vm', 'my_test_vm',
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm'
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )

    @mock.patch('ccitools.cmd.recreate_instance.NovaDB')
    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_from_image_bfi_correct(
            self, mock_crc, mock_landb, mock_db):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": "my_fake_image",
            "flavor": {
                "original_name": "m2.small",
            },
            "locked": False,
            "metadata": {},
            "user_id": 'fake_user',
            "OS-EXT-SRV-ATTR:host": "host",
            "OS-EXT-AZ:availability_zone": "cern-geneva-a",
            "os-extended-volumes:volumes_attached": []
        }
        instance_mock = mock.MagicMock(
            status='SHUTOFF',
            **attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.side_effect = iter([
            mock.MagicMock(status='SHUTOFF', **attrs),
            mock.MagicMock(status='ACTIVE', **attrs)
        ])
        flavor_mock = mock.MagicMock()
        flavor_mock.name = 'm2.small'
        instance_mock.manager._list.side_effect = iter([
            [flavor_mock],
            []
        ])

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        image_mock = mock.MagicMock(status='active')
        image_mock.name = 'my_test_vm_snap_'
        mock_client.get_image.return_value = image_mock
        mock_client.get_images_by_name.return_value = [image_mock]

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_image_active.retry.wait = wait_none()
        cmd.wait_until_image_active.retry.stop = stop_after_attempt(1)
        cmd.wait_until_vm_is_gone.retry.wait = wait_none()
        cmd.wait_until_vm_is_gone.retry.stop = stop_after_attempt(1)

        cmd.main([
            '--vm', 'my_test_vm',
        ])

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm',
            all=False
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )

    @mock.patch('ccitools.cmd.recreate_instance.NovaDB')
    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_from_image_bfi_image_tags(
            self, mock_crc, mock_landb, mock_db):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": "my_fake_image",
            "flavor": {
                "original_name": "m2.small",
            },
            "locked": False,
            "metadata": {},
            "user_id": 'fake_user',
            "OS-EXT-SRV-ATTR:host": "host",
            "OS-EXT-AZ:availability_zone": "cern-geneva-a",
            "os-extended-volumes:volumes_attached": []
        }
        instance_mock = mock.MagicMock(
            status='SHUTOFF',
            **attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.side_effect = iter([
            mock.MagicMock(status='SHUTOFF', **attrs),
            mock.MagicMock(status='ACTIVE', **attrs)
        ])
        flavor_mock = mock.MagicMock()
        flavor_mock.name = 'm2.small'
        instance_mock.manager._list.side_effect = iter([
            [flavor_mock],
            []
        ])

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        image_mock = mock.MagicMock(status='active')
        image_mock.name = 'my_test_vm_snap_'
        mock_client.get_image.return_value = image_mock
        mock_client.get_images_by_name.return_value = [image_mock]
        mock_novadb = mock_db.return_value
        mock_novadb.get_cell_connection_string.return_value = 'conn'
        mock_novadb.get_delete_on_termination.return_value = 0

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_image_active.retry.wait = wait_none()
        cmd.wait_until_image_active.retry.stop = stop_after_attempt(1)
        cmd.wait_until_vm_is_gone.retry.wait = wait_none()
        cmd.wait_until_vm_is_gone.retry.stop = stop_after_attempt(1)

        cmd.main([
            '--vm', 'my_test_vm',
            '--image-tag', 'tag1', '--image-tag', 'tag2'
        ])

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm',
            all=False
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )

    @mock.patch('ccitools.cmd.recreate_instance.NovaDB')
    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_from_image_bfi_new_project(
            self, mock_crc, mock_landb, mock_db):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": "my_fake_image",
            "flavor": {
                "original_name": "m2.small",
                "disk": 10,
                'vcpus': 1,
                'ram': 1875
            },
            "locked": False,
            "metadata": {},
            "user_id": 'fake_user',
            "OS-EXT-SRV-ATTR:host": "host",
            "OS-EXT-AZ:availability_zone": "cern-geneva-a",
            "os-extended-volumes:volumes_attached": []
        }
        instance_mock = mock.MagicMock(
            status='SHUTOFF',
            **attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.side_effect = iter([
            mock.MagicMock(status='SHUTOFF', **attrs),
            mock.MagicMock(status='ACTIVE', **attrs)
        ])
        flavor_mock = mock.MagicMock()
        flavor_mock.name = 'm2.small'
        flavor_mock.disk = 10
        instance_mock.manager._list.side_effect = iter([
            [flavor_mock],
            [],
            [flavor_mock],
            []
        ])
        quota_mock = mock.MagicMock()
        d = {
            "quota": {
                "cern": {
                    "compute": {
                        "instances": 61,
                        "cores": 36,
                        "ram": 21000,
                    }
                }
            },
            "current": {
                "cern": {
                    "compute": {
                        "instances": 1,
                        "cores": 1,
                        "ram": 1875,
                    }
                }
            },
        }
        quota_mock.__getitem__.side_effect = d.__getitem__
        quota_mock.__iter__.side_effect = d.__iter__

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        image_mock = mock.MagicMock(status='active')
        image_mock.name = 'my_test_vm_snap_'
        mock_client.get_image.return_value = image_mock
        mock_client.get_images_by_name.return_value = [image_mock]
        mock_client.find_region_vm.return_value = 'cern'
        mock_client.get_project_quota.return_value = quota_mock
        mock_novadb = mock_db.return_value
        mock_novadb.get_cell_connection_string.return_value = 'conn'
        mock_novadb.get_delete_on_termination.return_value = 0

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_image_active.retry.wait = wait_none()
        cmd.wait_until_image_active.retry.stop = stop_after_attempt(1)
        cmd.wait_until_vm_is_gone.retry.wait = wait_none()
        cmd.wait_until_vm_is_gone.retry.stop = stop_after_attempt(1)

        cmd.main([
            '--vm', 'my_test_vm',
            '--new-project-id', 'new'
        ])

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm',
            all=False
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='new',
            user_id='fake_user'
        )

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_bfv_vm_volume_not_found(
            self, mock_crc, mock_landb):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": '',
            "flavor": {
                "original_name": "m2.small",
            },
            "metadata": {},
            "user_id": 'fake_user',
            "OS-EXT-AZ:availability_zone": "cern-geneva-a",
            "os-extended-volumes:volumes_attached": [
                {
                    "id": "fake_volume_id_1",
                },
                {
                    "id": "fake_volume_id_2",
                }
            ]
        }
        instance_mock = mock.MagicMock(**attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.side_effect = iter([
            mock.MagicMock(status='SHUTOFF', **attrs),
            mock.MagicMock(status='ACTIVE', **attrs)
        ])
        flavor_mock = mock.MagicMock()
        flavor_mock.name = 'm2.small'
        instance_mock.manager._list.side_effect = iter([
            [flavor_mock],
            []
        ])

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        mock_client.get_volumes.side_effect = [
            mock.MagicMock(
                id="fake_volume_id_2",
                attachments=[{'device': '/dev/vdb'}]),
            mock.MagicMock(
                id="fake_volume_id_1",
                attachments=[{'device': '/dev/vdc'}])
        ]

        snapshot_mock = mock.MagicMock(status='available')
        snapshot_mock.name = 'my_test_vm_snap_'
        snapshot_mock.manager.get.return_value = snapshot_mock
        mock_client.create_volume_snapshot.return_value = (
            snapshot_mock
        )

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_volume_snapshot_available.retry.wait = wait_none()
        cmd.wait_until_volume_snapshot_available.retry.stop = (
            stop_after_attempt(1))

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--vm', 'my_test_vm',
                '--recreate-from', 'volume'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )

    @mock.patch('ccitools.cmd.recreate_instance.NovaDB')
    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_bfv_vm_volume_correct(
            self, mock_crc, mock_landb, mock_db):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": '',
            "flavor": {
                "original_name": "m2.small",
                "disk": 20,
            },
            "locked": False,
            "metadata": {},
            "user_id": 'fake_user',
            "OS-EXT-SRV-ATTR:host": "host",
            "OS-EXT-AZ:availability_zone": "cern-geneva-a",
            "os-extended-volumes:volumes_attached": [
                {
                    "id": "fake_volume_id_1",
                },
                {
                    "id": "fake_volume_id_2",
                }
            ]
        }
        instance_mock = mock.MagicMock(**attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.side_effect = iter([
            mock.MagicMock(status='SHUTOFF', **attrs),
            mock.MagicMock(status='ACTIVE', **attrs)
        ])
        flavor_mock = mock.MagicMock()
        flavor_mock.name = 'm2.small'
        instance_mock.manager._list.side_effect = iter([
            [flavor_mock],
            []
        ])

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        mock_client.get_volumes.return_value = [
            mock.MagicMock(
                id="fake_volume_id_2",
                attachments=[{'device': '/dev/vdb'}]),
            mock.MagicMock(
                id="fake_volume_id_1",
                attachments=[{'device': '/dev/vda'}])
        ]
        snapshot_mock = mock.MagicMock(status='available')
        snapshot_mock.name = 'my_test_vm_snap_'
        snapshot_mock.manager.get.return_value = snapshot_mock
        mock_client.create_volume_snapshot.return_value = (
            snapshot_mock
        )
        mock_novadb = mock_db.return_value
        mock_novadb.get_cell_connection_string.return_value = 'conn'
        mock_novadb.get_delete_on_termination.return_value = 0

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_volume_snapshot_available.retry.wait = wait_none()
        cmd.wait_until_volume_snapshot_available.retry.stop = (
            stop_after_attempt(1))
        cmd.wait_until_vm_is_gone.retry.wait = wait_none()
        cmd.wait_until_vm_is_gone.retry.stop = stop_after_attempt(1)

        cmd.main([
            '--vm', 'my_test_vm',
            '--recreate-from', 'volume'
        ])

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm',
            all=False
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_bfi_into_volume_raises_exception(
            self, mock_crc, mock_landb):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": "my_fake_image",
            "flavor": {
                "original_name": "m2.small",
                "disk": 20,
            },
            "metadata": {},
            "user_id": 'fake_user',
            "OS-EXT-AZ:availability_zone": "cern-geneva-a",
            "os-extended-volumes:volumes_attached": [
                {
                    "id": "fake_volume_id_1",
                },
                {
                    "id": "fake_volume_id_2",
                }
            ]
        }
        instance_mock = mock.MagicMock(**attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.side_effect = iter([
            mock.MagicMock(status='SHUTOFF', **attrs),
            mock.MagicMock(status='ACTIVE', **attrs)
        ])
        flavor_mock = mock.MagicMock()
        flavor_mock.name = 'm2.small'
        instance_mock.manager._list.side_effect = iter([
            [flavor_mock],
            []
        ])
        image_mock = mock.MagicMock(
            status='active',
            size=8 * (1024.0**3),
            min_disk=10
        )
        image_mock.name = 'my_test_vm_snap_'

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_image.return_value = image_mock
        mock_client.get_images_by_name.return_value = [image_mock]
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'

        snapshot_mock = mock.MagicMock(status='available')
        snapshot_mock.name = 'my_test_vm_snap_'
        snapshot_mock.manager.get.return_value = snapshot_mock
        mock_client.create_volume_snapshot.return_value = (
            snapshot_mock
        )

        volume_mock = mock.MagicMock(status='available')
        volume_mock.name = 'my_test_vm_vol_'
        volume_mock.manager.get.return_value = volume_mock
        mock_client.create_volume.side_effect = Exception()
        mock_client.get_volumes.return_value = [
            mock.MagicMock(
                id="fake_volume_id_2",
                attachments=[{'device': '/dev/vdb'}]),
            mock.MagicMock(
                id="fake_volume_id_1",
                attachments=[{'device': '/dev/vdc'}]),
            volume_mock,
        ]

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_image_active.retry.wait = wait_none()
        cmd.wait_until_image_active.retry.stop = stop_after_attempt(1)
        cmd.wait_until_volume_available.retry.wait = wait_none()
        cmd.wait_until_volume_available.retry.stop = stop_after_attempt(1)
        cmd.wait_until_vm_is_gone.retry.wait = wait_none()
        cmd.wait_until_vm_is_gone.retry.stop = stop_after_attempt(1)

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--vm', 'my_test_vm',
                '--recreate-from', 'volume'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm'
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )

    @mock.patch('ccitools.cmd.recreate_instance.NovaDB')
    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_bfi_into_volume_correct(
            self, mock_crc, mock_landb, mock_db):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": "my_fake_image",
            "flavor": {
                "original_name": "m2.small",
                "disk": 20,
            },
            "locked": False,
            "metadata": {},
            "user_id": 'fake_user',
            "OS-EXT-SRV-ATTR:host": "host",
            "OS-EXT-AZ:availability_zone": "cern-geneva-a",
            "os-extended-volumes:volumes_attached": [
                {
                    "id": "fake_volume_id_1",
                },
                {
                    "id": "fake_volume_id_2",
                }
            ]
        }
        instance_mock = mock.MagicMock(**attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.side_effect = iter([
            mock.MagicMock(status='SHUTOFF', **attrs),
            mock.MagicMock(status='ACTIVE', **attrs)
        ])
        flavor_mock = mock.MagicMock()
        flavor_mock.name = 'm2.small'
        instance_mock.manager._list.side_effect = iter([
            [flavor_mock],
            []
        ])
        image_mock = mock.MagicMock(
            status='active',
            size=8 * (1024.0**3),
            min_disk=10
        )
        image_mock.name = 'my_test_vm_snap_'

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_image.return_value = image_mock
        mock_client.get_images_by_name.return_value = [image_mock]
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'

        snapshot_mock = mock.MagicMock(status='available')
        snapshot_mock.name = 'my_test_vm_snap_'
        snapshot_mock.manager.get.return_value = snapshot_mock
        mock_client.create_volume_snapshot.return_value = (
            snapshot_mock
        )

        volume_mock = mock.MagicMock(status='available')
        volume_mock.name = 'my_test_vm_vol_'
        volume_mock.manager.get.return_value = volume_mock
        mock_client.create_volume.return_value = (
            volume_mock
        )
        mock_client.get_volumes.return_value = [
            mock.MagicMock(
                id="fake_volume_id_2",
                attachments=[{'device': '/dev/vdb'}]),
            mock.MagicMock(
                id="fake_volume_id_1",
                attachments=[{'device': '/dev/vdc'}]),
            volume_mock,
        ]
        mock_novadb = mock_db.return_value
        mock_novadb.get_cell_connection_string.return_value = 'conn'
        mock_novadb.get_delete_on_termination.return_value = 0

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_image_active.retry.wait = wait_none()
        cmd.wait_until_image_active.retry.stop = stop_after_attempt(1)
        cmd.wait_until_volume_available.retry.wait = wait_none()
        cmd.wait_until_volume_available.retry.stop = stop_after_attempt(1)
        cmd.wait_until_vm_is_gone.retry.wait = wait_none()
        cmd.wait_until_vm_is_gone.retry.stop = stop_after_attempt(1)

        cmd.main([
            '--vm', 'my_test_vm',
            '--recreate-from', 'volume'
        ])

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm',
            all=False
        )
        mock_client.grant_role_in_project.assert_called_with(
            project_id='fake',
            user_id='fake_user'
        )

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_from_image_flavor_not_found(
            self, mock_crc, mock_landb):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": "my_fake_image",
            "flavor": {
                "original_name": "fake_flavor",
                "disk": 10,
                'vcpus': 1,
                'ram': 1875
            },
            "metadata": {},
            "user_id": 'fake_user',
            "OS-EXT-AZ:availability_zone": "cern-geneva-a",
            "os-extended-volumes:volumes_attached": []
        }
        instance_mock = mock.MagicMock(
            status='SHUTDOWN',
            **attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.side_effect = iter([
            mock.MagicMock(status='SHUTDOWN', **attrs),
            mock.MagicMock(status='ACTIVE', **attrs)
        ])
        flavor_mock = mock.MagicMock()
        flavor_mock.name = 'm2.small'
        flavor_mock.disk = 10
        instance_mock.manager._list.side_effect = iter([
            [flavor_mock],
            [],
            [flavor_mock],
            []
        ])

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        image_mock = mock.MagicMock(status='active')
        image_mock.name = 'my_test_vm_snap_'
        mock_client.get_image.return_value = image_mock
        mock_client.get_images_by_name.return_value = [image_mock]

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_image_active.retry.wait = wait_none()
        cmd.wait_until_image_active.retry.stop = stop_after_attempt(1)
        cmd.wait_until_vm_is_gone.retry.wait = wait_none()
        cmd.wait_until_vm_is_gone.retry.stop = stop_after_attempt(1)

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--vm', 'my_test_vm',
                '--new-project-id', 'new'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm'
        )

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_from_image_not_disk_space(
            self, mock_crc, mock_landb):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": "my_fake_image",
            "flavor": {
                "original_name": "m2.small",
                "disk": 10,
                'vcpus': 1,
                'ram': 1875
            },
            "metadata": {},
            "user_id": 'fake_user',
            "OS-EXT-AZ:availability_zone": "cern-geneva-a",
            "os-extended-volumes:volumes_attached": []
        }
        instance_mock = mock.MagicMock(
            status='SHUTDOWN',
            **attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.side_effect = iter([
            mock.MagicMock(status='SHUTDOWN', **attrs),
            mock.MagicMock(status='ACTIVE', **attrs)
        ])
        flavor_mock = mock.MagicMock()
        flavor_mock.name = 'm2.small'
        flavor_mock.disk = 5
        instance_mock.manager._list.side_effect = iter([
            [flavor_mock],
            [],
            [flavor_mock],
            []
        ])

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        image_mock = mock.MagicMock(status='active')
        image_mock.name = 'my_test_vm_snap_'
        mock_client.get_image.return_value = image_mock
        mock_client.get_images_by_name.return_value = [image_mock]

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_image_active.retry.wait = wait_none()
        cmd.wait_until_image_active.retry.stop = stop_after_attempt(1)
        cmd.wait_until_vm_is_gone.retry.wait = wait_none()
        cmd.wait_until_vm_is_gone.retry.stop = stop_after_attempt(1)

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--vm', 'my_test_vm',
                '--new-project-id', 'new'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm'
        )

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_from_image_quota_instances_failure(
            self, mock_crc, mock_landb):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": "my_fake_image",
            "flavor": {
                "original_name": "m2.small",
                "disk": 10,
                'vcpus': 1,
                'ram': 1875
            },
            "metadata": {},
            "user_id": 'fake_user',
            "OS-EXT-AZ:availability_zone": "cern-geneva-a",
            "os-extended-volumes:volumes_attached": []
        }
        instance_mock = mock.MagicMock(
            status='SHUTDOWN',
            **attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.side_effect = iter([
            mock.MagicMock(status='SHUTDOWN', **attrs),
            mock.MagicMock(status='ACTIVE', **attrs)
        ])
        flavor_mock = mock.MagicMock()
        flavor_mock.name = 'm2.small'
        flavor_mock.disk = 10
        instance_mock.manager._list.side_effect = iter([
            [flavor_mock],
            [],
            [flavor_mock],
            []
        ])
        quota_instances_mock = mock.MagicMock()
        d = {
            "quota": {
                "cern": {
                    "compute": {
                        "instances": 1,
                        "cores": 80,
                        "ram": 45000,
                    }
                }
            },
            "current": {
                "cern": {
                    "compute": {
                        "instances": 1,
                        "cores": 5,
                        "ram": 1000,
                    }
                }
            }
        }

        quota_instances_mock.__getitem__.side_effect = d.__getitem__
        quota_instances_mock.__iter__.side_effect = d.__iter__

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        image_mock = mock.MagicMock(status='active')
        image_mock.name = 'my_test_vm_snap_'
        mock_client.get_image.return_value = image_mock
        mock_client.get_images_by_name.return_value = [image_mock]
        mock_client.find_region_vm.return_value = 'cern'
        mock_client.get_project_quota.return_value = quota_instances_mock

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_image_active.retry.wait = wait_none()
        cmd.wait_until_image_active.retry.stop = stop_after_attempt(1)
        cmd.wait_until_vm_is_gone.retry.wait = wait_none()
        cmd.wait_until_vm_is_gone.retry.stop = stop_after_attempt(1)

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--vm', 'my_test_vm',
                '--new-project-id', 'new'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm'
        )

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_from_image_quota_cores_failure(
            self, mock_crc, mock_landb):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": "my_fake_image",
            "flavor": {
                "original_name": "m2.small",
                "disk": 10,
                'vcpus': 1,
                'ram': 1875
            },
            "metadata": {},
            "user_id": 'fake_user',
            "OS-EXT-AZ:availability_zone": "cern-geneva-a",
            "os-extended-volumes:volumes_attached": []
        }
        instance_mock = mock.MagicMock(
            status='SHUTDOWN',
            **attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.side_effect = iter([
            mock.MagicMock(status='SHUTDOWN', **attrs),
            mock.MagicMock(status='ACTIVE', **attrs)
        ])
        flavor_mock = mock.MagicMock()
        flavor_mock.name = 'm2.small'
        flavor_mock.disk = 10
        instance_mock.manager._list.side_effect = iter([
            [flavor_mock],
            [],
            [flavor_mock],
            []
        ])
        quota_cores_mock = mock.MagicMock()
        d = {
            "quota": {
                "cern": {
                    "compute": {
                        "instances": 4,
                        "cores": 1,
                        "ram": 45000,
                    }
                }
            },
            "current": {
                "cern": {
                    "compute": {
                        "instances": 1,
                        "cores": 1,
                        "ram": 1875,
                    }
                }
            }
        }

        quota_cores_mock.__getitem__.side_effect = d.__getitem__
        quota_cores_mock.__iter__.side_effect = d.__iter__

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        image_mock = mock.MagicMock(status='active')
        image_mock.name = 'my_test_vm_snap_'
        mock_client.get_image.return_value = image_mock
        mock_client.get_images_by_name.return_value = [image_mock]
        mock_client.find_region_vm.return_value = 'cern'
        mock_client.get_project_quota.return_value = quota_cores_mock

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_image_active.retry.wait = wait_none()
        cmd.wait_until_image_active.retry.stop = stop_after_attempt(1)
        cmd.wait_until_vm_is_gone.retry.wait = wait_none()
        cmd.wait_until_vm_is_gone.retry.stop = stop_after_attempt(1)

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--vm', 'my_test_vm',
                '--new-project-id', 'new'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm'
        )

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_from_image_quota_ram_failure(
            self, mock_crc, mock_landb):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "image": "my_fake_image",
            "flavor": {
                "original_name": "m2.small",
                "disk": 10,
                'vcpus': 1,
                'ram': 1875
            },
            "metadata": {},
            "user_id": 'fake_user',
            "OS-EXT-AZ:availability_zone": "cern-geneva-a",
            "os-extended-volumes:volumes_attached": []
        }
        instance_mock = mock.MagicMock(
            status='SHUTDOWN',
            **attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.side_effect = iter([
            mock.MagicMock(status='SHUTDOWN', **attrs),
            mock.MagicMock(status='ACTIVE', **attrs)
        ])
        flavor_mock = mock.MagicMock()
        flavor_mock.name = 'm2.small'
        flavor_mock.disk = 10
        instance_mock.manager._list.side_effect = iter([
            [flavor_mock],
            [],
            [flavor_mock],
            []
        ])
        quota_ram_mock = mock.MagicMock()
        d = {
            "quota": {
                "cern": {
                    "compute": {
                        "instances": 3,
                        "cores": 60,
                        "ram": 1875,
                    }
                }
            },
            "current": {
                "cern": {
                    "compute": {
                        "instances": 1,
                        "cores": 1,
                        "ram": 1875,
                    }
                }
            }
        }

        quota_ram_mock.__getitem__.side_effect = d.__getitem__
        quota_ram_mock.__iter__.side_effect = d.__iter__

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        image_mock = mock.MagicMock(status='active')
        image_mock.name = 'my_test_vm_snap_'
        mock_client.get_image.return_value = image_mock
        mock_client.get_images_by_name.return_value = [image_mock]
        mock_client.find_region_vm.return_value = 'cern'
        mock_client.get_project_quota.return_value = quota_ram_mock

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_image_active.retry.wait = wait_none()
        cmd.wait_until_image_active.retry.stop = stop_after_attempt(1)
        cmd.wait_until_vm_is_gone.retry.wait = wait_none()
        cmd.wait_until_vm_is_gone.retry.stop = stop_after_attempt(1)

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--vm', 'my_test_vm',
                '--new-project-id', 'new'
            ])
        self.assertEqual(cm.exception.code, 1)

        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm'
        )

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_bfv_volume_quota_error(
            self, mock_crc, mock_landb):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "flavor": {
                "original_name": "m2.small",
                "disk": 10,
                'vcpus': 1,
                'ram': 1875
            },
            "metadata": {},
            "user_id": 'fake_user',
            "OS-EXT-AZ:availability_zone": "cern-geneva-a",
            "os-extended-volumes:volumes_attached": [
                {
                    "id": "fake_volume_id_1",
                },
                {
                    "id": "fake_volume_id_2",
                }
            ]
        }
        instance_mock = mock.MagicMock(**attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.side_effect = iter([
            mock.MagicMock(status='SHUTOFF', **attrs),
            mock.MagicMock(status='ACTIVE', **attrs)
        ])
        flavor_mock = mock.MagicMock()
        flavor_mock.name = 'm2.small'
        flavor_mock.disk = 10
        instance_mock.manager._list.side_effect = iter([
            [flavor_mock],
            [],
            [flavor_mock],
            []
        ])

        image_mock = mock.MagicMock(
            status='active',
            size=8 * (1024.0**3),
            min_disk=10
        )
        image_mock.name = 'my_test_vm_snap_'
        quota_volumes_mock = mock.MagicMock()
        d = {
            "quota": {
                "cern": {
                    "compute": {
                        "instances": 3,
                        "cores": 60,
                        "ram": 5400,
                    },
                    "blockstorage": {
                        "standard": {
                            "gigabytes": 100,
                            "volumes": 1
                        }
                    }
                }
            },
            "current": {
                "cern": {
                    "compute": {
                        "instances": 1,
                        "cores": 20,
                        "ram": 1875,
                    },
                    "blockstorage": {
                        "standard": {
                            "gigabytes": 20,
                            "volumes": 1
                        }
                    }
                }
            }
        }

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        mock_client.get_volumes.return_value = [
            mock.MagicMock(
                id="fake_volume_id_2",
                attachments=[{'device': '/dev/vdb'}]),
            mock.MagicMock(
                id="fake_volume_id_1",
                attachments=[{'device': '/dev/vda'}])
        ]
        mock_client.get_image.return_value = image_mock

        quota_volumes_mock.__getitem__.side_effect = d.__getitem__
        quota_volumes_mock.__iter__.side_effect = d.__iter__
        mock_client.find_region_vm.return_value = 'cern'
        mock_client.get_project_quota.return_value = quota_volumes_mock

        snapshot_mock = mock.MagicMock(status='available')
        snapshot_mock.name = 'my_test_vm_snap_'
        snapshot_mock.manager.get.return_value = snapshot_mock
        mock_client.create_volume_snapshot.return_value = (
            snapshot_mock
        )

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_volume_snapshot_available.retry.wait = wait_none()
        cmd.wait_until_volume_snapshot_available.retry.stop = (
            stop_after_attempt(1))
        cmd.wait_until_vm_is_gone.retry.wait = wait_none()
        cmd.wait_until_vm_is_gone.retry.stop = stop_after_attempt(1)

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--vm', 'my_test_vm',
                '--recreate-from', 'volume',
                '--new-project-id', 'new'
            ])
        self.assertEqual(cm.exception.code, 1)
        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm'
        )

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_bfv_volume_size_quota_error(
            self, mock_crc, mock_landb):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "flavor": {
                "original_name": "m2.small",
                "disk": 10,
                'vcpus': 1,
                'ram': 1875
            },
            "metadata": {},
            "user_id": 'fake_user',
            "OS-EXT-AZ:availability_zone": "cern-geneva-a",
            "os-extended-volumes:volumes_attached": [
                {
                    "id": "fake_volume_id_1",
                },
                {
                    "id": "fake_volume_id_2",
                }
            ]
        }
        instance_mock = mock.MagicMock(**attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.side_effect = iter([
            mock.MagicMock(status='SHUTOFF', **attrs),
            mock.MagicMock(status='ACTIVE', **attrs)
        ])
        flavor_mock = mock.MagicMock()
        flavor_mock.name = 'm2.small'
        flavor_mock.disk = 10
        instance_mock.manager._list.side_effect = iter([
            [flavor_mock],
            [],
            [flavor_mock],
            []
        ])

        image_mock = mock.MagicMock(
            status='active',
            size=8 * (1024.0**3),
            min_disk=10
        )
        image_mock.name = 'my_test_vm_snap_'
        quota_volumes_mock = mock.MagicMock()
        d = {
            "quota": {
                "cern": {
                    "compute": {
                        "instances": 3,
                        "cores": 60,
                        "ram": 5400,
                    },
                    "blockstorage": {
                        "standard": {
                            "gigabytes": 100,
                            "volumes": 5
                        }
                    }
                }
            },
            "current": {
                "cern": {
                    "compute": {
                        "instances": 1,
                        "cores": 20,
                        "ram": 1875,
                    },
                    "blockstorage": {
                        "standard": {
                            "gigabytes": 100,
                            "volumes": 1
                        }
                    }
                }
            }
        }

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        mock_client.get_volumes.return_value = [
            mock.MagicMock(
                id="fake_volume_id_2",
                attachments=[{'device': '/dev/vdb'}]),
            mock.MagicMock(
                id="fake_volume_id_1",
                attachments=[{'device': '/dev/vda'}])
        ]
        mock_client.get_image.return_value = image_mock

        quota_volumes_mock.__getitem__.side_effect = d.__getitem__
        quota_volumes_mock.__iter__.side_effect = d.__iter__
        mock_client.find_region_vm.return_value = 'cern'
        mock_client.get_project_quota.return_value = quota_volumes_mock

        snapshot_mock = mock.MagicMock(status='available')
        snapshot_mock.name = 'my_test_vm_snap_'
        snapshot_mock.manager.get.return_value = snapshot_mock
        mock_client.create_volume_snapshot.return_value = (
            snapshot_mock
        )

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_volume_snapshot_available.retry.wait = wait_none()
        cmd.wait_until_volume_snapshot_available.retry.stop = (
            stop_after_attempt(1))
        cmd.wait_until_vm_is_gone.retry.wait = wait_none()
        cmd.wait_until_vm_is_gone.retry.stop = stop_after_attempt(1)

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--vm', 'my_test_vm',
                '--recreate-from', 'volume',
                '--new-project-id', 'new'
            ])
        self.assertEqual(cm.exception.code, 1)
        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm'
        )

    @mock.patch('ccitools.cmd.recreate_instance.LanDB')
    @mock.patch('ccitools.cmd.recreate_instance.CloudRegionClient')
    def test_recreate_bfv_volume_quota_correct(
            self, mock_crc, mock_landb):
        attrs = {
            "id": "1",
            "tenant_id": "fake",
            "flavor": {
                "original_name": "m2.small",
                "disk": 10,
                'vcpus': 1,
                'ram': 1875
            },
            "metadata": {},
            "user_id": 'fake_user',
            "OS-EXT-AZ:availability_zone": "cern-geneva-a",
            "os-extended-volumes:volumes_attached": [
                {
                    "id": "fake_volume_id_1",
                },
                {
                    "id": "fake_volume_id_2",
                }
            ]
        }
        instance_mock = mock.MagicMock(**attrs)
        instance_mock.name = "my_test_vm"
        instance_mock.manager.get.side_effect = iter([
            mock.MagicMock(status='SHUTOFF', **attrs),
            mock.MagicMock(status='ACTIVE', **attrs)
        ])
        flavor_mock = mock.MagicMock()
        flavor_mock.name = 'm2.small'
        flavor_mock.disk = 10
        instance_mock.manager._list.side_effect = iter([
            [flavor_mock],
            [],
            [flavor_mock],
            []
        ])

        image_mock = mock.MagicMock(
            status='active',
            size=8 * (1024.0**3),
            min_disk=10
        )
        image_mock.name = 'my_test_vm_snap_'
        quota_volumes_mock = mock.MagicMock()
        d = {
            "quota": {
                "cern": {
                    "compute": {
                        "instances": 3,
                        "cores": 60,
                        "ram": 5400,
                    },
                    "blockstorage": {
                        "standard": {
                            "gigabytes": 100,
                            "volumes": 5
                        }
                    }
                }
            },
            "current": {
                "cern": {
                    "compute": {
                        "instances": 1,
                        "cores": 20,
                        "ram": 1875,
                    },
                    "blockstorage": {
                        "standard": {
                            "gigabytes": 20,
                            "volumes": 1
                        }
                    }
                }
            }
        }

        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            ResponsiblePerson=mock.MagicMock(Email='test@cern.ch'),
            UserPerson=mock.MagicMock(Email='test@cern.ch')
        )

        mock_client = mock_crc.return_value
        mock_client.get_servers_by_name.return_value = instance_mock
        mock_client.session.auth.get_user_id.return_value = 'fake_user'
        mock_client.get_volumes.return_value = [
            mock.MagicMock(
                id="fake_volume_id_2",
                attachments=[{'device': '/dev/vdb'}]),
            mock.MagicMock(
                id="fake_volume_id_1",
                attachments=[{'device': '/dev/vda'}])
        ]
        mock_client.get_image.return_value = image_mock

        quota_volumes_mock.__getitem__.side_effect = d.__getitem__
        quota_volumes_mock.__iter__.side_effect = d.__iter__
        mock_client.find_region_vm.return_value = 'cern'
        mock_client.get_project_quota.return_value = quota_volumes_mock

        snapshot_mock = mock.MagicMock(status='available')
        snapshot_mock.name = 'my_test_vm_snap_'
        snapshot_mock.manager.get.return_value = snapshot_mock
        mock_client.create_volume_snapshot.return_value = (
            snapshot_mock
        )

        cmd = recreate_instance.RecreateInstance()
        cmd.wait_until_vm_state.retry.wait = wait_none()
        cmd.wait_until_vm_state.retry.stop = stop_after_attempt(1)
        cmd.wait_until_volume_snapshot_available.retry.wait = wait_none()
        cmd.wait_until_volume_snapshot_available.retry.stop = (
            stop_after_attempt(1))
        cmd.wait_until_vm_is_gone.retry.wait = wait_none()
        cmd.wait_until_vm_is_gone.retry.stop = stop_after_attempt(1)

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--vm', 'my_test_vm',
                '--recreate-from', 'volume',
                '--new-project-id', 'new'
            ])
        self.assertEqual(cm.exception.code, 1)
        mock_client.get_servers_by_name.assert_called_with(
            'my_test_vm'
        )
