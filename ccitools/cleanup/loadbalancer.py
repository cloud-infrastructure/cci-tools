import dateutil.parser
import logging

from ccitools.cleanup import base
from ccitools import common
from datetime import datetime
from datetime import timezone

# configure logging
logger = logging.getLogger(__name__)


class LoadbalancerResourceCleanup(base.ResourceCleanup):
    def __init__(self, cloud, quiet):
        super(LoadbalancerResourceCleanup, self).__init__(
            'LOADBALANCERS', cloud, quiet)

    def get_pending(self):
        lbs_pending = []
        logger.info("Looking for leftover loadbalancers ...")
        for lb in self.cloud.get_loadbalancers(
                user_name='svcprobe', tag='cleanup'):
            created_at = dateutil.parser.parse(lb['created_at'])
            days_since_created = (
                datetime.now(timezone.utc)
                - created_at.replace(tzinfo=timezone.utc)).days
            if base.RALLY_REGEX.match(lb['name']) and days_since_created >= 1:
                lbs_pending.append(lb)
        return lbs_pending

    def delete_leftovers_before(self):
        if self.leftovers:
            self.cloud.delete_loadbalancers(self.leftovers, cascade=True)

    def delete_leftovers_after(self):
        pass

    def get_broken(self):
        broken_lbs = []
        for lb in self.cloud.get_loadbalancers(
                user_name='svcprobe', tag='cleanup'):
            if lb in self.leftovers:
                broken_lbs.append(lb)
        return broken_lbs

    def format_table(self):
        lb_fields = [
            ('ID', 'id'),
            ('Name', 'name'),
            ('Project ID', 'project_id'),
            ('Created', 'created_at'),
            ('Provisioning Status', 'provisioning_status'),
            ('Operating Status', 'operating_status'),
            ('Region', 'region'),
        ]
        lbs_table = common.get_resources_info_table(
            self.leftovers, lb_fields)

        return lbs_table
