#!/usr/bin/python3

import json
import logging
import prettytable
import pytz
import sys

from ccitools.cmd.base.foreman import BaseForemanCMD
from ccitools.common import ping
from ccitools.common import ssh_executor
from ccitools.utils.sendmail import MailClient
from ccitools.utils.servicenowv2 import ServiceNowClient
from datetime import datetime
from datetime import timedelta
from dateutil import parser

# configure logging
logger = logging.getLogger(__name__)


class HealthReportPuppetCMD(BaseForemanCMD):
    def __init__(self):
        super(HealthReportPuppetCMD, self).__init__(
            description="Health report for Puppet")

    def health_report(self, snowclient, threshold, mail_to):
        ref_date = datetime.utcnow() - timedelta(hours=threshold)

        logger.info("Quering Foreman to get out of sync hosts...")
        out_of_sync = self.foreman.search_query(
            "hosts",
            "last_report < {0}".format(ref_date.isoformat())
        )
        logger.info("Foreman returned %s out of sync host visible "
                    "with the present credentials", len(out_of_sync))
        logger.info("From this list, we will exclude nodes not belonging "
                    "to 'cloud_*' and the ones in 'spare'")

        not_evaluated_table = prettytable.PrettyTable(
            ["host", "hostgroup", "last_puppet_run",
             "puppet_state", "tickets"])
        evaluated_table = not_evaluated_table.copy()

        for host in out_of_sync:
            hostname = host['name'][:-8]
            hostgroup = host['hostgroup_title']
            puppet_state = "?"
            host_status = "OK"

            if "cloud" not in hostgroup or "spare" in hostgroup:
                logger.info("Skipping host %s belonging to %s...",
                            hostname, hostgroup)
                continue

            tickets = []
            last = parser.parse(host['last_report'])
            today = pytz.utc.localize(datetime.now())
            last_in_days = (today - last).days

            # Get puppet disabled message
            cmd = "test -f /opt/puppetlabs/puppet/cache/state/"\
                  "agent_disabled.lock"\
                  " && cat /opt/puppetlabs/puppet/cache/state/"\
                  "agent_disabled.lock"
            try:
                logger.info("Checking puppet status on %s...", hostname)
                puppet_state, err = ssh_executor(hostname, cmd,
                                                 connect_timeout=5,
                                                 keep_alive_interval=2)
                if not puppet_state and not err:
                    puppet_state = "ERROR"
                else:
                    puppet_state = "Manually disabled: '%s'" % json.loads(
                        puppet_state)['disabled_message']

            except Exception:
                logger.info("Cannot connect to to %s. Trying to ping it...",
                            hostname)

                if ping(hostname):
                    logger.info("Host is pingable")
                    host_status = "Impossible to SSH (ownership?)"
                else:
                    logger.info("Host seems to be OFF")
                    host_status = "OFF (not pingable)"

            query = "active=true^stateNOT " \
                    "IN7,6^u_configuration_items_listLIKE%s" % hostname
            logger.info("Getting list of snow tickets related to %s", hostname)
            tickets = snowclient.ticket.raw_query_incidents(query)

            if puppet_state == '?':
                not_evaluated_table.add_row(
                    [hostname, hostgroup, "%s days ago" % (last_in_days),
                     host_status, ' '.join([
                         ticket.number for ticket in tickets])])
            else:
                evaluated_table.add_row(
                    [hostname, hostgroup, "%s days ago" % (last_in_days),
                     puppet_state, ' '.join(
                        [ticket.number for ticket in tickets])])

        not_evaluated_output = not_evaluated_table.get_string(
            sortby="hostgroup")
        evaluated_output = evaluated_table.get_string(sortby="hostgroup")
        print(not_evaluated_output)
        print(evaluated_output)

        mail_body = """Dear all,
<br/>
<br/>
This is the list of machines that did not run puppet """ \
"""in the last %s hours or more.<br/><br/>
To try to improve its readability, """ \
"""the information is now divided in two tables:
<ul>
  <li>The first one gathers all machines where puppet is """ \
  """either <b>manually disabled</b>(*) or in an <b>ERROR</b> state.</li>
  <li>The second one shows every host """ \
  """the script could <b>not connect to</b>.</li>
</ul>
(*) It is strongly encouraged to specify a reason every time """ \
"""you need to disable puppet on a host.<br/><br/>
Please verify if any of your machines are present, """ \
"""
especially if they are in an <b>ERROR state</b> """ \
"""and if there are <b>no opened tickets.</b><br/><br/>
List of machines where puppet status was evaluated:
<pre style="font-family:monospace;">%s</pre>
List of machines where puppet status could not be evaluated:
<pre style="font-family:monospace;">%s</pre>
""" % (threshold, evaluated_output, not_evaluated_output)

        mailclient = MailClient()
        mailclient.send_mail(mail_to, "Health report for Puppet machines",
                             mail_body, 'svc.rdeck@cern.ch', '', '', 'html')

    def main(self, args=None):
        self.parser.add_argument(
            "--threshold", type=int, default=24,
            help="Num of hours threshold without running Puppet")
        self.parser.add_argument(
            "--instance", default="cern", help="Service-Now instance")
        self.parser.add_argument(
            "--mail-to", required=True,
            help="User or egroup the report will be sent to")

        args = self.parse_args(args)

        snowclient = ServiceNowClient(instance=args.instance)

        self.health_report(snowclient, args.threshold, args.mail_to)


# Needs static method for setup.cfg
def main(args=None):
    HealthReportPuppetCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logging.error(e)
        sys.exit(1)
