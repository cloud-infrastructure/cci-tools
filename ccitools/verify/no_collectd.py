import logging

from ccitools.common import ssh_executor
from ccitools.errors import SSHException
from ccitools.verify import base

# configure logging
logger = logging.getLogger(__name__)


class NoCollectdAction(base.ActionBase):

    def try_fix(self):
        """Implement the fix for the exception thrown in collectd."""
        # Try to restart the service
        try:
            logger.info("Restarting the collectd service")
            ssh_executor(
                self.host,
                ('systemctl restart collectd')
            )
        except SSHException:
            pass

    def check_alarm_state(self, after_try=False):
        try:
            logger.info("Checking collectd service status")
            out, err = ssh_executor(
                self.host,
                ('systemctl list-units '
                 '| grep -i collectd | awk \'NR==1{print $4}\'')
            )
            if out and out[0].strip() == 'running':
                return False
        except SSHException:
            pass

        self.reason = "%s - Collectd service not running" % self.host
        return True
