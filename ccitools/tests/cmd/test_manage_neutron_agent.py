import logging

from ccitools.cmd import manage_neutron_agent
from ccitools.tests.cmd import fixtures
from ccitools.tests import fixtures as base_fixtures
from neutronclient.common import exceptions as neutron_exceptions
from unittest import mock
from unittest import TestCase


class TestManageNeutronAgent(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):
        with self.assertRaises(SystemExit) as cm:
            manage_neutron_agent.main([])
        self.assertEqual(cm.exception.code, 2)

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient'
                '.get_neutron_agents_list')
    def test_enable_agent_throws_exception(
            self, mock_list, mock_ks, mock_cc):
        mock_list.side_effect = iter([
            neutron_exceptions.NeutronClientException('Fail'),
        ])
        with self.assertRaises(SystemExit) as cm:
            manage_neutron_agent.main([
                '--hosts', 'raise_exception'
                '--enable'
            ])
        self.assertEqual(cm.exception.code, 3)

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient'
                '.get_neutron_agents_list')
    def test_enable_agent_list_empty(self, mock_list, mock_ks, mock_cc):
        mock_list.return_value = []
        with self.assertRaises(SystemExit) as cm:
            manage_neutron_agent.main([
                '--hosts', 'non_existing'
                '--enable'
            ])
        self.assertEqual(cm.exception.code, 3)

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient'
                '.get_neutron_agents_list')
    def test_enable_agent_enable_dryrun(
            self, mock_list, mock_ks, mock_cc):
        mock_list.return_value = [base_fixtures.Resource(fixtures.AGENT)]
        manage_neutron_agent.main([
            '--hosts', 'non_existing'
            '--enable'
        ])

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient'
                '.get_neutron_agents_list')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient'
                '.change_status_neutron_agent')
    def test_enable_agent_enable_perform(self, mock_status, mock_list,
                                         mock_ks, mock_cc):
        mock_list.return_value = [fixtures.AGENT]
        manage_neutron_agent.main([
            '--hosts', 'host1',
            '--enable',
            '--perform'
        ])
        mock_list.assert_called_with(host='host1.cern.ch')
        mock_status.assert_called_with(fixtures.AGENT, True)

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient'
                '.get_neutron_agents_list')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient'
                '.change_status_neutron_agent')
    def test_enable_agent_enable_perform_raise(self, mock_status, mock_list,
                                               mock_ks, mock_cc):
        mock_list.return_value = [fixtures.AGENT]
        mock_status.side_effect = iter([
            neutron_exceptions.NeutronClientException('Fail')
        ])
        with self.assertRaises(SystemExit) as cm:
            manage_neutron_agent.main([
                '--hosts', 'host1',
                '--enable',
                '--perform'
            ])
        self.assertEqual(cm.exception.code, 3)
        mock_list.assert_called_with(host='host1.cern.ch')
        mock_status.assert_called_with(fixtures.AGENT, True)

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient'
                '.get_neutron_agents_list')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient'
                '.change_status_neutron_agent')
    def test_enable_agent_disable_perform(
            self, mock_status, mock_list, mock_ks, mock_cc):
        mock_list.return_value = [fixtures.AGENT]
        manage_neutron_agent.main([
            '--hosts', 'host1',
            '--disable',
            '--perform'
        ])
        mock_list.assert_called_with(host='host1.cern.ch')
        mock_status.assert_called_with(fixtures.AGENT, False)

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient'
                '.get_neutron_agents_list')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient'
                '.delete_neutron_agent')
    def test_enable_agent_delete_perform(
            self, mock_delete, mock_list, mock_ks, mock_cc):
        mock_list.return_value = [fixtures.AGENT]
        manage_neutron_agent.main([
            '--hosts', 'host1',
            '--delete',
            '--perform'
        ])
        mock_list.assert_called_with(host='host1.cern.ch')
        mock_delete.assert_called_with(fixtures.AGENT)
