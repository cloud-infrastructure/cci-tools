import abc
import logging
import re
import time

from ccitools.common import ssh_executor

# configure logging
logger = logging.getLogger(__name__)


class ActionBase(object, metaclass=abc.ABCMeta):

    def __init__(self, host, delay=120):
        self.host = host
        self.pattern = None
        self.reason = None
        self.delay = delay

    @abc.abstractmethod
    def try_fix(self):
        """Implement the fix for the exception thrown in collectd."""

    def check_alarm_state(self, after_try=False):
        # Sleep a delay to allow collectd to recheck the status
        if after_try:
            time.sleep(self.delay)

        logger.info("Running collectdctl...")
        out, err = ssh_executor(
            self.host,
            'collectdctl listval state=ERROR')

        for line in out:
            if re.search(self.pattern, line):
                self.reason = " - %s\n" % self.host
                return True
        return False

    def get_reason(self):
        return self.reason
