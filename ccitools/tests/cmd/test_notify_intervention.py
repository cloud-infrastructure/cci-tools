import logging

from ccitools.cmd.notify_intervention import NotifyInterventionCMD
from ccitools.tests.cmd import fixtures
from tenacity import stop_after_attempt
from tenacity import wait_none
from unittest import mock
from unittest import TestCase


class TestNotifyIntervention(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch('ccitools.cmd.notify_intervention.urllib.request')
    @mock.patch('ccitools.cmd.notify_intervention.XldapClient')
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.notify_intervention.MailClient')
    def test_nomain(self, mock_mc, mock_crc, mock_xld, mock_url):

        mock_xlc = mock_xld.return_value
        mock_cl = mock_crc.return_value

        # 1. test default args
        with self.assertRaises(SystemExit) as cm:
            NotifyInterventionCMD().main([])
        self.assertEqual(cm.exception.code, 2)

        # 2. Read template throws exception
        cmd = NotifyInterventionCMD()
        cmd.readTemplate.retry.wait = wait_none()
        cmd.readTemplate.retry.stop = stop_after_attempt(1)
        mock_url.urlopen.return_value.getcode.return_value = 404
        with self.assertRaises(Exception) as cm:
            cmd.main([
                '--subject', 'fake_s',
                '--body', 'fake_b',
                '--intervention', '{\
                                        "hypervisors": "fake_h",\
                                        "vms": "vms_f",\
                                        "projects": "fake_p\
                                        "}',
            ])
        self.assertEqual(Exception, Exception)

        # 3. test proper data 1
        m = fixtures.FAKE_INSTANCE_OUTPUT
        mock_url.urlopen.return_value.getcode.return_value = 200
        mock_url.urlopen.return_value.read.return_value = b'fake_template'
        mock_xlc.get_security_groups_members.return_value = 'fake_gm'
        mock_cl.get_servers_by_hypervisors.return_value = m
        mock_cl.get_servers_by_names.return_value = m
        mock_cl.get_servers_by_projects.return_value = m
        mock_cl.get_project.return_value.name = 'fake_project'

        NotifyInterventionCMD().main([
            '--subject', 'fake_s',
            '--body', 'fake_b',
            '--intervention', '{\
                                    "hypervisors": "fake_h",\
                                    "vms": "vms_f",\
                                    "projects": "fake_p",\
                                    "date": "20-06-2019 00:00",\
                                    "new_date": "20-06-2019 10:00",\
                                    "duration": "55",\
                                    "otg": "otg"\
                                }',
        ])

        # 4.  test proper data 2

        NotifyInterventionCMD().main([
            '--subject', 'fake_s',
            '--body', 'fake_b',
            '--intervention', '{\
                                    "hypervisors": "fake_h",\
                                    "vms": "vms_f",\
                                    "projects": "fake_p",\
                                    "date": "",\
                                    "new_date": "",\
                                    "duration": "",\
                                    "otg": "inc"\
                                }',
        ])

        # 5.  test proper data 3

        NotifyInterventionCMD().main([
            '--subject', 'fake_s',
            '--body', 'fake_b',
            '--intervention', '{\
                                    "hypervisors": "fake_h",\
                                    "vms": "vms_f",\
                                    "projects": "fake_p",\
                                    "date": "20-06-2019 00:00",\
                                    "new_date": "21-06-2019 00:00",\
                                    "duration": "22",\
                                    "otg": "rqf"\
                                }',
            '--notify-main-user',
            '--notify-members',
            '--perform'
        ])

        # 6.  test proper data 4

        m = fixtures.FAKE_INSTANCE_OUTPUT2
        mock_cl.get_servers_by_hypervisors.return_value = m
        mock_cl.get_servers_by_names.return_value = m
        mock_cl.get_servers_by_projects.return_value = m
        mock_cl.get_project.return_value.name = 'fake_project'
        mock_cl.get_project_members.return_value = 'fake_pro_mem'
        mock_cl.find_project.return_value.name = 'fake'

        NotifyInterventionCMD().main([
            '--subject', 'fake_s',
            '--body', 'fake_b',
            '--intervention', '{\
                                    "hypervisors": "fake_h",\
                                    "vms": "vms_f",\
                                    "projects": "fake_p",\
                                    "date": "",\
                                    "new_date": "",\
                                    "duration": "",\
                                    "otg": ""\
                                }',
            '--notify-main-user',
            '--notify-members',
            '--perform',
            '--calendar'
        ])

        # 7.  test server deleted

        m = fixtures.FAKE_INSTANCE_OUTPUT3
        mock_cl.get_servers_by_hypervisors.return_value = m
        mock_cl.get_servers_by_names.return_value = m
        mock_cl.get_servers_by_projects.return_value = m
        mock_cl.get_project.return_value.name = 'fake_project'

        NotifyInterventionCMD().main([
            '--subject', 'fake_s',
            '--body', 'fake_b',
            '--intervention', '{\
                                    "hypervisors": "fake_h",\
                                    "vms": "vms_f",\
                                    "projects": "fake_p",\
                                    "date": "",\
                                    "new_date": "",\
                                    "duration": "",\
                                    "otg": ""\
                                }',
        ])

        # 8. test intervention void

        NotifyInterventionCMD().main([
            '--subject', 'fake_s',
            '--body', 'fake_b',
            '--intervention', '{"fake": "fake_p"}',
        ])

        # 9.  test mailclient exception

        m = fixtures.FAKE_INSTANCE_OUTPUT
        mock_cl.get_servers_by_hypervisors.return_value = m
        mock_cl.get_servers_by_names.return_value = m
        mock_cl.get_servers_by_projects.return_value = m
        mock_cl.get_project.return_value.name = 'fake_project'
        mock_cl.get_project_members.return_value = 'fake_pro_mem'
        mock_cl.find_project.return_value.name = 'fake'
        mock_mc.return_value.send_mail_ics.side_effect = iter([Exception])

        NotifyInterventionCMD().main([
            '--subject', 'fake_s',
            '--body', 'fake_b',
            '--intervention', '{\
                                    "hypervisors": "fake_h",\
                                    "vms": "vms_f",\
                                    "projects": "fake_p",\
                                    "date": "",\
                                    "new_date": "",\
                                    "duration": "",\
                                    "otg": ""\
                                }',
            '--notify-main-user',
            '--notify-members',
            '--perform',
            '--calendar'
        ])

        # 10.  test mailclient exception

        mock_xlc.get_email.side_effect = iter([Exception])

        NotifyInterventionCMD().main([
            '--subject', 'fake_s',
            '--body', 'fake_b',
            '--intervention', '{\
                                    "hypervisors": "fake_h",\
                                    "vms": "vms_f",\
                                    "projects": "fake_p",\
                                    "date": "",\
                                    "new_date": "",\
                                    "duration": "",\
                                    "otg": ""\
                                }',
            '--notify-main-user',
            '--notify-members',
            '--perform',
            '--calendar'
        ])
