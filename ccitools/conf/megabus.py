from oslo_config import cfg


server = cfg.StrOpt(
    'server',
    default='agileinf-mb.cern.ch',
    help="megabus server")

port = cfg.StrOpt(
    'port',
    default='61213',
    help="megabus port")

user = cfg.StrOpt(
    'user',
    default='fake_user',
    help="megabus user")

passw = cfg.StrOpt(
    'passw',
    default='fake_pass',
    help="megabus user")

ttl = cfg.IntOpt(
    'ttl',
    default=172800,
    help="ttl for megabus")

queue = cfg.StrOpt(
    'queue',
    default='/topic/ai.hypervisor.drain',
    help="megabus queue")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    server,
    port,
    user,
    passw,
    ttl,
    queue,
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
