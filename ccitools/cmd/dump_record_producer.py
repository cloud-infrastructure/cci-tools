#!/usr/bin/python3

import logging
import prettytable
import sys
import yaml

from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools.common import summary_callback
from ccitools.utils.servicenowv2 import ServiceNowClient

# configure logging
logger = logging.getLogger(__name__)


class DumpRecordProducerCMD(BaseCloudCMD):
    def __init__(self):
        super(DumpRecordProducerCMD, self).__init__(
            description="Script to dump record producers from ServiceNOW")

    def _dump_metadata_info(self, metadata):
        print('Quota Information:\n')
        table = prettytable.PrettyTable(
            ["Region", "Service", "Quota", "Requested"])

        self.cloudclient.service_quota_iterator(
            callback=summary_callback,
            show_all=True,
            meta=yaml.safe_load(metadata),
            table=table)

        table.border = True
        table.header = True
        table.align["Region"] = 'c'
        table.align["Service"] = 'c'
        table.align["Quota"] = 'c'
        table.align["Requested"] = 'c'
        print(table)

    def dump_quota_update(self, args):
        snowclient = ServiceNowClient(instance=args.instance)
        rp_obj = snowclient.get_quota_update_rp(args.ticket_number)

        caller = snowclient.user.get_user_info_by_sys_id(
            rp_obj.request.info.u_caller_id)

        t = prettytable.PrettyTable(["Field", "Value"])
        t.add_row(["Ticket:", "%s" % (args.ticket_number)])
        t.add_row(["Caller:", "%s" % (caller.name)])
        t.add_row(["Name of the project:", "%s" % (rp_obj.project_name)])
        t.align["Field"] = 'r'
        t.align["Value"] = 'l'
        t.border = True
        t.header = False
        print(t)
        self._dump_metadata_info(rp_obj.metadata)

    def dump_project_deletion(self, args):
        snowclient = ServiceNowClient(instance=args.instance)

        rp_obj = snowclient.get_project_deletion_rp(args.ticket_number)
        caller = snowclient.user.get_user_info_by_sys_id(
            rp_obj.request.info.u_caller_id)

        t = prettytable.PrettyTable(["Field", "Value"])
        t.add_row(["Ticket:", "%s" % (args.ticket_number)])
        t.add_row(["Caller:", "%s" % (caller.name)])
        t.add_row(["Name of the project:", "%s" % (rp_obj.project_name)])
        t.align["Field"] = 'r'
        t.align["Value"] = 'l'
        t.border = True
        t.header = False
        print(t)

    def dump_project_request(self, args):
        snowclient = ServiceNowClient(instance=args.instance)

        # get ticket information
        rp_obj = snowclient.get_project_creation_rp(args.ticket_number)
        caller = snowclient.user.get_user_info_by_sys_id(
            rp_obj.request.info.u_caller_id)

        # build table and print
        t = prettytable.PrettyTable(["Field", "Value"])
        t.add_row(["Ticket:", "%s" % (args.ticket_number)])
        t.add_row(["Caller:", "%s" % (caller.name)])
        t.add_row(["Name of the project:", "%s" % (rp_obj.project_name)])
        t.add_row(["Description:", "%s" % (rp_obj.description)])
        t.add_row(["E-Group:", "%s" % (rp_obj.egroup)])
        t.add_row(["Owner:", "%s" % (rp_obj.owner)])
        if hasattr(rp_obj, 'chargegroup'):
            t.add_row(["Chargegroup:", "%s" % (rp_obj.chargegroup)])
        if hasattr(rp_obj, 'chargerole'):
            t.add_row(["Chargerole:", "%s" % (rp_obj.chargerole)])
        t.border = True
        t.header = False
        t.align["Field"] = 'r'
        t.align["Value"] = 'l'
        print(t)
        self._dump_metadata_info(rp_obj.metadata)

    def main(self, args=None):
        subparsers = self.parser.add_subparsers(metavar='<subcommand>')

        parser_quota = subparsers.add_parser('quota-update')
        parser_quota.add_argument("--ticket-number", required=True)
        parser_quota.add_argument(
            "--instance", default="cern", help="Service now instance")
        parser_quota.set_defaults(func=self.dump_quota_update)

        parser_deletion = subparsers.add_parser('project-deletion')
        parser_deletion.add_argument("--ticket-number", required=True)
        parser_deletion.add_argument(
            "--instance", default="cern", help="Service now instance")
        parser_deletion.set_defaults(func=self.dump_project_deletion)

        parser_creation = subparsers.add_parser('project-request')
        parser_creation.add_argument("--ticket-number", required=True)
        parser_creation.add_argument(
            "--instance", default="cern", help="Service now instance")
        parser_creation.set_defaults(func=self.dump_project_request)

        args = self.parse_args(args)

        try:
            call = args.func
        except AttributeError:
            self.parser.error("Too few arguments")

        call(args)


# Needs static method for setup.cfg
def main(args=None):
    DumpRecordProducerCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logging.exception(e)
        sys.exit(-1)
