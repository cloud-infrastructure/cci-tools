#!/usr/bin/python3

import argparse
import ccitools.conf
import logging
import prettytable
import sys
import yaml

from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools.common import summary_callback
from ccitools.common import summary_update_callback
from ccitools.utils.servicenowv2 import ServiceNowClient
from ccitools.utils.snow.ticket import RequestState
from keystoneclient import exceptions


# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF

CLOSING_COMMENT = """Dear %s,

The quota has been updated, and this ticket is being resolved.

Please note that this ticket should no longer be updated, as
it has been automatically resolved. If you have any further queries,
please do not hesitate to open a new ticket by using [code]
<a href="https://cern.service-now.com/service-portal?id=sc_cat_item&name=incident&fe=cloud-infrastructure">
the following link</a>.[/code]

Best regards,
Cloud Infrastructure Team.
"""  # noqa

SNOW_MESSAGE = """Dear HW Resources manager,

Could you please review the following quota update request?

%s
Also, could you please make sure that the numbers in the form correspond to \
your decision, before you reassign this ticket to the "Cloud Infrastructure \
3rd Level" FE? This will allow for the automated update of \
the project quota. Thanks!

Best regards,
        Cloud Infrastructure Team"""

USER_MESSAGE = """Dear %s,

Your quota update request has been received and sent to
HW Resources management in order to be evaluated.

Your request will be applied after approval.

Thank you,
        Cloud Infrastructure Team"""


class UserNotProjectMemberError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        """Return string representation."""
        return repr(self.value)


class UpdateQuotaCMD(BaseCloudCMD):
    def __init__(self):
        super(UpdateQuotaCMD, self).__init__(
            description="Script to handle quota updates requests")

    def request_update_summary(self, rp, project):
        metadata = yaml.safe_load(
            rp.metadata if hasattr(rp, 'metadata') else {'quota': {}})

        metadata['current'] = (
            self.cloudclient.get_project_quota(
                project_id=project.id)['quota']
        )

        table = prettytable.PrettyTable(
            ["Region", "Service", "Quota", "Current",
             "Requested", "Variation"])

        request_tags = {}

        self.cloudclient.service_quota_iterator(
            callback=summary_update_callback,
            show_all=True,
            meta=metadata,
            table=table,
            request_tags=request_tags)

        table.border = True
        table.header = True
        table.align["Region"] = 'c'
        table.align["Service"] = 'c'
        table.align["Quota"] = 'c'
        table.align["Current"] = 'c'
        table.align["Requested"] = 'c'
        table.align["Variation"] = 'c'
        return table, request_tags

    def quota_update_summary(self, project):
        table = prettytable.PrettyTable(
            ["Region", "Service", "Quota", "Assigned"])

        metadata = {
            'quota': self.cloudclient.get_project_quota(
                project_id=project.id)['quota']
        }

        self.cloudclient.service_quota_iterator(
            callback=summary_callback,
            show_all=True,
            meta=metadata,
            table=table)

        table.border = True
        table.header = True
        table.align["Region"] = 'c'
        table.align["Service"] = 'c'
        table.align["Quota"] = 'c'
        table.align["Assigned"] = 'c'
        return table

    def summary_to_monospace(self, summary):
        summary = "<br/>".join(summary.get_string().split("\n"))
        summary = "%s%s%s" % ("[code]<pre>", summary, "</pre>[/code]")
        return summary

    def worknote_message(self, summary):
        return SNOW_MESSAGE % (self.summary_to_monospace(summary))

    def invalid_request(self, snowclient, ticket_number, comment, exec_mode):
        """Check for invalid request.

        If the verification process fails:
           Write a comment on the ticket notifying the caller
           Mark the ticket as waiting for user

        :param ticket_number:
        :param comment:
        """
        if exec_mode:
            logger.error(
                "The request is invalid. Informing the user and "
                "setting ticket as 'waiting for user'")
            ticket = snowclient.ticket.get_ticket(ticket_number)
            ticket.add_comment(comment)
            ticket.change_state(RequestState.WAITING_FOR_USER)
            ticket.save()
        raise Exception(comment)

    def preprocess_request(self, args, snowclient):
        rp = snowclient.get_quota_update_rp(args.ticket_number)
        try:
            project = self.cloudclient.find_project(rp.project_name)
            summary, _ = self.request_update_summary(rp, project)
            logger.info("Request Summary:\n%s", summary)
            if args.exec_mode:
                logger.info("Adding comment to '%s'", args.ticket_number)
                ticket = snowclient.ticket.get_ticket(args.ticket_number)
                username = snowclient.user.get_user_info_by_sys_id(
                    ticket.info.u_caller_id).first_name
                rp.request.add_comment(USER_MESSAGE % username)
                logger.info("Adding worknote to '%s'", args.ticket_number)
                rp.request.add_work_note(self.worknote_message(summary))
                logger.info("Changing FE and assignment_group")
                snowclient.change_functional_element(ticket, args.fe,
                                                     args.assignment_group)
                rp.request.save()

        except ValueError as ve:
            error_message = (
                "All the numeric fields must be positive intergers. "
                "Please verify the request.")
            logger.error(str(ve))
            self.invalid_request(snowclient, args.ticket_number,
                                 error_message, args.exec_mode)
        except exceptions.NotFound:
            error_message = "Project '%s' not found. " \
                            "Please verify the request." % rp.project_name
            self.invalid_request(snowclient, args.ticket_number,
                                 error_message, args.perform)
        except UserNotProjectMemberError as ex:
            self.invalid_request(snowclient, args.ticket_number,
                                 ex, args.exec_mode)

    def execute_request(self, args, snowclient):
        rp = snowclient.get_quota_update_rp(args.ticket_number)
        project_name = rp.project_name
        try:
            project = self.cloudclient.find_project(rp.project_name)
            summary, request_tags = self.request_update_summary(rp, project)
            logger.info("Request Summary:\n%s", summary)
            if args.exec_mode:
                metadata = yaml.safe_load(
                    rp.metadata if hasattr(rp, 'metadata') else {'quota': {}})
                self.cloudclient.set_project_quota(
                    project_id=project.id,
                    quota=metadata['quota'])
                logger.info("'%s' quota updated successfully", project_name)

                logtable = self.quota_update_summary(project)
                logger.info("Project quota after the update:\n%s", logtable)
                rp.request.add_work_note(args.worknote)
                ticket = snowclient.ticket.get_ticket(args.ticket_number)
                username = snowclient.user.get_user_info_by_sys_id(
                    ticket.info.u_caller_id).first_name
                rp.request.resolve(CLOSING_COMMENT % username, args.resolver)
                rp.request.save()
                if args.send_metrics:
                    self.metrics.publish_resource_request_metrics(
                        "quota_update", rp, metadata['quota'],
                        'large_compute_request' in request_tags
                        and request_tags['large_compute_request'])
                logger.info("Quota updated and %s marked as 'resolved'",
                            args.ticket_number)

        except ValueError:
            raise Exception(
                "All the numeric fields must be positive integers")
        except exceptions.NotFound:
            raise Exception("'%s' project not found" % rp.project_name)
        except UserNotProjectMemberError as ex:
            raise Exception(ex)

    def main(self, args=None):
        subparsers = self.parser.add_subparsers(metavar='<subcommand>')

        self.parser.add_argument(
            "--instance", default="cern",
            help="Service now instance")
        self.parser.add_argument(
            "--ticket-number", required=True,
            help="Quota update request ticket number")

        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true',
                               help="If present, sends email")
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')

        parser_preprocess = subparsers.add_parser('preprocess')
        parser_preprocess.add_argument(
            "--fe", required=True,
            help="Functional element "
            "to assign the ticket to")
        parser_preprocess.add_argument(
            "--assignment-group", required=True,
            help="Group to assign the ticket to")
        parser_preprocess.set_defaults(func=self.preprocess_request)

        parser_execute = subparsers.add_parser('execute')
        parser_execute.add_argument(
            "--comment",
            help="Message to be written on "
                 "the ticket as closing comment")
        parser_execute.add_argument(
            "--worknote",
            help="Worknote to be written on the ticket")
        parser_execute.set_defaults(func=self.execute_request)
        parser_execute.add_argument(
            "--resolver",
            help="User who will resolve the ticket")
        parser_execute.add_argument(
            "--send-metrics",
            action=argparse.BooleanOptionalAction,
            default=True,
            help="Send metrics to MONIT")

        args = self.parse_args(args)

        try:
            call = args.func
        except AttributeError:
            self.parser.error("Too few arguments")

        snowclient = ServiceNowClient(instance=args.instance)

        call(args, snowclient)


# Needs static method for setup.cfg
def main(args=None):
    UpdateQuotaCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)
