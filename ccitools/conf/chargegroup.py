from oslo_config import cfg


url = cfg.StrOpt(
    'url',
    default='https://gar.cern.ch/public/list_full',
    help="URL to fetch chargegroups")

local = cfg.StrOpt(
    'local',
    default='/etc/ccitools/chargegroups.json',
    help="Local file for chargegroups")

resolver_url = cfg.StrOpt(
    'resolver_url',
    default='https://gar.cern.ch/public/user_resolver/list_all',
    help="URL to resolve user chargegroup mapping")

verify = cfg.BoolOpt(
    'verify',
    default=False,
    help='Verify SSL connection')

coordinator_groups = cfg.DictOpt(
    'coordinator_groups',
    default={
        # ATLAS
        'd28883af-0627-4bfa-b23c-616ea19b2c07':
            'cloud-infrastructure-atlas-resource-coordinators',
        # ALICE
        '6395494b-62ce-4a0e-ad06-da4b89f6625e':
            'cloud-infrastructure-alice-resource-coordinators',
        # CMS
        '23b588c6-064a-437e-bc0c-3b3df0aa6e97':
            'cloud-infrastructure-cms-resource-coordinators',
        # LHCb
        '686da915-220d-4bd6-b73b-7f791fd3cea8':
            'cloud-infrastructure-lhcb-resource-coordinators',
    }
)

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    url,
    local,
    resolver_url,
    verify,
    coordinator_groups
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
