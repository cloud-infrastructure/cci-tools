import logging

from ccitools.verify import base

# configure logging
logger = logging.getLogger(__name__)


class LoadHighAction(base.ActionBase):
    def __init__(self, host):
        super(LoadHighAction, self).__init__(host)
        self.pattern = r'.*\/load\/load-relative'

    def try_fix(self):
        # There is no fix for this alarm, ignoring it.
        pass
