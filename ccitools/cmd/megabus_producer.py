#!/usr/bin/python3

import ccitools.conf
import dateutil.parser
import json
import logging
import stomp
import sys
import time

from ccitools.cmd.base.cloud import BaseCloudCMD

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF

EXCLUDED_ELEMENTS = ['exec_mode', 'verbose_level', 'debug',
                     'cancel_intervention']


class MegabusProducerCMD(BaseCloudCMD):

    def __init__(self):
        super(MegabusProducerCMD, self).__init__(
            description="Megabus intervention message producer")

    def get_message(self, data, cloud):
        message = {}
        vms = []

        for key, value in data.items():
            if key == 'hosts':
                for host in value[0].split():
                    try:
                        vms += [str(server.name) for
                                server in cloud.get_servers_by_hypervisor(
                                    host)]
                    except Exception as ex:
                        logger.error("Cannot get servers from '%s'. "
                                     "Error: %s", host, ex)
                        continue

                message['vms'] = list(set(vms))

            elif key == 'intervention_time' and value:
                message['intervention_time'] = dateutil.parser.parse(
                    value, dayfirst=True).isoformat('T')
            elif value:
                message[key] = value
        return json.dumps(message, ensure_ascii=False)

    def send_new_intervention(self, args, cloud):
        data = args.__dict__.copy()
        for key in EXCLUDED_ELEMENTS:
            data.pop(key)
        data['intervention_status'] = 'scheduled'
        message = self.get_message(data, cloud)

        queue = CONF.megabus.queue
        client = stomp.Connection([(CONF.megabus.server, CONF.megabus.port)])
        client.connect(CONF.megabus.user,
                       CONF.megabus.passw,
                       wait=True)

        exp = (int(time.time()) + int(CONF.megabus.ttl)) * 1000
        expires = {'expires': exp}

        if args.exec_mode:
            client.send(queue, message.encode(), expires)
            logger.info("Intervention information pushed to megabus.")
            logger.info("\n%s", message)
        else:
            logger.info("[DRYRUN] Intervention information NOT "
                        "pushed to megabus.")
            logger.info("\n%s", message)

        client.disconnect()

    def send_cancel_intervention(self, args, cloud):
        data = args.__dict__.copy()
        for key in EXCLUDED_ELEMENTS:
            data.pop(key)
        data['intervention_status'] = 'cancelled'
        message = self.get_message(data, cloud)

        queue = CONF.megabus.queue
        client = stomp.Connection([(CONF.megabus.server, CONF.megabus.port)])
        client.connect(CONF.megabus.user,
                       CONF.megabus.passw,
                       wait=True)

        exp = (int(time.time()) + int(CONF.megabus.ttl)) * 1000
        expires = {'expires': exp}

        if args.exec_mode:
            client.send(queue, message.encode(), expires)
            logger.info("Cancelled intervention information "
                        "pushed to megabus.")
            logger.info("\n%s", message)
        else:
            logger.info("[DRYRUN] Cancelled intervention information NOT "
                        "pushed to megabus.")
            logger.info("\n%s", message)

        client.disconnect()

    def send_main(self, args, cloud):
        if args.cancel_intervention != '':
            self.send_cancel_intervention(args, cloud)
        else:
            self.send_new_intervention(args, cloud)

    def main(self, args=None):
        self.parser.add_argument(
            "--type", required=True, help="Type of intervention")
        self.parser.add_argument(
            "--version", required=True, help="Version")
        self.parser.add_argument(
            "--hosts", nargs='*', default=[], help="List of hypervisor names")
        self.parser.add_argument(
            "--intervention-time", nargs='?',
            default=None, help="Date. Format: dd-mm-aaaa hh:mm")
        self.parser.add_argument(
            "--duration", nargs='?', default=None, help="Duration")
        self.parser.add_argument(
            "--cancel_intervention",
            default='',
            help="Indicate if an intervention gets cancelled")

        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true')
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')

        args = self.parse_args(args)

        self.send_main(args, self.cloudclient)


# Needs static method for setup.cfg
def main(args=None):
    MegabusProducerCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)
