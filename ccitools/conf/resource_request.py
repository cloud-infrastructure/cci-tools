from oslo_config import cfg


large_compute_request_ram_threshold = cfg.IntOpt(
    'large_compute_request_ram_threshold',
    default=128,
    help="Threshold of RAM variation that makes the request a large one")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    large_compute_request_ram_threshold
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
