#!/usr/bin/python3

import logging
import sys

from ccitools.cmd.base.cloud import BaseCloudCMD
import ccitools.conf
from keystoneclient.v3 import client as keystone_client

# configure logging
logger = logging.getLogger(__name__)

CONF = ccitools.conf.CONF


class SyncAssignmentsCMD(BaseCloudCMD):
    def __init__(self):
        super(SyncAssignmentsCMD, self).__init__(
            description="Sync Assignments between regions")

    def get_keystone_client(self, region):
        return keystone_client.Client(
            session=self.cloudclient.session,
            region_name=region)

    def list_projects_in_region(self, client, domain=None, user=None,
                                parent=None, **kwargs):
        return client.projects.list(
            domain=domain,
            user=user,
            parent=parent,
            **kwargs)

    def grant_roles_in_region(self, users, groups, role, project):
        for user in users:
            self.client_target.roles.grant(
                role=role,
                user=user,
                project=project)
        for group in groups:
            self.client_target.roles.grant(
                role=role,
                group=group,
                project=project)

    def revoke_roles_in_region(self, users, groups, role, project):
        for user in users:
            self.client_target.roles.revoke(
                role=role,
                user=user,
                project=project)
        for group in groups:
            self.client_target.roles.revoke(
                role=role,
                group=group,
                project=project)

    def check_projects(self, args):
        # We compare both regions and spot the projects that are missing
        logger.info("Querying the list of project(s) from %s region",
                    args.source)
        source = self.list_projects_in_region(
            client=self.client_source,
            region=args.source,
            domain=args.domain)
        logger.info("Retrieved %s project(s) in %s region",
                    len(source),
                    args.source)
        set_source = set(p.id for p in source)

        logger.info("Querying the list of project(s) from %s region",
                    args.target)
        target = self.list_projects_in_region(
            client=self.client_target,
            region=args.target,
            domain=args.domain)
        logger.info("Retrieved %s project(s) in %s region",
                    len(target),
                    args.target)
        set_target = set(p.id for p in target)

        missing = set_source - set_target
        logger.info("We have %s project(s) missing in %s region",
                    len(missing),
                    args.target)

        if len(missing):
            logger.fatal('There are missing project(s) in the target')
            raise Exception("There are missing project(s) in the target")
        self.projects = set(e.id for e in source)

    def check_roles(self, args):
        # We compare both regions and spot the roles that are missing
        logger.info("Querying the list of role(s) from %s region",
                    args.source)
        source = self.client_source.roles.list()
        logger.info("Retrieved %s role(s) in %s region",
                    len(source),
                    args.source)
        set_source = set(e.id for e in source)

        logger.info("Querying the list of role(s) from %s region",
                    args.target)
        target = self.client_target.roles.list()
        logger.info("Retrieved %s role(s) in %s region",
                    len(target),
                    args.target)
        set_target = set(e.id for e in target)

        missing = set_source - set_target
        logger.info("We have %s role(s) missing in %s region",
                    len(missing),
                    args.target)

        if len(missing):
            logger.fatal('There are missing role(s) in the target')
            raise Exception("There are missing role(s) in the target")
        self.roles = set(e.id for e in source)

    def _regenerate_helper_dict(self, assignments):
        ret = {}
        for a in assignments:
            if ('project' in a.scope
                    and a.scope['project']['id'] in self.projects):
                project_id = a.scope['project']['id']
                if project_id not in ret:
                    ret[project_id] = {}
                role_id = a.role['id']
                if role_id in self.roles and role_id not in ret[project_id]:
                    ret[project_id][role_id] = {}
                if hasattr(a, 'user'):
                    if 'users' not in ret[project_id][role_id]:
                        ret[project_id][role_id]['users'] = set()
                    ret[project_id][role_id]['users'].add(a.user['id'])
                if hasattr(a, 'group'):
                    if 'groups' not in ret[project_id][role_id]:
                        ret[project_id][role_id]['groups'] = set()
                    ret[project_id][role_id]['groups'].add(a.group['id'])
        return ret

    def sync_assignments(self, args):
        logger.info(
            "Retrieving assignments from %s region...",
            args.source)
        source_assign = self.client_source.role_assignments.list()
        logger.info(
            "Found %s assignments in %s region...",
            len(source_assign),
            args.source)
        logger.info(
            "Retrieving assignments from %s region...",
            args.target)
        target_assign = self.client_target.role_assignments.list()
        logger.info(
            "Found %s assignments in %s region...",
            len(target_assign),
            args.source)

        logger.info("Processing assignments...")
        source = self._regenerate_helper_dict(source_assign)
        target = self._regenerate_helper_dict(target_assign)

        logger.info("Processing projects...")
        for project_id, s_roles in source.items():
            logger.debug("Checking project %s", project_id)
            t_roles = target[project_id] if project_id in target else {}
            for role_id, s_data in s_roles.items():
                s_users = s_data['users'] if 'users' in s_data else set()
                s_groups = s_data['groups'] if 'groups' in s_data else set()
                t_data = t_roles[role_id] if role_id in t_roles else {}
                t_users = t_data['users'] if 'users' in t_data else set()
                t_groups = t_data['groups'] if 'groups' in t_data else set()

                users_to_add = s_users - t_users
                users_to_remove = t_users - s_users
                groups_to_add = s_groups - t_groups
                groups_to_remove = t_groups - s_groups

                if len(users_to_add) or len(groups_to_add):
                    logger.info(
                        "We have %s users(s) and %s group(s) to add in "
                        "region %s on project %s and role %s",
                        len(users_to_add),
                        len(groups_to_add),
                        args.target,
                        project_id,
                        role_id)
                    if not args.dryrun:
                        self.grant_roles_in_region(
                            users=users_to_add,
                            groups=groups_to_add,
                            role=role_id,
                            project=project_id)
                    else:
                        logger.info(
                            "DRYRUN: %s users and %s groups added in "
                            "project %s, role %s and region %s",
                            users_to_add,
                            groups_to_add,
                            role_id,
                            project_id,
                            args.target)

                if len(users_to_remove) or len(groups_to_remove):
                    logger.info(
                        "We have %s users(s) and %s group(s) to remove in "
                        "region %s on project %s and role %s",
                        len(users_to_remove),
                        len(groups_to_remove),
                        args.target,
                        project_id,
                        role_id)
                    if not args.dryrun:
                        self.revoke_roles_in_region(
                            users=users_to_remove,
                            groups=groups_to_remove,
                            role=role_id,
                            project=project_id)
                    else:
                        logger.info(
                            "DRYRUN: %s users  and %s groups removed in "
                            "project %s, role %s and region %s",
                            users_to_remove,
                            groups_to_remove,
                            role_id,
                            project_id,
                            args.target)

    def main(self, args=None):
        """Get broken nodes and send email."""
        self.parser.add_argument(
            '--dryrun',
            action='store_true',
            help='read-only mode')
        self.parser.add_argument(
            '--domain',
            default='default',
            help='Default domain to synchronize')
        self.parser.add_argument(
            '--source',
            required=True,
            help='Source region to get the assignments')
        self.parser.add_argument(
            '--target',
            required=True,
            help='Target region to set the assignments')
        args = self.parse_args(args)

        if args.dryrun:
            logger.info(
                "--- THIS IS A DRYRUN. NO CHANGES WILL BE MADE ---")

        self.client_source = self.get_keystone_client(args.source)
        self.client_target = self.get_keystone_client(args.target)

        # In our case, users are the same as both setups should be using LDAP
        # in the default domain
        self.check_projects(args)
        self.check_roles(args)
        self.sync_assignments(args)


def main(args=None):
    SyncAssignmentsCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
