from oslo_config import cfg


url = cfg.StrOpt(
    'url',
    default='https://authorization-service-api.web.cern.ch/api/v1.0/',
    help="resources rest api")

token_url = cfg.StrOpt(
    'token_url',
    default='https://auth.cern.ch/auth/realms/cern/api-access/token',
    help="authorization service token api")

client_id = cfg.StrOpt(
    'client_id',
    default='client_id',
    help="authorization service client id")

client_secret = cfg.StrOpt(
    'client_secret',
    default='client_secret',
    help="authorization service client secret")

audience = cfg.StrOpt(
    'audience',
    default=('authorization-service-api'),
    help="authorization service audience")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    url,
    token_url,
    client_id,
    client_secret,
    audience
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
