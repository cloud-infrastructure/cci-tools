from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools.cmd.base.foreman import BaseForemanCMD


class BaseCloudForemanCMD(BaseForemanCMD, BaseCloudCMD):
    def __init__(self, description):
        BaseCloudCMD.__init__(self, description=description)
        BaseForemanCMD.__init__(self, description=description)
