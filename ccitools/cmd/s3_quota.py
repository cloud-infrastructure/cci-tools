#!/usr/bin/python3

import logging
import stevedore
import sys

from ccitools.cmd.base.cloud import BaseCloudCMD
from collections import namedtuple
from itertools import compress
from keystoneclient import exceptions as keystone_exceptions
from osc_lib import utils

# configure logging
logger = logging.getLogger(__name__)

# formatters
_formatter_namespace = 'cliff.formatter.list'
_formatter_default = 'table'


def _load_formatter_plugins():
    # Here so tests can override
    return stevedore.ExtensionManager(
        _formatter_namespace,
        invoke_on_load=True, )


_formatter_plugins = _load_formatter_plugins()


class S3QuotaCMD(BaseCloudCMD):
    def __init__(self):
        super(S3QuotaCMD, self).__init__(
            description="Manages quotas in s3")

    def show_s3_quota(self, args):
        if args.all:
            projects = self.cloudclient.list_projects(
                domain='default',
                tags_any='s3quota'
            )
        else:
            try:
                projects = [self.cloudclient.find_project(args.project)]
            except keystone_exceptions.NotFound:
                logger.error('No project found with this id and/or name')
                projects = []

        results = []
        for project in projects:
            (containers_used, size_used) = (
                self.cloudclient.get_s3_project_usage(
                    project_id=project.id, region=args.region)
            )
            (containers_total, size_total) = (
                self.cloudclient.get_s3_project_quota(
                    project_id=project.id, region=args.region)
            )

            d = {
                'id': project.id,
                'name': project.name,
                'max_buckets': containers_total,
                'max_gb': size_total,
                'used_gb': size_used,
            }
            results.append(namedtuple("Quota", d.keys())(*d.values()))

        # Format output
        if not results:
            logger.error('There is no project to show')
            raise SystemExit(1)

        formatter = _formatter_plugins[args.formatter].obj
        columns = ['ID', 'Name', 'Max_Buckets', 'Max_GB', 'Used_GB']

        data = (utils.get_item_properties(s, columns, formatters={})
                for s in results)
        columns_to_include, selector = self._generate_columns_and_selector(
            args, columns)
        if selector:
            data = (list(compress(row, selector)) for row in data)
        formatter.emit_list(columns_to_include, data, sys.stdout, args)

    def update_s3_quota(self, args):
        try:
            project = self.cloudclient.find_project(args.project)
        except keystone_exceptions.NotFound:
            logger.error('No project found with this id and/or name')
            raise SystemExit(1)

        logger.info('Retrieving previous project quota')
        (containers, size) = self.cloudclient.get_s3_project_quota(
            project_id=project.id,
            region=args.region)

        logger.info('Setting new project quota')
        self.cloudclient.set_s3_project_quota(
            project_id=project.id,
            containers=args.containers if args.containers else containers,
            size_gb=args.size if args.size else size,
            region=args.region
        )

    def _generate_columns_and_selector(self, args, column_names):
        if not args.columns:
            columns_to_include = column_names
            selector = None
        else:
            columns_to_include = [c for c in column_names if c in args.columns]
            if not columns_to_include:
                raise ValueError(
                    'No recognized column names in %s' % str(args.columns))
            # Set up argument to compress()
            selector = [(c in columns_to_include) for c in column_names]
        return columns_to_include, selector

    def main(self, args=None):
        self.parser.add_argument('--region',
                                 default='cern',
                                 help='Region for the s3 quota request')
        subparsers = self.parser.add_subparsers(metavar='<subcommand>')

        parser_show = subparsers.add_parser(
            'show',
            help='Shows the S3 quota of the project '
                 'passed by parameter.')

        parser_show.set_defaults(func=self.show_s3_quota)

        behaviour = parser_show.add_mutually_exclusive_group()
        behaviour.add_argument('--all',
                               action='store_true')
        behaviour.add_argument('--project',
                               help='Project (ID or Name) to show S3 quota')

        formatter_group = parser_show.add_argument_group(
            title='output formatters',
            description='output formatter options', )
        formatter_choices = sorted(_formatter_plugins.names())
        formatter_default = _formatter_default
        if formatter_default not in formatter_choices:
            formatter_default = formatter_choices[0]
        formatter_group.add_argument(
            '-f',
            '--format',
            dest='formatter',
            action='store',
            choices=formatter_choices,
            default=formatter_default,
            help='the output format, defaults to %s' % formatter_default, )
        formatter_group.add_argument(
            '-c',
            '--column',
            action='append',
            default=[],
            dest='columns',
            metavar='COLUMN',
            help='specify the column(s) to include, can be repeated', )
        for formatter in _formatter_plugins:
            formatter.obj.add_argument_group(self.parser)

        parser_update = subparsers.add_parser(
            'update',
            help='Sets S3 quota on the project '
                 'passed by parameter.')
        parser_update.add_argument('--project',
                                   required=True,
                                   help='Project to show S3 quota')
        parser_update.add_argument('--containers',
                                   type=int,
                                   help='Quota on containers to set')
        parser_update.add_argument('--size',
                                   type=int,
                                   help='Quota on size (GB) to set')
        parser_update.set_defaults(func=self.update_s3_quota)

        args = self.parse_args(args)

        try:
            call = args.func
        except AttributeError:
            self.parser.error("Too few arguments")

        call(args)


# Needs static method for setup.cfg
def main(args=None):
    S3QuotaCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
