#!/usr/bin/python3

import logging
import sys

from ccitools.cmd.base.cloud import BaseCloudCMD
from keystoneclient.v3 import client as keystone_client

_logger = logging.getLogger(__name__)


class AssignmentCleanupCMD(BaseCloudCMD):

    def __init__(self):
        super(AssignmentCleanupCMD, self).__init__(
            description="Tool to cleanup the assignment table")

    def assignment_cleanup(self, args):
        _logger.warning("Checking missing assignment(s)")
        retcode = 0
        keystoneclient = keystone_client.Client(
            session=self.cloudclient.session)

        _logger.debug("Retrieving list of projects on default domain")
        projects = keystoneclient.projects.list(domain='default')
        _logger.info("Retrieved %s project(s)", len(projects))
        project_hash = {x.id: x for x in projects}

        _logger.debug("Retrieving list of users on default domain")
        users = keystoneclient.users.list(domain='default')
        _logger.info("Retrieved %s users(s)", len(users))
        user_hash = {x.id: x for x in users}

        _logger.debug("Retrieving list of groups on default domain")
        groups = keystoneclient.groups.list(domain='default')
        _logger.info("Retrieved %s group(s)", len(groups))
        group_hash = {x.id: x for x in groups}

        _logger.debug("Retrieving list of roles on default domain")
        roles = keystoneclient.roles.list()
        _logger.info("Retrieved %s role(s)", len(roles))
        role_hash = {x.id: x for x in roles}

        _logger.debug("Retrieving list of role assignments on default domain")
        role_assignments = keystoneclient.role_assignments.list()
        _logger.info(
            "Retrieved %s role assignment(s)", len(role_assignments))

        for role_asig in role_assignments:
            if (hasattr(role_asig, 'scope')
                    and 'project' in role_asig.scope
                    and role_asig.scope['project']['id'] in project_hash):
                _logger.debug("Project scope set into default domain")

                if hasattr(role_asig, 'user'):
                    if role_asig.user['id'] not in user_hash:
                        user = role_asig.user['id']
                        role = role_hash[role_asig.role['id']]
                        project = project_hash[role_asig.scope[
                            'project']['id']]
                        if project.name != 'Personal {0}'.format(user):
                            if role.name == 'owner':
                                # Check if there is another user with the role
                                # on the project and the user exists
                                if any(user_hash[o.user['id']]
                                       for o in keystoneclient.
                                       role_assignments.
                                       list(project=project.id, role=role.id)
                                       if o.user['id'] != user):
                                    _logger.warning(
                                        "Assignment to be deleted user '%s'"
                                        " with role '%s' on project '%s'",
                                        user, role.name, project.name)
                                    retcode = 1
                                    if args.clean:
                                        keystoneclient.roles.revoke(
                                            role.id,
                                            user=user,
                                            project=project.id)
                                        _logger.info("Assignment deleted")
                                else:
                                    _logger.error(
                                        "Check in resources for user '%s'"
                                        " with role '%s' on project '%s'",
                                        user, role.name, project.name)
                                    retcode = 1
                            else:
                                _logger.warning(
                                    "Assignment to be deleted user '%s'"
                                    " with role '%s' on project '%s'",
                                    user, role.name, project.name)
                                retcode = 1
                                if args.clean:
                                    keystoneclient.roles.revoke(
                                        role.id, user=user, project=project.id)
                                    _logger.info("Assignment deleted")
                elif hasattr(role_asig, 'group'):
                    if role_asig.group['id'] not in group_hash:
                        group = role_asig.group['id']
                        role = role_hash[role_asig.role['id']]
                        project = project_hash[
                            role_asig.scope['project']['id']]
                        _logger.warning(
                            "Role assignment to be deleted group '%s'"
                            " with role '%s' on project '%s'",
                            group, role.name, project.name)
                        retcode = 1
                        if args.clean:
                            keystoneclient.roles.revoke(role.id,
                                                        group=group,
                                                        project=project.id)
                            _logger.info("Assignment deleted")
                else:
                    _logger.error(
                        "Unexpected type %s TO BE CHECKED", role_asig)
                    retcode = 1
        return retcode

    def personal_cleanup(self, args):
        _logger.warning("Checking personal assignment(s)")
        retcode = 0
        keystoneclient = keystone_client.Client(
            session=self.cloudclient.session)

        _logger.debug("Retrieving list of projects on default domain")
        projects = keystoneclient.projects.list(
            domain='default',
            tags_any='expiration',
            enabled=True,
        )
        _logger.info("Retrieved %s personal project(s)", len(projects))
        project_hash = {x.id: x for x in projects}

        _logger.debug("Retrieving list of roles on default domain")
        roles = keystoneclient.roles.list()
        _logger.info("Retrieved %s role(s)", len(roles))
        role_hash = {x.id: x for x in roles}

        if not roles:
            raise Exception("Error fetching roles")
        role_id = [x.id for x in roles if x.name == 'Member'].pop()

        role_assignments = keystoneclient.role_assignments.list(
            role=role_id
        )
        _logger.info("Retrieved %s member assignment(s)", len(projects))

        assign_hash = {}
        for role_assig in role_assignments:
            if 'project' in role_assig.scope:
                key = role_assig.scope['project']['id']
                if key not in assign_hash:
                    assign_hash[key] = []
                assign_hash[key].append(role_assig)

        for project in projects:
            if project.id in assign_hash:
                _logger.info('Found member assignment in personal project '
                             '%s', project.id)

                for role_asig in assign_hash[project.id]:

                    if hasattr(role_asig, 'user'):
                        user = role_asig.user['id']
                        role = role_hash[role_asig.role['id']]
                        project = project_hash[project.id]
                        _logger.warning(
                            "Assignment to be deleted user '%s'"
                            " with role '%s' on project '%s'",
                            user, role.name, project.name)
                        retcode = 1
                        if args.clean:
                            keystoneclient.roles.revoke(
                                role.id,
                                user=user,
                                project=project.id)
                            _logger.info("Assignment deleted")

                    elif hasattr(role_asig, 'group'):
                        group = role_asig.group['id']
                        role = role_hash[role_asig.role['id']]
                        project = project_hash[
                            role_asig.scope['project']['id']]
                        _logger.warning(
                            "Role assignment to be deleted group '%s'"
                            " with role '%s' on project '%s'",
                            group, role.name, project.name)
                        retcode = 1
                        if args.clean:
                            keystoneclient.roles.revoke(
                                role.id,
                                group=group,
                                project=project.id)
                            _logger.info("Assignment deleted")
                    else:
                        _logger.error(
                            "Unexpected type %s TO BE CHECKED",
                            role_asig
                        )
                        retcode = 1
        return retcode

    def main(self, args=None):
        self.parser.add_argument(
            '--missing',
            action='store_true',
            help='Process only missing/leftovers', )

        self.parser.add_argument(
            '--personal',
            action='store_true',
            help='Process only personal inconsistencies', )

        self.parser.add_argument(
            '--clean',
            default=False,
            action='store_true',
            help='Clean the inconsistencies found', )

        args = self.parse_args(args)

        ret_assignment = 0
        ret_personal = 0

        if args.missing:
            ret_assignment = self.assignment_cleanup(args)

        if args.personal:
            ret_personal = self.personal_cleanup(args)
        return ret_assignment | ret_personal


# Needs static method for setup.cfg
def main(args=None):
    AssignmentCleanupCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        _logger.exception(e)
        sys.exit(1)
