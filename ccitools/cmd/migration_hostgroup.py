#!/usr/bin/python3

import ccitools.conf
import json
import logging
import re
import sys
import time

from ccitools.cmd.base.foreman import BaseForemanCMD
from ccitools.cmd.base.rundeck import BaseRundeckCMD
from ccitools.cmd.get_uptime import GetHostsUptime

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF

SLEEP_TIME = 60


class ExecuteRundeckjob(BaseRundeckCMD):
    def __init__(self):
        super(ExecuteRundeckjob, self).__init__(
            description=("Launch rundeck job using the API"))

    def getjobstatus(self, rdeckclient, execution_id):
        # return job status
        output = rdeckclient.get_execution_info(execution_id)
        match = re.findall("status=*.+' ", output)
        status = match[0]
        status = status.replace("status=", "")
        status = status.replace("'", "")
        status = status.replace(" ", "")
        logger.info("exec info:{}".format(status))
        return status

    def getrunningjob(self):
        # check for existing job execution first
        # todo
        args = ["--auth", "sso"]
        args = self.parse_args(args)
        return False

    def executejob(self, rdeckclient, host, exec_mode):
        logger.info("execute job here")
        job_id = "bca4f6a5-8918-4da0-aa38-a58be2eb35e6"
        output = rdeckclient.run_job_by_id(
            job_id,
            parameters={
                'behaviour': exec_mode,
                'hosts': host,
                'instance': 'cern'
            }
        )
        json_out = json.loads(output)
        logger.info("Follow the execution here: %s",
                    json_out['permalink'])
        return json_out['id']

    def jobmanager(self, hosts, exec_mode):
        # hardcode migrate and reboot job ID
        args = ["--auth", "sso"]
        args = self.parse_args(args)
        for host in hosts:
            # execute migrate and reboot job
            logger.info("current host : {}".format(host))
            execution_id = self.executejob(self.rundeckcli, host, exec_mode)
            # check for status
            status = self.getjobstatus(self.rundeckcli, execution_id)
            while (status == 'scheduled' or status == 'running'):
                logger.info("migration job with exec_id {} is : {}"
                            .format(execution_id, status))
                time.sleep(SLEEP_TIME)
                status = self.getjobstatus(self.rundeckcli, execution_id)


class MigrationHostgroupCMD(BaseForemanCMD):

    def __init__(self):
        super(MigrationHostgroupCMD, self).__init__(
            description="Checks Nodes if they exist in foreman")

    def get_node_foreman(self, hostgroup, exec_mode):
        """Return list of hosts in a hostgroup."""
        filter = "hostgroup_fullname = " + hostgroup
        HOSTS = []
        try:
            HOSTS.extend(self.foreman.search_query("hosts", filter))
        except Exception as e:
            logger.error("Error in getting hosts from hostgroup {}".format(e))
            sys.exit(1)
        data = set([host['name'] for host in HOSTS])
        logger.info("HOSTS : %s", list(data))

        # create space separated host string
        str_host = ' '.join(str(h) for h in list(data))

        # sort the hosts according to uptime
        cmd = GetHostsUptime()
        str_host = cmd.ssh_uptime(str_host, 'perform')
        logger.info("HOSTS with UPTIME : %s", str_host)

        # remove uptime
        data = []
        for host in str_host:
            host = host.split(':')
            data.append(host[0])

        logger.info("sorted hosts : {}".format(data))

        # check for existing running job first
        Exec_job = ExecuteRundeckjob()
        Exec_job.getrunningjob()

        # prepare migration job for execution
        Exec_job.jobmanager(data, exec_mode)

    def main(self, args=None):
        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true',)
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')
        self.parser.add_argument(
            "--hostgroup", required=True,
            help="Returns list of hosts in a provided hostgroup")

        args = self.parse_args(args)

        # execution mode
        exec_mode = ""
        if not args.exec_mode:
            logger.info("[DRYRUN] Execution mode.")
            exec_mode = "dryrun"
        else:
            logger.info("[PERFORM] Execution mode.")
            exec_mode = "perform"

        # process each hostgroup
        for hostgroup in args.hostgroup.split():
            self.get_node_foreman(hostgroup, exec_mode)


# Needs static method for setup.cfg
def main(args=None):
    MigrationHostgroupCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
