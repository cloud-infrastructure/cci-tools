import json
import logging
import shlex
import subprocess  # nosec

from ccitools.errors import RundeckAPIError
import requests

logger = logging.getLogger(__name__)


class RundeckAPIClient(object):
    """RundeckAPI Client to interact with the Rundeck API."""

    def __init__(self,
                 server,
                 port,
                 sso_cookie=None,
                 token=None,
                 verify=False,
                 api_version=20,
                 timeout=60):
        """Initialize the class with the params needed to use the API.

        :param server: Rundeck Server Ex: cci-rundeck.cern.ch
        :param port: Rundeck port
        :param token: user API key token
        :param sso_cookie: cookie SSO
        """
        self.server = server
        self.api_key = token
        self.cookies = sso_cookie
        self.base_url = 'https://%s:%s' % (self.server, port)
        self.verify = verify
        self.api_version = api_version
        self.timeout = timeout

        if self.api_key:
            self.headers = {'Content-Type': 'application/json',
                            'X-RunDeck-Auth-Token': self.api_key}
        if self.cookies:
            self.headers = {'Content-Type': 'application/json'}

    def __send_request(self, request_type, url, **kwargs):
        r_headers = self.headers.copy()
        if 'headers' in kwargs:
            r_headers.update(kwargs.pop('headers', None))
        kwargs["verify"] = self.verify

        if request_type.lower() == 'delete':
            r = requests.delete(url=url, headers=r_headers,
                                cookies=self.cookies, timeout=self.timeout,
                                **kwargs)
        elif request_type.lower() == 'get':
            r = requests.get(url=url, headers=r_headers, cookies=self.cookies,
                             timeout=self.timeout, **kwargs)
        elif request_type.lower() == 'post':
            r = requests.post(url=url, headers=r_headers, cookies=self.cookies,
                              timeout=self.timeout, **kwargs)
        elif request_type.lower() == 'put':
            r = requests.put(url=url, headers=r_headers, cookies=self.cookies,
                             timeout=self.timeout, **kwargs)
        else:
            raise RundeckAPIError("Specified request_type '%s' not supported" %
                                  request_type)

        if r.status_code != requests.codes.ok:
            logger.info(r.text)
            raise RundeckAPIError(
                "The request: '%s' failed with status code '%s'"
                % (url, r.status_code))
        return r

    def __health_check(self):
        """Return True if the server is available."""
        url = '%s/api/%s/system/info' % (
            self.base_url,
            self.api_version)
        try:
            self.__send_request('put', url)
            return True
        except requests.exceptions.RequestException:
            return False
        return False

    def __get_total_project_executions(self, project_name):
        """Get total number of executions for a certain project.

        :param project_name: Name of the project
        """
        url = '%s/api/%s/project/%s/executions' % (
            self.base_url,
            self.api_version,
            project_name)
        headers = {'Accept': 'application/json'}
        r = self.__send_request('get', url, headers=headers)
        total_executions = json.loads(r.text)['paging']['total']
        return total_executions

    def abort_execution_by_id(self, execution_id):
        """Abort one execution by ID.

        :param execution_id: Execution id
        """
        url = '%s/api/%s/execution/%s/abort' % (
            self.base_url,
            self.api_version,
            execution_id)
        self.__send_request('get', url)

    def delete_execution_by_id(self, execution_id):
        """Delete one execution by ID.

        :param execution_id: Execution id
        """
        url = '%s/api/%s/execution/%s' % (
            self.base_url,
            self.api_version,
            execution_id)
        self.__send_request('delete', url)

    def delete_executions_for_job(self, job_id):
        """Delete all executions for a job.

        :param job_id: Job id
        """
        url = '%s/api/%s/job/%s/executions' % (
            self.base_url,
            self.api_version,
            job_id)
        self.__send_request('delete', url)

    def export_jobs(self, project_name):
        """Get all the jobs from a project in a XML exportable format.

        :param project_name: Project name
        """
        url = '%s/api/%s/project/%s/jobs/export' % (
            self.base_url,
            self.api_version,
            project_name)
        r = self.__send_request('get', url)
        return r.text

    def export_project(self, project_name):
        """Export the project date in XML format.

        :param project_name: Project name
        """
        url = '%s/api/%s/project/%s/export' % (
            self.base_url,
            self.api_version,
            project_name)
        r = self.__send_request('get', url)
        return r

    def get_execution_info(self, execution_id):
        """Get information about a certain execution.

        :param execution_id: Execution ID
        """
        url = '%s/api/%s/execution/%s' % (
            self.base_url,
            self.api_version,
            execution_id)
        r = self.__send_request('get', url)
        return r.text

    def get_execution_output(self, execution_id):
        """Get information about a certain execution.

        :param execution_id: Execution ID
        """
        url = '%s/api/%s/execution/%s/output' % (
            self.base_url,
            self.api_version,
            execution_id)
        r = self.__send_request('get', url)
        return r.text

    def import_job(self, job_xml):
        """Import the job.

        :param job_xml: Job xml definition
        """
        # danielfr: Not working with SSO
        url = '%s/api/%s/jobs/import' % (
            self.base_url,
            self.api_version)
        headers = '"X-Rundeck-Auth-Token:%s"' % self.api_key
        form = 'xmlBatch=@"%s"' % job_xml
        cmd = "curl -k --header %s -F %s %s" % (headers, form, url)
        args = shlex.split(cmd)
        p = subprocess.Popen(  # nosec
            args,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        out, err = p.communicate()
        p.wait()

    def jobs_executions(self, job_id):
        """Get the list of the executions for a certain job.

        :param job_id: Job id
        """
        url = '%s/api/%s/job/%s/executions' % (
            self.base_url,
            self.api_version,
            job_id)
        r = self.__send_request('get', url)
        return r.text

    def list_projects(self):
        """Get the list of the existing projects."""
        url = '%s/api/%s/projects' % (
            self.base_url,
            self.api_version)
        r = self.__send_request('get', url)
        return r.text

    def list_jobs_project(self, project_name):
        """Get all the jobs from a project.

        :param project_name: Project name
        """
        url = '%s/api/%s/project/%s/jobs' % (
            self.base_url,
            self.api_version,
            project_name)
        r = self.__send_request('get', url)
        return r.text

    def list_scheduled_executions(self, project_name):
        """Get all the scheduled executions in a Rundeck project.

        Returns a list in JSON format.

        :param project_name: Project name
        """
        url = '%s/api/%s/project/%s/executions' % (
            self.base_url,
            self.api_version,
            project_name)
        headers = {'Accept': 'application/json'}
        query = {
            'statusFilter': 'scheduled',
            'executionTypeFilter': 'user-scheduled',
        }
        r = self.__send_request('get', url, params=query,
                                headers=headers)
        return r.text

    def project_executions(self, project_name, query={}):
        """Get the list of the executions for a certain project.

        :param project_name: Project name
        :param query: Python dict with filtering params
        """
        rest_query = ""
        for param, value in query.items():
            rest_query += "&%s=%s" % (param, value)

        # Use this request with caution
        # We want to retrieve all executions for the given project_name
        # So we make sure that later on we can filter out
        # all possible executions based on job options
        total_executions = self.__get_total_project_executions(project_name)

        url = '%s/api/%s/project/%s/executions?max=%s%s' % (
            self.base_url,
            self.api_version,
            project_name,
            total_executions,
            rest_query)

        headers = {'Accept': 'application/json'}
        r = self.__send_request('get', url, data=json.dumps(query),
                                headers=headers)
        return r.text

    def run_job_by_id(self, job_id, parameters={}, log_level="INFO"):
        """Run one job by ID.

        :param job_id: Job id
        """
        url = '%s/api/%s/job/%s/run' % (
            self.base_url,
            self.api_version,
            job_id)

        arg_string = " ".join('-{} {}'.format(key, val) for key, val
                              in parameters.items())
        payload = {'argString': arg_string,
                   'loglevel': log_level}
        headers = {'Accept': 'application/json'}

        r = self.__send_request('post', url, data=json.dumps(payload),
                                headers=headers)

        return r.text

    def run_scheduled_job_by_id(self, job_id, datetime, parameters={},
                                log_level="INFO", asUser=None):
        """Run one job by ID at the specified datetime.

        Parameter datetime follows ISO-8601 date and time stamp with timezone,
        with optional milliseconds., eg "2017-09-08T15:42:42+0200"

        :param job_id: Job id
        :param datetime: Date and time for execution
        :param parameters: Dictionary of job parameters
        :param log_level: Log level for the executed job
        """
        url = '%s/api/%s/job/%s/run' % (
            self.base_url,
            self.api_version,
            job_id)

        payload = {'options': parameters,
                   'loglevel': log_level,
                   'runAtTime': datetime,
                   'asUser': asUser}
        headers = {'Accept': 'application/json'}

        r = self.__send_request('post', url, data=json.dumps(payload),
                                headers=headers)

        return r.text

    def takeover_schedule(self, server_uuid=None):
        """Takeover scheduled jobs from eventual affected servers.

        :param server_uuid: UUID of the server to be taken over
        """
        url = '%s/api/%s/scheduler/takeover' % (
            self.base_url,
            self.api_version)
        if server_uuid:
            payload = "{ server: { uuid: \"" + server_uuid + "\" } }"
        else:
            payload = '{ server: { all: "true" } }'

        r = self.__send_request('put', url, data=payload)
        return r.text
