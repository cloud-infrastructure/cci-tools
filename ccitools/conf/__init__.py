from oslo_config import cfg

from ccitools.conf import accounting
from ccitools.conf import anomalous_vm
from ccitools.conf import bmc_access
from ccitools.conf import chargegroup
from ccitools.conf import cleanup
from ccitools.conf import coordination
from ccitools.conf import cornerstone
from ccitools.conf import dbreporter
from ccitools.conf import es
from ccitools.conf import fim
from ccitools.conf import foreman
from ccitools.conf import gitlab
from ccitools.conf import influxdb
from ccitools.conf import jira
from ccitools.conf import keystone
from ccitools.conf import landb
from ccitools.conf import ldap
from ccitools.conf import loadbalancer
from ccitools.conf import mail
from ccitools.conf import megabus
from ccitools.conf import monit
from ccitools.conf import nova
from ccitools.conf import puppetdb
from ccitools.conf import resource_request
from ccitools.conf import resources
from ccitools.conf import rgw
from ccitools.conf import rundeck
from ccitools.conf import s3
from ccitools.conf import snowflake

conf_modules = [
    anomalous_vm,
    chargegroup,
    cleanup,
    coordination,
    cornerstone,
    dbreporter,
    fim,
    foreman,
    gitlab,
    influxdb,
    jira,
    keystone,
    landb,
    ldap,
    mail,
    megabus,
    monit,
    nova,
    puppetdb,
    resources,
    resource_request,
    rgw,
    rundeck,
    s3,
    snowflake,
    loadbalancer,
    es,
    bmc_access,
    accounting,
]

CONF = cfg.CONF

for module in conf_modules:
    module.register_opts(CONF)

CONF(args=[], project='ccitools')
