import logging

from ccitools.cmd.dump_ticket import main
from ccitools.tests import fixtures
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.cmd.dump_ticket.ServiceNowClient')
class TestDumpTicket(TestCase):

    def setUp(self):
        logging.disable(logging.INFO)

    def test_default_args(self, mock_snc):
        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)

    def test_dump_ticket_not_found(self, mock_snc):
        mock_snow = mock_snc.return_value

        mock_snow.ticket.get_ticket.return_value = None

        with self.assertRaises(SystemExit) as cm:
            main([
                '--ticket-number', 'fake_ticket',
                '--instance', 'fake_cern',
            ])
        self.assertEqual(cm.exception.code, -1)

    def test_dump_ticket(self, mock_snc):
        mock_snow = mock_snc.return_value

        mock_snow.ticket.get_ticket.return_value = (
            fixtures.Resource({
                'info': {
                    'u_caller_id': 'fake_id',
                    'short_description': 'short_fake_description',
                    'u_functional_element': 'fake_fe',
                    'assigned_to': 'fake_id',
                }
            })
        )

        mock_snow.fe.fe_client.get.return_value = (
            fixtures.Resource({'name': 'test_fe'})
        )

        mock_snow.user.get_user_info_by_sys_id.side_effect = iter([
            fixtures.Resource({'name': 'test_caller'}),
            fixtures.Resource({'name': 'test_assigner'})
        ])

        main([
            '--ticket-number', 'fake_ticket',
            '--instance', 'fake_cern',
        ])
