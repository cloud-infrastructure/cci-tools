#!/usr/bin/python3

import logging
import prettytable
import sys

from ccitools.cmd import base
from ccitools.utils import aihelper
from cryptography.fernet import Fernet

logger = logging.getLogger(__name__)


class RotateFernetKeysCmd(base.BaseCMD):

    def __init__(self):
        super(RotateFernetKeysCmd, self).__init__(
            description="Rotate keystone "
                        "fernet tokens on Teigi")

    def main_rotate_keys(self, args, aiclient):
        keys = args.keys
        table = prettytable.PrettyTable(["teigi_secret_tag", "Old", "New"])
        old_values = [0] * (keys + 1)
        new_values = [0] * (keys + 1)

        for i in range(0, keys + 1):
            old_values[i] = aiclient.get_teigi_key(args, i)

        for i in range(1, keys):
            new_values[i] = old_values[i + 1]

        new_values[0] = Fernet.generate_key()
        new_values[keys] = old_values[0]

        for i in range(0, keys + 1):
            aiclient.update_teigi_key(args, i, new_values[i])
            tag = "%s_%s" % (args.teigi_tag, i)
            table.add_row([tag, old_values[i], new_values[i]])

    def main(self, args=None):
        self.parser.add_argument("--hostgroup", required=True,
                                 help="Hostgroup")
        self.parser.add_argument("--keys", type=int, default=6,
                                 help="Number of fernet keys")
        self.parser.add_argument("--teigi-tag",
                                 help="Teigi tag. Ex: keystone_fernet_")

        args = self.parse_args(args)

        aiclient = aihelper.AIClient()
        self.main_rotate_keys(args, aiclient)


# Needs static method for setup.cfg
def main(args=None):
    RotateFernetKeysCmd().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.error(e)
        sys.exit(-1)
