import logging

from ccitools.cmd.megabus_producer import main
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
@mock.patch('ccitools.cmd.megabus_producer.stomp.Connection')
class TestMegabusProducer(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self, mock_st, mock_crc):
        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)

    def test_dryrun_proper_data(self, mock_st, mock_crc):
        mock_crc = mock_crc.return_value
        mock_st = mock_st.return_value

        main([
            '--type', 'fake_type',
            '--version', 'fake_ver',
            '--hosts', 'fh1 fh2',
            '--intervention-time', '01-01-2019 00:00',
            '--duration', 'f_0',
            '--dryrun',
        ])

    def test_perform_proper_data(self, mock_st, mock_crc):
        mock_crc = mock_crc.return_value
        mock_st = mock_st.return_value

        main([
            '--type', 'fake_type',
            '--version', 'fake_ver',
            '--hosts', 'fh1 fh2',
            '--intervention-time', '01-01-2019 00:00',
            '--duration', 'f_0',
            '--perform',
        ])

    def test_exception(self, mock_st, mock_crc):
        mock_crc = mock_crc.return_value
        mock_st = mock_st.return_value
        mock_crc.get_servers_by_hypervisor.return_value = None

        main([
            '--type', 'fake_type',
            '--version', 'fake_ver',
            '--hosts', 'fh1 fh2',
            '--intervention-time', '01-01-2019 00:00',
            '--duration', 'f_0',
            '--dryrun',
        ])
