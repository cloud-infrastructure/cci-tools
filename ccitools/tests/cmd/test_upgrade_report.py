import logging
import re

from ccitools.tests.cmd import fixtures
from parameterized import parameterized
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.cmd.upgrade_report.keystone_client.Client')
@mock.patch('ccitools.cmd.upgrade_report.nova_client.Client')
@mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
@mock.patch('ccitools.utils.cloud.cloud_config')
class TestUpgradeReportCMD(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.aitools_mock = mock.MagicMock()
        modules = {
            'aitools': self.aitools_mock,
            'aitools.foreman': self.aitools_mock.foreman,
        }
        self.patcher = mock.patch.dict('sys.modules', modules)
        self.patcher.start()
        self.addCleanup(self.patcher.stop)

    @parameterized.expand([
        [80 * (1024**3), ""],
        [81 * (1024**3), "*"],
        [160 * (1024**3), "*"],
        [161 * (1024**3), "**"],
        [320 * (1024**3), "**"],
        [321 * (1024**3), "***"],
        [700 * (1024**3), "***"],
    ])
    def test_get_cold_hint(self, mock_cc, mock_crc, mock_nc, mock_kc,
                           size_input, rating):
        from ccitools.cmd.upgrade_report import UpgradeReportCMD
        cmd = UpgradeReportCMD()
        self.assertEqual(cmd.get_cold_hint(size_input), rating)

    def test_main_without_aggregates(self, mock_cc, mock_crc, mock_nc,
                                     mock_kc):
        from ccitools.cmd.upgrade_report import main
        mock_crc.return_value = mock.MagicMock()
        mock_nc.return_value = mock.MagicMock()
        mock_nc.return_value.aggregates.list.return_value = []
        m = mock.mock_open()
        with mock.patch('ccitools.cmd.upgrade_report.open', m):
            main(['--output', '/tmp/file.txt', '--parallelism', '1'])
            m.assert_called_once_with('/tmp/file.txt', 'w')
            m().write.assert_has_calls([
                mock.call(''),
                mock.call('\n')])
            mock_nc.return_value.aggregates.list.assert_called_once()

    def test_main_with_aggregates(self, mock_cc, mock_crc, mock_nc, mock_kc):
        from ccitools.cmd.upgrade_report import main

        mock_crc.return_value = mock.MagicMock()
        # Keystone
        mock_kc.return_value = mock.MagicMock()
        mock_kc.return_value.projects.list.return_value = fixtures.PROJECT_LIST
        mock_kc.return_value.roles.list.return_value = fixtures.ROLE_LIST

        # Foreman
        self.aitools_mock.foreman.ForemanClient().search_query.side_effect = \
            lambda resource, query: fixtures.FOREMAN_HOSTS_CELL[re.search(
                "test_shared_[0-9]+", query)[0]]

        # Nova
        nova_client_mock = mock.MagicMock()
        mock_nc.return_value = nova_client_mock
        nova_client_mock.aggregates.list.return_value = fixtures.AGGREGATE_LIST
        nova_client_mock.servers.list.side_effect = \
            lambda search_opts: fixtures.SERVERS_HOST[search_opts['host']]
        nova_client_mock.migrations.list.return_value = []

        with mock.patch('ccitools.cmd.upgrade_report.ssh_executor',
                        return_value=[[161 * (1024 ** 3)]]) as mock_ssh:
            m = mock.mock_open()
            with mock.patch('ccitools.cmd.upgrade_report.open', m):
                main(['--output', '/tmp/file.txt', '--parallelism', '1'])

                self.aitools_mock.foreman.ForemanClient()\
                    .search_query.assert_called()
                nova_client_mock.servers.list.assert_called()
                nova_client_mock.migrations.list.assert_called()
                mock_kc.return_value.projects.list.assert_called_once()
                nova_client_mock.aggregates.list.assert_called_once()
                mock_ssh.assert_called_once()

                m.assert_called_once_with('/tmp/file.txt', 'w')
                self.maxDiff = None
                m().write.assert_has_calls([
                    mock.call(fixtures.UPGRADE_REPORT_FILLED),
                    mock.call('\n')])
