#!/usr/bin/python3

import ccitools.conf
import logging
import socket


from ccitools.cmd.base.cloud import BaseCloudCMD
from landbclient.client import LanDB

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class AssignResourceClassIronic(BaseCloudCMD):
    def __init__(self):
        super(AssignResourceClassIronic, self).__init__(
            description="Generates resource class for the baremetal node.")
        self.parser.add_argument(
            "--node", help="Node name")
        self.parser.add_argument(
            "--flavor_prefix", default="p1",
            help="Flavor prefix, p1 for physical, s1 for the storage.")
        self.parser.add_argument(
            "--add_rack", default=False, action='store_true',
            help="If set to True, rack location will be added in the RC.")

    def main(self, args=None):
        """Parse node's name and generate resource class."""
        args = self.parse_args(args)

        self.landb = LanDB(
            username=CONF.landb.user,
            password=CONF.landb.password,
            host=CONF.landb.host,
            port=CONF.landb.port,
            protocol=CONF.landb.protocol,
            version=CONF.landb.version)

        node = args.node
        delivery = node[0: 9]
        node_ip = socket.gethostbyname(node)
        device = self.landb.device_info(
            self.landb.device_search(ip_addr=node_ip)[0])
        rack_location = ''
        if args.add_rack:
            rack_location = device.Zone + '_'
        for interface in device.Interfaces:
            if interface.Name == device.DeviceName + '.CERN.CH':
                svc_name = interface.ServiceName.replace('-', '_')
                resource_class = 'BAREMETAL_{}_{}_{}{}'.format(
                    args.flavor_prefix.upper(), delivery.upper(),
                    rack_location, svc_name)
                ironic_command = "openstack baremetal node set {} " \
                    "--resource-class {}"
                print(ironic_command.format(node, resource_class))


def main(args=None):
    AssignResourceClassIronic().main(args)
