#!/usr/bin/python3

import ccitools.conf
import defusedxml.ElementTree
import logging
import os
import pickle  # nosec
import sys

from ccitools.cmd.base.rundeck import BaseRundeckCMD

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class ExportRundeckProjectsCMD(BaseRundeckCMD):
    def __init__(self):
        super(ExportRundeckProjectsCMD, self).__init__(
            description="Export Rundeck Projects")

    # Return a list with all the project names
    def get_project_names(self, projectsinfo_xml):
        project_names = []
        root = defusedxml.ElementTree.fromstring(projectsinfo_xml)
        for name in root.findall('project'):
            project_names.append(name.find('name').text)
        return project_names

    def save_exported_project(self, exportedproject_zip, project_name, output):
        """Save the exported project as a zip file in the given output.

        exportedproject_zip: zip object
        project_name: used to save the file str
        output: path to save the output
        """
        if not os.path.exists(output):
            os.makedirs(output)
        with open('%s/%s.zip' % (output, project_name), 'wb') as output:
            pickle.dump(exportedproject_zip, output, pickle.HIGHEST_PROTOCOL)
            logger.info("'%s.zip' exported in '%s'" % (project_name, output))

    def main(self, args=None):
        self.parser.add_argument(
            "--output",
            dest='output',
            nargs='?',
            default="/tmp",  # nosec
            help="Path where the XMLs are exported")

        args = self.parse_args(args)

        projects = self.get_project_names(self.rundeckcli.list_projects())
        for project in projects:
            request = self.rundeckcli.export_project(project)
            logger.info("Exporting '%s' project in '%s'..." % (
                project, args.output))
            self.save_exported_project(request.content, project, args.output)


# Needs static method for setup.cfg
def main(args=None):
    ExportRundeckProjectsCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)
