from oslo_config import cfg


webservice = cfg.StrOpt(
    'webservice',
    default=('https://resources.web.cern.ch/resources'
             '/Manage/CloudInfrastructure/Administration'
             '/Service.asmx?WSDL'),
    help="fim webservice url")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    webservice
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
