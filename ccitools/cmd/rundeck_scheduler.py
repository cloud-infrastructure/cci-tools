#!/usr/bin/python3

import ast
import json
import logging
import sys

from ccitools.cmd.base.rundeck import BaseRundeckCMD
from ccitools.common import date_parser
from ccitools.common import date_validator

# configure logging
logger = logging.getLogger(__name__)


class RundeckSchedulerCMD(BaseRundeckCMD):

    def __init__(self):
        super(RundeckSchedulerCMD, self).__init__(
            description="Schedules a Rundeck Job")

    def rundeck_scheduler(self, rdeckclient, exec_mode, job_id, datetime,
                          log_level, parameters, user):

        # Parse datetime to meet ISO-8601 criteria
        date_parsed_iso = date_parser(datetime)

        # Make sure datetime is not in the past
        if not date_validator(date_parsed_iso):
            logger.error("Oops! Cannot schedule for a date in the past. "
                         "Please, make sure you specified "
                         "correct values of date and time.")
            sys.exit(1)

        # A bit of hacking to parse optional 'parameters' str to dict
        if parameters:
            options = ast.literal_eval(parameters)
        else:
            options = {}

        if exec_mode:
            # Schedule job at specified datetime
            logger.info("Scheduling job with ID %s for %s...", job_id,
                        datetime)
            output = rdeckclient.run_scheduled_job_by_id(
                job_id, date_parsed_iso, options, log_level, user)
            json_out = json.loads(output)
            logger.info("Follow the execution here: %s", json_out['permalink'])
        else:
            logger.info("[DRYRUN] Schedule job with ID %s for %s",
                        job_id, datetime)

    def main(self, args=None):
        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true',
                               help="If present, sends email")
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')

        self.parser.add_argument("--job-id", required=True,
                                 help="ID of the job to be scheduled")
        self.parser.add_argument("--datetime", required=True,
                                 help="Date and time of execution. "
                                 "Input must follow ISO-8601 date "
                                 "and time stamp with timezone, "
                                 "eg. 2017-09-08T15:42:42+0200")
        self.parser.add_argument("--log-level", default="INFO",
                                 help="Logging level for the scheduled job")
        self.parser.add_argument("--parameters", default={},
                                 help="Dictionary with parameters for the "
                                 "scheduled job, eg. {'myopt1':'value',"
                                 "'myopt2':'value'}")
        self.parser.add_argument("--user", required=True,
                                 help="User that runs the scheduled job")

        args = self.parse_args(args)

        self.rundeck_scheduler(
            self.rundeckcli,
            args.exec_mode,
            args.job_id,
            args.datetime,
            args.log_level,
            args.parameters,
            args.user)


# Needs static method for setup.cfg
def main(args=None):
    RundeckSchedulerCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
