import logging

from ccitools.cmd.report_gitlab_mrs import main
from unittest import mock
from unittest import TestCase


class TestReportGitlabMRsCMD(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch('ccitools.cmd.report_gitlab_mrs.Gitlab')
    def test_dryrun(self, mock_gitlab):
        mock_gl = mock_gitlab.return_value
        mock_gl.projects.list.return_value = []

        main(['--dryrun'])
