import ccitools.conf
import json
import logging
import warnings

from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session

logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF

ENDPOINTS = {
    'project_list': 'OpenstackProject',
    'project_create': 'OpenstackProject',
    'project_delete': 'OpenstackProject/{0}',
    'project_get': 'OpenstackProject/{0}',
    'project_activate': 'OpenstackProject/{0}/Activate',
    'group_get': 'Group/{0}',
    'user_get': 'Identity/{0}'
}


class ResourcesClient(object):

    def __init__(self):
        self.session = self.get_session()

    def get_session(self):
        client = BackendApplicationClient(
            client_id=CONF.resources.client_id)
        session = OAuth2Session(client=client)
        session.fetch_token(
            token_url=CONF.resources.token_url,
            client_id=CONF.resources.client_id,
            client_secret=CONF.resources.client_secret,
            audience=CONF.resources.audience)
        return session

    def _build_url(self, url, params=None):
        if params:
            url = url.format(*params)
        return CONF.resources.url + url

    def _dispatch(self, url, params=None, data=None, method='GET'):
        headers = {'Content-Type': 'application/json'}
        url = self._build_url(url, params)
        method = method.upper()
        # See https://github.com/shazow/urllib3/issues/497
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            return self.session.request(
                method=method, url=url, data=data,
                headers=headers)

    def create_project(self, name, description, category,
                       owner, administrators):
        """Create a new OpenStack Project.

        :param name: Project name
        :param description: Project description
        :param category: Category of the project
        :param owner: Project owner
        :param administrator: Project administrators
        """
        owner_id = self.get_user_id(owner)
        group_id = self.get_group_id(administrators)

        data = {
            "administratorsId": group_id,
            "resourceIdentifier": name,
            "displayName": name,
            "description": description,
            "resourceCategory": category,
            "ownerId": owner_id
        }
        data = json.dumps(data)
        response = self._dispatch(
            ENDPOINTS['project_create'],
            data=data,
            method='POST')
        response.raise_for_status()
        output = json.loads(response.text)
        return output['data']['id']

    def list_projects(self):
        response = self._dispatch(
            ENDPOINTS['project_list'],
            method='GET')
        output = json.loads(response.text)
        return output['data']

    def get_project(self, id_name):
        response = self._dispatch(
            ENDPOINTS['project_get'],
            params=[id_name],
            method='GET')
        response.raise_for_status()
        output = json.loads(response.text)
        return output['data']

    def activate_project(self, id):
        """Activate an existing OpenStack Project.

        :param id: Project id
        """
        response = self._dispatch(
            ENDPOINTS['project_activate'],
            params=[id],
            data={},
            method='PUT')
        response.raise_for_status()

    def delete_project(self, id):
        """Delete an existing OpenStack Project.

        :param id: Project id
        """
        response = self._dispatch(
            ENDPOINTS['project_delete'],
            params=[id],
            data={},
            method='DELETE')
        response.raise_for_status()

    def get_user_id(self, user):
        """Retrieve the user id.

        :param user: User name
        """
        response = self._dispatch(
            ENDPOINTS['user_get'],
            params=[user],
            data={},
            method='GET')
        output = json.loads(response.text)
        return output['data']['id']

    def get_group_id(self, group):
        """Retrieve the group id.

        :param group: Group name
        """
        response = self._dispatch(
            ENDPOINTS['group_get'],
            params=[group],
            data={},
            method='GET')
        output = json.loads(response.text)
        return output['data']['id']
