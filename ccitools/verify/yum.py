import logging

from ccitools.common import ssh_executor
from ccitools.verify import base

# configure logging
logger = logging.getLogger(__name__)


class DistroSyncAction(base.ActionBase):

    def __init__(self, host):
        super(DistroSyncAction, self).__init__(host)
        self.pattern = r'.*\/tail-distro_sync\/count-yum_error'

    def try_fix(self):
        """Implement the fix for the exception thrown in collectd."""
        logger.info("Rotating distrosync log...")
        ssh_executor(self.host, "/usr/sbin/logrotate -f /etc/logrotate.conf")

        logger.info("Running distro_sync...")
        ssh_executor(self.host, "/usr/bin/sh /usr/local/sbin/distro_sync.sh")


class UnfinishedTransactionsAction(base.ActionBase):

    def __init__(self, host):
        super(UnfinishedTransactionsAction, self).__init__(host)
        self.pattern = r'.*\/yum-unfinished_transactions\/gauge'

    def try_fix(self):
        """Implement the fix for the exception thrown in collectd."""
        logger.info("Triggering yum-complete-transaction ")
        ssh_executor(self.host, "/usr/sbin/yum-complete-transaction -y")


class BrokenReposAction(base.ActionBase):

    def __init__(self, host):
        super(BrokenReposAction, self).__init__(host)
        self.pattern = r'.*\/yum-broken_repos\/gauge'

    def try_fix(self):
        # There is no fix for this alarm, ignoring it.
        pass
