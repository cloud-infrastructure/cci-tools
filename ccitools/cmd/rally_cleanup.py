#!/usr/bin/python3

import ccitools.conf
import getpass
import logging
import socket
import sys
import time

from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools import common
from ccitools.utils.servicenowv2 import ServiceNowClient
from stevedore import driver

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class RallyCleanupCMD(BaseCloudCMD):

    def __init__(self):
        super(RallyCleanupCMD, self).__init__(
            description="Cleans old Rally jobs")

    def clean_old_rally_jobs(self, args, snowclient):
        """Execute all phases of Rally leftovers cleanup.

        1- Obtains lists of servers and volumes to be deleted
        2- Displays information about these target resources
        3- Forces deletion of found rally leftovers
        4- Waits for an specified time interval (in seconds)
        5- Triggers deletion of rally leftovers that were required before.
        6- Waits until the required leftovers are deleted (in seconds).
        7- Checks confirmation of deletion of all these resources

        :param cloud: CloudClient object
        """
        # Step 0 load extensions
        resources = []
        for config in CONF.cleanup.available_jobs.values():
            try:
                kwargs = common.merge_dicts(
                    {
                        'cloud': self.cloudclient,
                        'quiet': False,
                    },
                    config['kwargs'] if 'kwargs' in config else {},
                )
                mgr = driver.DriverManager(
                    namespace='ccitools.cleanup',
                    name=config['driver'],
                    invoke_on_load=True,
                    invoke_kwds=kwargs,
                )
                resources.append(mgr.driver)
            except Exception as ex:
                logger.exception(ex)

        # Step 1
        to_clean = False
        for resource in resources:
            if resource.leftovers:
                resource_table = resource.format_table()
                logger.info("################################")
                logger.info("#### PENDING %s", resource.name)
                logger.info("################################")
                logger.info(resource_table)
                to_clean = True

        if not to_clean:
            logger.info("There is nothing to clean up, exiting.")
            return

        # Step 3
        for resource in resources:
            resource.delete_leftovers_before()

        # Step 4
        logger.info("Waiting %s seconds for deletion "
                    "of Rally leftovers..." % args.timeout)
        time.sleep(float(args.timeout))

        # Step 5
        for resource in resources:
            resource.delete_leftovers_after()

        # Step 6
        time.sleep(min(10, float(args.timeout)))

        # Step 7
        broken = False
        for resource in resources:
            resource.refresh()
            if resource.leftovers:
                broken = True

        if broken:
            logger.error("Ooops! For some reason some resources "
                         "could not deleted:")

            logger.info("Building SNOW ticket for further investigations...")
            description = "[Rundeck] Failed to clean Rally leftovers"
            incident = snowclient.ticket.create_INC(
                short_description=description,
                functional_element=CONF.cleanup.functional_element,
                assignment_group=CONF.cleanup.assignment_group
            )
            logger.info("Created incident number: %s", incident.info.number)
            incident.add_comment(args.execution_reference)
            incident.save()

            for resource in resources:
                if resource.leftovers:
                    resource_table = resource.format_table()
                    logger.error("################################")
                    logger.error("#### BROKEN %s", resource.name)
                    logger.error("################################")
                    logger.error(resource_table)

                    error_msg = "Could not clean the following list " \
                                "of Rally resources:\n[code]<pre>%s</pre>" \
                                "[/code]" \
                                % resource_table.get_string().replace(
                                    '\n', "<br/>")
                    incident.add_comment(error_msg)
                    incident.save()

            logger.error("Manual intervention is required. "
                         "Please, check logs for more detailed information")
            sys.exit(1)
        else:
            logger.info("All Rally leftovers have been cleaned successfully!")

    def main(self, args=None):
        self.parser.add_argument("--execution-reference",
                                 help="Adds an execution reference to the "
                                      "work notes",
                                 default="Executed by '%s' on '%s'" % (
                                     getpass.getuser(), socket.getfqdn()))
        self.parser.add_argument("--instance", default="cern",
                                 help="Service now instance")

        self.parser.add_argument("--timeout", default="300",
                                 help="Waiting time interval")

        args = self.parse_args(args)

        self.clean_old_rally_jobs(
            args,
            ServiceNowClient(instance=args.instance)
        )


# Needs static method for setup.cfg
def main(args=None):
    RallyCleanupCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
