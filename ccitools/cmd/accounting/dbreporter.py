#!/usr/bin/python3

import ccitools.conf
import datetime
import logging
import requests
import sys


from ccitools.cmd.accounting.handler import QueryHandler
from ccitools.cmd import base
from ccitools import common

from influxdb import InfluxDBClient

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class AccountingDBReporter(base.BaseCMD):
    def __init__(self):
        super(AccountingDBReporter, self).__init__(
            description=(
                "Uploads Accounting information into Accounting APIs"))

    def get_stats(self, influx_client, metric, influx_date,
                  select_clause, from_clause, group_clause):
        return QueryHandler(
            influx_client,
            [CONF.dbreporter.query.format(
                select_clause,
                from_clause,
                ', '.join(group_clause),
                influx_date
            )],
            group_clause,
            {from_clause: metric}
        ).run()

    def translate_chargegroups(self, stats, chargegroups):
        # Translate chargegroup uuid's to names
        for entry in stats:
            if entry['keys']['chargegroup'] in chargegroups:
                entry['keys']['chargegroup'] = (
                    chargegroups[entry['keys']['chargegroup']])
        return stats

    def main(self, args):
        self.parser.add_argument(
            '--date',
            dest='str_date',
            help='Date that will be used by the '
                 'DBReporter extractor (DD-MM-YYYY format).',
            default=datetime.date.today().strftime('%d-%m-%Y')
        )

        self.parser.add_argument(
            '--dryrun',
            dest='dryrun',
            help='Do not send the calculated datapoints',
            action='store_true',
            default=False
        )

        self.parser.add_argument(
            '--override',
            dest='override',
            help='Override previous datapoints',
            action='store_true',
            default=False
        )

        self.parser.add_argument(
            '--override-reason',
            default='cloud override',
            help=('Reason for the override of previous datapoints')
        )

        args = self.parse_args(args)

        logger.info("Connecting to influxdb")
        influx_client = InfluxDBClient(
            CONF.influxdb.host,
            CONF.influxdb.port,
            CONF.influxdb.user,
            CONF.influxdb.password,
            CONF.influxdb.database,
            ssl=True
        )

        date = datetime.datetime.strptime(args.str_date, '%d-%m-%Y')
        influx_date = date.strftime('\'%Y-%m-%d\'')

        logger.info("Retrieving chargegroups")
        chargegroups = {
            ch['uuid']: ch['name'] for ch in common.get_chargegroups(
                inactive=True
            )}

        logger.info("Retrieving stats")
        for svc in CONF.dbreporter.services:
            logger.info("Processing service %s", svc)
            data = []

            for metric in CONF.dbreporter.services[svc]['metrics']:
                logger.info("Processing service %s metric %s",
                            svc, metric)
                metric_cfg = CONF.dbreporter.services[svc]['metrics'][metric]

                stats = self.get_stats(
                    influx_client,
                    metric,
                    influx_date,
                    metric_cfg['select'],
                    metric_cfg['table'],
                    metric_cfg['group'])

                logger.debug(
                    'Translate chargegroup uuids to names for service %s '
                    'metric %s', svc, metric)
                stats = self.translate_chargegroups(stats, chargegroups)

                logger.debug('Generate accounting entries')
                # Generate accounting entries
                metric_data = {
                    "MessageFormatVersion": 3,
                    "FromChargeGroup": CONF.dbreporter.services[svc]['from'],
                    "TimeStamp": date.strftime("%Y-%m-%d"),
                    "TimePeriod": "day",
                    "TimeAggregate": metric_cfg['aggr'],
                    "MetricName": metric_cfg['name'],
                    "AccountingDoc": CONF.dbreporter.services[svc]['doc'],
                    "data": []
                }

                for stat in stats:
                    # Do not send unkwown chargegroups
                    if stat['keys']['chargegroup'] == 'unknown':
                        continue
                    entry = {
                        "ToChargeGroup": stat['keys']['chargegroup'],
                        "ToChargeRole": stat['keys']['chargerole'],
                        "MetricValue": (
                            stat[metric]['usage']
                            * metric_cfg['factor']
                        )
                    }
                    if (metric_cfg['qos']
                            and stat['keys'][metric_cfg['qos']]):
                        entry["QualityOfService"] = stat['keys'][
                            metric_cfg['qos']]
                    metric_data['data'].append(entry)

                if not metric_data['data']:
                    logger.info('No data to send on service %s metric %s, '
                                'skipping...', svc, metric)
                elif 'dryrun' in metric_cfg and metric_cfg['dryrun']:
                    logger.info('Skip sending data for service %s metric %s',
                                svc, metric)
                else:
                    data.append(metric_data)

            if not data:
                logger.info('No data to send for service %s skipping...', svc)
                continue

            logger.debug('Send data for service %s', svc)
            headers = {
                "Content-Type": "application/json",
                "API-Key": CONF.dbreporter.apikeys[svc]
            }

            if len(data) > 1:
                headers["Multi-metric"] = 'Yes'
            else:
                data = data[0]

            if args.override:
                headers["Override"] = 'Yes'
                headers["Override-reason"] = (
                    args.override_reason)

            if args.dryrun or ('dryrun' in CONF.dbreporter.services[svc]
                               and CONF.dbreporter.services[svc]['dryrun']):
                logger.info('Dryrun on service %s, skipping...', svc)
                continue

            try:
                response = requests.post(
                    CONF.dbreporter.accounting_url,
                    json=data,
                    headers=headers,
                    verify=CONF.dbreporter.verify,
                    timeout=60)
                response.raise_for_status()
            except requests.exceptions.HTTPError:
                logger.error('Error while storing data')

            if response.status_code not in [200]:
                logger.error(
                    'Status code: %s. Message: %s',
                    response.status_code,
                    response.text)

        logger.info("Execution finished")


# Needs static method for setup.cfg
def main(args=None):
    AccountingDBReporter().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)
