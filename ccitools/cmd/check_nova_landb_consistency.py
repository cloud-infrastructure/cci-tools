#!/usr/bin/python3

import ccitools.conf
import logging
import suds
import sys


from ccitools.cmd.base.cloud import BaseCloudCMD
from landbclient.client import LanDB

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class LandbError(Exception):
    """Generic landb client error."""

    message = ""
    vm = None

    def __init__(self, vm=None, message=None, **kwargs):
        super(LandbError, self).__init__(message)
        self.vm = vm
        self.kwargs = kwargs
        self.kwargs['vm'] = vm
        if message:
            self.message = message
        self.message = self.message % kwargs

    def __str__(self):
        """Return string representation of error."""
        return self.message

    def __unicode__(self):
        """Return unicode string representation of error."""
        return self.message


class MissingLandbError(LandbError):
    """Entry missing on landb."""

    message = "%(vm)s existing in nova and missing in landb"


class MissingNovaError(LandbError):
    """Entry missing in nova."""

    message = "%(vm)s existing in landb and missing in nova :" \
              " last modified %(timestamp)s"


class WrongInfoError(LandbError):
    """Inconsistent or wrong info between landb and nova."""

    message = "%(vm)s info mismatch : nova is %(nova)s : landb is %(landb)s"


class MissingInterfaceError(LandbError):
    """Missing interface in nova instance."""

    message = "missing interface in %(vm)s"


class CheckNovaLandbConsistency(BaseCloudCMD):

    def __init__(self):
        super(CheckNovaLandbConsistency, self).__init__(
            description="Check consistency between nova and landb")
        self.prefix_routers = ['k513', 'L9918']

    def check(self, clusters=[], cells=None):
        """Check consistency check between nova and landb."""
        vms_landb, vms_nova, hypervisors = {}, {}, []

        if cells is not None:
            clusters = self._vms_landb_info(cells, clusters)

        for cluster in clusters:
            hypervisors.extend(self.landb.ipservice_devices(cluster))
            devices, err = [], True
            while err:
                try:
                    devices = self.landb.cluster_devices(
                        "VMPOOL %s" % cluster, detail=True)
                except suds.WebFault as exc:
                    if 'not found in database' in str(exc):
                        continue
                err = False
            vms_landb.update(devices)

        hypervisors = ["{0}.cern.ch".format(h) for h in hypervisors]

        for entry in self.cloudclient.get_servers_by_hypervisors(
                hypervisors, detail=True):
            vms_nova[entry.name.lower()] = entry

        return self._check_vms(vms_landb, vms_nova)

    def _vms_landb_info(self, cells, clusters):
        clusters_uuid, cell_mapping, subnet_region = {}, {}, {}

        for item in self.cloudclient.get_neutron_subnets():
            clusters_uuid.update(
                {item['name']: item['id']})
            subnet_region.update(
                {item['id']: item['region_name']})

        for key, value in clusters_uuid.items():
            if self.cloudclient.show_neutron_subnet(
                    str(value), subnet_region[str(value)]):
                subnet = self.cloudclient.show_neutron_subnet(
                    str(value), subnet_region[str(value)])
                if subnet['tags']:
                    cell_mapping.update({key: subnet['tags']})

        for cell in cells:
            for key, value in cell_mapping.items():
                if cell in str(value):
                    clusters.extend([key])
                    break
            else:
                raise ValueError("%s cell not in given cell_mapping" % cell)

        clusters = [w.replace('VM', 'IP') for w in clusters]
        clusters = [w.replace('-V6', '') for w in clusters]

        return list(set(clusters))

    def _check_vms(self, vms_landb, vms_nova):
        """Check and compare the given landb and nova vm lists."""
        result = []

        vms_landb_set = set([v.lower() for v in vms_landb.keys()])
        vms_nova_set = set([v.lower() for v in vms_nova.keys()])

        for vm in vms_landb_set - vms_nova_set:
            if vm.startswith(tuple(self.prefix_routers)):
                continue
            timestamp = vms_landb[vm].LastChangeDate.TimeUTC.ctime()
            result.append(MissingNovaError(vm=vm, timestamp=timestamp))
            del vms_landb[vm]
        for vm in vms_nova_set - vms_landb_set:
            result.append(MissingLandbError(vm=vm))
            del vms_nova[vm]

        for vm in vms_nova.keys():
            try:
                landb_if = vms_landb[vm].Interfaces[0]
                nova_if = vms_nova[vm].addresses['CERN_NETWORK']
                if nova_if:
                    if landb_if.IPAddress != nova_if[0]['addr']:
                        result.append(WrongInfoError(
                            vm=vm, landb=landb_if, nova=nova_if))
                    if (len(nova_if) > 1
                            and landb_if.IPv6Address != nova_if[1]['addr']):
                        result.append(WrongInfoError(
                            vm=vm, landb=landb_if, nova=nova_if))
                else:
                    result.append(MissingInterfaceError(vm=vm))
            except Exception as exc:
                result.append(
                    LandbError(
                        vm=vm, message="failed to check %(vm)s : %(e)s", e=exc)
                )

        return result

    def main(self, args):

        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument(
            '--cluster', default=[], type=str, nargs='+',
            help='cluster to be checked for consistency')
        behaviour.add_argument(
            '--cell', default=None, type=str, nargs='+',
            help='nova cell to be checked for consistency')

        args = self.parse_args(args)

        self.landb = LanDB(
            username=CONF.landb.user,
            password=CONF.landb.password,
            host=CONF.landb.host,
            port=CONF.landb.port,
            protocol=CONF.landb.protocol,
            version=CONF.landb.version)

        result = self.check(
            args.cluster, args.cell)
        return (('Device', 'Failure', 'Message'),
                ((n.vm, type(n).__name__, n.message) for n in result))


# Needs static method for setup.cfg
def main(args=None):
    CheckNovaLandbConsistency().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)
