#!/usr/bin/python3

import ccitools.conf
import logging
import stevedore
import sys

from ccitools.cmd.base.cloud import BaseCloudCMD
from cinderclient import exceptions as cinder_exceptions
from cinderclient.v3 import client as cinder_client
from collections import namedtuple
from glanceclient.common import exceptions as glance_exceptions
from glanceclient.v2 import client as glance_client
from itertools import compress
from novaclient import client as nova_client
from osc_lib import utils


# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF

# formatters
_formatter_namespace = 'cliff.formatter.list'
_formatter_default = 'table'


def _load_formatter_plugins():
    # Here so tests can override
    return stevedore.ExtensionManager(
        _formatter_namespace,
        invoke_on_load=True, )


_formatter_plugins = _load_formatter_plugins()


class CloudDRStatus(BaseCloudCMD):
    def __init__(self):
        super(CloudDRStatus, self).__init__(
            description="Command-line interface to generate "
                        "DRreports of the cloud")

    def _generate_columns_and_selector(self, args, column_names):
        if not args.columns:
            columns_to_include = column_names
            selector = None
        else:
            columns_to_include = [c for c in column_names if c in args.columns]
            if not columns_to_include:
                raise ValueError(
                    'No recognized column names in %s' % str(args.columns))
            # Set up argument to compress()
            selector = [(c in columns_to_include) for c in column_names]
        return columns_to_include, selector

    def cloud_dr_status(self, args):
        search_opts = {
            'all_tenants': True,
            'name': args.regex
        }

        project_cache = dict(
            [(p.id, p) for p in self.cloudclient.list_projects()])

        nc = nova_client.Client(
            version='2.1',
            session=self.cloudclient.session)

        gc = glance_client.Client(
            session=self.cloudclient.session)

        image_cache = dict()

        nc = nova_client.Client(
            version='2.1',
            session=self.cloudclient.session)
        data = []

        cc = cinder_client.Client(
            session=self.cloudclient.session)

        for vm in nc.servers.list(search_opts=search_opts):
            image_name = 'NONE'
            if not vm.image:
                image_name = 'BOOT_FROM_VOLUME'
                volumes = [
                    v['id'] for v in getattr(
                        vm, 'os-extended-volumes:volumes_attached')
                ]
                bfv = None
                for volume_id in volumes:
                    try:
                        volume = cc.volumes.get(volume_id)
                        for at in volume.attachments:
                            if 'device' in at and at['device'] == '/dev/vda':
                                bfv = volume
                                break
                    except cinder_exceptions.NotFound:
                        pass
                if bfv and hasattr(bfv, 'volume_image_metadata') and \
                        'image_name' in bfv.volume_image_metadata:
                    image_name = bfv.volume_image_metadata['image_name']

            elif vm.image['id'] not in image_cache:
                try:
                    new_image = gc.images.get(vm.image['id'])
                    image_name = new_image.name
                except glance_exceptions.HTTPNotFound:
                    image_name = 'NOT FOUND'
                image_cache[vm.image['id']] = image_name
            else:
                image_name = image_cache[vm.image['id']]

            d = {
                'id': vm.id,
                'name': vm.name,
                'project_name': project_cache[vm.tenant_id].name,
                'project_type': project_cache[vm.tenant_id].type,
                'image_name': image_name,
                'bfv': not vm.image,
                'host': getattr(vm, 'OS-EXT-SRV-ATTR:host'),
                'avz_zone': getattr(vm, 'OS-EXT-AZ:availability_zone'),
                'instance_name': getattr(vm, 'OS-EXT-SRV-ATTR:instance_name'),
                'status': vm.status
            }
            data.append(namedtuple("VM", d.keys())(*d.values()))

        formatter = _formatter_plugins[args.formatter].obj
        # Only consider columns that have desired elements
        columns = [
            'id', 'name', 'project_name', 'project_type', 'image_name', 'bfv',
            'host', 'avz_zone', 'instance_name', 'status'
        ]

        data = (utils.get_item_properties(s, columns, formatters={})
                for s in data)
        columns_to_include, selector = self._generate_columns_and_selector(
            args, columns)
        if selector:
            data = (list(compress(row, selector)) for row in data)
        formatter.emit_list(columns_to_include, data, sys.stdout, args)

    def main(self, args=None):
        self.parser.add_argument(
            '--regex', default='^cci-',
            help='Regex to search globally for machines')
        formatter_group = self.parser.add_argument_group(
            title='output formatters',
            description='output formatter options', )
        formatter_choices = sorted(_formatter_plugins.names())
        formatter_default = _formatter_default
        if formatter_default not in formatter_choices:
            formatter_default = formatter_choices[0]
        formatter_group.add_argument(
            '-f',
            '--format',
            dest='formatter',
            action='store',
            choices=formatter_choices,
            default=formatter_default,
            help='the output format, defaults to %s' % formatter_default, )
        formatter_group.add_argument(
            '-c',
            '--column',
            action='append',
            default=[],
            dest='columns',
            metavar='COLUMN',
            help='specify the column(s) to include, can be repeated', )
        for formatter in _formatter_plugins:
            formatter.obj.add_argument_group(self.parser)

        args = self.parse_args(args)

        self.cloud_dr_status(args)


# Needs static method for setup.cfg
def main(args=None):
    CloudDRStatus().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
