from oslo_config import cfg


connection = cfg.StrOpt(
    'connection',
    default='mysql+pymysql://user:pass@127.0.0.1:3306/keystone',
    help="keystone connection url")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    connection
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
