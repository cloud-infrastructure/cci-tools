import logging

from ccitools.cmd.trust_manage import main
from ccitools.cmd.trust_manage import Trust
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase


class TestTrustManage(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):
        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)

    @mock.patch('sqlalchemy.create_engine')
    @mock.patch('sqlalchemy.orm.sessionmaker')
    def test_list_trusts_error(self, mock_sess_maker, mock_engine):
        mock_bind = mock_engine.return_value
        mock_session = mock_sess_maker.return_value.return_value
        mock_session.query.return_value.filter_by.side_effect = (
            iter([Exception('fake error')]))

        main(['list'])

        mock_engine.assert_called_with(
            'mysql+pymysql://user:pass@127.0.0.1:3306/keystone'
        )
        mock_sess_maker.assert_called_with(
            bind=mock_bind
        )
        mock_session.query.assert_called_with(Trust)
        mock_session.query.return_value.filter_by.assert_called_with(
            deleted_at=None
        )

    @mock.patch('sqlalchemy.create_engine')
    @mock.patch('sqlalchemy.orm.sessionmaker')
    def test_list_empty_trusts(self, mock_sess_maker, mock_engine):
        mock_bind = mock_engine.return_value
        mock_session = mock_sess_maker.return_value.return_value
        mock_session.query.return_value.filter_by.return_value = []

        main(['list'])

        mock_engine.assert_called_with(
            'mysql+pymysql://user:pass@127.0.0.1:3306/keystone'
        )
        mock_sess_maker.assert_called_with(
            bind=mock_bind
        )
        mock_session.query.assert_called_with(Trust)
        mock_session.query.return_value.filter_by.assert_called_with(
            deleted_at=None
        )

    @mock.patch('sqlalchemy.create_engine')
    @mock.patch('sqlalchemy.orm.sessionmaker')
    def test_list_trusts(self, mock_sess_maker, mock_engine):
        mock_bind = mock_engine.return_value
        mock_session = mock_sess_maker.return_value.return_value
        mock_session.query.return_value.filter_by.return_value = (
            fixtures.TRUSTS
        )

        main(['list'])

        mock_engine.assert_called_with(
            'mysql+pymysql://user:pass@127.0.0.1:3306/keystone'
        )
        mock_sess_maker.assert_called_with(
            bind=mock_bind
        )
        mock_session.query.assert_called_with(Trust)
        mock_session.query.return_value.filter_by.assert_called_with(
            deleted_at=None
        )

    @mock.patch('sqlalchemy.create_engine')
    @mock.patch('sqlalchemy.orm.sessionmaker')
    def test_list_trusts_columns(self, mock_sess_maker, mock_engine):
        mock_bind = mock_engine.return_value
        mock_session = mock_sess_maker.return_value.return_value
        mock_session.query.return_value.filter_by.return_value = (
            fixtures.TRUSTS
        )

        main(['-c', 'id', 'list'])

        mock_engine.assert_called_with(
            'mysql+pymysql://user:pass@127.0.0.1:3306/keystone'
        )
        mock_sess_maker.assert_called_with(
            bind=mock_bind
        )
        mock_session.query.assert_called_with(Trust)
        mock_session.query.return_value.filter_by.assert_called_with(
            deleted_at=None
        )

    @mock.patch('sqlalchemy.create_engine')
    @mock.patch('sqlalchemy.orm.sessionmaker')
    def test_list_trusts_columns_error(self, mock_sess_maker, mock_engine):
        mock_bind = mock_engine.return_value
        mock_session = mock_sess_maker.return_value.return_value
        mock_session.query.return_value.filter_by.return_value = (
            fixtures.TRUSTS
        )

        main(['-c', 'fake', 'list'])

        mock_engine.assert_called_with(
            'mysql+pymysql://user:pass@127.0.0.1:3306/keystone'
        )
        mock_sess_maker.assert_called_with(
            bind=mock_bind
        )
        mock_session.query.assert_called_with(Trust)
        mock_session.query.return_value.filter_by.assert_called_with(
            deleted_at=None
        )

    @mock.patch('sqlalchemy.create_engine')
    @mock.patch('sqlalchemy.orm.sessionmaker')
    def test_list_trusts_all_params(self, mock_sess_maker, mock_engine):
        mock_bind = mock_engine.return_value
        mock_session = mock_sess_maker.return_value.return_value
        mock_query1 = mock_session.query.return_value.filter_by.return_value
        mock_query2 = mock_query1.filter_by.return_value
        mock_query3 = mock_query2.filter_by.return_value
        mock_query4 = mock_query3.filter_by.return_value
        mock_query4.filter_by.return_value = fixtures.TRUSTS

        main(['--trust-id', 'fake_id',
              '--trustor', 'trustor_id',
              '--trustee', 'trustee_id',
              '--project', 'fake_project',
              'list'])

        mock_engine.assert_called_with(
            'mysql+pymysql://user:pass@127.0.0.1:3306/keystone'
        )
        mock_sess_maker.assert_called_with(
            bind=mock_bind
        )
        mock_session.query.assert_called_with(Trust)
        mock_session.query.return_value.filter_by.assert_called_with(
            deleted_at=None
        )

    def test_update_trusts_missing_trustor(self):
        with self.assertRaises(SystemExit) as cm:
            main(['update'])
        self.assertEqual(cm.exception.code, 2)

    @mock.patch('sqlalchemy.create_engine')
    @mock.patch('sqlalchemy.orm.sessionmaker')
    def test_update_trusts_dry_run(self, mock_sess_maker, mock_engine):
        mock_bind = mock_engine.return_value
        mock_session = mock_sess_maker.return_value.return_value
        mock_mquery = mock_session.query.return_value.filter_by.return_value
        mock_query = mock_mquery.filter_by.return_value

        main(['--trust-id', 'abc', 'update', '--dry-run',
              '--new-trustor', 'new_trustor'])

        mock_engine.assert_called_with(
            'mysql+pymysql://user:pass@127.0.0.1:3306/keystone'
        )
        mock_sess_maker.assert_called_with(
            bind=mock_bind
        )
        mock_session.query.assert_called_with(Trust)
        mock_session.query.return_value.filter_by.assert_called_with(
            deleted_at=None
        )
        mock_mquery.filter_by.assert_called_with(
            id='abc'
        )
        mock_query.update.assert_not_called()
        mock_session.commit.assert_not_called()

    @mock.patch('sqlalchemy.create_engine')
    @mock.patch('sqlalchemy.orm.sessionmaker')
    def test_update_trusts_empty(self, mock_sess_maker, mock_engine):
        mock_bind = mock_engine.return_value
        mock_session = mock_sess_maker.return_value.return_value
        mock_session.query.return_value.filter_by.return_value = []

        main(['--trust-id', 'abc', 'update', '--new-trustor', 'new_trustor'])

        mock_engine.assert_called_with(
            'mysql+pymysql://user:pass@127.0.0.1:3306/keystone'
        )
        mock_sess_maker.assert_called_with(
            bind=mock_bind
        )
        mock_session.query.assert_called_with(Trust)
        mock_session.query.return_value.filter_by.assert_called_with(
            deleted_at=None
        )
        mock_session.commit.assert_not_called()

    @mock.patch('sqlalchemy.create_engine')
    @mock.patch('sqlalchemy.orm.sessionmaker')
    def test_update_trusts_error(self, mock_sess_maker, mock_engine):
        mock_bind = mock_engine.return_value
        mock_session = mock_sess_maker.return_value.return_value
        mock_mquery = mock_session.query.return_value.filter_by.return_value
        mock_query = mock_mquery.filter_by.return_value
        mock_query.update.side_effect = iter([Exception('fake error')])

        main(['--trust-id', 'abc', 'update', '--new-trustor', 'new_trustor'])

        mock_engine.assert_called_with(
            'mysql+pymysql://user:pass@127.0.0.1:3306/keystone'
        )
        mock_sess_maker.assert_called_with(
            bind=mock_bind
        )
        mock_session.query.assert_called_with(Trust)
        mock_session.query.return_value.filter_by.assert_called_with(
            deleted_at=None
        )
        mock_mquery.filter_by.assert_called_with(
            id='abc'
        )
        mock_query.update.assert_called_with(
            {'trustor_user_id': 'new_trustor'})
        mock_session.commit.assert_not_called()

    @mock.patch('sqlalchemy.create_engine')
    @mock.patch('sqlalchemy.orm.sessionmaker')
    def test_update_trusts(self, mock_sess_maker, mock_engine):
        mock_bind = mock_engine.return_value
        mock_session = mock_sess_maker.return_value.return_value
        mock_mquery = mock_session.query.return_value.filter_by.return_value
        mock_query = mock_mquery.filter_by.return_value

        main(['--trust-id', 'abc', 'update', '--new-trustor', 'new_trustor'])

        mock_engine.assert_called_with(
            'mysql+pymysql://user:pass@127.0.0.1:3306/keystone'
        )
        mock_sess_maker.assert_called_with(
            bind=mock_bind
        )
        mock_session.query.assert_called_with(Trust)
        mock_session.query.return_value.filter_by.assert_called_with(
            deleted_at=None
        )
        mock_mquery.filter_by.assert_called_with(
            id='abc'
        )
        mock_query.update.assert_called_with(
            {'trustor_user_id': 'new_trustor'})
        mock_session.commit.assert_called_with()

    @mock.patch('sqlalchemy.create_engine')
    @mock.patch('sqlalchemy.orm.sessionmaker')
    def test_update_trusts_unfiltered(self, mock_sess_maker, mock_engine):
        mock_session = mock_sess_maker.return_value.return_value
        mock_query = mock_session.query.return_value.filter_by.return_value

        main(['update', '--new-trustor', 'new_trustor'])

        mock_engine.assert_not_called()
        mock_sess_maker.assert_not_called()
        mock_session.query.assert_not_called()
        mock_query.update.assert_not_called()
        mock_session.commit.assert_not_called()
