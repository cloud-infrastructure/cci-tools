import logging

from ccitools.utils.cornerstone import CornerstoneClient
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.utils.cornerstone.ClientRestV2')
class TestCornerstone(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_create_project(self, mock_client):
        mock_cli = mock_client.return_value

        CornerstoneClient().create_project(
            id='fake_id',
            name='fake_name',
            description='fake_description',
            status='fake_status',
            type='fake_type',
            owner='fake_owner',
            administrators='fake_administrators'
        )

        mock_client.assert_called_with(
            auth='oidc',
            params={
                'id': 'client_id',
                'secret': 'client_secret',
                'audience': 'authorization-service-api',
                'token_url': ('https://auth.cern.ch/auth/realms/cern'
                              '/api-access/token')
            },
            url='https://cornerstone.cern.ch/api/wsdl'
        )

        mock_cli.project_create.assert_called_with(
            id='fake_id',
            name='fake_name',
            description='fake_description',
            status='fake_status',
            type='fake_type',
            owner='fake_owner',
            administrator='fake_administrators'
        )

    def test_update_project(self, mock_client):
        mock_cli = mock_client.return_value

        CornerstoneClient().update_project(
            id='fake_id',
            name='new_fake_name',
            description='new_fake_description',
            status='new_fake_status',
            type='new_fake_type',
            owner='new_fake_owner',
            administrators='new_fake_administrators'
        )

        mock_client.assert_called_with(
            auth='oidc',
            params={
                'id': 'client_id',
                'secret': 'client_secret',
                'audience': 'authorization-service-api',
                'token_url': ('https://auth.cern.ch/auth/realms/cern'
                              '/api-access/token')
            },
            url='https://cornerstone.cern.ch/api/wsdl'
        )

        mock_cli.project_update.assert_called_with(
            id='fake_id',
            name='new_fake_name',
            description='new_fake_description',
            status='new_fake_status',
            type='new_fake_type',
            owner='new_fake_owner',
            administrator='new_fake_administrators'
        )

    def test_delete_project(self, mock_client):
        mock_cli = mock_client.return_value

        CornerstoneClient().delete_project(
            id='fake_id',
        )

        mock_client.assert_called_with(
            auth='oidc',
            params={
                'id': 'client_id',
                'secret': 'client_secret',
                'audience': 'authorization-service-api',
                'token_url': ('https://auth.cern.ch/auth/realms/cern'
                              '/api-access/token')
            },
            url='https://cornerstone.cern.ch/api/wsdl'
        )

        mock_cli.project_delete.assert_called_with(
            id='fake_id',
        )
