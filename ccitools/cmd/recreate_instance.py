#!/usr/bin/python3

import argparse
import ccitools.conf
import distutils.util
import functools
import logging
import novaclient.exceptions
import os
import re
import requests
import socket
import sys
import tempfile
import tenacity
import urllib.request
import uuid

from ccitools.cmd.base import BaseCMD
from ccitools.cmd.common import Phase
from ccitools.cmd.common import Phases
from ccitools import common
from ccitools.utils.cloud import CloudRegionClient
from ccitools.utils.db import NovaDB
from landbclient.client import LanDB
from tooz import coordination

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF

INSTANCE_SCHEDULE_TIMEOUT = 600
INSTANCE_SCHEDULE_RETRY_INTERVAL = 5

INSTANCE_OPERATION_TIMEOUT = 7200
INSTANCE_OPERATION_INTERVAL = 120

SNAPSHOT_TIMEOUT = 172800
SNAPSHOT_RETRY_INTERVAL = 60

VOLUME_CREATION_TIMEOUT = 86400
VOLUME_CREATION_RETRY_INTERVAL = 300

SHUTDOWN_TIMEOUT = 600
SHUTDOWN_RETRY_INTERVAL = 20

MAX_NOTIFICATION_TIMEOUT = 300
NOTIFICATION_RETRY_INTERVAL = 30

APPLIANCE_URL = ('http://download.libguestfs.org/binaries/appliance/'
                 'appliance-1.40.1.tar.xz')


class RecreateInstance(BaseCMD):

    def __init__(self):
        super(RecreateInstance, self).__init__(
            description=("Delete and recreate the VM"))

        self.parser.add_argument(
            '--os-cloud',
            metavar='<cloud-config-name>',
            dest='cloud',
            default=common.env('OS_CLOUD', default='cern'),
            help='Cloud name in clouds.yaml (Env: OS_CLOUD)', )
        self.parser.add_argument(
            "--vm",
            required=True,
            help="VM that will be recreated")
        self.parser.add_argument(
            '--recreate-from',
            default='-',
            choices=['-', 'none', 'volume'])
        self.parser.add_argument(
            '--suffix',
            default=common.env('RD_JOB_EXECID', default=''),
            help=('Suffix used to create the resources.'
                  'If not set, defaults to $RD_JOB_EXECID')
        )
        self.parser.add_argument(
            '--new-project-id',
            nargs='?',
            help=('Specify a project if necessary, '
                  'otherwise it will be ignored')
        )
        self.parser.add_argument(
            '--flavor_map',
            default=r'm1\.large:m2.xlarge,m1\.tiny:m2.small,m1\.*:m2.',
            type=common.flavor_map,
        )
        self.parser.add_argument(
            '--new_name',
            default='',
            help=('New name in case we want to rename the instance')
        )
        self.parser.add_argument(
            '--volume-type',
            default='standard',
            choices=['standard', 'io1', 'io2', 'io3', 'cp1', 'cpio1'],
            help=('Type of volume used for recreation')
        )
        self.parser.add_argument(
            "--skip-volume-snapshot",
            action='store_true',
            default=False,
            help=('Skip creation of volume snapshot')
        )
        self.parser.add_argument(
            "--exclusive",
            action='store_true',
            default=False,
            help=('Prevents multiple executions of the recreate step')
        )
        self.parser.add_argument(
            '--sysprep',
            type=lambda x: bool(distutils.util.strtobool(x)),
            default=False
        )
        self.parser.add_argument(
            '--sysprep-snapshot',
            type=lambda x: bool(distutils.util.strtobool(x)),
            default=False
        )
        self.parser.add_argument(
            '--sysprep-params',
            default=("--operations net-hwaddr,udev-persistent-net,customize "
                     "--delete '/etc/krb5.keytab' "
                     "--delete '/var/lib/cern-private-cloud-addons/state'"),
            help=('Parameters to be passed to the virt-sysprep command')
        )
        self.parser.add_argument(
            "--start-from",
            default=1,
            help=('Start from step indicated in case of a previous execution')
        )
        self.parser.add_argument(
            '--image-tag',
            dest='image_tags',
            metavar='<tag>',
            action='append',
            default=['recreate_procedure'],
            help=('Optionally add a tag to intermediate images')
        )

        self.parser.add_argument(
            "--landb-skip",
            action='store_true',
            default=False,
            help=('Skip deallocation of Landb Information')
        )

        self.parser.add_argument(
            "--create-port",
            action='store_true',
            default=False,
            help=('Create a port with the same IP')
        )

        self.parser.add_argument(
            "--nowait",
            action='store_true',
            default=False,
            help=('Do not wait for DNS to launch the VM creation')
        )

        self.parser.add_argument(
            "--project-map-cell",
            default='',
            help=('Map project to cell for creation')
        )

        self.parser.add_argument(
            "--preserve",
            action='store_true',
            default=False,
            help=('Keep the original state of the machine')
        )

        self.parser.add_argument(
            "--protect",
            action='store_true',
            default=False,
            help=('Prevent operations when recreating the VM')
        )

        self.parser.add_argument(
            "--notify-user",
            action='store_true',
            default=False,
            help=('Notify users when the recreation starts and'
                  'also when it finishes')
        )

        self.parser.add_argument(
            "--notif-start-subject",
            default="[Cloud Infrastructure] Your Virtual Machine $vm "
                    "is going to be cold migrated",
            help="email subject for start of notification"
        )
        self.parser.add_argument(
            "--notif-start-template",
            default="https://gitlab.cern.ch/cloud-infrastructure/"
                    "cci-notification-templates/raw/master/"
                    "notify_start_recreate_instance",
            help="Url to the template to be used as email body"
        )
        self.parser.add_argument(
            "--notif-end-subject",
            default="[Cloud Infrastructure] Your Virtual Machine $vm "
                    "has been cold migrated",
            help="email subject for end of notification"
        )
        self.parser.add_argument(
            "--notif-end-template",
            default="https://gitlab.cern.ch/cloud-infrastructure/"
                    "cci-notification-templates/raw/master/"
                    "notify_end_recreate_instance",
            help="Url to the template to be used as email body"
        )
        self.parser.add_argument(
            "--mail-from",
            nargs='?',
            default="noreply@cern.ch"
        )
        self.parser.add_argument(
            "--mail-cc",
            nargs='?',
            default=""
        )
        self.parser.add_argument(
            "--mail-bcc",
            nargs='?',
            default=""
        )
        self.parser.add_argument(
            "--mail-content-type",
            default="plain",
            help="Mail content type (plain/html)"
        )

    def parse_args(self, args):
        args = super(RecreateInstance, self).parse_args(args)
        self.cloud = args.cloud
        return args

    def get_cloudclient(self, project_name=None, project_id=None):
        namespace = None
        if project_id is not None or project_name is not None:
            logger.info("Getting new CloudClient for project: '%s'",
                        project_id if project_id is not None else project_name)
            namespace = argparse.Namespace(
                project_id=project_id,
                project_name=project_name)
        return CloudRegionClient(cloud=self.cloud, namespace=namespace)

    @tenacity.retry(
        stop=tenacity.stop_after_delay(INSTANCE_OPERATION_TIMEOUT),
        wait=tenacity.wait_fixed(INSTANCE_OPERATION_INTERVAL))
    def wait_until_dns_entry_free(self, vm_name):
        try:
            socket.gethostbyname(vm_name)
            logger.info(
                "Server '%s' still present on DNS. "
                "Waiting %s seconds before checking again.",
                vm_name,
                INSTANCE_OPERATION_INTERVAL
            )
            raise tenacity.TryAgain
        except socket.gaierror:
            logger.info("VM not present on DNS anymore. Moving on !!")

    @tenacity.retry(stop=tenacity.stop_after_delay(INSTANCE_OPERATION_TIMEOUT),
                    wait=tenacity.wait_fixed(INSTANCE_OPERATION_INTERVAL))
    def wait_until_vm_state(self, vm, state):
        vm_data = vm.manager.get(vm.id)
        if not vm_data.status == state:
            logger.info(
                "Server '%s' still in '%s' status",
                vm_data.name,
                vm_data.status
            )
            raise tenacity.TryAgain

    @tenacity.retry(stop=tenacity.stop_after_delay(INSTANCE_OPERATION_TIMEOUT),
                    wait=tenacity.wait_fixed(INSTANCE_OPERATION_INTERVAL))
    def wait_until_vm_is_gone(self, vm):
        try:
            vm_data = vm.manager.get(vm.id)
            logger.info(
                "Server '%s' still present with status '%s' "
                "Waiting %s seconds before checking again.",
                vm_data.name,
                vm_data.status,
                INSTANCE_OPERATION_INTERVAL
            )
            raise tenacity.TryAgain
        except novaclient.exceptions.NotFound:
            logger.info("VM has been deleted. Moving on !!")

    @tenacity.retry(
        stop=tenacity.stop_after_delay(INSTANCE_SCHEDULE_TIMEOUT),
        wait=tenacity.wait_fixed(INSTANCE_SCHEDULE_RETRY_INTERVAL))
    def wait_while_scheduling(self, vm):
        vm_data = vm.manager.get(vm.id)
        if getattr(vm_data, 'OS-EXT-STS:task_state') == 'scheduling':
            logger.info(
                "Server '%s' still in task_state '%s'",
                vm_data.name,
                getattr(vm_data, 'OS-EXT-STS:task_state')
            )
            raise tenacity.TryAgain

    @tenacity.retry(
        stop=tenacity.stop_after_delay(VOLUME_CREATION_TIMEOUT),
        wait=tenacity.wait_fixed(VOLUME_CREATION_RETRY_INTERVAL))
    def wait_until_volume_available(self, volume):
        volume_data = volume.manager.get(volume.id)
        if not volume_data.status == 'available':
            logger.info(
                "Volume '%s' still in '%s' status",
                volume_data.name,
                volume_data.status
            )
            raise tenacity.TryAgain

    @tenacity.retry(stop=tenacity.stop_after_delay(SNAPSHOT_TIMEOUT),
                    wait=tenacity.wait_fixed(SNAPSHOT_RETRY_INTERVAL))
    def wait_until_volume_snapshot_available(self, snapshot, cloud):
        snapshot_data = snapshot.manager.get(snapshot.id)
        if not snapshot_data.status == 'available':
            logger.info(
                "Volume Snapshot '%s' still in '%s' status",
                snapshot_data.name,
                snapshot_data.status
            )
            raise tenacity.TryAgain

    @tenacity.retry(stop=tenacity.stop_after_delay(SNAPSHOT_TIMEOUT),
                    wait=tenacity.wait_fixed(SNAPSHOT_RETRY_INTERVAL))
    def wait_until_image_active(self, image_id, cloud):
        image = cloud.get_image(image_id)
        if not image.status == 'active':
            logger.info(
                "Snapshot '%s' still in '%s' status",
                image.name,
                image.status
            )
            raise tenacity.TryAgain

    def verify_landb_metadata(self, vm):
        try:
            landb_client = LanDB(
                username=CONF.landb.user,
                password=CONF.landb.password,
                host=CONF.landb.host,
                port=CONF.landb.port,
                protocol=CONF.landb.protocol,
                version=CONF.landb.version)
            info = landb_client.device_info(vm.name)
            if '@cern.ch' not in info.ResponsiblePerson.Email:
                raise Exception(
                    "landb-responsible is not a CERN mail address"
                )
            if '@cern.ch' not in info.UserPerson.Email:
                raise Exception(
                    "landb-mainuser is not a CERN mail address"
                )
        except Exception as e:
            logger.error("Error verifying landb metadata: %s" % e)

    def get_compatible_flavor(self, name, flavor_map):
        result = name
        for k, v in flavor_map:
            if re.match(k, name):
                result = re.sub(k, v, name)
                break
        return result

    def check_compute_quota(self, vm, region, quota, flavor):
        try:
            if flavor.disk >= vm.flavor['disk']:
                total = quota['quota']
                current = quota['current']
                quota_instances = \
                    total[region]['compute']['instances'] - \
                    current[region]['compute']['instances']
                quota_cores = total[region]['compute']['cores'] - \
                    current[region]['compute']['cores']
                quota_ram = total[region]['compute']['ram'] - \
                    current[region]['compute']['ram']
                vm_ram = vm.flavor['ram'] / 1024
                if quota_instances < 1:
                    raise Exception('Not enough instances quota')
                if quota_cores < vm.flavor['vcpus']:
                    raise Exception('Not enough vcpus quota')
                if quota_ram < vm_ram:
                    raise Exception('Not enough ram quota')
            else:
                raise Exception('Not enough disk space')
        except Exception as ex:
            error_msg = "Error compute quota: %s" % (str(ex))
            logger.info(error_msg)
            raise ex

    def check_volume_quota(self, volume_type, region, quota, volume_size):
        try:
            total = quota['quota']
            current = quota['current']
            quota_volumes = \
                total[region]['blockstorage'][volume_type]['volumes'] -\
                current[region]['blockstorage'][volume_type]['volumes']
            quota_volumes_size = \
                total[region]['blockstorage'][volume_type]['gigabytes'] -\
                current[region]['blockstorage'][volume_type]['gigabytes']
            if quota_volumes < 1:
                raise Exception('Not enough volumes quota')
            if quota_volumes_size < volume_size:
                raise Exception('Not enough volumes size')
        except Exception as ex:
            error_msg = "Error volume quota: %s" % (str(ex))
            logger.info(error_msg)
            raise ex

    def main_recreate(self, args):
        self.coordinator = coordination.get_coordinator(
            CONF.coordination.backend_url,
            str(uuid.uuid4()).encode())
        self.coordinator.start()

        # Layout the different commands and phases to run
        cmd = Phases([
            Phase(
                "Check if the operation could be executed",
                func=self.precheck),
            Phase(
                "Adapt quotas if required",
                func=self.fix_quota),
            Phase(
                "Add service account as project member",
                func=self.add_account),
            Phase(
                "Notify start of intervention on machine",
                func=self.notify_start),
            Phase(
                "Shutdown machine",
                func=self.shutdown_machine),
            Phase(
                "Sysprep machine",
                func=self.sysprep_machine),
            Phase(
                "Create snapshot of instance",
                func=self.create_snapshot),
            Phase(
                "Create volume from snapshot",
                func=self.create_volume),
            Phase(
                "Recreate VM",
                func=self.recreate_instance),
            Phase(
                "Notify end of intervention on machine",
                func=self.notify_end)
        ])

        # Execute the phases in order
        cmd.execute(args.start_from, args, state={})

        self.coordinator.stop()

        # Finally log the status of the execution
        logger.info(str(cmd))

    def precheck(self, args, phase, state):
        try:
            logger.info("Checking Information about VM '%s'...", args.vm)
            cloud = self.get_cloudclient()
            vm = cloud.get_servers_by_name(args.vm)
            region = cloud.find_region_vm(vm.id)
            quota = cloud.get_project_quota(
                args.new_project_id if args.new_project_id else vm.tenant_id)

            self.verify_landb_metadata(vm)

            if args.new_project_id:
                new_flavor = self.get_compatible_flavor(
                    vm.flavor['original_name'],
                    args.flavor_map)

                flavors = []
                flavors_public = vm.manager._list(
                    "/flavors/detail?is_public=true",
                    "flavors")
                flavors_non_public = vm.manager._list(
                    "/flavors/detail?is_public=false",
                    "flavors")
                flavors.extend(flavors_public)
                flavors.extend(flavors_non_public)

                flavor = [
                    flavor for flavor in flavors if flavor.name == new_flavor
                ][0]

                self.check_compute_quota(
                    vm,
                    region,
                    quota,
                    flavor)

                if args.recreate_from == 'volume' and vm.image:
                    image = cloud.get_image(vm.image['id'])
                    volume_size = functools.reduce(max, [
                        round(image.size / (1024.0**3)),
                        vm.flavor['disk'],
                        image.min_disk])
                    self.check_volume_quota(
                        args.volume_type,
                        region,
                        quota,
                        volume_size)
            phase.ok('Precheck executed successfully')
            return vm.tenant_id
        except Exception as ex:
            error_msg = "Precheck failed: %s" % (str(ex))
            phase.fail(error_msg)
            raise ex

    def fix_quota(self, args, phase, state):
        cloud = self.get_cloudclient()
        vm = cloud.get_servers_by_name(args.vm)

        if args.recreate_from != 'volume':
            logger.info("There is no need to update the quota. "
                        "No changes needed. Moving on !!")
            phase.ok('Fix quota skipped')
            return

        if not vm.image:
            logger.info("There is no need to update quota as"
                        "we are reusing the volume. Moving on !!")
            phase.ok('Fix quota skipped')
            return

        cinder_quota = cloud.get_cinder_project_quota(
            project_id=vm.tenant_id,
            region='cern',
            usage=True
        )

        volumes_needed = 1
        gigabytes_needed = int(vm.flavor['disk']) + 1

        volumes_key = 'volumes_{0}'.format(args.volume_type)
        volumes_limit = int(getattr(cinder_quota, volumes_key)['limit'])
        volumes_inuse = int(getattr(cinder_quota, volumes_key)['in_use'])

        gigabytes_key = 'gigabytes_{0}'.format(args.volume_type)
        gigabytes_limit = int(getattr(cinder_quota, gigabytes_key)['limit'])
        gigabytes_inuse = int(getattr(cinder_quota, gigabytes_key)['in_use'])

        update_quota = False

        if (volumes_inuse + volumes_needed - volumes_limit > 0):
            volume_update = volumes_limit + volumes_needed
            logger.warning(
                "Not enough quota for creating one extra volume."
                "Should increase volumes from '%s' to '%s'",
                volumes_limit, volume_update)
            update_quota = True
        else:
            volume_update = volumes_limit

        if (gigabytes_inuse + gigabytes_needed - gigabytes_limit > 0):
            gigabytes_update = gigabytes_limit + gigabytes_needed
            logger.warning(
                "Not enough space for creating one extra volume."
                "Should increase gigabytes from '%s' to '%s'",
                gigabytes_limit, gigabytes_update)
            update_quota = True
        else:
            gigabytes_update = gigabytes_limit

        if update_quota:
            project = cloud.find_project(vm.tenant_id)
            logger.info("Increasing quota of '%s'...", project.name)
            cloud.update_quota_cinder(
                vm.tenant_id,
                volume_update,
                gigabytes_update,
                volume_update * 2,
                args.volume_type,
                'cern')
        else:
            logger.info("Project has enough quota\n"
                        "No changes needed. Moving on !!")
        phase.ok('Precheck executed successfully')

    def add_account(self, args, phase, state):
        try:
            cloud = self.get_cloudclient()
            project_id = cloud.get_servers_by_name(args.vm).tenant_id
            logger.info("Adding account to project '%s'...", project_id)
            user_id = cloud.session.auth.get_user_id(cloud.session)
            cloud.grant_role_in_project(
                project_id=project_id,
                user_id=user_id)
            if args.create_port:
                cloud.grant_role_in_project(
                    project_id=project_id,
                    role='network_admin',
                    user_id=user_id)
            if args.new_project_id:
                cloud.grant_role_in_project(
                    project_id=args.new_project_id,
                    user_id=user_id)
                if args.create_port:
                    cloud.grant_role_in_project(
                        project_id=project_id,
                        role='network_admin',
                        user_id=user_id)
            phase.ok('Account added successfully')
        except Exception as ex:
            error_msg = "Add account failed: %s" % (str(ex))
            phase.fail(error_msg)
            raise ex

    def notify_start(self, args, phase, state):
        if args.notify_user:
            try:
                landb_client = LanDB(
                    username=CONF.landb.user,
                    password=CONF.landb.password,
                    host=CONF.landb.host,
                    port=CONF.landb.port,
                    protocol=CONF.landb.protocol,
                    version=CONF.landb.version)

                info = landb_client.device_info(args.vm)
                state['notify'] = list(set([
                    info.ResponsiblePerson.Email,
                    info.UserPerson.Email
                ]))

                body_template = self.readTemplate(args.notif_start_template)
                common.send_notification(
                    mails_to=state['notify'],
                    values={'vm': args.vm},
                    subject_template=args.notif_start_subject,
                    body_template=body_template,
                    mail_from=args.mail_from,
                    mail_cc=args.mail_cc,
                    mail_bcc=args.mail_bcc,
                    content_type=args.mail_content_type
                )
                phase.ok('Notification for start sent successfully')
            except Exception as ex:
                error_msg = "Notify Start failed: %s" % (str(ex))
                phase.fail(error_msg)
                raise ex
        else:
            logger.info("There is no need to send notifications."
                        "Moving on !!")
            phase.ok('Notify start skipped')

    def notify_end(self, args, phase, state):
        if args.notify_user:
            try:
                if 'notify' not in state.keys() or not state['notify']:
                    landb_client = LanDB(
                        username=CONF.landb.user,
                        password=CONF.landb.password,
                        host=CONF.landb.host,
                        port=CONF.landb.port,
                        protocol=CONF.landb.protocol,
                        version=CONF.landb.version)

                    info = landb_client.device_info(args.vm)
                    state['notify'] = list(set([
                        info.ResponsiblePerson.Email,
                        info.UserPerson.Email
                    ]))

                body_template = self.readTemplate(args.notif_end_template)
                common.send_notification(
                    mails_to=state['notify'],
                    values={'vm': args.vm},
                    subject_template=args.notif_end_subject,
                    body_template=body_template,
                    mail_from=args.mail_from,
                    mail_cc=args.mail_cc,
                    mail_bcc=args.mail_bcc,
                    content_type=args.mail_content_type
                )
                phase.ok('Notification for end sent successfully')
            except Exception as ex:
                error_msg = "Notify End failed: %s" % (str(ex))
                phase.fail(error_msg)
                raise ex
        else:
            logger.info("There is no need to send notifications."
                        "Moving on !!")
            phase.ok('Notify end skipped')

    @tenacity.retry(stop=tenacity.stop_after_delay(MAX_NOTIFICATION_TIMEOUT),
                    wait=tenacity.wait_fixed(NOTIFICATION_RETRY_INTERVAL),
                    before_sleep=tenacity.before_sleep_log(
                        logger,
                        logging.WARNING))
    def readTemplate(self, url):
        """Read content of a remote template.

        :param url URL where the template is located
        """
        output = urllib.request.urlopen(url)  # nosec
        if output.getcode() == 200:
            return output.read().decode()
        else:
            raise Exception("Template '%s' not found" % url)

    def shutdown_machine(self, args, phase, state):
        try:
            logger.info("Stopping VM '%s'...", args.vm)
            cloud = self.get_cloudclient()
            vm = cloud.get_servers_by_name(args.vm)

            # Save the previous state of the machine
            # so we can let it
            state['status'] = vm.status
            state['locked'] = vm.locked

            # Unlock the instance if it was previously locked
            if vm.locked:
                vm.unlock()
                vm = vm.manager.get(vm.id)

            # Handle PAUSED and SUSPENDED status
            if vm.status == 'PAUSED':
                vm.unpause()
                self.wait_until_vm_state(vm, 'ACTIVE')
            elif vm.status == 'SUSPENDED':
                vm.resume()
                self.wait_until_vm_state(vm, 'ACTIVE')

            vm.stop()
            self.wait_until_vm_state(vm, 'SHUTOFF')

            if args.protect:
                logger.info('Instance locked to prevent user operations')
                vm.lock()
            phase.ok('VM stopped successfully')
        except novaclient.exceptions.Conflict:
            phase.ok('VM stopped already stopped')
        except Exception as ex:
            error_msg = "VM stop failed: %s" % (str(ex))
            phase.fail(error_msg)
            raise ex

    def sysprep_machine(self, args, phase, state):
        try:
            fd, path = tempfile.mkstemp()
            if not args.sysprep:
                logger.info("Sysprep not configured, skipping....")
                phase.ok('Sysprep skipped')
                return

            cloud_admin = self.get_cloudclient()
            project_id = cloud_admin.get_servers_by_name(args.vm).tenant_id

            if args.sysprep_snapshot:
                logger.info("Creating sysprep snapshot of VM '%s'...", args.vm)
                cloud = self.get_cloudclient(project_id=project_id)
                vm = cloud.get_servers_by_name(args.vm, all=False)

                if not vm.image:
                    # Boot from volume => volume snapshot
                    attachments = [vol['id'] for vol in getattr(
                        vm, 'os-extended-volumes:volumes_attached')]

                    boot_volume = None
                    for volume in cloud.get_volumes(project_id=project_id):
                        if (volume.id in attachments
                                and volume.attachments[0][
                                    'device'] == '/dev/vda'):
                            boot_volume = volume
                            break

                    snapshot_id = cloud.create_volume_snapshot(
                        boot_volume,
                        "{0}_base_{1}".format(
                            vm.name.lower(),
                            args.suffix.lower()))
                    logger.info('Volume snapshot created %s', snapshot_id)
                    self.wait_until_volume_snapshot_available(
                        snapshot_id, cloud)
                else:
                    # Boot from image => instance snapshot
                    snapshot_id = vm.create_image(
                        "{0}_base_{1}".format(
                            vm.name.lower(),
                            args.suffix.lower()),
                    )
                    logger.info('VM snapshot created %s', snapshot_id)
                    self.wait_until_image_active(snapshot_id, cloud)

                    if args.image_tags:
                        cloud.update_image(snapshot_id, tags=args.image_tags)

            # Now that the snapshot has been executed we can start sysprep
            vm = cloud_admin.get_servers_by_name(args.vm)

            logger.info('Downloading appliance file into tempfile')
            r = requests.get(APPLIANCE_URL, stream=True, timeout=1800)
            with os.fdopen(fd, 'wb') as tmp:
                file_written = 0
                total_length = int(r.headers.get('content-length'))
                for chunk in r.iter_content(chunk_size=4194304):
                    if chunk:
                        file_written += tmp.write(chunk)
                        logger.info(
                            "%s MB out of %s MB downloaded",
                            round((file_written / 1024**2) + 1),
                            round((total_length / 1024**2) + 1))
                logger.info('Appliance file downloaded')
                host = getattr(vm, 'OS-EXT-SRV-ATTR:host')
                domain = getattr(vm, 'OS-EXT-SRV-ATTR:instance_name')
                # Copy the appliance file into the server
                common.scp_file(host, path, path)
                # Extract it into /tmp
                common.ssh_executor(
                    host,
                    'tar xvfJ {0} -C /tmp/ && rm {0}'.format(path))
                # Execute the sysprep command
                common.ssh_executor(
                    host,
                    ('LIBGUESTFS_PATH=/tmp/appliance/'
                     'virt-sysprep {0} -d {1}').format(
                        args.sysprep_params,
                        domain
                    )
                )
            phase.ok('Sysprep executed successfully')
        except Exception as ex:
            error_msg = "Sysprep failed: %s" % (str(ex))
            phase.fail(error_msg)
            raise ex
        finally:
            os.remove(path)

    def create_snapshot(self, args, phase, state):
        try:
            logger.info("Creating snapshot of VM '%s'...", args.vm)
            cloud_admin = self.get_cloudclient()
            vm_admin = cloud_admin.get_servers_by_name(args.vm)
            project_id = vm_admin.tenant_id
            host = getattr(vm_admin, 'OS-EXT-SRV-ATTR:host')
            cloud = self.get_cloudclient(project_id=project_id)
            vm = cloud.get_servers_by_name(args.vm, all=False)

            if not vm.image:
                # Boot from volume => volume snapshot
                if args.skip_volume_snapshot:
                    logger.info("Volume snapshot not configured, skipping....")
                    phase.ok('Volume snapshot skipped')
                    return

                attachments = [vol['id'] for vol in getattr(
                    vm, 'os-extended-volumes:volumes_attached')]
                boot_volume = None
                for volume in cloud.get_volumes(project_id=project_id):
                    if (volume.id in attachments
                            and volume.attachments[0][
                                'device'] == '/dev/vda'):
                        boot_volume = volume
                        break

                snapshot = cloud.create_volume_snapshot(
                    boot_volume,
                    "{0}_snap_{1}".format(
                        vm.name.lower(),
                        args.suffix.lower()))
                logger.info('Volume snapshot created %s', snapshot)
                self.wait_until_volume_snapshot_available(
                    snapshot, cloud)
            else:
                # Grab lock on the host if needed
                lock = self.coordinator.get_lock(host.encode())
                if args.exclusive:
                    logger.info('Grabbing lock on host...')
                    lock.acquire()
                    logger.info(
                        'Lock acquired path: %s/%s',
                        lock._lock.path,
                        lock._lock.node
                    )

                # Boot from image => instance snapshot
                snapshot_id = vm.create_image(
                    "{0}_snap_{1}".format(
                        vm.name.lower(),
                        args.suffix.lower()),
                )
                logger.info('VM snapshot created %s', snapshot_id)
                self.wait_until_image_active(snapshot_id, cloud)

                # Remove exclusive lock on the host
                if args.exclusive:
                    logger.info("Releasing lock on host")
                    lock.release()
                    logger.info('Lock on host released')

                # TODO(jcastro) Temporary fix for image min_disk bigger than
                # flavor disk size
                image = cloud.get_image(snapshot_id)
                if image.min_disk:
                    cloud.update_image(snapshot_id, min_disk=0)

                if args.image_tags:
                    cloud.update_image(snapshot_id, tags=args.image_tags)

                if args.new_project_id and args.new_project_id != project_id:
                    cloud_admin.update_image(snapshot_id, visibility='shared')
                    cloud_admin.image_add_project(snapshot_id,
                                                  args.new_project_id)

            phase.ok('Snapshot created successfully')
        except Exception as ex:
            error_msg = "Snapshot failed: %s" % (str(ex))
            phase.fail(error_msg)
            raise ex

    def create_volume(self, args, phase, state):
        if args.recreate_from != 'volume':
            logger.info('Step only needed when recreating from volume')
            phase.ok('Step only needed when recreating from volume')
            return

        try:
            cloud_admin = self.get_cloudclient()
            project_id = cloud_admin.get_servers_by_name(args.vm).tenant_id
            cloud = self.get_cloudclient(project_id=project_id)
            vm = cloud_admin.get_servers_by_name(args.vm)

            if not vm.image:
                logger.info('Skip volume creation on boot from volume VM')
                phase.ok('Skip Volume creation on boot from volume VM')
            else:
                logger.info("Creating volume for VM '%s'...", args.vm)
                image = cloud.get_images_by_name(
                    "{0}_snap_{1}".format(
                        vm.name.lower(),
                        args.suffix.lower()))[0]

                volume_size = functools.reduce(max, [
                    round(image.size / (1024.0**3)),
                    vm.flavor['disk'],
                    image.min_disk])

                volume = cloud.create_volume(
                    name="{0}_vol_{1}".format(
                        vm.name.lower(),
                        args.suffix.lower()),
                    size=volume_size,
                    volume_type=args.volume_type,
                    image=image.id)
                logger.info('Volume created %s', volume)
                self.wait_until_volume_available(volume)

                phase.ok('Volume created successfully')
        except Exception as ex:
            error_msg = "Volume creation failed: %s" % (str(ex))
            phase.fail(error_msg)
            raise ex

    def recreate_instance(self, args, phase, state):  # noqa: C901
        try:
            if args.recreate_from != 'none':
                cloud = self.get_cloudclient()
                vm = cloud.get_servers_by_name(args.vm)

                vm_metadata = vm.metadata
                vm_az = getattr(vm, 'OS-EXT-AZ:availability_zone')
                vm_volumes = getattr(
                    vm, 'os-extended-volumes:volumes_attached')

                # If we try to use '-' as availability zone, it collapses
                if vm_az == '-':
                    vm_az = None

                new_flavor = self.get_compatible_flavor(
                    vm.flavor['original_name'],
                    args.flavor_map)

                flavors = []
                flavors_public = vm.manager._list(
                    "/flavors/detail?is_public=true",
                    "flavors")
                flavors_non_public = vm.manager._list(
                    "/flavors/detail?is_public=false",
                    "flavors")
                flavors.extend(flavors_public)
                flavors.extend(flavors_non_public)

                flavor = [
                    flavor for flavor in flavors if flavor.name == new_flavor
                ][0]

                new_project_id = (args.new_project_id if args.new_project_id
                                  else vm.tenant_id)

                logger.info("Setting VM properties...")
                project = cloud.get_project(new_project_id)
                if ('landb-mainuser' not in vm_metadata.keys()
                        and hasattr(project, 'landb-mainuser')):
                    logger.info(
                        "'landb-mainuser' not set. "
                        "Setting value to '%s'...",
                        vm.user_id)
                    vm_metadata['landb-mainuser'] = vm.user_id

                if ('landb-responsible' not in vm_metadata.keys()
                        and hasattr(project, 'landb-responsible')):
                    logger.info(
                        "'landb-responsible' not set. "
                        "Setting value to '%s'...", vm.user_id)
                    vm_metadata['landb-responsible'] = vm.user_id

                # Ensure the volumes are not flagged in delete_on_termination
                volume_delete_status = self.retrieve_volumes_state(
                    vm.id, vm_volumes)
                self.reset_volumes_state(vm.id, vm_volumes)

                # Calculate block device mapping
                block_dev_mapping = [{
                    'uuid': volume['id'],
                    'source_type': 'volume',
                    'destination_type': 'volume',
                    'delete_on_termination': False
                } for volume in vm_volumes]

                image, block_dev_mapping = self.process_vm_boot_mode(
                    args, cloud, vm, block_dev_mapping)

                cloud = self.get_cloudclient(project_id=new_project_id)

                # Create port
                port = None
                if args.create_port:
                    addr_v4 = [addr for addr in vm.addresses['CERN_NETWORK']
                               if addr['version'] == 4].pop()
                    addr_v6 = [addr for addr in vm.addresses['CERN_NETWORK']
                               if addr['version'] == 6].pop()
                    mac = addr_v4['OS-EXT-IPS-MAC:mac_addr'].lower()
                    port = cloud.create_port(
                        mac=mac,
                        ip4=addr_v4['addr'],
                        ip6=addr_v6['addr']
                    )['port']

                # We can unlock the VM so we can delete it
                if args.protect:
                    logger.info('Instance unlocked so we can proceed with'
                                ' the deletion')
                    vm.unlock()
                    vm = vm.manager.get(vm.id)

                # Grab lock on the project if needed
                lock = self.coordinator.get_lock(new_project_id.encode())
                if args.exclusive:
                    logger.info('Grabbing lock on project...')
                    lock.acquire()
                    logger.info(
                        'Lock acquired path: %s/%s',
                        lock._lock.path,
                        lock._lock.node
                    )

                # To avoid issues with aliases --load-X- on creation we need
                # to remove them beforehand, then the property set will create
                # them afterwards
                if 'landb-alias' in vm_metadata.keys():
                    alias = vm_metadata['landb-alias']
                    temp_alias = ','.join(
                        [a for a in alias.split(',')
                            if '--LOAD-' not in a.upper()]
                    )
                    if alias != temp_alias:
                        if not temp_alias:
                            vm.manager.delete_meta(vm, ["landb-alias"])
                        else:
                            vm.manager.set_meta(vm, {
                                "landb-alias": temp_alias
                            })

                # To avoid recreation of the computer account we set a
                # property before deletion
                deletion_properties = {"cern-activedirectory": "false"}
                if args.landb_skip:
                    deletion_properties["landb-skip"] = "true"
                vm.manager.set_meta(vm, deletion_properties)

                # All the info required to rebuild the instance has been
                # fetched. Triggering the deletion of the instance
                logger.info("Deleting VM '%s'...", args.vm)
                vm.delete()
                if not args.nowait:
                    self.wait_until_dns_entry_free(args.vm)
                else:
                    self.wait_until_vm_is_gone(vm)

                if (args.new_project_id and len(vm_volumes) > 0
                        and args.new_project_id != vm.tenant_id):
                    # We need to transfer the volumes from source_project
                    # to target
                    volumes_to_transfer = [
                        volume['id'] for volume in vm_volumes]
                    cloud.transfer_volumes(volumes_to_transfer,
                                           vm.tenant_id,
                                           args.new_project_id)

                # if protect set exclusive_access
                if args.protect:
                    logger.info("Setting exclusive access to project '%s'...",
                                vm.tenant_id)
                    state['acls'] = self.get_cloudclient(
                    ).set_exclusive_access(
                        user_id=cloud.session.auth.get_user_id(cloud.session),
                        project_id=vm.tenant_id
                    )
                    logger.info('List of ACLs revoked')
                    logger.info([{
                        'project': acl.scope['project']['id'],
                        'role': acl.role['id'],
                        'to': (acl.user['id'] if hasattr(acl, 'user')
                               else acl.group['id'])
                    } for acl in state['acls']])

                # set the project mapping property if specified
                if args.project_map_cell:
                    logger.info("Saving old property if project was mapped")
                    old_mapping = self.get_cloudclient().get_project_property(
                        new_project_id,
                        'cells_mapping')
                    logger.info("Adding project property")
                    self.get_cloudclient().set_project_property(
                        new_project_id,
                        'cells_mapping',
                        args.project_map_cell)
                    vm_az = None

                nics = [{
                    'port-id': port['id']
                }] if args.create_port and 'id' in port.keys() else 'auto'

                creation_properties = {} if not args.landb_skip else {
                    "cern-services": "false",
                    "cern-checkdns": "false",
                    "cern-checklandbdevice": "false"
                }

                # And now create the machine
                cloud.create_server(
                    name=args.new_name if args.new_name else args.vm,
                    image=image,
                    flavor=flavor,
                    block_device_mapping_v2=block_dev_mapping,
                    nics=nics,
                    availability_zone=vm_az,
                    meta=creation_properties
                )

                vm = self.get_cloudclient(
                    project_id=new_project_id).get_servers_by_name(
                        args.new_name if args.new_name else args.vm,
                        all=False)

                # unset the project mapping property
                if args.project_map_cell:
                    logger.info("VM scheduled waiting until building")
                    self.wait_while_scheduling(vm)
                    if old_mapping:
                        logger.info("Setting old value for project mapping")
                        self.get_cloudclient().set_project_property(
                            new_project_id,
                            'cells_mapping',
                            old_mapping)
                    else:
                        logger.info("Remove project property")
                        self.get_cloudclient().set_project_property(
                            new_project_id,
                            'cells_mapping',
                            "")

                if args.protect:
                    logger.info("Unsetting exclusive access to project '%s'",
                                vm.tenant_id)
                    self.get_cloudclient().reset_exclusive_access(
                        user_id=cloud.session.auth.get_user_id(cloud.session),
                        project_id=vm.tenant_id,
                        acls=state['acls']
                    )

                # Remove exclusive lock on the project
                if args.exclusive:
                    logger.info("Releasing lock on project")
                    lock.release()
                    logger.info('Lock on project released')

                logger.info("VM Launched, waiting until it becomes active")
                self.wait_until_vm_state(vm, 'ACTIVE')

                if args.create_port:
                    logger.info("Set preserve port on delete")
                    db = NovaDB()
                    cell_db = db.get_cell_connection_string(vm.id)
                    logger.info(
                        "Previous preserve port value %s",
                        db.get_preserve_on_delete(cell_db, vm.id)
                    )
                    db.set_preserve_on_delete(
                        cell_db,
                        vm.id,
                        False
                    )
                    logger.info(
                        "Actual preserve port value %s",
                        db.get_preserve_on_delete(cell_db, vm.id)
                    )

                # Ensure the volumes are not flagged in delete_on_termination
                self.ensure_volumes_in_same_state(vm.id, volume_delete_status)

                logger.info("VM is active setting metadata information")
                vm.manager.set_meta(vm, vm_metadata)

                if args.landb_skip:
                    logger.info("Removing creation properties")
                    vm.manager.delete_meta(vm, [
                        "cern-services",
                        "cern-checkdns",
                        "cern-checklandbdevice"
                    ])

                if args.preserve:
                    logger.info('Leaving the machine into the same state')
                    if state['status'] == 'SHUTOFF':
                        vm.stop()
                        self.wait_until_vm_state(vm, 'SHUTOFF')
                    elif state['status'] == 'PAUSED':
                        vm.paused()
                        self.wait_until_vm_state(vm, 'PAUSED')
                    elif state['status'] == 'SUSPENDED':
                        vm.suspend()
                        self.wait_until_vm_state(vm, 'SUSPENDED')

                    logger.info('Locking the machine if it was locked')
                    if state['locked']:
                        vm.lock()

                logger.info("Done :)")
                phase.ok("VM recreated successfully")
            else:
                logger.info("'%s' NOT recreated", args.vm)
                phase.ok("VM not recreated as requested")
        except Exception as ex:
            error_msg = "VM creation failed: %s" % (str(ex))
            phase.fail(error_msg)
            raise ex

    def retrieve_volumes_state(self, id, volumes):
        # Ensure the volumes are not flagged in delete_on_termination
        status = {}
        db = NovaDB()
        conn = db.get_cell_connection_string(id)
        for volume in volumes:
            status[volume['id']] = (
                db.get_delete_on_termination(
                    conn,
                    id,
                    volume['id']
                )
            )
        return status

    def reset_volumes_state(self, id, volumes):
        # Ensure the volumes have the same flags for delete_on_termination
        db = NovaDB()
        conn = db.get_cell_connection_string(id)
        for volume in volumes:
            db.set_delete_on_termination(conn, id, volume['id'], False)

    def ensure_volumes_in_same_state(self, id, status):
        # Ensure the volumes have the same flags for delete_on_termination
        db = NovaDB()
        conn = db.get_cell_connection_string(id)
        for volume_id, value in status.items():
            db.set_delete_on_termination(conn, id, volume_id, value)

    def process_vm_boot_mode(self, args, cloud, vm, block_dev_mapping):
        if not vm.image:
            logger.info("Recreating boot from volume")
            # The original VM is boot from volume
            # let's recreate it as is
            image = ''
            volumes = cloud.get_volumes(project_id=vm.tenant_id)

            bfv_dict = None
            for mapping in block_dev_mapping:
                entry = [vol for vol in volumes
                         if vol.id == mapping['uuid']][0]
                if not entry.attachments:
                    continue
                device = entry.attachments[0]['device']
                if device in ['/dev/vda', '/dev/hda', '/dev/sda']:
                    bfv_dict = mapping
                    break

            if not bfv_dict:
                logger.error('Could not find volume to boot into')
                raise Exception('Could not find volume to boot into')

            block_dev_mapping.remove(bfv_dict)
            bfv_dict['boot_index'] = 0
            block_dev_mapping.insert(0, bfv_dict)

            # The machine is a boot from image
        elif args.recreate_from == 'volume':
            logger.info("Recreating boot from image as boot from volume")
            image = ''

            vol_name = "%s_vol_%s" % (
                vm.name.lower(),
                args.suffix.lower())
            volumes = cloud.get_volumes(project_id=vm.tenant_id)

            volume = [
                vol for vol in volumes if vol_name == vol.name.lower()
            ][0]

            bfv_dict = {
                'uuid': volume.id,
                'source_type': 'volume',
                'destination_type': 'volume',
                'boot_index': 0,
                'delete_on_termination': False
            }
            block_dev_mapping.insert(0, bfv_dict)

        else:
            logger.info("Recreating boot from image")
            img_name = '%s_snap_%s' % (
                vm.name.lower(),
                args.suffix.lower())
            images = cloud.get_images_by_name(img_name.lower())
            image = [
                img for img in images if img_name == img.name.lower()
            ][0]
        return image, block_dev_mapping

    def main(self, args=None):
        try:
            args = self.parse_args(args)
            self.main_recreate(args)
        except Exception as e:
            logger.exception(e)
            sys.exit(1)


# Needs static method for setup.cfg
def main(args=None):
    RecreateInstance().main(args)


if __name__ == "__main__":
    main()
