import logging

from ccitools.cmd.interventions_manager import main
from unittest import TestCase


class TestInterventionsManager(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):
        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)
