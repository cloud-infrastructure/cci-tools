#!/usr/bin/python3

import ccitools.conf
import logging
import sys
import time

from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools.common import ssh_executor

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class HypervisorStateCMD(BaseCloudCMD):

    SLEEP_TIME = 60       # check if Hypervisor is up every 60 sec
    Valid_hosts = []    # list of HV that are not ready yet
    Invalid_hosts = []  # list of HV that have been rebooted
    UPTIME_DICT = {}    # host:uptime dict
    HOSTS = []          # all hosts

    def __init__(self):
        super(HypervisorStateCMD, self).__init__(
            description="Get hypervisor state")

    def process_uptime(self, uptime):
        # process args uptime and create dictionary of it
        uptime = uptime.replace("'", "")
        uptime = uptime.replace("[", "")
        uptime = uptime.replace("]", "")
        uptime = uptime.replace(" ", "")
        uptime_list = uptime.split(',')
        for uptime in uptime_list:
            value = uptime.split(":")
            self.UPTIME_DICT[value[0]] = value[1]
        logger.info("uptime dict : {}".format(self.UPTIME_DICT))

    def ssh_uptime(self, hosts, exec_mode):
        """SSH into host and return uptime."""
        if not exec_mode:
            for host in self.Invalid_hosts:
                self.Valid_hosts.append(host)
            self.Invalid_hosts.clear()
            return

        # sleep for 60 sec before any checks
        time.sleep(self.SLEEP_TIME)

        for host in hosts:
            counter = 0
            output = error = None
            logger.info("Current host : {}".format(host))
            while counter <= 10:
                try:
                    # execute random command to check if
                    output, error = ssh_executor(host, "cat /proc/uptime")
                    break
                except Exception:
                    logger.info("Trying to connect to %s" % host)
                    counter = counter + 1
                    time.sleep(self.SLEEP_TIME)
            # Map uptime to host
            if output:
                uptime = str(output[0])
                logger.info("uptime : {}".format(uptime))
                uptime = uptime.split(' ')
                # compare uptime
                if float(uptime[0]) < float(self.UPTIME_DICT[host]):
                    self.Valid_hosts.append(host)
                    self.Invalid_hosts.remove(host)
                else:
                    logger.info("{} not rebooted".format(host))
                    logger.info("{} uptime {}".format(host, uptime[0]))

    def main(self, args=None):
        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true',)
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')
        self.parser.add_argument(
            "--hypervisors", required=True,
            help="Name of hosts to migrate VMs from.")
        self.parser.add_argument(
            "--uptime", required=True,
            help="key value pair of hosts : uptime"
        )

        args = self.parse_args(args)

        # process each hypervisor
        for hv in args.hypervisors.split():
            self.Invalid_hosts.append(hv)
            self.HOSTS.append(hv)
        logger.info("hosts : {}".format(self.HOSTS))

        # execution mode
        if not args.exec_mode:
            logger.info("[DRYRUN] Execution mode.")
            logger.info("RUNDECK:DATA:VALID_HOSTS=%s" % " "
                        .join(self.Invalid_hosts))
            return
        else:
            logger.info("[PERFORM] Execution mode.")

        # create uptime dict
        self.process_uptime(args.uptime)

        # call ssh_uptime
        self.ssh_uptime(self.HOSTS, args.exec_mode)
        if self.Invalid_hosts:
            logger.info("Following hosts are not ready %s " % ","
                        .join(self.Invalid_hosts))

        if self.Valid_hosts:
            # ^RUNDECK:DATA:(.+?)\s*=\s*(.+)$
            # make sure it's space separated else parsing problem in next job
            logger.info("RUNDECK:DATA:VALID_HOSTS=%s" % " "
                        .join(self.Valid_hosts))


# Needs static method for setup.cfg
def main(args=None):
    HypervisorStateCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
