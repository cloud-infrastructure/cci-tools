import ccitools.conf
import logging
import sys

from ccitools.cmd.base import BaseCMD
from ccitools.common import env
from ccitools.utils.cloud import CloudRegionClient
from ccitools.utils.sdk import SDKClient

# configure logging
logging.getLogger('requests').setLevel(logging.WARNING)
logger = logging.getLogger(__name__)

CONF = ccitools.conf.CONF
CONSOLE_MESSAGE_FORMAT = '%(levelname)s: %(message)s'
dump_stack_trace = False


class BaseCloudCMD(BaseCMD):
    def __init__(self, description):
        super(BaseCloudCMD, self).__init__(description=description)

        self.parser.add_argument(
            '--os-cloud',
            metavar='<cloud-config-name>',
            dest='cloud',
            default=env('OS_CLOUD', default='cern'),
            help='Cloud name in clouds.yaml (Env: OS_CLOUD)', )

        self.parser.add_argument(
            '--os-region-name',
            metavar='<os-region-name>',
            dest='region_name',
            default=env('OS_REGION_NAME', default=None),
            help='Cloud region name in clouds.yaml (Env: OS_REGION_NAME)', )

    def parse_args(self, args):
        args = super(BaseCloudCMD, self).parse_args(args)
        self.cloudclient = CloudRegionClient(
            cloud=args.cloud, region_name=args.region_name)
        return args

    def configure_logging(self, args):
        """Typical app logging setup."""
        global dump_stack_trace
        root_logger = logging.getLogger('')
        # Always send higher-level messages to the console via stderr
        console = logging.StreamHandler(sys.stderr)
        formatter = logging.Formatter(CONSOLE_MESSAGE_FORMAT)
        console.setFormatter(formatter)
        root_logger.addHandler(console)
        # Set logging to the requested level
        dump_stack_trace = False
        if args.verbose_level >= 3:
            # Three or more --verbose
            root_logger.setLevel(logging.DEBUG)
        elif args.verbose_level == 2:
            # Two --verbose
            root_logger.setLevel(logging.INFO)
        elif args.verbose_level == 1:
            # One --verbose
            root_logger.setLevel(logging.WARNING)
        else:
            # This is the default case, no --debug, --verbose
            root_logger.setLevel(logging.ERROR)
        if args.debug:
            # --debug forces traceback
            dump_stack_trace = True
            root_logger.setLevel(logging.DEBUG)


class BaseSDKCMD(BaseCMD):
    def __init__(self, description):
        super(BaseSDKCMD, self).__init__(description=description)

        self.parser.add_argument(
            '--os-cloud',
            metavar='<cloud-config-name>',
            dest='cloud',
            default=env('OS_CLOUD', default='cern'),
            help='Cloud name in clouds.yaml (Env: OS_CLOUD)', )

        self.parser.add_argument(
            '--os-interface',
            metavar='<cloud-config-name>',
            dest='interface',
            default=env('OS_INTERFACE', default='public'),
            help='Interface to be used (Env: OS_INTERFACE)', )

    def parse_args(self, args):
        args = super(BaseSDKCMD, self).parse_args(args)
        self.client = SDKClient(cloud=args.cloud, interface=args.interface)
        return args

    def configure_logging(self, args):
        """Typical app logging setup."""
        global dump_stack_trace
        root_logger = logging.getLogger('')
        # Always send higher-level messages to the console via stderr
        console = logging.StreamHandler(sys.stderr)
        formatter = logging.Formatter(CONSOLE_MESSAGE_FORMAT)
        console.setFormatter(formatter)
        root_logger.addHandler(console)
        # Set logging to the requested level
        dump_stack_trace = False
        if args.verbose_level >= 3:
            # Three or more --verbose
            root_logger.setLevel(logging.DEBUG)
        elif args.verbose_level == 2:
            # Two --verbose
            root_logger.setLevel(logging.INFO)
        elif args.verbose_level == 1:
            # One --verbose
            root_logger.setLevel(logging.WARNING)
        else:
            # This is the default case, no --debug, --verbose
            root_logger.setLevel(logging.ERROR)
        if args.debug:
            # --debug forces traceback
            dump_stack_trace = True
            root_logger.setLevel(logging.DEBUG)
