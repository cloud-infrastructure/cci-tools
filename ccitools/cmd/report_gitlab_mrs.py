#!/usr/bin/python3

import ccitools.conf
from gitlab.exceptions import GitlabListError
from gitlab import Gitlab
import html
import logging
import prettytable
import sys

from ccitools.cmd.base import BaseCMD
from ccitools.utils.sendmail import MailClient
from datetime import datetime
from dateutil import tz

logger = logging.getLogger(__name__)

CONF = ccitools.conf.CONF


class ReportGitlabMRsCMD(BaseCMD):

    def __init__(self):
        super(ReportGitlabMRsCMD, self).__init__(
            description="Tool to report opened MRs on gitlab")

    def fetch_merge_requests(self, args):
        gl = Gitlab(
            url=CONF.gitlab.url,
            private_token=CONF.gitlab.private_token,
            api_version=CONF.gitlab.version)

        projects = []
        merge_requests = []

        if args.projects:
            projects = args.projects
        elif args.groups:
            logger.info("Retrieving list of projects based on groups")
            for group_id in args.groups:
                group = gl.groups.get(group_id)
                projects.extend(
                    [pr.id for pr in group.projects.list(
                        get_all=True,
                        membership=True)])
        else:
            logger.info("Retrieving list of projects from the user")
            projects.extend(
                [pr.id for pr in gl.projects.list(
                    get_all=True,
                    membership=True)])

        logger.info("Retrieving list of merge requests based on projects")
        for project_id in projects:
            if project_id in args.excludes:
                logger.info("Excluding project %d", project_id)
                continue
            try:
                project = gl.projects.get(project_id)
                mrs = project.mergerequests.list(
                    state=args.state,
                    lazy=False,
                    wip='no')
                merge_requests.extend(mrs)
            except GitlabListError:
                pass

        mrs_table = prettytable.PrettyTable(
            ['Created At', 'Title', 'Author', 'Assignee',
             'Reviewers', 'Url'])
        mrs_table_html = prettytable.PrettyTable(
            ['Created At', 'Title', 'Author', 'Assignee',
             'Reviewers', 'Url'])
        for mr in merge_requests:
            date_utc = datetime.strptime(mr.created_at,
                                         "%Y-%m-%dT%H:%M:%S.%f%z")
            date_local = date_utc.astimezone(
                tz.tzlocal()).strftime(
                    "%Y-%m-%d %H:%M:%S")
            mrs_table.add_row([
                date_local,
                mr.title,
                mr.author['username'],
                mr.assignee['username'] if mr.assignee else '',
                str.join(
                    ',', [rw['username'] for rw in mr.reviewers]),
                mr.web_url])
            mrs_table_html.add_row([
                date_local,
                mr.title,
                mr.author['username'],
                mr.assignee['username'] if mr.assignee else '',
                str.join(
                    ',', [rw['username'] for rw in mr.reviewers]),
                '<a href=\"' + mr.web_url + '\">' + mr.web_url + '</a>'])

        if args.exec_mode:
            mailclient = MailClient("localhost")
            mail_body = args.body
            if not merge_requests:
                mail_body += "No pending MRs"
            else:
                mail_body += mrs_table_html.get_html_string(
                    sortby="Created At",
                    format=True)
                mail_body = html.unescape(mail_body)
            try:
                mailclient.send_mail(
                    args.to, args.subject, mail_body,
                    "noreply@cern.ch", "", "", "html",)
                logger.info("Email sent successfully to %s", args.to)
            except Exception as ex:
                logger.error("Impossible to send mailto '%s'. "
                             "Error message: '%s'", args.to, ex)
            logger.info("To: %-20s Subject: \"%s\"", args.to, args.subject)
        else:
            if not merge_requests:
                logger.warning('No pending MRs')
            else:
                logger.warning("Pending MRs: \n%s",
                               mrs_table.get_string(sortby="Created At"))

    def main(self, args=None):
        self.parser.add_argument(
            "--state",
            default='opened',
            const='all',
            nargs='?',
            choices=['opened', 'closed', 'locked', 'merged'],
            help='filter merge requests by opened, closed, locked or merged,'
                 '(default: %(default)s)')
        self.parser.add_argument(
            "--project",
            action='append',
            dest='projects',
            metavar='PROJECT',
            default=[],
            help="filter the project(s), can be repeated")
        self.parser.add_argument(
            "--group",
            action='append',
            dest='groups',
            metavar='GROUP',
            default=[],
            help="filter the projects by group(s), can be repeated")
        self.parser.add_argument(
            "--exclude",
            action='append',
            dest='excludes',
            metavar='PROJECT',
            type=int,
            default=[],
            help="exclude some projects from the list of projects to check,"
                 "can be repeated")
        self.parser.add_argument(
            "--subject",
            help="email subject",
            default='[Cloud Infrastructure] Merge request report')
        self.parser.add_argument(
            "--to",
            help="email to",
            default='cloud-infrastructure-3rd-level@cern.ch')
        self.parser.add_argument(
            "--body",
            help="Message to be sent in the body",
            default=('Hello,<br/><br/>'
                     'This is the report for pending merge requests.<br/>'
                     'Please have a look at them and close them if possible.'
                     '<br/><br/>Best regards,'
                     '<br/>Cloud Infrastructure Team'
                     '<br/><br/>'))
        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true',
                               help="If present, sends email")
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')
        args = self.parse_args(args)
        self.fetch_merge_requests(args)


# Needs static method for setup.cfg
def main(args=None):
    ReportGitlabMRsCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
