from oslo_config import cfg


backend_url = cfg.StrOpt(
    'backend_url',
    default='file:///tmp',
    help="The backend URL to use for distributed coordination.")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    backend_url
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
