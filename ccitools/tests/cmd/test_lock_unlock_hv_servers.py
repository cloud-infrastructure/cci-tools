import logging

from ccitools.cmd.lock_unlock_hv_servers import main
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
class TestLockUnlockServers(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self, mock_crc):
        mock_crc.return_value = mock_crc
        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)

    def test_no_servers(self, mock_crc):
        mock_crc.return_value = mock_crc
        mock_crc.get_servers_by_hypervisor.return_value = None
        main([
            '--action', 'lock',
            '--hypervisors', 'fake_hv',
            '--perform',
        ])

    def test_servers_perform_lock(self, mock_crc):
        mock_crc.return_value = mock_crc
        srv_mock = mock.Mock()
        srv_mock.name = 'fake_n'
        mock_crc.get_servers_by_hypervisor.return_value = [
            srv_mock
        ]
        main([
            '--action', 'lock',
            '--hypervisors', 'fake_hv',
            '--perform',
        ])

    def test_servers_perform_unlock(self, mock_crc):
        mock_crc.return_value = mock_crc
        srv_mock = mock.Mock()
        srv_mock.name = 'fake_n'
        mock_crc.get_servers_by_hypervisor.return_value = [
            srv_mock
        ]
        main([
            '--action', 'unlock',
            '--hypervisors', 'fake_hv',
            '--perform',
        ])

    def test_servers_dryrun(self, mock_crc):
        mock_crc.return_value = mock_crc
        srv_mock = mock.Mock()
        srv_mock.name = 'fake_n'
        mock_crc.get_servers_by_hypervisor.return_value = [
            srv_mock
        ]
        main([
            '--action', 'unlock',
            '--hypervisors', 'fake_hv',
            '--dryrun',
        ])
