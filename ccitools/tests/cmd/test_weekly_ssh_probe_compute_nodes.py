import logging

from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.utils.cloud.cloud_config')
@mock.patch('aitools.foreman.ForemanClient')
@mock.patch('ccitools.utils.servicenowv2.ServiceNowClient')
@mock.patch('ccitools.utils.sendmail.MailClient')
@mock.patch('ccitools.utils.cloud.CloudRegionClient'
            '.get_hypervisors_list')
@mock.patch('ccitools.common.ssh_executor')
@mock.patch('ccitools.common.ping')
class TestHealthReportHvCMD(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

        self.aitools_mock = mock.MagicMock()
        modules = {
            'aitools': self.aitools_mock,
            'aitools.foreman': self.aitools_mock.foreman,
        }
        self.patcher = mock.patch.dict('sys.modules', modules)
        self.patcher.start()
        self.addCleanup(self.patcher.stop)

    def test_default_args(self, mock_ping, mock_ssh, mock_list, mock_mc,
                          mock_snc, mock_fc, mock_cc):
        from ccitools.cmd.weekly_ssh_probe_compute_nodes import main

        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)
        mock_snc.assert_not_called()

    def test_exception_ping(self, mock_ping, mock_ssh,
                            mock_list, mock_mc, mock_snc,
                            mock_fc, mock_cc):
        mock_list.return_value = [
            mock.MagicMock(hypervisor_hostname='fake_host1.cern.ch',
                           hypervisor_type='QEMU')]
        mock_ssh.return_value = None
        mock_ping.return_value = True
        mock_snc.return_value.ticket.raw_query_incidents.return_value = True
        from ccitools.cmd.weekly_ssh_probe_compute_nodes import main

        main([
            '--mail-to', 'fakemail'
        ])
        mock_mc.assert_called()
        mock_ping.assert_called()
        mock_snc.assert_called_with(instance='cern')
        mock_ssh.assert_called_with('fake_host1.cern.ch', 'hostname',
                                    connect_timeout=120,
                                    banner_timeout=120, keep_alive_interval=2)

    def test_exception_ping_no_ticket(self, mock_ping, mock_ssh,
                                      mock_list, mock_mc, mock_snc,
                                      mock_fc, mock_cc):
        mock_list.return_value = [
            mock.MagicMock(hypervisor_hostname='fake_host1.cern.ch',
                           hypervisor_type='QEMU')]
        mock_ssh.return_value = None
        mock_ping.return_value = True
        mock_snc.return_value.ticket.raw_query_incidents.return_value = False
        from ccitools.cmd.weekly_ssh_probe_compute_nodes import main

        main([
            '--mail-to', 'fakemail'
        ])
        mock_mc.assert_called()
        mock_ping.assert_called()
        mock_snc.assert_called_with(instance='cern')
        mock_ssh.assert_called_with('fake_host1.cern.ch', 'hostname',
                                    connect_timeout=120,
                                    banner_timeout=120, keep_alive_interval=2)

    def test_exception_f_ping(self, mock_ping, mock_ssh,
                              mock_list, mock_mc, mock_snc,
                              mock_fc, mock_cc):
        mock_list.return_value = [
            mock.MagicMock(hypervisor_hostname='fake_host1.cern.ch',
                           hypervisor_type='QEMU')]
        mock_ssh.return_value = None
        mock_ping.return_value = False
        from ccitools.cmd.weekly_ssh_probe_compute_nodes import main

        main([
            '--mail-to', 'fakemail'
        ])
        mock_mc.assert_called()
        mock_ping.assert_called()
        mock_snc.assert_called_with(instance='cern')
        mock_ssh.assert_called_with('fake_host1.cern.ch', 'hostname',
                                    connect_timeout=120,
                                    banner_timeout=120, keep_alive_interval=2)

    def test_exception_f_ping_no_ticket(self, mock_ping, mock_ssh,
                                        mock_list, mock_mc, mock_snc,
                                        mock_fc, mock_cc):
        mock_list.return_value = [
            mock.MagicMock(hypervisor_hostname='fake_host1.cern.ch',
                           hypervisor_type='QEMU')]
        mock_ssh.return_value = None
        mock_ping.return_value = False
        mock_snc.return_value.ticket.raw_query_incidents.return_value = False
        from ccitools.cmd.weekly_ssh_probe_compute_nodes import main

        main([
            '--mail-to', 'fakemail'
        ])
        mock_mc.assert_called()
        mock_ping.assert_called()
        mock_snc.assert_called_with(instance='cern')
        mock_ssh.assert_called_with('fake_host1.cern.ch', 'hostname',
                                    connect_timeout=120,
                                    banner_timeout=120, keep_alive_interval=2)

    def test_hv_list(self, mock_ping, mock_ssh,
                     mock_list, mock_mc, mock_snc,
                     mock_fc, mock_cc):
        mock_list.return_value = [
            mock.MagicMock(hypervisor_hostname='fake_host1.cern.ch',
                           hypervisor_type='QEMU')]
        mock_ssh.return_value = 'ok'

        from ccitools.cmd.weekly_ssh_probe_compute_nodes import main
        main([
            '--mail-to', 'fakemail'
        ])
        mock_mc.assert_called()
        mock_ping.assert_not_called()
        mock_snc.assert_called_with(instance='cern')
        mock_ssh.assert_called_with('fake_host1.cern.ch', 'hostname',
                                    connect_timeout=120,
                                    banner_timeout=120, keep_alive_interval=2)

    def test_fail_ssh(self, mock_ping, mock_ssh,
                      mock_list, mock_mc, mock_snc,
                      mock_fc, mock_cc):
        mock_list.return_value = [
            mock.MagicMock(hypervisor_hostname='fake_host1.cern.ch',
                           hypervisor_type='QEMU')]
        mock_ssh.return_value = [], None

        from ccitools.cmd.weekly_ssh_probe_compute_nodes import main
        main([
            '--mail-to', 'fakemail'
        ])
        mock_mc.assert_called()
        mock_ping.assert_not_called()
        mock_snc.assert_called_with(instance='cern')
        mock_ssh.assert_called_with('fake_host1.cern.ch', 'hostname',
                                    connect_timeout=120,
                                    banner_timeout=120, keep_alive_interval=2)

    def test_fail_ssh_no_ticket(self, mock_ping, mock_ssh,
                                mock_list, mock_mc, mock_snc,
                                mock_fc, mock_cc):
        mock_list.return_value = [
            mock.MagicMock(hypervisor_hostname='fake_host1.cern.ch',
                           hypervisor_type='QEMU')]
        mock_ssh.return_value = [], None
        mock_snc.return_value.ticket.raw_query_incidents.return_value = False

        from ccitools.cmd.weekly_ssh_probe_compute_nodes import main
        main([
            '--mail-to', 'fakemail'
        ])
        mock_mc.assert_called()
        mock_ping.assert_not_called()
        mock_snc.assert_called_with(instance='cern')
        mock_ssh.assert_called_with('fake_host1.cern.ch', 'hostname',
                                    connect_timeout=120,
                                    banner_timeout=120, keep_alive_interval=2)

    def test_hv_list_empty(self, mock_ping, mock_ssh,
                           mock_list, mock_mc, mock_snc,
                           mock_fc, mock_cc):
        mock_list.return_value = [
            mock.MagicMock(hypervisor_hostname='fake_host1.cern.ch',
                           hypervisor_type='not_QEMU')]
        mock_ssh.return_value = 'ok'

        from ccitools.cmd.weekly_ssh_probe_compute_nodes import main
        main([
            '--mail-to', 'fakemail'
        ])
        mock_mc.assert_called()
        mock_ping.assert_not_called()
        mock_snc.assert_called_with(instance='cern')
        mock_ssh.assert_not_called()
