#!/usr/bin/python3

import argparse
import json
import logging
import os
import sys

from keystoneauth1 import session as keystone_session
from novaclient import client as nova
from os_client_config import config as cloud_config
from osc_lib.api import auth

CONSOLE_MESSAGE_FORMAT = '%(levelname)s: %(message)s'
DEFAULT_VERBOSE_LEVEL = 1
USER_AGENT = 'cci-locked-instances'

_logger = logging.getLogger(__name__)

# --debug sets this True
dump_stack_trace = True


# Generally useful stuff often found in a utils module
def env(*vars, **kwargs):
    """Search for the first defined of possibly many env vars.

    Returns the first environment variable defined in vars, or
    returns the default defined in kwargs.
    """
    for v in vars:
        value = os.environ.get(v, None)
        if value:
            return value
    return kwargs.get('default', '')


# Common Example functions
def base_parser(parser):
    """Set up some of the common CLI options.

    These are the basic options that match the library CLIs so
    command-line/environment setups for those also work with these
    demonstration programs.
    """
    # Global arguments
    parser.add_argument(
        '--os-cloud',
        metavar='<cloud-config-name>',
        dest='cloud',
        default=env('OS_CLOUD'),
        help='Cloud name in clouds.yaml (Env: OS_CLOUD)', )
    parser.add_argument(
        '-v',
        '--verbose',
        action='count',
        dest='verbose_level',
        default=0,
        help='Increase verbosity of output. Can be repeated.', )
    parser.add_argument(
        '--debug',
        default=False,
        action='store_true',
        help='show tracebacks on errors', )
    parser.add_argument(
        '--json',
        default=False,
        action='store_true',
        help='dumps the list of instances in json that were found', )
    return parser


def configure_logging(opts):
    """Typical app logging setup.

    Based on OSC/cliff
    """
    global dump_stack_trace
    root_logger = logging.getLogger('')
    # Requests logs some stuff at INFO that we don't want
    # unless we have DEBUG
    requests_log = logging.getLogger("requests")
    requests_log.setLevel(logging.ERROR)
    # Other modules we don't want DEBUG output for so
    # don't reset them below
    iso8601_log = logging.getLogger("iso8601")
    iso8601_log.setLevel(logging.ERROR)
    # Always send higher-level messages to the console via stderr
    console = logging.StreamHandler(sys.stderr)
    formatter = logging.Formatter(CONSOLE_MESSAGE_FORMAT)
    console.setFormatter(formatter)
    root_logger.addHandler(console)
    # Set logging to the requested level
    dump_stack_trace = False
    if opts.verbose_level >= 3:
        # Three or more --verbose
        root_logger.setLevel(logging.DEBUG)
        requests_log.setLevel(logging.DEBUG)
    elif opts.verbose_level == 2:
        # Two --verbose
        root_logger.setLevel(logging.INFO)
    elif opts.verbose_level == 1:
        # One --verbose
        root_logger.setLevel(logging.WARNING)
    else:
        # This is the default case, no --debug, --verbose
        root_logger.setLevel(logging.ERROR)
    if opts.debug:
        # --debug forces traceback
        dump_stack_trace = True
        root_logger.setLevel(logging.DEBUG)
        requests_log.setLevel(logging.DEBUG)
    return


def make_session(opts):
    """Create our base session using simple auth from ksc plugins."""
    try:
        cc = cloud_config.OpenStackConfig()
    except (IOError, OSError) as e:
        _logger.critical("Could not read clouds.yaml configuration file")
        raise e

    cloud = cc.get_one_cloud(cloud=opts.cloud, argparse=opts)
    return keystone_session.Session(auth=cloud.get_auth())


def run(opts):
    retcode = 0
    vms = []
    session = make_session(opts)

    nc = nova.Client('2.60', session=session,
                     region_name=os.environ.get('OS_REGION_NAME'))

    _logger.debug("Retrieving list of instances locked by admin")
    servers = nc.servers.list(
        search_opts={'all_tenants': True, 'locked_by': 'admin'})
    _logger.info("Retrieved {0} instance(s)".format(len(servers)))

    for server in servers:
        actions = [a for a in nc.instance_action.list(server.id)
                   if a.action == 'lock']
        last_lock = (sorted(actions, key=lambda k: k.updated_at,
                     reverse=True).pop(0) if actions else None)

        if last_lock and (last_lock.user_id != 'cloudexpire'
                          and server.host_status == 'UP'
                          or last_lock.user_id != 'svcrdeck'
                          and server.host_status == 'MAINTENANCE'):
            _logger.warning("Instance %s unexpectedly locked by %s on %s",
                            server.id,
                            last_lock.user_id,
                            getattr(
                                server,
                                'OS-EXT-SRV-ATTR:hypervisor_hostname'),
                            )
            retcode = 2
            vms.append({
                'id': server.id,
                'name': server.name,
                'host': getattr(server,
                                'OS-EXT-SRV-ATTR:hypervisor_hostname'),
                'host_status': server.host_status,
                'metadata': server.metadata,
                'last_lock': {
                    'user_id': last_lock.user_id,
                    'updated_at': last_lock.updated_at
                },
            })

    if opts.json:
        print(json.dumps(vms))

    return retcode


def setup(args):
    """Parse command line and configure logging."""
    opts = base_parser(
        auth.build_auth_plugins_option_parser(
            argparse.ArgumentParser(
                description='Tool to show the locked instances'))
    ).parse_args(args)
    configure_logging(opts)
    return opts


def main(args=None):
    run(setup(args))


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        _logger.exception(e)
        sys.exit(1)
