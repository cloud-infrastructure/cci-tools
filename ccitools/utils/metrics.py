import logging

from ccitools.utils.monit import MonitClient
from ccitools.utils.snow.ticket import RequestState
from datetime import datetime

logger = logging.getLogger(__name__)


class MetricsHelper(object):
    def __init__(self):
        self.monit_client = MonitClient()

    def publish_resource_request_metrics(self, request_type, rp, quota,
                                         large_compute_request):
        project_name = rp.project_name
        if rp.request.info.u_current_task_state not in (RequestState.RESOLVED,
                                                        RequestState.CLOSED):
            logger.warning(f"Request {rp.request.info.number} not resolved yet"
                           f" for project {project_name}, skipping for KPI "
                           "reporting")
            return

        open_date = datetime.fromisoformat(rp.request.info.opened_at)
        resolution_date = datetime.fromisoformat(rp.request.info.u_resolved_at)
        total_seconds = int((resolution_date - open_date).total_seconds())

        unique_quota_types = set([x for (k, v) in quota.items() for x in [*v]])

        try:
            self.monit_client.send_service_metrics(
                metric_family="resource_request_handling",
                metrics={
                    "resource_request_handling_seconds": total_seconds,
                },
                tags={
                    "request_type": request_type,
                    "regions": ','.join([*quota]),
                    "project_name": project_name,
                    "has_storage": "blockstorage" in unique_quota_types,
                    "has_compute": "compute" in unique_quota_types,
                    "is_large_compute_request": large_compute_request
                }
            )
            logger.info("Metrics published to MONIT for "
                        f"{rp.request.info.number}")

        except Exception as e:
            logger.warning(f"Metrics for request {rp.request.info.number} "
                           f"could not be published: {e}")
