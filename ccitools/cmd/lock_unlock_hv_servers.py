#!/usr/bin/python3

import ccitools.conf
import logging
import sys

from ccitools.cmd.base.cloud import BaseCloudCMD

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class LockUnlockHVServersCMD(BaseCloudCMD):

    def __init__(self):
        super(LockUnlockHVServersCMD, self).__init__(
            description="Locks/Unlocks VMs in the hypervisor")

    def lock_unlock_hv_servers(self, cloudclient,
                               exec_mode, action, hypervisors):

        # Perform action on list of servers
        for hv_name in hypervisors.split():
            servers = cloudclient.get_servers_by_hypervisor(hv_name)
            if servers:
                server_names = [server.name for server in servers]
                if exec_mode:
                    if action == 'lock':
                        logger.info("Locking the following servers:\n%s",
                                    ", ".join(server_names))
                        for server in servers:
                            server.lock()
                    else:
                        logger.info("Unlocking the following servers:\n%s",
                                    ", ".join(server_names))
                        for server in servers:
                            server.unlock()
                else:
                    logger.info("[DRYRUN] %sing the following servers:\n%s",
                                action, ", ".join(server_names))
            else:
                logger.info("No servers found in %s.", hv_name)

    def main(self, args=None):
        self.parser.add_argument(
            "--action", required=True,
            help="Action to perfom on servers (lock/unlock)")
        self.parser.add_argument(
            "--hypervisors", required=True, help="List of hypervisors")

        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true',
                               help="If present, sends email")
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')

        args = self.parse_args(args)

        self.lock_unlock_hv_servers(
            self.cloudclient, args.exec_mode, args.action, args.hypervisors)


# Needs static method for setup.cfg
def main(args=None):
    LockUnlockHVServersCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
