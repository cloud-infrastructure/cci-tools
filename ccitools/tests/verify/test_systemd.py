import logging

from ccitools.errors import SSHException
from ccitools.verify.systemd import SystemDNeedReloadAction
from ccitools.verify.systemd import SystemDStateAction
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.verify.systemd.ssh_executor')
@mock.patch('ccitools.verify.base.ssh_executor')
class TestSystemDNeedReloadAction(TestCase):
    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.client = SystemDNeedReloadAction(host='fake.domain')
        self.client.delay = 0

    def test_try_fix(self, mock_common, mock_service):
        self.client.try_fix()
        mock_service.assert_called_with(
            'fake.domain',
            'systemctl daemon-reload'
        )

    def test_try_fix_raise(self, mock_common, mock_service):
        mock_service.side_effect = iter([SSHException()])
        self.client.try_fix()

    def test_check_alarm_state(self, mock_common, mock_service):
        mock_common.return_value = [], None
        self.assertFalse(self.client.check_alarm_state())

        mock_common.return_value = (
            ['fake.domain/another_alarm'], None)
        self.assertFalse(self.client.check_alarm_state())

        error_str = (
            'fake.domain/systemd-needreload/boolean-NeedDaemonReload')
        mock_common.return_value = ([error_str], None)
        self.assertTrue(self.client.check_alarm_state())

        mock_common.return_value = [], None
        self.assertFalse(self.client.check_alarm_state(after_try=True))


@mock.patch('ccitools.verify.systemd.ssh_executor')
@mock.patch('ccitools.verify.base.ssh_executor')
class TestSystemDStateAction(TestCase):
    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.client = SystemDStateAction(host='fake.domain')
        self.client.delay = 0

    def test_try_fix(self, mock_common, mock_service):
        self.client.try_fix()

    def test_check_alarm_state(self, mock_common, mock_service):
        mock_common.return_value = [], None
        self.assertFalse(self.client.check_alarm_state())

        mock_common.return_value = (
            ['fake.domain/another_alarm'], None)
        self.assertFalse(self.client.check_alarm_state())

        error_str = (
            'fake.domain/systemd-systemd_state/boolean-running')
        mock_common.return_value = ([error_str], None)
        self.assertTrue(self.client.check_alarm_state())

        mock_common.return_value = [], None
        self.assertFalse(self.client.check_alarm_state(after_try=True))
