from oslo_config import cfg


hosts = cfg.DictOpt(
    'hosts',
    default={
        "cern": "s3.cern.ch",
        "pdc": "s3-mu.cern.ch"
    },
    help="Hosts to use for s3")

ports = cfg.DictOpt(
    'ports',
    default={
        "cern": 443,
        "pdc": 443
    },
    help="Port to use for s3")

access_keys = cfg.DictOpt(
    'access_keys',
    default={
        "cern": "access_key",
        "pdc": "access_key"
    },
    help="access key for s3")

secret_keys = cfg.DictOpt(
    'secret_keys',
    default={
        "cern": "secret_key",
        "pdc": "secret_key"
    },
    help='secret key for s3'
)

aws_signatures = cfg.DictOpt(
    'aws_signatures',
    default={
        "cern": "AWS2",
        "pdc": "AWS2"
    },
    help='signature to use to communicate with the api'
)

is_secure = cfg.DictOpt(
    'is_secure',
    default={
        "cern": 'True',
        "pdc": 'True'
    },
    help='validate SSL certificates during request'
)


GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    hosts,
    ports,
    access_keys,
    secret_keys,
    aws_signatures,
    is_secure,
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
