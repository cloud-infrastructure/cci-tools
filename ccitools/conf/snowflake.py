from oslo_config import cfg


repository = cfg.StrOpt(
    'repository',
    default=('https://svcrdeck@gitlab.cern.ch:8443/'
             'cloud-infrastructure/cci-internal-docs.wiki.git'),
    help="Repository that contains the logbook with the snow weeks")

folder = cfg.StrOpt(
    'folder',
    default='/tmp/snowflake/',   # nosec
    help="Local folder where to clone the repository")

filename = cfg.StrOpt(
    'filename',
    default='logbook.md',
    help="Filename that contains the list of snow weeks")

message = cfg.StrOpt(
    'message',
    default=('Hello, this is a reminder for the rota week that starts '
             'on **%s**. The snowflake is **%s**. Good luck!'),
    help="Message to display in the notification")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    repository,
    folder,
    filename,
    message
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
