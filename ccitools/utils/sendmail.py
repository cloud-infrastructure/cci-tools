from __future__ import print_function

import logging
import smtplib
import time
import uuid

from datetime import datetime
from datetime import timedelta
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
from pathlib import Path
from pytz import timezone

logger = logging.getLogger(__name__)


class MailClient(object):

    def __init__(self, smtp_server='localhost'):
        self.smtp_server = smtp_server

    def __create_mail(self, mail_to, mail_subject, mail_body, mail_from,
                      mail_cc, mail_bcc, mail_content_type):
        msg = MIMEMultipart()
        msg['To'] = mail_to
        msg['From'] = mail_from
        msg['Cc'] = mail_cc
        msg['Bcc'] = mail_bcc
        msg['Subject'] = mail_subject
        msg.attach(MIMEText(mail_body, mail_content_type))
        return msg

    def send_mail(self, mail_to, mail_subject, mail_body, mail_from, mail_cc,
                  mail_bcc, mail_content_type, files=[]):
        logger.info("Sending email to '%s'...", mail_to)
        msg = self.__create_mail(mail_to, mail_subject, mail_body, mail_from,
                                 mail_cc, mail_bcc, mail_content_type)
        for path in files:
            part = MIMEBase('application', "octet-stream")
            with open(path, 'rb') as file:
                part.set_payload(file.read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition',
                            'attachment; filename={}'.format(Path(path).name))
            msg.attach(part)

        to_addrs = list(filter(None, [
            msg['To'],
            msg['Cc'],
            msg['Bcc']
        ]))

        smtpObj = smtplib.SMTP(self.smtp_server)
        smtpObj.sendmail(msg['From'], to_addrs, msg.as_string())
        smtpObj.quit()

    def __cap(self, s, entry):
        return s if len(s) <= entry else s[0:entry - 3] + '...'

    def set_valarm(self, trigger, description, duration="", repeat=""):
        """Quick method version that creates VALARM compontent for iCalendar.

        TRIGGER and DURATION properties use following time format:
        vD --> v days
        xH --> x hours
        yM --> y minutes
        zS --> z seconds
        """
        if trigger:
            if 'D' in trigger:
                trigger = "TRIGGER;VALUE=DURATION:-P%s\n" % trigger
            else:
                trigger = "TRIGGER;VALUE=DURATION:-PT%s\n" % trigger
        else:
            raise Exception("'trigger' property must be specified.")

        if repeat:
            if duration:
                repeat = 'REPEAT:%s\n' % repeat
                if 'D' in duration:
                    duration = 'DURATION:P%s\n' % duration
                else:
                    duration = 'DURATION:PT%s\n' % duration
            else:
                raise Exception("Both 'repeat' and 'duration' options must "
                                "be present when setting an alarm.")

        description = "DESCRIPTION:%s\n" % description
        alarm = ("BEGIN:VALARM\n"
                 + trigger
                 + repeat
                 + duration
                 + "ACTION:DISPLAY\n"
                 + description
                 + "END:VALARM")
        return alarm

    def __attach_ics(self, msg, organizer, attendees, title, description, date,
                     duration, alarm=""):

        tz = timezone('Europe/Amsterdam')
        attendees = attendees.split(",")
        title = self.__cap(title, 128)

        local_dt_start = tz.localize(
            datetime.fromtimestamp(
                time.mktime(
                    time.strptime(date, "%d-%m-%Y %H:%M"))))

        utc_dt_start = local_dt_start.astimezone(timezone('UTC'))
        delta = timedelta(hours=duration)
        utc_dt_end = utc_dt_start + delta
        utc_dt_stamp = datetime.now(timezone('UTC'))

        dtstamp = utc_dt_stamp.strftime("%Y%m%dT%H%M%SZ")
        dtstart = utc_dt_start.strftime("%Y%m%dT%H%M%SZ")
        dtend = utc_dt_end.strftime("%Y%m%dT%H%M%SZ")

        attendee = ""
        for att in attendees:
            attendee += "ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN=" + att + ":MAILTO:" + att + "\n"  # noqa

        ical = """BEGIN:VCALENDAR
PRODID:-//IT-OIS-CV//RUNDECK
VERSION:2.0
METHOD:REQUEST
BEGIN:VEVENT
DTSTART:%s
DTEND:%s
DTSTAMP:%s
ORGANIZER;CN="Cloud Service Automation":mailto:%s
UID:%s
%sCREATED:%s
DESCRIPTION:%s
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:%s
TRANSP:OPAQUE
%s
END:VEVENT
END:VCALENDAR""" % (dtstart, dtend, dtstamp, organizer, uuid.uuid4(), attendee,
                    dtstamp, description, title, alarm)

        msg.set_default_type('mixed')
        msg['Reply-To'] = organizer
        msg['Date'] = formatdate(localtime=False)

        eml_body = description

        part_email = MIMEText(eml_body, "html")
        part_cal = MIMEText(ical, 'calendar;method=REQUEST')

        ical_atch = MIMEBase('application/ics', ' ;name="%s"' % "invite.ics")
        ical_atch.set_payload(ical)
        encoders.encode_base64(ical_atch)
        ical_atch.add_header('Content-Disposition', 'attachment',
                             filename="%s" % ("invite.ics"))

        msg.attach(part_email)
        msg.attach(part_cal)
        msg.attach(ical_atch)

        return msg

    def send_mail_ics(self, mail_subject, mail_body,
                      mail_cc, mail_bcc, mail_content_type, organizer,
                      attendees, title, description, date, duration,
                      alarm):
        mail_to = attendees
        mail_from = organizer
        logger.info("Sending email with ICS to '%s'...", mail_to)
        msg = self.__create_mail(mail_to, mail_subject, mail_body, mail_from,
                                 mail_cc, mail_bcc, mail_content_type)

        msg_ics = self.__attach_ics(msg, organizer, attendees, title,
                                    description, date, duration, alarm)
        to_addrs = list(filter(None, [
            msg_ics['To'],
            msg_ics['Cc'],
            msg_ics['Bcc']
        ]))

        smtpObj = smtplib.SMTP(self.smtp_server)
        smtpObj.ehlo()
        smtpObj.sendmail(msg_ics['From'], to_addrs, msg_ics.as_string())
        smtpObj.close()
