import logging
import requests_mock

from ccitools.cmd.reload_nodes_from_puppetdb import main
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase


class TestReloadNodesFromPuppetDB(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):
        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)

    @requests_mock.Mocker()
    def test_no_hostgroup_nor_os(self, mock_req):
        mock_req.get(
            ('http://fake:8888/v1/facts/operatingsystem?query=%5B%22in%22%2C+'
             '%22certname%22%2C+%5B%22extract%22%2C+%22certname%22%2C+'
             '%5Bselect-facts%22%2C+%5B%22and%22%2C+%5B%22%3D%22%2C+'
             '%22name%22%2C+%22hostgroup%22%5D%2C+%5B%22~%22%2C+%22value'
             '%22%2C+%22fake_hostgroup%22%5D%5D%5D%5D%5D'),
            text='Not Found', status_code=404)

        mock_req.get(
            ('http://fake:8888/v1/facts/hostgroup?query=%5B%22in%22%2C+'
             '%22certname%22%2C+%5B%22extract%22%2C+%22certname%22%2C+'
             '%5Bselect-facts%22%2C+%5B%22and%22%2C+%5B%22%3D%22%2C+'
             '%22name%22%2C+%22hostgroup%22%5D%2C+%5B%22~%22%2C+'
             '%22value%22%2C+%22fake_hostgroup%22%5D%5D%5D%5D%5D'),
            text='Not Found', status_code=404)

        main(['--apiurl', 'http://fake:8888/v1',
              '--hostgroup', 'fake_hostgroup',
              '--output', '/tmp/output.txt'])

    @mock.patch('ccitools.cmd.reload_nodes_from_puppetdb.HTTPKerberosAuth')
    @requests_mock.Mocker()
    def test_hostgroups_and_os(self, mock_kerb, mock_req):
        mock_kerb.return_value = None
        mock_req.get(
            ('http://fake:8888/v1/facts/operatingsystem?query=%5B%22in%22%2C+'
             '%22certname%22%2C+%5B%22extract%22%2C+%22certname%22%2C+'
             '%5Bselect-facts%22%2C+%5B%22and%22%2C+%5B%22%3D%22%2C+'
             '%22name%22%2C+%22hostgroup%22%5D%2C+%5B%22~%22%2C+%22value'
             '%22%2C+%22fake_hostgroup%22%5D%5D%5D%5D%5D'),
            text=fixtures.OPERATING_SYSTEM_FACTS)

        mock_req.get(
            ('http://fake:8888/v1/facts/hostgroup?query=%5B%22in%22%2C+'
             '%22certname%22%2C+%5B%22extract%22%2C+%22certname%22%2C+'
             '%5Bselect-facts%22%2C+%5B%22and%22%2C+%5B%22%3D%22%2C+'
             '%22name%22%2C+%22hostgroup%22%5D%2C+%5B%22~%22%2C+'
             '%22value%22%2C+%22fake_hostgroup%22%5D%5D%5D%5D%5D'),
            text=fixtures.HOSTGROUP_FACTS)

        main(['--apiurl', 'http://fake:8888/v1',
              '--hostgroup', 'fake_hostgroup',
              '--output', '/tmp/output.txt'])

        mock_kerb.assert_called_with()
