import json
import logging

from ccitools.common import ssh_executor
from ccitools.verify import base


# configure logging
logger = logging.getLogger(__name__)


class PuppetAgentAction(base.ActionBase):

    def __init__(self, host):
        super(PuppetAgentAction, self).__init__(host)
        self.pattern = r'.*\/puppet\/puppet_time'

    def try_fix(self):
        """Implement the fix for the exception thrown in collectd."""
        pass

    def check_alarm_state(self, after_try=False):
        message = self.puppet_agent_disabled_message()
        if bool(message):
            self.reason = " - %s --> Puppet was manually disabled. " \
                          "Reason: '%s'\n" % (self.host, message)
        else:
            return super(PuppetAgentAction, self).check_alarm_state(after_try)
        return bool(message)

    def puppet_agent_disabled_message(self):
        out, err = ssh_executor(
            self.host,
            "cat $(puppet config print vardir)/state/agent_disabled.lock")
        if out:
            return json.loads(out[0])['disabled_message']
        return None


class PuppetAgentRunErrorsAction(base.ActionBase):

    def __init__(self, host):
        super(PuppetAgentRunErrorsAction, self).__init__(host)
        self.pattern = r'.*\/puppet\/resources-failed'

    def check_alarm_state(self, after_try=False):
        message = self.puppet_agent_disabled_message()
        if bool(message):
            self.reason = " - %s --> Puppet was manually disabled. " \
                          "Reason: '%s'\n" % (self.host, message)
        else:
            return super(PuppetAgentRunErrorsAction, self).check_alarm_state(
                after_try)
        return bool(message)

    def puppet_agent_disabled_message(self):
        out, err = ssh_executor(
            self.host,
            "cat $(puppet config print vardir)/state/agent_disabled.lock")
        if out:
            return json.loads(out[0])['disabled_message']
        return None

    def try_fix(self):
        # There is no fix for this alarm, ignoring it.
        pass


class PuppetAgentCannotCompileAction(base.ActionBase):
    def __init__(self, host):
        super(PuppetAgentCannotCompileAction, self).__init__(host)
        self.pattern = r'.*\/puppet\/boolean-compiled'

    def check_alarm_state(self, after_try=False):
        message = self.puppet_agent_disabled_message()
        if bool(message):
            self.reason = " - %s --> Puppet was manually disabled. " \
                          "Reason: '%s'\n" % (self.host, message)
        else:
            return super(
                PuppetAgentCannotCompileAction, self
            ).check_alarm_state(after_try)
        return bool(message)

    def puppet_agent_disabled_message(self):
        out, err = ssh_executor(
            self.host,
            "cat $(puppet config print vardir)/state/agent_disabled.lock")
        if out:
            return json.loads(out[0])['disabled_message']
        return None

    def try_fix(self):
        # There is no fix for this alarm, ignoring it.
        pass
