import logging

from ccitools.cmd.check_scheduled_rundeck_jobs import main
from unittest import TestCase


class TestAssignmentCleanup(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):
        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)
