from oslo_config import cfg

server = cfg.StrOpt(
    'server',
    default='https://monit-timberprivate.cern.ch:443/es',
    help="ES server")

username = cfg.StrOpt(
    'user',
    default='fake user',
    help="username to connect to ES")

password = cfg.StrOpt(
    'password',
    default='fake password',
    help="password to connect to ES")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    server,
    username,
    password
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
