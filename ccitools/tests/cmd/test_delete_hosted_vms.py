import logging

from ccitools.cmd import delete_hosted_vms
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.utils.cloud.cloud_config')
@mock.patch('ccitools.utils.cloud.CloudRegionClient.'
            'get_servers_by_hypervisor')
class TestDeleteHostedVMs(TestCase):
    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_machine_proper_data_perform(self, mock_cl, mock_cc):
        delete_hosted_vms.DELETION_TIMEOUT = 0
        delete_hosted_vms.DELETION_RETRY_INTERVAL = 0
        mock_cl.return_value = fixtures.DELETE_HOSTED_OUTPUT

        delete_hosted_vms.main([
            '--behaviour', 'perform',
            '--hosts', 'fake_host.cern.ch',
        ])

    def test_machine_proper_data_dryrun(self, mock_cl, mock_cc):
        delete_hosted_vms.DELETION_TIMEOUT = 0
        delete_hosted_vms.DELETION_RETRY_INTERVAL = 0
        mock_cl.return_value = []

        delete_hosted_vms.main([
            '--behaviour', 'dryrun',
            '--hosts', 'fake_host.cern.ch',
        ])

    def test_machine_side_effect(self, mock_cl, mock_cc):
        delete_hosted_vms.DELETION_TIMEOUT = 0
        delete_hosted_vms.DELETION_RETRY_INTERVAL = 0

        mock_cl.return_value = mock.MagicMock.side_effect = (
            iter([fixtures.DELETE_HOSTED_OUTPUT, 0])
        )

        delete_hosted_vms.main([
            '--behaviour', 'perform',
            '--hosts', 'fake_host.cern.ch',
        ])
