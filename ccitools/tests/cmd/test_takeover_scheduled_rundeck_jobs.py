import logging

from ccitools.cmd.takeover_scheduled_rundeck_jobs import main
from ccitools.errors import RundeckAPIError
from unittest import mock
from unittest import TestCase


class TestTakeoverScheduledRundeckJobs(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):
        with self.assertRaises(Exception):
            main(['--auth', 'sso'])

    @mock.patch('ccitools.cmd.base.rundeck.RundeckAPIClient')
    def test_token_auth(self, mock_cl):

        main(['--auth', 'token'])

        mock_cl.assert_called_with(
            port='443',
            server='cci-rundeck.cern.ch',
            token='secret_token',
            verify=False,
            api_version=20
        )
        mock_cl.return_value.takeover_schedule.assert_called_with()

    @mock.patch('ccitools.cmd.base.rundeck.RundeckAPIClient')
    def test_token_auth_error(self, mock_cl):
        mock_cl.return_value.takeover_schedule.side_effect = (
            iter([RundeckAPIError])
        )

        main(['--auth', 'token'])

        mock_cl.assert_called_with(
            port='443',
            server='cci-rundeck.cern.ch',
            token='secret_token',
            verify=False,
            api_version=20
        )
        mock_cl.return_value.takeover_schedule.assert_called_with()

    @mock.patch('ccitools.cmd.base.rundeck.auth_get_sso_cookie')
    @mock.patch('ccitools.cmd.base.rundeck.RundeckAPIClient')
    def test_cookie_auth(self, mock_cl, mock_cookie):
        mock_cookie.return_value = 'fake_cookie'

        main(['--auth', 'sso'])

        mock_cl.assert_called_with(
            port='443',
            server='cci-rundeck.cern.ch',
            sso_cookie='fake_cookie',
            verify=False,
            api_version=20
        )
        mock_cl.return_value.takeover_schedule.assert_called_with()

    @mock.patch('ccitools.cmd.base.rundeck.auth_get_sso_cookie')
    @mock.patch('ccitools.cmd.base.rundeck.RundeckAPIClient')
    def test_cookie_auth_error(self, mock_cl, mock_cookie):
        mock_cookie.return_value = 'fake_cookie'
        mock_cl.return_value.takeover_schedule.side_effect = (
            iter([RundeckAPIError])
        )

        main(['--auth', 'sso'])

        mock_cl.assert_called_with(
            port='443',
            server='cci-rundeck.cern.ch',
            sso_cookie='fake_cookie',
            verify=False,
            api_version=20
        )
        mock_cl.return_value.takeover_schedule.assert_called_with()
