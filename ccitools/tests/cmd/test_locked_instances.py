import logging

from ccitools.cmd.locked_instances import main
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase


class TestLockedInstances(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args_no_auth(self):
        with self.assertRaises(Exception):
            main([])

    def test_verbose_level_1(self):
        with self.assertRaises(Exception):
            main(['-v'])

    def test_verbose_level_2(self):
        with self.assertRaises(Exception):
            main(['-vv'])

    def test_verbose_level_3(self):
        with self.assertRaises(Exception):
            main(['-vvv'])

    def test_debug_level(self):
        with self.assertRaises(Exception):
            main(['--debug'])

    @mock.patch('ccitools.cmd.locked_instances.cloud_config')
    def test_default_args_raise_IOError(self, mock_cc):
        mock_cc.OpenStackConfig.side_effect = (
            iter([IOError])
        )

        with self.assertRaises(IOError):
            main([])

    @mock.patch('ccitools.cmd.locked_instances.cloud_config')
    @mock.patch('ccitools.cmd.locked_instances.keystone_session')
    @mock.patch('ccitools.cmd.locked_instances.nova')
    def test_no_servers(self, mock_nc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_nc.Client.return_value

        mock_client.servers.list.return_value = []

        main([])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'locked_by': 'admin'
            }
        )

    @mock.patch('ccitools.cmd.locked_instances.cloud_config')
    @mock.patch('ccitools.cmd.locked_instances.keystone_session')
    @mock.patch('ccitools.cmd.locked_instances.nova')
    def test_no_servers_json(self, mock_nc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_nc.Client.return_value

        mock_client.servers.list.return_value = []

        main(['--json'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'locked_by': 'admin'
            }
        )

    @mock.patch('ccitools.cmd.locked_instances.cloud_config')
    @mock.patch('ccitools.cmd.locked_instances.keystone_session')
    @mock.patch('ccitools.cmd.locked_instances.nova')
    def test_servers(self, mock_nc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_nc.Client.return_value

        mock_client.servers.list.return_value = (
            fixtures.LOCKED_INSTANCES_SERVERS)

        mock_client.instance_action.list.side_effect = (
            iter(fixtures.LOCKED_INSTANCES_ACTIONS))

        main([])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.servers.list.assert_called_with(
            search_opts={
                'all_tenants': True,
                'locked_by': 'admin'
            }
        )
