"""Fixture for tests."""
import datetime
import pytz

from ccitools.tests import fixtures as base_fixtures
from unittest import mock

from ccitools.utils.snow.ticket import RequestState  # py3

TICKET_EMPTY = {
    'number': 'INCXXXXXXX',
    'u_alarms_list': 'fake_alarm',
    'u_configuration_items_list': 'fake',
    'description': 'fake_description',
}

TICKETS_EMPTY = [
    base_fixtures.Resource(TICKET_EMPTY)
]

TICKET_FQDN = {
    'number': 'INCXXXXXXX',
    'u_alarms_list': 'fake_alarm',
    'u_configuration_items_list': 'fake.cern.ch',
    'description': 'fake_description',
}

TICKETS_FQDN = [
    base_fixtures.Resource(TICKET_FQDN)
]

COMMENTS_EMPTY = []

COMMENTS_GNIWEB = [
    {
        'sys_created_by': 'gniweb'
    }
]

COMMENTS_MULTIPLE = [
    {
        'sys_created_by': 'other'
    },
    {
        'sys_created_by': 'gniweb'
    }
]

TBAG_AI_CONF = u"""
[tbag]
tbag_hostname = fake_host
tbag_port = 8888
tbag_timeout = 60
"""

RUNDECK_SCHEDULER_OUTPUT = u'''
{"permalink": "here is a link"}
'''
PROJECT = {
    "id": "1",
    "name": "project-fake",
    "description": "fake-description",
    "enabled": True,
    "type": "service"
}

PROJECT_OUTPUT = base_fixtures.Resource(PROJECT)

NOVA_QUOTA = {
    'instances': 1,
    'cores': 1,
    'ram': 1024,
}

NOVA_QUOTA_OUTPUT = base_fixtures.Resource(NOVA_QUOTA)

CINDER_QUOTA = {
    'volumes': 6,
    'gigabytes': 6,
    'volumes_standard': 1,
    'gigabytes_standard': 1,
    'volumes_cp1': 1,
    'gigabytes_cp1': 1,
    'volumes_cpio1': 1,
    'gigabytes_cpio1': 1,
    'volumes_io1': 1,
    'gigabytes_io1': 1,
    'volumes_vault-100': 1,
    'gigabytes_vault-100': 1,
    'volumes_vault-500': 1,
    'gigabytes_vault-500': 1,
}

CINDER_QUOTA_OUTPUT = base_fixtures.Resource(CINDER_QUOTA)

MANILA_GLOBAL_QUOTA = {
    'gigabytes': 0,
    'share_networks': 10,
    'shares': 0,
    'snapshot_gigabytes': 0,
    'snapshots': 0,
}

MANILA_TESTING_QUOTA = {
    'gigabytes': 0,
    'share_networks': 10,
    'shares': 0,
    'snapshot_gigabytes': 0,
    'snapshots': 0,
}

MANILA_PROD_QUOTA = {
    'gigabytes': 0,
    'share_networks': 10,
    'shares': 0,
    'snapshot_gigabytes': 0,
    'snapshots': 0,
}

MANILA_QUOTA_OUTPUT = {
    'global': base_fixtures.Resource(MANILA_GLOBAL_QUOTA),
    'Geneva CephFS Testing': base_fixtures.Resource(MANILA_TESTING_QUOTA),
    'Meyrin CephFS': base_fixtures.Resource(MANILA_PROD_QUOTA),
}

NEUTRON_QUOTA_OUTPUT = {
    'subnet': 0,
    'network': 0,
    'security_group_rule': 4,
    'pool': 0,
    'subnetpool': -1,
    'listener': -1,
    'member': 0,
    'floatingip': 0,
    'security_group': 1,
    'router': 0,
    'port': 0,
    'loadbalancer': 10,
    'healthmonitor': -1
}

SERVER_DATA = {
    "id": "1",
    "name": "fake-server",
    "created": "2001-01-01T12:00:00Z",
    "tenant_id": "fake",
    "hostId": "fake",
    "status": "active",
    'updated': 'fake_update',
    "OS-EXT-STS:task_state": "fake",
    'OS-EXT-SRV-ATTR:host': "hypervisor1"
}

SERVER = mock.Mock(**SERVER_DATA)
type(SERVER).name = mock.PropertyMock(
    return_value=SERVER_DATA['name'])

SERVER_MIGRATING_DATA = {
    "id": "1",
    "name": "fake-server",
    "created": "2001-01-01T12:00:00Z",
    "tenant_id": "fake",
    "hostId": "fake",
    "status": "MIGRATING",
    'updated': 'fake_update',
    "OS-EXT-STS:task_state": "fake"
}

SERVER_MIGRATING = mock.Mock(**SERVER_MIGRATING_DATA)
type(SERVER_MIGRATING).name = mock.PropertyMock(
    return_value=SERVER_MIGRATING_DATA['name'])

VOLUME_DATA = {
    'id': '1',
    'name': 'fake-volume',
    'status': 'available',
    'created_at': '2001-01-01T12:00:00.000000',
    'os-vol-tenant-attr:tenant_id': None
}

VOLUME = mock.Mock(**VOLUME_DATA)
type(VOLUME).name = mock.PropertyMock(
    return_value=VOLUME_DATA['name'])

SERVICE = {
    'id': 'fake_id',
}

AGENT = {
    'id': 'fake_id',
}

BROKEN_SERVER_DATA = {
    'id': '2',
    'name': 'rally-fake2',
    'created': '2001-01-01T12:00:00Z',
    'tenant_id': '1',
    'hostId': '1',
    'status': 'error',
    'updated': 'fake_update',
    'OS-EXT-STS:task_state': None
}

BROKEN_SERVER = mock.Mock(**BROKEN_SERVER_DATA)
type(BROKEN_SERVER).name = mock.PropertyMock(
    return_value=BROKEN_SERVER_DATA['name'])

BROKEN_SERVER_CREATING_DATA = {
    'id': '3',
    'name': 'rally-fake3',
    'created': '2001-01-01T12:00:00Z',
    'tenant_id': '1',
    'hostId': '1',
    'status': 'creating',
    'updated': 'fake_update',
    'OS-EXT-STS:task_state': None
}

BROKEN_SERVER_CREATING = mock.Mock(**BROKEN_SERVER_CREATING_DATA)
type(BROKEN_SERVER_CREATING).name = mock.PropertyMock(
    return_value=BROKEN_SERVER_CREATING_DATA['name'])

BROKEN_SERVER_DELETING_DATA = {
    'id': '3',
    'name': 'rally-fake3',
    'created': '2001-01-01T12:00:00Z',
    'tenant_id': '1',
    'hostId': '1',
    'status': 'deleting',
    'updated': 'fake_update',
    'OS-EXT-STS:task_state': None
}

BROKEN_SERVER_DELETING = mock.Mock(**BROKEN_SERVER_DELETING_DATA)
type(BROKEN_SERVER_DELETING).name = mock.PropertyMock(
    return_value=BROKEN_SERVER_DELETING_DATA['name'])
BROKEN_SERVER_DELETING.reset_state.side_effect = Exception('error')

BROKEN_VOLUME_DATA = {
    'id': '2',
    'name': 'rally-fake2',
    'status': 'error',
    'created_at': '2001-01-01T12:00:00.000000',
    'os-vol-tenant-attr:tenant_id': None
}

BROKEN_VOLUME = mock.Mock(**BROKEN_VOLUME_DATA)
type(BROKEN_VOLUME).name = mock.PropertyMock(
    return_value=BROKEN_VOLUME_DATA['name']
)

BROKEN_VOLUME_CREATING_DATA = {
    'id': '3',
    'name': 'rally-fake3',
    'status': 'creating',
    'created_at': '2001-01-01T12:00:00.000000',
    'os-vol-tenant-attr:tenant_id': None
}

BROKEN_VOLUME_CREATING = mock.Mock(**BROKEN_VOLUME_CREATING_DATA)
type(BROKEN_VOLUME_CREATING).name = mock.PropertyMock(
    return_value=BROKEN_VOLUME_CREATING_DATA['name'])

BROKEN_VOLUME_DELETING_DATA = {
    'id': '3',
    'name': 'rally-fake3',
    'status': 'creating',
    'created_at': '2001-01-01T12:00:00.000000',
    'os-vol-tenant-attr:tenant_id': None
}

BROKEN_VOLUME_DELETING = mock.Mock(**BROKEN_VOLUME_DELETING_DATA)
type(BROKEN_VOLUME_DELETING).name = mock.PropertyMock(
    return_value=BROKEN_VOLUME_DELETING_DATA['name'])
BROKEN_VOLUME_DELETING.reset_state.side_effect = (
    Exception('error')
)

SHARE_DATA = {
    'id': '1',
    'name': 'fake-share',
    'status': 'available',
    'created_at': '2001-01-01T12:00:00.000000',
    'project_id': 'fake-project-id'
}

SHARE = mock.Mock(**SHARE_DATA)
type(SHARE).name = mock.PropertyMock(
    return_value=SHARE_DATA['name'])

BROKEN_SHARE_DATA = {
    'id': '2',
    'name': 'rally-fake2',
    'status': 'error',
    'created_at': '2001-01-01T12:00:00.000000',
    'project_id': 'fake-project-id'
}

BROKEN_SHARE = mock.Mock(**BROKEN_SHARE_DATA)
type(BROKEN_SHARE).name = mock.PropertyMock(
    return_value=BROKEN_SHARE_DATA['name']
)

BROKEN_SHARE_CREATING_DATA = {
    'id': '3',
    'name': 'rally-fake3',
    'status': 'creating',
    'created_at': '2001-01-01T12:00:00.000000',
    'project_id': 'fake-project-id'
}

BROKEN_SHARE_CREATING = mock.Mock(**BROKEN_SHARE_CREATING_DATA)
type(BROKEN_SHARE_CREATING).name = mock.PropertyMock(
    return_value=BROKEN_SHARE_CREATING_DATA['name'])

BROKEN_SHARE_DELETING_DATA = {
    'id': '3',
    'name': 'rally-fake3',
    'status': 'creating',
    'created_at': '2001-01-01T12:00:00.000000',
    'os-vol-tenant-attr:tenant_id': None
}

BROKEN_SHARE_DELETING = mock.Mock(**BROKEN_SHARE_DELETING_DATA)
type(BROKEN_SHARE_DELETING).name = mock.PropertyMock(
    return_value=BROKEN_SHARE_DELETING_DATA['name'])
BROKEN_SHARE_DELETING.reset_state.side_effect = (
    Exception('error')
)

KEYPAIR_DATA = {
    'id': '1',
    'name': 'fake-keypair',
    'created_at': '2001-01-01T12:00:00.000000'
}

LOADBALANCER_DATA = {
    'id': '1',
    'name': 'rally-fake1',
    'operating_status': 'ONLINE',
    'provisioning_status': 'ACTIVE',
    'created_at': '2001-01-01T12:00:00.000000'
}

LOADBALANCER = mock.Mock(**LOADBALANCER_DATA)
type(LOADBALANCER).name = mock.PropertyMock(
    return_value=LOADBALANCER_DATA['name'])

BROKEN_LOADBALANCER_DATA = {
    'id': '2',
    'name': 'rally-fake2',
    'operating_status': 'ERROR',
    'provisioning_status': 'ERROR',
    'created_at': '2001-01-01T12:00:00.000000'
}

BROKEN_LOADBALANCER = mock.Mock(**BROKEN_LOADBALANCER_DATA)
type(BROKEN_LOADBALANCER).name = mock.PropertyMock(
    return_value=BROKEN_LOADBALANCER_DATA['name']
)

BROKEN_LOADBALANCER_CREATING_DATA = {
    'id': '3',
    'name': 'rally-fake3',
    'operating_status': 'OFFLINE',
    'provisioning_status': 'PENDING_CREATE',
    'created_at': '2001-01-01T12:00:00.000000'
}

BROKEN_LOADBALANCER_CREATING = mock.Mock(**BROKEN_LOADBALANCER_CREATING_DATA)
type(BROKEN_LOADBALANCER_CREATING).name = mock.PropertyMock(
    return_value=BROKEN_LOADBALANCER_CREATING_DATA['name'])

BROKEN_LOADBALANCER_DELETING_DATA = {
    'id': '3',
    'name': 'rally-fake3',
    'operating_status': 'ACTIVE',
    'provisioning_status': 'PENDING_DELETE',
    'created_at': '2001-01-01T12:00:00.000000'
}

BROKEN_LOADBALANCER_DELETING = mock.Mock(**BROKEN_LOADBALANCER_DELETING_DATA)
type(BROKEN_LOADBALANCER_DELETING).name = mock.PropertyMock(
    return_value=BROKEN_LOADBALANCER_DELETING_DATA['name'])
BROKEN_LOADBALANCER_DELETING.reset_state.side_effect = (
    Exception('error')
)

KEYPAIR = mock.Mock(**KEYPAIR_DATA)
type(KEYPAIR).name = mock.PropertyMock(
    return_value=KEYPAIR_DATA['name'])

DELETE_HOSTED_SRV = mock.MagicMock()
DELETE_HOSTED_SRV.name.return_value = 'fake_name'
DELETE_HOSTED_SRV.server.return_value = 'fake_server'

DELETE_HOSTED_OUTPUT = [
    DELETE_HOSTED_SRV
]

SERVER_MANAGER = {
    "server": "fake_s",
    "name": "rally-fake",
    "OS-EXT-STS:vm_state": "stopped",
}

# Output need to be object() and not list[]

SERVER_MANAGER_OUTPUT = (
    base_fixtures.Resource(SERVER_MANAGER)
)

SERVER_MANAGER2 = {
    "server": "fake_s",
    "name": "rally-fake",
    "OS-EXT-STS:vm_state": "active",
}

# Output need to be object() and not list[]

SERVER_MANAGER_OUTPUT2 = (
    base_fixtures.Resource(SERVER_MANAGER2)
)

CATTLE_PROJECT = {
    "tenant_id": "fake_id",
    "accounting-group": "IT-Batch",
    'accountinggroup': 'fake',
    "name": "fake_name",
    "type": "compute",
}

CATTLE_PROJECT_OUTPUT = [
    base_fixtures.Resource(CATTLE_PROJECT)
]

CATTLE_PROJECT2 = {
    "tenant_id": "fake_id",
    "accounting-group": "fake",
    'accountinggroup': 'fake',
    "name": "Cloud Probe fake",
    "type": "fake",
}

CATTLE_PROJECT3 = {
    "tenant_id": "fake_id",
    "accounting-group": "fake",
    'accountinggroup': 'fake',
    "name": "fake",
    "type": "fake",
}
CATTLE_PROJECT_OUTPUT2 = base_fixtures.Resource(CATTLE_PROJECT)
CATTLE_PROJECT_OUTPUT3 = base_fixtures.Resource(CATTLE_PROJECT2)
CATTLE_PROJECT_OUTPUT4 = base_fixtures.Resource(CATTLE_PROJECT3)

FAKE_INSTANCE = {
    "id": "1",
    "name": "fake",
    "created": "2001-01-01T12:00:00Z",
    "tenant_id": "fake",
    "metadata": {
        'landb-responsible': 'fake_user',
        'landb-mainuser': 'fake_user',
    },
    "hostId": "fake",
    "status": "ACTIVE",
    "OS-EXT-STS:task_state": "active"
}

FAKE_INSTANCE_OUTPUT = [base_fixtures.Resource(FAKE_INSTANCE)]

FAKE_INSTANCE2 = {
    "id": "1",
    "name": "fake",
    "created": "2001-01-01T12:00:00Z",
    "tenant_id": "fake",
    "metadata": {
        'landb-mainuser': 'fake_user'
    },
    "hostId": "fake",
    "status": "ACTIVE",
    "OS-EXT-STS:task_state": "active",
    "user_id": "fake_id"
}

FAKE_INSTANCE_OUTPUT2 = [base_fixtures.Resource(FAKE_INSTANCE2)]

FAKE_INSTANCE3 = {
    "id": "1",
    "name": "fake",
    "created": "2001-01-01T12:00:00Z",
    "tenant_id": "fake",
    "metadata": {
        'landb-mainuser': 'fake_user'
    },
    "hostId": "fake",
    "status": "DELETED",
    "OS-EXT-STS:task_state": "active",
    "user_id": "fake_user"
}

FAKE_INSTANCE_OUTPUT3 = [base_fixtures.Resource(FAKE_INSTANCE3)]

FAKE_INSTANCE4 = {
    "id": "1",
    "name": "fake",
    "created": "2001-01-01T12:00:00Z",
    "tenant_id": "fake",
    "metadata": {
        'landb-mainuser': 'fake_user',
        'landb-responsible': 'fake_responsible'
    },
    "hostId": "fake",
    "status": "DELETED",
    "OS-EXT-STS:task_state": "active",
    "user_id": "driverab"
}

FAKE_INSTANCE_OUTPUT4 = [base_fixtures.Resource(FAKE_INSTANCE4)]

FAKE_HEALTH = {
    "OS-EXT-SRV-ATTR:host": "fake_host",
    "addresses": "1",
    "name": "fake_name",
    "updated": "2001-01-01T12:00:00Z",
    "fault": {'message': 'fake_m'},
    "status": "noBuild",
    "OS-EXT-STS:task_state": "fake_os",
    "OS-EXT-STS:vm_state": "fake_os",
    "user_id": "fake_id",
    "id": "fake_id",
}

FAKE_HEALTH_OUTPUT = [base_fixtures.Resource(FAKE_HEALTH)]

t1 = pytz.utc.localize(datetime.datetime.now())
t = t1.strftime("%Y-%b-%dT%H:%M:%SZ")

FAKE_HEALTH3 = {
    "OS-EXT-SRV-ATTR:host": "fake_host",
    "addresses": "",
    "name": "fake_name",
    "updated": t,
    "status": "BUILD",
    "OS-EXT-STS:task_state": "fake_os",
    "OS-EXT-STS:vm_state": "fake_os",
    "user_id": "fake_id",
}

FAKE_HEALTH_OUTPUT3 = [base_fixtures.Resource(FAKE_HEALTH3)]

FAKE_HEALTH4 = {
    "OS-EXT-SRV-ATTR:host": "",
    "addresses": "1",
    "name": "fake_name",
    "updated": t,
    "status": "BUILD",
    "OS-EXT-STS:task_state": "fake_os",
    "OS-EXT-STS:vm_state": "fake_os",
    "user_id": "fake_id",
    "id": "fake_id",
    "attachments": [{"server_id": "fake_ata"}],
}

FAKE_HEALTH_OUTPUT4 = [base_fixtures.Resource(FAKE_HEALTH4)]

FAKE_HEALTH5 = {
    "OS-EXT-SRV-ATTR:host": "",
    "addresses": "1",
    "name": "fake_name",
    "updated": t,
    "status": "BUILD",
    "OS-EXT-STS:task_state": "fake_os",
    "OS-EXT-STS:vm_state": "fake_os",
    "user_id": "fake_id",
    "id": "fake_id",
    "attachments": [],
}

FAKE_HEALTH_OUTPUT5 = [base_fixtures.Resource(FAKE_HEALTH5)]

RUNDECK_PROJECT_LIST_XML = u"""
<projects>
  <project url="fake_url">
    <name>fake_project</name>
    <description>fake_description</description>
    <!-- additional items -->
  </project>
</projects>
"""

RUNDECK_EXPORT_PROJECT = {
    'content': 'empty'
}

RUNDECK_EXPORT_PROJECT_JOBS = u"""
<joblist>
  <job scheduled="false" scheduleEnabled="false" enabled="true">
    <id>fake_job</id>
    <name>fake_job</name>
    <group>fake_group</group>
    <project>fake_project</project>
    <description>fake_description</description>
  </job>
</joblist>
"""

CLEANUP_PROJECTS = [
    base_fixtures.Resource({
        "id": "personal_id",
        "name": "Personal project"
    }),
    base_fixtures.Resource({
        "id": "personal_non_existing",
        "name": "Personal non_existing"
    }),
    base_fixtures.Resource({
        "id": "shared_id",
        "name": "Shared project"
    }),
]

CLEANUP_ROLES = [
    base_fixtures.Resource({
        "id": "owner_id",
        "name": "owner"
    }),
    base_fixtures.Resource({
        "id": "member_id",
        "name": "Member"
    })
]

CLEANUP_USERS = [
    base_fixtures.Resource({
        "id": "fake_user"
    })
]

CLEANUP_GROUPS = [
    base_fixtures.Resource({
        "id": "fake_group"
    })
]

CLEANUP_ASSIGNMENTS = [
    base_fixtures.Resource({
        "scope": {
            "project": {
                "id": "personal_non_existing"
            }
        },
        "role": {
            "id": "owner_id"
        },
        "user": {
            "id": "non_existing"
        },
    }),
    base_fixtures.Resource({
        "scope": {
            "domain": {
                "id": "default"
            }
        },
        "role": {
            "id": "member_id"
        },
        "user": {
            "id": "fake_user"
        },
    }),
    base_fixtures.Resource({
        "scope": {
            "project": {
                "id": "shared_id"
            }
        },
        "role": {
            "id": "owner_id"
        },
        "user": {
            "id": "fake_user"
        },
    }),
    base_fixtures.Resource({
        "scope": {
            "project": {
                "id": "shared_id"
            }
        },
        "role": {
            "id": "owner_id"
        },
        "user": {
            "id": "non_existing"
        },
    }),
    base_fixtures.Resource({
        "scope": {
            "project": {
                "id": "shared_id"
            }
        },
        "role": {
            "id": "member_id"
        },
        "user": {
            "id": "fake_user"
        },
    }),
    base_fixtures.Resource({
        "scope": {
            "project": {
                "id": "shared_id"
            }
        },
        "role": {
            "id": "member_id"
        },
        "user": {
            "id": "non_existing"
        },
    }),
    base_fixtures.Resource({
        "scope": {
            "project": {
                "id": "shared_id"
            }
        },
        "role": {
            "id": "member_id"
        },
        "group": {
            "id": "fake_group"
        },
    }),
    base_fixtures.Resource({
        "scope": {
            "project": {
                "id": "shared_id"
            }
        },
        "role": {
            "id": "member_id"
        },
        "group": {
            "id": "non_existing"
        },
    }),
    base_fixtures.Resource({
        "scope": {
            "project": {
                "id": "shared_id"
            }
        },
        "role": {
            "id": "member_id"
        }
    })
]

ACLS_CLEANUP_PROJECTS = [
    base_fixtures.Resource({
        "id": "personal_id1",
        "name": "Personal ok"
    }),
    base_fixtures.Resource({
        "id": "personal_id2",
        "name": "Personal with extra user"
    }),
    base_fixtures.Resource({
        "id": "personal_id3",
        "name": "Personal with extra group"
    }),
]

ACL_CLEANUP_ASSIGNMENTS = [
    base_fixtures.Resource({
        "scope": {
            "project": {
                "id": "personal_id2"
            }
        },
        "role": {
            "id": "member_id"
        },
        "user": {
            "id": "fake_user"
        },
    }),
    base_fixtures.Resource({
        "scope": {
            "project": {
                "id": "personal_id3"
            }
        },
        "role": {
            "id": "member_id"
        },
        "group": {
            "id": "fake_group"
        },
    }),
    base_fixtures.Resource({
        "scope": {
            "project": {
                "id": "personal_id1"
            }
        },
        "role": {
            "id": "member_id"
        }
    })
]

ITERATOR_PROJECTS = [
    base_fixtures.Resource({
        "id": "personal_id",
        "name": "Personal project"
    }),
    base_fixtures.Resource({
        "id": "shared_id",
        "name": "Shared project"
    }),
]

LOCKED_INSTANCES_SERVERS = [
    base_fixtures.Resource({
        'id': '1',
        'name': 'server-unlocked',
        'host_status': 'UP',
        'OS-EXT-SRV-ATTR:hypervisor_hostname': 'fake_hypervisor',
        'metadata': {},
    }),
    base_fixtures.Resource({
        'id': '2',
        'name': 'server-lock-admin',
        'host_status': 'UP',
        'OS-EXT-SRV-ATTR:hypervisor_hostname': 'fake_hypervisor',
        'metadata': {},
    }),
]

LOCKED_INSTANCES_ACTIONS = [
    [
        base_fixtures.Resource({
            'action': 'create',
            'message': '',
            'user_id': 'admin',
            'created_at': '2019-01-01T00:00:00.000000',
            'updated_at': '2019-01-01T00:00:00.000000',
        })
    ],
    [
        base_fixtures.Resource({
            'action': 'create',
            'message': '',
            'user_id': 'admin',
            'created_at': '2019-01-01T00:00:00.000000',
            'updated_at': '2019-01-01T00:00:00.000000',
        }),

        base_fixtures.Resource({
            'action': 'lock',
            'message': '',
            'user_id': 'admin',
            'created_at': '2019-01-02T00:00:00.000000',
            'updated_at': '2019-01-02T00:00:00.000000',
        })
    ]
]

OPERATING_SYSTEM_FACTS = u'''
[
    {
        "certname": "fake_host",
        "value": "fake_os"
    }
]
'''

HOSTGROUP_FACTS = u'''
[
    {
        "certname": "fake_host",
        "value": "fake_hostgroup"
    },
    {
        "certname": "fake_other",
        "value": "fake_hostgroup"
    }
]
'''

TRUSTS = [
    base_fixtures.Resource({
        'id': 'fake_id',
        'trustor_user_id': 'trustor_id',
        'trustee_user_id': 'trustee_id',
        'project_id': 'fake_project',
        'deleted_at': None,
        '_sa_instance_state': 'fake_state'
    }),
]

MAGNUM_CLUSTER_DATA = {
    "uuid": "c369aea6-8cd4-4cc8-b24e-fb4ba3a4d606",
    "name": "rally-abcd-1234",
    "status": "CREATE_COMPLETE",
    "status_reason": "",
    "created_at": "2000-01-01T00:00:00+00:00",
}

MAGNUM_CLUSTER = mock.Mock(**MAGNUM_CLUSTER_DATA)
type(MAGNUM_CLUSTER).name = mock.PropertyMock(
    return_value=MAGNUM_CLUSTER_DATA['name'])

MAGNUM_CLUSTER_DELETE_FAILED_DATA = {
    "uuid": "c369aea6-8cd4-4cc8-b24e-fb4ba3a4d606",
    "name": "rally-abcd-1234",
    "status": "DELETE_FAILED",
    "status_reason": "delete failed",
    "created_at": "2000-01-01T00:00:00+00:00",
}

MAGNUM_CLUSTER_DELETE_FAILED = mock.Mock(**MAGNUM_CLUSTER_DELETE_FAILED_DATA)
type(MAGNUM_CLUSTER_DELETE_FAILED).name = mock.PropertyMock(
    return_value=MAGNUM_CLUSTER_DELETE_FAILED_DATA['name'])

MAGNUM_CLUSTER_TEMPLATE_DATA = {
    "uuid": "677028cd-f797-42ba-b5ed-29b696e19ab1",
    "name": "rally-wxyz-7890",
    "created_at": "2000-01-01T00:00:00+00:00",
}
MAGNUM_CLUSTER_TEMPLATE = mock.Mock(**MAGNUM_CLUSTER_TEMPLATE_DATA)
type(MAGNUM_CLUSTER_TEMPLATE).name = mock.PropertyMock(
    return_value=MAGNUM_CLUSTER_TEMPLATE_DATA['name'])

DR_SERVERS_BFV = [
    base_fixtures.Resource({
        'id': 'fake_id1',
        'name': 'fake_vm1',
        'tenant_id': 'fake_project_id',
        'image': '',
        'status': 'ACTIVE',
        'OS-EXT-SRV-ATTR:host': 'fake_host',
        'OS-EXT-AZ:availability_zone': 'fake_avz_zone',
        'OS-EXT-SRV-ATTR:instance_name': 'fake_instance_name',
        'os-extended-volumes:volumes_attached': [
            {
                'id': "fake_volume_id"
            }
        ]
    }),
]

DR_SERVERS_BFI = [
    base_fixtures.Resource({
        'id': 'fake_id1',
        'name': 'fake_vm1',
        'tenant_id': 'fake_project_id',
        'image': {
            'id': 'fake_image_id'
        },
        'status': 'ACTIVE',
        'OS-EXT-SRV-ATTR:host': 'fake_host',
        'OS-EXT-AZ:availability_zone': 'fake_avz_zone',
        'OS-EXT-SRV-ATTR:instance_name': 'fake_instance_name',
    }),
]

DR_PROJECTS = [
    base_fixtures.Resource({
        'id': 'fake_project_id',
        'name': 'fake_project_name',
        'type': 'service'
    }),
]

DR_IMAGE = base_fixtures.Resource({
    'id': 'fake_image_id',
    'name': 'fake_image_name'
})

DR_VOLUME = base_fixtures.Resource({
    'id': 'fake_volume_id',
    'attachments': [
        {
            'device': '/dev/vda'
        }
    ]
})

DR_VOLUME_METADATA = base_fixtures.Resource({
    'id': 'fake_volume_id',
    'attachments': [
        {
            'device': '/dev/vda'
        }
    ],
    'volume_image_metadata': {
        'image_name': 'fake_image_name'
    }
})

CREATE_PROJECT_NOTSYNC = base_fixtures.Resource({
    "id": "1",
    "name": "project-fake",
    "fim-sync": 'False'
})

CREATE_PROJECT_SYNC = base_fixtures.Resource({
    "id": "1",
    "name": "project-fake",
    "fim-sync": 'True'
})

CHARGEGROUPS = """
{
    "data": [
        {
            "uuid": "4714e675-60b9-4fc7-80b2-61acd213f478",
            "name": "charge_group",
            "active": "True",
            "category": "IT",
            "org_unit": "IT-CM-RPS",
            "admins": []
        }
    ]
}"""

USER_CHARGEGROUPS_MAPPING = """
{
    "USER1": {
        "charge_group": "charge_group",
        "owner": "USER2",
        "type": " department"
    },
    "USER2": {
        "charge_group": "charge_group",
        "owner": "USER1",
        "type": " department"
    }
}"""

ACCOUNTING_PROJECTS = [
    base_fixtures.Resource({
        'id': 'fake_project_id1',
        'name': 'Personal USER1',
        'type': 'personal'
    }),
    base_fixtures.Resource({
        'id': 'fake_project_id2',
        'name': 'Personal USER2',
        'type': 'personal',
        'chargegroup': '4714e675-60b9-4fc7-80b2-61acd213f478'
    }),
]

SNOW_CREATE_RP = mock.MagicMock(
    project_name='project',
    egroup='fake@cern.ch',
    description='description',
    owner='fake',
    chargegroup='charge_group',
    chargerole='charge_role',
    metadata="{quota:{}}"
)

SNOW_CREATE_RP_UUID = mock.MagicMock(
    project_name='project',
    egroup='fake@cern.ch',
    description='description',
    owner='fake',
    chargegroup='4714e675-60b9-4fc7-80b2-61acd213f478',
    chargerole='charge_role',
    metadata="{quota:{}}"
)

SNOW_UPDATE_RP = mock.MagicMock(
    project_name='project',
    chargegroup='charge_group',
    chargerole='charge_role',
    metadata="{quota:{}}",
    request=mock.MagicMock(
        info=mock.MagicMock(
            u_current_task_state=RequestState.RESOLVED,
            opened_at='2024-07-11 09:00:00',
            u_resolved_at='2024-07-11 10:00:00',
            number='RQF123000'
        )
    )
)

SNOW_DELETE_RP = mock.MagicMock(
    project_name='project',
)

JIRA_NO_TICKETS = '''
{
    "issues": []
}
'''

JIRA_TICKETS = '''
{
    "issues": [
        {
            "expand": "operations,versionedRepresentations,editmeta,changelog,renderedFields",
            "fields": {
                "status": {
                    "description": "The issue is open and ready for the assignee to start work on it.",
                    "iconUrl": "https://its.cern.ch/jira/images/icons/statuses/open.png",
                    "id": "1",
                    "name": "Open",
                    "self": "https://its.cern.ch/jira/rest/api/2/status/1",
                    "statusCategory": {
                        "colorName": "blue-gray",
                        "id": 2,
                        "key": "new",
                        "name": "To Do",
                        "self": "https://its.cern.ch/jira/rest/api/2/statuscategory/2"
                    }
                },
                "summary": "This is a fake ticket",
                "updated": "2019-09-17T16:11:57.000+0200"
            },
            "id": "1111",
            "key": "OS-1111",
            "self": "https://its.cern.ch/jira/rest/api/2/issue/1111"
        }
    ]
}
'''  # noqa

MERGE_QUOTA_OUTPUT = {
    'quota': {
        'production': {
            'compute': {
                'instances': 5,
                'cores': 10,
                'ram': 20,
            },
            'blockstorage': {
                'standard': {
                    'volumes': 1,
                    'gigabytes': 2,
                }
            },
            'fileshare': {
                'production': {
                    'shares': 1,
                    'gigabytes': 2,
                }
            },
            'object': {
                'buckets': 1,
                'gigabytes': 2,
            },
            'network': {
                'loadbalancers': 2,
            }
        }
    }
}

CORE_METRICS = {
    'statement_id': 0,
    'series': [
        {
            'name': 'cores_usage_per_chargegroup_and_chargerole_and_type',
            'tags': {
                'chargegroup': '4714e675-60b9-4fc7-80b2-61acd213f478',
                'chargerole': 'default',
                'type': 'virtual',
            },
            'columns': [
                'time',
                'usage'
            ],
            'values': [
                [
                    '2021-09-28T00:00:00Z',
                    16.0
                ]
            ]
        }
    ]
}

VOLUME_METRICS = {
    'statement_id': 0,
    'series': [
        {
            'name': ('volumes_gb_quota_per_type_'
                     'per_chargegroup_and_chargerole'),
            'tags': {
                'chargegroup': '4714e675-60b9-4fc7-80b2-61acd213f478',
                'chargerole': 'default',
                'type': 'standard',
            },
            'columns': [
                'time',
                'usage'
            ],
            'values': [
                [
                    '2021-09-28T00:00:00Z',
                    1.0
                ]
            ]
        }
    ]
}

FILESHARE_METRICS = {
    'statement_id': 0,
    'series': [
        {
            'name': ('fileshares_gb_quota_per_type_'
                     'per_chargegroup_and_chargerole'),
            'tags': {
                'chargegroup': '4714e675-60b9-4fc7-80b2-61acd213f478',
                'chargerole': 'default',
                'type': 'Meyrin CephFS',
            },
            'columns': [
                'time',
                'usage'
            ],
            'values': [
                [
                    '2021-09-28T00:00:00Z',
                    2.0
                ]
            ]
        }
    ]
}

ACCOUNTING_RECEIVER_RESPONSE = """
{
    "message": {
        "stored": "1 new records in S3",
        "filtered": "0 records filtered out"
    },
    "details": {}
}
"""

PLACEMENT_ALLOCATION = """
{
    "hits": {
        "hits": [
            {
                "_source": {
                    "data": {
                        "raw": "Instance d92a5cdf-d8d0-4ac3-8911-ca595e44b38c has been moved to another host i88681451848578.cern.ch(i88681451848578.cern.ch). There are allocations remaining against the source host that might need to be removed: {'resources': {'VCPU': 16, 'MEMORY_MB': 30000, 'DISK_GB': 160}}."
                    }
                }
            }
        ]
    }
}
"""  # noqa

PROBE_ALLOCATION = """
{
    "hits": {
        "hits": [
            {
                "_source": {
                    "data": {
                        "raw": "Instance ac55ff16-a14a-433d-b926-0a777b865756 has allocations against this compute host but is not found in the database."
                    }
                }
            }
        ]
    }
}
"""  # noqa

ALLOCATIONS = {
    "regionone": {
        "allocations": {
            "b203eae5-7596-483b-8265-2203f990fb18": {
                "resources": {
                    "VCPU": 4,
                    "MEMORY_MB": 7500,
                    "DISK_GB": 40
                },
                "generation": 29011
            }
        },
        "project_id": "c34de133-1e4c-4a7f-b7b1-97fadb07ad61",
        "user_id": "svcprobe"
    }
}

PROJECT_LIST = [
    base_fixtures.Resource({
        "id": "1",
        "name": "project-fake-1",
    }),
    base_fixtures.Resource({
        "id": "2",
        "name": "project-fake-2",
    }),
]

ROLE_LIST = [
    base_fixtures.Resource({
        "id": "1",
        "name": "owner",
    }),
    base_fixtures.Resource({
        "id": "2",
        "name": "Member",
    }),
    base_fixtures.Resource({
        "id": "3",
        "name": "coordinator",
    }),
]

AGGREGATE_LIST = [
    base_fixtures.Resource({
        'metadata': {
            'cell': 'test_shared_001'
        }
    }),
    base_fixtures.Resource({
        'metadata': {
            'cell': 'test_shared_002'
        }
    }),
    base_fixtures.Resource({
        'metadata': {}
    })
]

FOREMAN_HOSTS_CELL = {
    'test_shared_001': [
        {'name': 'i01.cern.ch'}
    ],
    'test_shared_002': [
        {'name': 'i02.cern.ch'}
    ]
}

SERVERS_HOST = {
    'i01.cern.ch': [
        base_fixtures.Resource({
            'id': 'id-11',
            'name': 'vm-fake-11',
            'tenant_id': '1',
            'image': 'image-1',
            'status': 'ACTIVE',
            'OS-EXT-STS:task_state': 'none',
            'OS-EXT-STS:power_state': 1,
            'flavor': {'original_name': 'flavor-large-01'},
            'OS-EXT-SRV-ATTR:host': 'i01.cern.ch'
        })
    ],
    'i02.cern.ch': [
        base_fixtures.Resource({
            'id': 'id-21',
            'name': 'vm-fake-21',
            'tenant_id': '2',
            'image': '',
            'status': 'ACTIVE',
            'OS-EXT-STS:task_state': 'none',
            'OS-EXT-STS:power_state': 1,
            'flavor': {'original_name': 'c.02'},
            'OS-EXT-SRV-ATTR:host': 'i02.cern.ch'
        })
    ]
}

UPGRADE_REPORT_FILLED = """+-----------------+-------+------------+--------+----------------+-----------------+-------+-------------+----------+------+----------+
| cell            | id    | name       | status | project        | flavor          | bfv   | host        | eds      | cold | attempts |
+-----------------+-------+------------+--------+----------------+-----------------+-------+-------------+----------+------+----------+
| test_shared_001 | id-11 | vm-fake-11 | ACTIVE | project-fake-1 | flavor-large-01 | False | i01.cern.ch | 161.0 GB | **   |        0 |
| test_shared_002 | id-21 | vm-fake-21 | ACTIVE | project-fake-2 | c.02            | True  | i02.cern.ch | N/A      |      |        0 |
+-----------------+-------+------------+--------+----------------+-----------------+-------+-------------+----------+------+----------+""" # noqa
