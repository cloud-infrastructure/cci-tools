cci-tools
=========


[![pipeline status](https://gitlab.cern.ch/cloud-infrastructure/cci-tools/badges/master/pipeline.svg)](https://gitlab.cern.ch/cloud-infrastructure/cci-tools/-/commits/master)
[![coverage report](https://gitlab.cern.ch/cloud-infrastructure/cci-tools/badges/master/coverage.svg)](https://gitlab.cern.ch/cloud-infrastructure/cci-tools/-/commits/master)

CERN Cloud Infrastructure Tools (aka `cci-tools`) provides a set of Python
scripts and libraries following good software & coding practices, ensuring as
much as possible the reusability of the code.


Prerequisites
-------------
Please install these packages to start development on this library

### RedHat/CentOs
```
$ sudo yum install gcc python-devel python3-devel python-tox openldap-devel krb5-libs
```

### Debian/Ubuntu
```
$ sudo apt-get install gcc libsasl2-dev python-dev python3-dev python-tox libldap2-dev libssl-dev libkrb5-dev krb5-config
```

Release Management
------------------

### Changelog

In general, all the merge requests done in the repository should cover a new
feature, bug fix, etc... triggering a new version of the package.

This implies that every new feature should:

* Add a new entry in `cci-tools.spec`.
* Bump the version in `cci-tools.spec`.

### Tests

Tests can be run with:

```bash
$ cd path/to/cci-tools
$ tox
```

### Add a new command into cci-tools

In order to create a new command ccitools, please follow the steps:

1. Create a new class under ccitools/cmd.
   You can inherit from the BaseCMD class that has some basic argument parser for logging.

2. Once created, the file containing the class should have a main method like 
```python
def main(args=None):
    MyNewCMD().main(args)

if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
```

3. Add the entry of your new command in setup.cfg so it gets installed by pip in the environment
```
[entry_points]
console_scripts =
    cci-my-new-cmd            = ccitools.cmd.my_new_cmd:main
```

4. To run the command log in one of the development, you can use the venv environment that will
   install the requirements of your command.
```bash
$ tox -evenv /bin/bash
cci-my-new-cmd
```
