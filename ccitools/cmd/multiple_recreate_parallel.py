#!/usr/bin/python3

import ccitools.conf
import json
import logging
import sys
import time

from ccitools.cmd.base.rundeck import BaseRundeckCMD


# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF

DELETION_TIMEOUT = 3600
DELETION_RETRY_INTERVAL = 120


class MultipleRecreateParallel(BaseRundeckCMD):
    def __init__(self):
        super(MultipleRecreateParallel, self).__init__(
            description=("Launch migrations in parallel using the API"))

    def main_decommission(self, rdeckclient, args):
        vm_names = args.vms.split()
        logger.info("Starting '%s' pararell jobs to migrate "
                    "the following Hyper-V servers: %s",
                    len(vm_names),
                    ', '.join(vm_names))
        logger.info("VMs will be migrated in batches of '%s' "
                    "with a delay between them of '%s' seconds",
                    args.parallel,
                    args.delay)
        startat = 0
        while (startat <= len(vm_names)):
            vms_batch = vm_names[startat:startat + int(args.parallel)]
            if vms_batch:
                for vm_name in vms_batch:
                    logger.info("Starting migration of '%s'...", vm_name)
                    if args.new_project_id is not None:
                        output = rdeckclient.run_job_by_id(
                            args.job_id,
                            parameters={
                                'vm_name': args.vm_name,
                                'recreate_from': args.recreate,
                                'new_project_id': args.new_project_id,
                                'volume_type': args.volume_type,
                                'sysprep': args.sysprep,
                                'sysprep_params': args.sysprep_params,
                                'sysprep_snapshot': args.sysprep_snapshot})
                    else:
                        output = rdeckclient.run_job_by_id(
                            args.job_id,
                            parameters={
                                'vm_name': args.vm_name,
                                'recreate_from': args.recreate,
                                'volume_type': args.volume_type,
                                'sysprep': args.sysprep,
                                'sysprep_params': args.sysprep_params,
                                'sysprep_snapshot': args.sysprep_snapshot})
                    json_out = json.loads(output)
                    logger.info("Follow the execution here: %s",
                                json_out['permalink'])

                startat += int(args.parallel)
                if startat < len(vm_names):
                    logger.info("Sleeping for '%s' seconds before launching "
                                "the next batch of Migrations",
                                args.delay)
                    time.sleep(args.delay)
                else:
                    logger.info("No more VMs to migrate :)")
                    break

    def main(self, args=None):
        self.parser.add_argument(
            '--vms',
            required=True,
            help='List of servers')
        self.parser.add_argument(
            '--recreate-from',
            default='-',
            choices=['-', 'volume', 'image'])
        self.parser.add_argument(
            '--delay',
            type=int,
            default=1,
            help='Delay between executions')
        self.parser.add_argument(
            '--parallel',
            type=int,
            default=2,
            help='Parallel execution')
        self.parser.add_argument(
            '--job-id',
            help='Job ID')
        self.parser.add_argument(
            '--new_project_id',
            default='',
            nargs="?")
        self.parser.add_argument(
            '--volume_type',
            default='standard')
        self.parser.add_argument(
            '--sysprep',
            default='no',
            choices=['no', 'yes'])
        self.parser.add_argument(
            '--sysprep_params',
            help='sysprep parameters')
        self.parser.add_argument(
            '--sysprep_snapshot',
            default='no',
            choices=['no', 'yes'])
        self.parser.add_argument(
            '--auth-method',
            choices=['token', 'sso'])

        args = self.parse_args(args)

        self.main_decommission(self.rundeckcli, args)


# Needs static method for setup.cfg
def main(args=None):
    MultipleRecreateParallel().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)
