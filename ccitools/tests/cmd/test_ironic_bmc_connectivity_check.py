from ccitools.cmd.ironic_bmc_connectivity_check import BmcConnectivityCheckCMD
import logging
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.utils.cloud.cloud_config')
@mock.patch('ccitools.cmd.ironic_bmc_connectivity_check.ServiceNowClient')
@mock.patch('ccitools.cmd.ironic_bmc_connectivity_check.logging.Logger.error')
@mock.patch('ccitools.cmd.ironic_bmc_connectivity_check.logging.Logger.info')
@mock.patch('ccitools.utils.sendmail.MailClient.send_mail')
@mock.patch('ccitools.cmd.ironic_bmc_connectivity_check.requests.get')
@mock.patch('ccitools.utils.cloud.CloudRegionClient.get_baremetal_node_by_id')
@mock.patch('ccitools.cmd.ironic_bmc_connectivity_check.socket.gethostbyaddr')
@mock.patch('ccitools.cmd.ironic_bmc_connectivity_check.socket.gethostbyname')
class TestBmcConnectivityCheckCMD(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_one_broken_node_ticket_exists(self, mock_get_ip, mock_get_host,
                                           mock_get_name, mock_requests,
                                           mock_mail, mock_info,
                                           mock_error, mock_snow, mock_cc):
        kibana_log = """{"hits": {"hits": [{"_source": {"data": { "raw": \
"During sync_power_state, could not get power state for node fake_uuid, \
attempt 10 of 10. Error: IPMI call failed: power status.."}}}]}}"""
        mock_response = mock_requests.return_value
        type(mock_response).text = mock.PropertyMock(
            return_value=kibana_log)
        mock_node = mock_get_name.return_value
        type(mock_node).name = mock.PropertyMock(
            return_value='fake_name')
        type(mock_node).power_state = mock.PropertyMock(
            return_value=None)
        mock_get_ip.return_value = 'fake_ip'
        mock_get_host.return_value = 'fake_device_name', None, None
        mock_snow.return_value.ticket.raw_query_incidents.return_value = [{
            'number': 'fake_ticket'}]

        bcc = BmcConnectivityCheckCMD()
        bcc.main([])

        mock_get_name.assert_called_with('fake_uuid')
        call_1 = mock.call("Broken nodes are: %s", {'fake_name'})
        call_2 = mock.call("Getting the device names")
        call_3 = mock.call("Getting snow tickets related to %s",
                           'fake_device_name')
        call_4 = mock.call("Snow tickets already exist for %s",
                           'fake_device_name')
        call_5 = mock.call("Sending email")
        mock_info.assert_has_calls([call_1, call_2, call_3, call_4, call_5])
        mock_mail.assert_called_once()
        mock_error.assert_not_called()
        mock_snow.return_value.ticket.create_INC.assert_not_called()

    def test_one_broken_node_no_ticket(self, mock_get_ip, mock_get_host,
                                       mock_get_name, mock_requests,
                                       mock_mail, mock_info, mock_error,
                                       mock_snow, mock_cc):
        kibana_log = """{"hits": {"hits": [{"_source": {"data": { "raw": \
"During sync_power_state, could not get power state for node fake_uuid, \
attempt 10 of 10. Error: IPMI call failed: power status.."}}}]}}"""
        mock_response = mock_requests.return_value
        type(mock_response).text = mock.PropertyMock(
            return_value=kibana_log)
        mock_node = mock_get_name.return_value
        type(mock_node).name = mock.PropertyMock(
            return_value='fake_name')
        type(mock_node).power_state = mock.PropertyMock(
            return_value=None)
        mock_get_ip.return_value = 'fake_ip'
        mock_get_host.return_value = 'fake_device_name', None, None
        mock_snow.return_value.ticket.raw_query_incidents.return_value = False

        mock_inc = mock_snow.return_value.ticket.create_INC.return_value
        type(mock_inc).info = mock.PropertyMock(return_value=mock.MagicMock())
        type(type(mock_inc).info).number = mock.PropertyMock(
            return_value='fake_inc')

        bcc = BmcConnectivityCheckCMD()
        bcc.main([])

        mock_get_name.assert_called_with('fake_uuid')
        call_1 = mock.call("Broken nodes are: %s", {'fake_name'})
        call_2 = mock.call("Getting the device names")
        call_3 = mock.call("Getting snow tickets related to %s",
                           'fake_device_name')
        call_4 = mock.call("Opening a SNOW ticket for %s",
                           'fake_device_name')
        call_5 = mock.call("Created incident number: %s", 'fake_inc')
        call_6 = mock.call("Sending email")
        mock_info.assert_has_calls([call_1, call_2, call_3,
                                    call_4, call_5, call_6])
        mock_mail.assert_called_once()
        mock_error.assert_not_called()
        mock_snow.return_value.ticket.create_INC.assert_called_once()

    def test_some_broken_nodes(self, mock_get_ip, mock_get_host,
                               mock_get_name, mock_requests,
                               mock_mail, mock_info, mock_error,
                               mock_snow, mock_cc):
        kibana_log = """{"hits":{"hits":[{"_source":{"data":{"raw":"During \
sync_power_state, could not get power state for node fake_uuid_1, attempt 10 \
of 10. Error: IPMI call failed: power status.."}}},{"_source":{"data":{"raw":\
"During sync_power_state, could not get power state for node fake_uuid_2, \
attempt 10 of 10. Error: IPMI call failed: power status.."}}},{"_source":\
{"data":{"raw":"During sync_power_state, could not get power state for node \
fake_uuid_3, attempt 10 of 10. Error: IPMI call failed: power status.."}}}]}}
        """
        mock_response = mock_requests.return_value
        type(mock_response).text = mock.PropertyMock(
            return_value=kibana_log)

        mock_node = mock_get_name.return_value
        type(mock_node).name = mock.PropertyMock(
            side_effect=['fake_name1', 'fake_name3'])
        type(mock_node).power_state = mock.PropertyMock(
            side_effect=[None, 'On', None])

        mock_get_ip.return_value = 'fake_ip'
        mock_get_host.return_value = 'fake_device_name', None, None
        mock_snow.return_value.ticket.raw_query_incidents.return_value = [
            {'number': 'fake_ticket'}]

        bcc = BmcConnectivityCheckCMD()
        bcc.main([])

        calls = [
            mock.call('fake_uuid_1'), mock.call('fake_uuid_2'),
            mock.call('fake_uuid_3')]
        mock_get_name.assert_has_calls(calls, any_order=True)

        call_1 = mock.call("Broken nodes are: %s",
                           {'fake_name1', 'fake_name3'})
        call_2 = mock.call("Getting the device names")
        call_3 = mock.call("Getting snow tickets related to %s",
                           'fake_device_name')
        call_4 = mock.call("Snow tickets already exist for %s",
                           'fake_device_name')
        call_5 = mock.call("Sending email")
        mock_info.assert_has_calls([call_1, call_2, call_3, call_4,
                                    call_3, call_4, call_5])
        mock_mail.assert_called_once()
        mock_error.assert_not_called()
        mock_snow.return_value.ticket.create_INC.assert_not_called()

    def test_no_broken_nodes(self, mock_get_ip, mock_get_host, mock_get_name,
                             mock_requests, mock_mail, mock_info,
                             mock_error, mock_snow, mock_cc):
        kibana_log = """{"hits":{"hits":[]}}"""

        mock_response = mock_requests.return_value
        type(mock_response).text = mock.PropertyMock(
            return_value=kibana_log)

        bcc = BmcConnectivityCheckCMD()
        bcc.main([])

        mock_get_name.assert_not_called()
        mock_info.assert_has_calls([mock.call('No broken nodes'),
                                    mock.call('Sending email')])
        mock_mail.assert_called_once()
        mock_error.assert_not_called()
        mock_get_ip.assert_not_called()
        mock_get_host.assert_not_called()
        mock_snow.return_value.ticket.create_INC.assert_not_called()

    def test_wrong_node_uuid(self, mock_get_ip, mock_get_host, mock_get_name,
                             mock_requests, mock_mail, mock_info,
                             mock_error, mock_snow, mock_cc):
        kibana_log = """{"hits": {"hits": [{"_source": {"data": { "raw": \
"During sync_power_state, could not get power state for node , \
attempt 10 of 10. Error: IPMI call failed: power status.."}}}]}}"""
        mock_response = mock_requests.return_value
        type(mock_response).text = mock.PropertyMock(
            return_value=kibana_log)

        bcc = BmcConnectivityCheckCMD()
        bcc.main([])

        mock_get_name.assert_not_called()
        mock_info.assert_has_calls([mock.call('No broken nodes'),
                                    mock.call('Sending email')])
        mock_mail.assert_called_once()
        mock_error.assert_called_with(
            "Can not retrieve node uuid from the log")
        mock_get_ip.assert_not_called()
        mock_get_host.assert_not_called()
        mock_snow.return_value.ticket.create_INC.assert_not_called()

    def test_retrieve_node_name_error(self, mock_get_ip, mock_get_host,
                                      mock_get_name, mock_requests, mock_mail,
                                      mock_info, mock_error,
                                      mock_snow, mock_cc):
        kibana_log = """{"hits": {"hits": [{"_source": {"data": { "raw": \
"During sync_power_state, could not get power state for node fake_uuid, \
attempt 10 of 10. Error: IPMI call failed: power status.."}}}]}}"""
        mock_response = mock_requests.return_value
        type(mock_response).text = mock.PropertyMock(
            return_value=kibana_log)
        mock_get_name.return_value = None

        bcc = BmcConnectivityCheckCMD()
        bcc.main([])

        mock_get_name.assert_called_with('fake_uuid')
        mock_info.assert_has_calls([mock.call('No broken nodes'),
                                    mock.call('Sending email')])
        mock_mail.assert_called_once()
        mock_error.assert_called_with(
            "Can not retrieve node's name by uuid: %s", 'fake_uuid')
        mock_get_ip.assert_not_called()
        mock_get_host.assert_not_called()
        mock_snow.return_value.ticket.create_INC.assert_not_called()
