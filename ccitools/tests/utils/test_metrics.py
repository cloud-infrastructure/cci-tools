
from ccitools import conf
from unittest import mock
from unittest import TestCase

from ccitools.tests.cmd import fixtures
from ccitools.utils.metrics import MetricsHelper
from ccitools.utils.snow.ticket import RequestState

CONF = conf.CONF


class TestMetricsHelper(TestCase):

    @mock.patch('ccitools.utils.metrics.MonitClient')
    def test_publish_resource_request_metrics_empty(self, mock_mc):
        monit_mock = mock_mc.return_value
        rp = fixtures.SNOW_UPDATE_RP

        MetricsHelper().publish_resource_request_metrics("quota_update", rp,
                                                         {}, False)
        monit_mock.send_service_metrics.assert_called_once_with(
            metric_family="resource_request_handling",
            metrics={"resource_request_handling_seconds": 3600},
            tags=mock.ANY
        )
        self.assertDictEqual(
            monit_mock.send_service_metrics.call_args.kwargs['tags'], {
                "request_type": "quota_update",
                "regions": '',
                "project_name": 'project',
                "has_storage": False,
                "has_compute": False,
                "is_large_compute_request": False
            }
        )

    @mock.patch('ccitools.utils.metrics.MonitClient')
    def test_publish_resource_request_metrics_wrong_state(self, mock_mc):
        monit_mock = mock_mc.return_value
        rp = mock.MagicMock()
        rp.request.info.u_current_task_state = RequestState.ASSIGNED
        MetricsHelper().publish_resource_request_metrics("quota_update", rp,
                                                         {}, False)
        monit_mock.send_service_metrics.assert_not_called()

    @mock.patch('ccitools.utils.metrics.MonitClient')
    def test_publish_resource_request_metrics_with_compute(self, mock_mc):
        monit_mock = mock_mc.return_value
        rp = fixtures.SNOW_UPDATE_RP
        quota = {
            'cern': {
                'compute': {
                    'instances': 12
                }
            }
        }
        MetricsHelper().publish_resource_request_metrics("quota_update", rp,
                                                         quota, False)
        monit_mock.send_service_metrics.assert_called_once_with(
            metric_family="resource_request_handling",
            metrics={"resource_request_handling_seconds": 3600},
            tags=mock.ANY
        )
        self.assertDictEqual(
            monit_mock.send_service_metrics.call_args.kwargs['tags'], {
                "request_type": "quota_update",
                "regions": 'cern',
                "project_name": 'project',
                "has_storage": False,
                "has_compute": True,
                "is_large_compute_request": False
            }
        )

    @mock.patch('ccitools.utils.metrics.MonitClient')
    def test_publish_resource_request_metrics_with_storage(self, mock_mc):
        monit_mock = mock_mc.return_value
        rp = fixtures.SNOW_UPDATE_RP
        quota = {
            'alt': {
                'blockstorage': {
                    'volumes': 12
                }
            }
        }
        MetricsHelper().publish_resource_request_metrics("quota_update", rp,
                                                         quota, False)
        monit_mock.send_service_metrics.assert_called_once_with(
            metric_family="resource_request_handling",
            metrics={"resource_request_handling_seconds": 3600},
            tags=mock.ANY
        )
        self.assertDictEqual(
            monit_mock.send_service_metrics.call_args.kwargs['tags'], {
                "request_type": "quota_update",
                "regions": 'alt',
                "project_name": 'project',
                "has_storage": True,
                "has_compute": False,
                "is_large_compute_request": False
            }
        )

    @mock.patch('ccitools.utils.metrics.MonitClient')
    def test_publish_resource_request_metrics_with_everything(self, mock_mc):
        monit_mock = mock_mc.return_value
        rp = fixtures.SNOW_UPDATE_RP
        quota = {
            'alt': {
                'blockstorage': {
                    'volumes': 12
                },
                'compute': {
                    'instances': 10
                }
            },
        }
        MetricsHelper().publish_resource_request_metrics("quota_update", rp,
                                                         quota, False)
        monit_mock.send_service_metrics.assert_called_once_with(
            metric_family="resource_request_handling",
            metrics={"resource_request_handling_seconds": 3600},
            tags=mock.ANY
        )
        self.assertDictEqual(
            monit_mock.send_service_metrics.call_args.kwargs['tags'], {
                "request_type": "quota_update",
                "regions": 'alt',
                "project_name": 'project',
                "has_storage": True,
                "has_compute": True,
                "is_large_compute_request": False
            }
        )

    @mock.patch('ccitools.utils.metrics.MonitClient')
    def test_publish_resource_request_metrics_with_large_req(self, mock_mc):
        monit_mock = mock_mc.return_value
        rp = fixtures.SNOW_UPDATE_RP
        quota = {
            'alt': {
                'blockstorage': {
                    'volumes': 12
                },
                'compute': {
                    'ram': 1200
                }
            },
        }
        MetricsHelper().publish_resource_request_metrics("quota_update", rp,
                                                         quota, True)
        monit_mock.send_service_metrics.assert_called_once_with(
            metric_family="resource_request_handling",
            metrics={"resource_request_handling_seconds": 3600},
            tags=mock.ANY
        )
        self.assertDictEqual(
            monit_mock.send_service_metrics.call_args.kwargs['tags'], {
                "request_type": "quota_update",
                "regions": 'alt',
                "project_name": 'project',
                "has_storage": True,
                "has_compute": True,
                "is_large_compute_request": True
            }
        )

    @mock.patch('ccitools.utils.metrics.MonitClient')
    def test_publish_resource_request_metrics_with_multi_region(self, mock_mc):
        monit_mock = mock_mc.return_value
        rp = fixtures.SNOW_UPDATE_RP
        quota = {
            'alt': {
                'blockstorage': {
                    'volumes': 12
                },
                'compute': {
                    'ram': 1200
                }
            },
            'cern': {
                'thing': {
                    'something': 12
                },
                'compute': {
                    'ram': 1200
                }
            }
        }
        MetricsHelper().publish_resource_request_metrics("quota_update", rp,
                                                         quota, True)
        monit_mock.send_service_metrics.assert_called_once_with(
            metric_family="resource_request_handling",
            metrics={"resource_request_handling_seconds": 3600},
            tags=mock.ANY
        )
        self.assertDictEqual(
            monit_mock.send_service_metrics.call_args.kwargs['tags'], {
                "request_type": "quota_update",
                "regions": 'alt,cern',
                "project_name": 'project',
                "has_storage": True,
                "has_compute": True,
                "is_large_compute_request": True
            }
        )
