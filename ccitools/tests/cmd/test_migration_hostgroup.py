import logging

from unittest import mock
from unittest import TestCase


class TestMigrationHostgroupCMD(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.aitools_mock = mock.MagicMock()
        modules = {
            'aitools': self.aitools_mock,
            'aitools.foreman': self.aitools_mock.foreman,
        }
        self.patcher = mock.patch.dict('sys.modules', modules)
        self.patcher.start()
        self.addCleanup(self.patcher.stop)

    def test_default_args(self):
        from ccitools.cmd.migration_hostgroup import main
        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)

    @mock.patch('ccitools.cmd.base.rundeck.RundeckAPIClient')
    @mock.patch('ccitools.cmd.base.rundeck.auth_get_sso_cookie')
    @mock.patch('ccitools.cmd.get_uptime.ssh_executor')
    @mock.patch('aitools.foreman.ForemanClient')
    def test_hostgroup_dryrun(self, mock_fc, mock_ssh, mock_sso, mock_rdeck):
        from ccitools.cmd import migration_hostgroup
        migration_hostgroup.SLEEP_TIME = 0
        mock_fc.return_value.search_query.return_value = [
            {
                'name': 'host1.example.com',
            },
            {
                'name': 'host2.example.com',
            }
        ]
        mock_ssh.side_effect = iter(
            [
                ('1.0 1.0', None),
                ('2.0 2.0', None),
            ]
        )
        mock_cli = mock_rdeck.return_value
        mock_cli.run_job_by_id.side_effect = iter([
            '{"id":"fake_id1","permalink":"fake_link1"}',
            '{"id":"fake_id2","permalink":"fake_link2"}',
        ])
        mock_cli.get_execution_info.side_effect = iter([
            "status='scheduled' ",
            "status='running' ",
            "status='succeeded' ",
            "status='succeeded' "
        ])
        migration_hostgroup.main(
            [
                '--dryrun',
                '--hostgroup', 'fake_fake',
            ]
        )

    @mock.patch('ccitools.cmd.base.rundeck.RundeckAPIClient')
    @mock.patch('ccitools.cmd.base.rundeck.auth_get_sso_cookie')
    @mock.patch('ccitools.cmd.get_uptime.ssh_executor')
    @mock.patch('aitools.foreman.ForemanClient')
    def test_hostgroup_perform(self, mock_fc, mock_ssh, mock_sso, mock_rdeck):
        from ccitools.cmd import migration_hostgroup
        migration_hostgroup.SLEEP_TIME = 0
        mock_fc.return_value.search_query.return_value = [
            {
                'name': 'host1.example.com',
            },
            {
                'name': 'host2.example.com',
            }
        ]
        mock_ssh.side_effect = iter(
            [
                ('1.0 1.0', None),
                ('2.0 2.0', None),
            ]
        )
        mock_cli = mock_rdeck.return_value
        mock_cli.run_job_by_id.side_effect = iter([
            '{"id":"fake_id1","permalink":"fake_link1"}',
            '{"id":"fake_id2","permalink":"fake_link2"}',
        ])
        mock_cli.get_execution_info.side_effect = iter([
            "status='scheduled' ",
            "status='running' ",
            "status='succeeded' ",
            "status='succeeded' "
        ])
        migration_hostgroup.main(
            [
                '--perform',
                '--hostgroup', 'fake_fake',
            ]
        )
