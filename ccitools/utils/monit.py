import ccitools.conf
import logging
import requests

logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class MonitClient(object):

    def send_service_metrics(self, metric_family, metrics, tags={}):
        if len(metrics) == 0:
            return

        payload = {
            "producer": CONF.monit.service_metrics_producer,
            "type": metric_family,
            "idb_tags": [*tags],
            "idb_fields": [*metrics],
        } | metrics | tags

        resp = requests.post(
            url=CONF.monit.service_metrics_endpoint,
            json=payload,
            timeout=60
        )

        resp.raise_for_status()
