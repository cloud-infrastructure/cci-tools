import logging

from ccitools.cmd import migration
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase


class MigrationIds:
    pass


class ColdServer:
    def __init__(self, name, id):
        self.id = id
        self.name = name
        self._info = {}

    def no_image(self):
        self._info["image"] = None


@mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
class TestMigrationInNode(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self, mock_crc):

        with self.assertRaises(SystemExit) as cm:
            migration.main([])
        self.assertEqual(cm.exception.code, 2)

    def test_dryrun_data(self, mock_crc):
        mock_crc.return_value = mock_crc
        mock_crc.get_servers_by_hypervisor.return_value = (
            [fixtures.SERVER]
        )
        migration.main([
            '--hypervisors', 'h1',
            '--dryrun'
        ])

    def test_no_servers(self, mock_crc):
        mock_crc.return_value = mock_crc
        mock_crc.get_servers_by_hypervisor.return_value = (
            None
        )
        migration.main([
            '--hypervisors', 'h1',
            '--perform'
        ])

    def test_migration_fail(self, mock_crc):
        mock_crc.return_value = mock_crc
        mock_crc.get_servers_by_hypervisor.return_value = (
            'server1', 'server2'
        )
        with self.assertRaises(SystemExit) as cm:
            migration.main([
                '--hypervisors', 'h1',
                '--perform'
            ])
            self.assertEqual(cm.exeception.code, 127)

    @mock.patch('ccitools.utils.cloud.nova_client')
    @mock.patch('time.sleep')
    def test_live_migration_dryrun(self, mock_crc, mock_nc, mock_time):
        mock_crc.return_value = mock_crc
        match = mock_nc.hypervisors.search.return_value
        server = mock_crc.get_server(fixtures.SERVER)
        self.assertEqual(
            migration.MigrationInNodeCMD().live_migration(
                mock_crc, server, match, False
            ),
            True
        )

    @mock.patch('ccitools.utils.cloud.nova_client')
    @mock.patch('time.sleep')
    def test_live_migration_no_block(self, mock_crc, mock_nc, mock_time):
        mock_crc.return_value = mock_crc
        match = mock_nc.hypervisors.search.return_value
        server = ColdServer("cold_server", "12")
        server.no_image()
        self.assertEqual(
            migration.MigrationInNodeCMD().live_migration(
                mock_crc, server, match, True
            ),
            False
        )

    @mock.patch('ccitools.utils.cloud.nova_client')
    @mock.patch('time.sleep')
    def test_live_migration_timeout(self, mock_crc, mock_nc, mock_time):
        mock_crc.return_value = mock_crc
        hv = mock_nc.hypervisors.search.return_value
        server = mock_crc.get_server(1)
        self.assertEqual(
            migration.MigrationInNodeCMD().live_migration(
                mock_crc, server, hv, True
            ),
            False
        )

    @mock.patch('ccitools.utils.cloud.nova_client')
    @mock.patch('time.sleep')
    def test_live_migration_error(self, mock_crc, mock_nc, mock_time):
        mock_crc.return_value = mock_crc
        hv = mock_nc.hypervisors.search.return_value
        server = mock_crc.get_server()
        server.status.return_value = 'ERROR'
        self.assertEqual(
            migration.MigrationInNodeCMD().live_migration(
                mock_crc, server, hv, True
            ),
            False
        )

    @mock.patch('ccitools.utils.cloud.nova_client')
    @mock.patch('ccitools.cmd.migration.ping_instance')
    @mock.patch('time.sleep')
    @mock.patch('ccitools.cmd.migration.probe_instance_availability')
    def test_live_migration_ping_fail(self, mock_crc, mock_nc, mock_ping,
                                      mock_t, mock_probe):
        mock_crc.return_value = mock_crc
        hv = mock_nc.hypervisors.search.return_value
        server = mock_crc.get_server()
        mock_ping.return_value = False
        server.status.return_value = 'ACTIVE'
        mock_probe.return_value = False
        self.assertEqual(
            migration.MigrationInNodeCMD().live_migration(
                mock_crc, server, hv, True
            ),
            False
        )

    @mock.patch('ccitools.utils.cloud.nova_client')
    @mock.patch('ccitools.cmd.migration.ping_instance')
    @mock.patch('time.sleep')
    @mock.patch('ccitools.cmd.migration.probe_instance_availability')
    def test_live_migration_ping_pass(self, mock_crc, mock_nc, mock_ping,
                                      mock_t, mock_probe):
        mock_crc.return_value = mock_crc
        hv = mock_nc.hypervisors.search.return_value
        server = mock_crc.get_server()
        mock_ping.return_value = True
        server.status.return_value = 'ACTIVE'
        mock_probe.return_value = True
        self.assertEqual(
            migration.MigrationInNodeCMD().live_migration(
                mock_crc, server, hv, True
            ),
            False
        )

    @mock.patch('ccitools.utils.cloud.nova_client')
    @mock.patch('time.sleep')
    def test_cold_migration_error(self, mock_crc, mock_nc, mock_time):
        mock_crc.return_value = mock_crc
        hv = mock_nc.hypervisors.search.return_value
        server = mock_crc.get_server()
        server.status = 'ERROR'
        self.assertEqual(
            migration.MigrationInNodeCMD().cold_migration(
                mock_crc, server, hv, True
            ),
            False
        )

    @mock.patch('ccitools.utils.cloud.nova_client')
    @mock.patch('time.sleep')
    def test_cold_migration_dryrun(self, mock_crc, mock_nc, mock_time):
        mock_crc.return_value = mock_crc
        hv = mock_nc.hypervisors.search.return_value
        server = mock_crc.get_server(1)
        self.assertEqual(
            migration.MigrationInNodeCMD().cold_migration(
                mock_crc, server, hv, False
            ),
            True
        )

    @mock.patch('ccitools.utils.cloud.nova_client')
    @mock.patch('time.sleep')
    def test_cold_migration_timeout(self, mock_crc, mock_nc, mock_time):
        mock_crc.return_value = mock_crc
        hv = mock_nc.hypervisors.search.return_value
        server = mock_crc.get_server(1)
        self.assertEqual(
            migration.MigrationInNodeCMD().cold_migration(
                mock_crc, server, hv, True
            ),
            False
        )

    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_hypervisor_has_VMs(self, mock_crc, mock_nc):
        mock_crc.return_value = mock_crc
        hv = mock_nc.hypervisors.search.return_value
        self.assertIsNone(
            migration.MigrationInNodeCMD().hypervisor_has_vms(
                mock_crc, hv, 'False'
            )
        )

    def test_post_migration(self, mock_crc):
        mock_crc.return_value = mock_crc
        self.assertEqual(
            migration.MigrationInNodeCMD().post_migration_VM_Check_in_HV(
                mock_crc, 'h1', True
            ),
            True
        )

    def test_post_migration_dryrun(self, mock_crc):
        mock_crc.return_value = mock_crc
        self.assertEqual(
            migration.MigrationInNodeCMD().post_migration_VM_Check_in_HV(
                mock_crc, 'h1', False
            ),
            False
        )

    @mock.patch('ccitools.utils.cloud.nova_client')
    @mock.patch('ccitools.cmd.migration.MigrationInNodeCMD.make_nova_client')
    def test_abort_live_migration_dryrun(self, mock_crc, mock_nc, mock_mnc):
        # mock_mig.get_migration_id.return_value = 1
        mock_crc.return_value = mock_crc
        mock_mnc.return_value = mock_nc
        server = mock_crc.get_server(1)
        result = migration.MigrationInNodeCMD().abort_live_migration(
            mock_crc, server, False)
        self.assertEqual(result, True)

    @mock.patch('ccitools.cmd.migration.subprocess.check_call')
    def test_ping_instance(self, mock_cc, mock_run):
        hostname = '127.0.0.1'
        mock_stdout = mock.MagicMock()
        mock_stdout.configure_mock(
            **{
                "stdout.decode.return_value": '("output", "Error")'
            }
        )
        mock_run.return_value = mock_stdout
        self.assertEqual(
            migration.ping_instance(
                hostname
            ),
            True
        )

    @mock.patch('time.sleep')
    @mock.patch('ccitools.cmd.migration.ping_instance')
    def test_probe_instance_availiablity(self, mock_cc, mock_time, mock_ping):
        hostname = '127.0.0.1'
        mock_cc.return_value = mock_cc
        mock_ping.return_value = True
        self.assertEqual(
            migration.probe_instance_availability(hostname), True)

    @mock.patch('time.sleep')
    @mock.patch('ccitools.cmd.migration.ping_instance')
    def test_probe_instance_availiablity_fail(self, mock_cc, mock_time,
                                              mock_ping):
        hostname = '127.0.0.1'
        mock_cc.return_value = mock_cc
        mock_ping.return_value = False
        self.assertEqual(
            migration.probe_instance_availability(hostname), True)

    @mock.patch('time.sleep')
    def test_probe_instance_availiablity_fail2(self, mock_cc, mock_time,):
        hostname = 'ubuntu@cern.ch'
        mock_cc.return_value = mock_cc
        self.assertEqual(
            migration.probe_instance_availability(hostname), False)

    @mock.patch('ccitools.utils.cloud.nova_client')
    def test_migration_id(self, mock_crc, mock_nc):
        mock_crc.return_value = mock_crc
        migration_obj = MigrationIds()
        migration_obj.id = '1'
        migration_obj.status = 'running'
        mock_nc.migrations.list.return_value = [migration_obj]
        server = mock_crc.get_server(fixtures.SERVER)
        self.assertEqual(
            migration.MigrationInNodeCMD().get_migration_id(mock_nc, server),
            '1'
        )
