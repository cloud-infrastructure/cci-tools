#!/usr/bin/python3

import ccitools.conf
import logging
import sqlalchemy
import sqlalchemy.orm
import sys

from ccitools.cmd import base

# configure logging
logger = logging.getLogger(__name__)
Base = sqlalchemy.orm.declarative_base()
CONF = ccitools.conf.CONF


class ScheduledExecution(Base):
    __tablename__ = 'scheduled_execution'
    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    job_name = sqlalchemy.Column(sqlalchemy.String)
    scheduled = sqlalchemy.Column(sqlalchemy.String)
    rduser = sqlalchemy.Column(sqlalchemy.String)


class CheckScheduledRundeckJobs(base.BaseCMD):

    def __init__(self):
        super(CheckScheduledRundeckJobs, self).__init__(
            description="Check scheduled rundeck jobs")

    def main_check_user(self, connection_str, user):
        engine = sqlalchemy.create_engine(connection_str)
        Session = sqlalchemy.orm.sessionmaker(bind=engine)
        session = Session()
        try:
            session.query(ScheduledExecution).\
                filter(ScheduledExecution.scheduled == '1').\
                filter(ScheduledExecution.rduser != user).\
                update({'rduser': user})
            session.commit()
            logger.info("DONE! All jobs should be run by '%s' now :)" % user)
        except Exception as ex:
            logger.error("There was a problem: %s" % ex)
        finally:
            session.close()

    def main(self, args):

        self.parser.add_argument(
            "--user",
            required=True,
            help="Username all scheduled job should be run by")

        args = self.parse_args(args)

        connection_str = CONF.rundeck.connection

        self.main_check_user(connection_str, args.user)


# Needs static method for setup.cfg
def main(args=None):
    CheckScheduledRundeckJobs().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)
