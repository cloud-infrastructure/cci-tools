import logging

from ccitools.utils.fim import FIMClient
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.common.get_transport')
@mock.patch('ccitools.utils.fim.Client')
class TestFIM(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_create_project(self, mock_zc, mock_tr):
        mock_cli = mock_zc.return_value

        FIMClient('localhost').create_project(
            name='fake_name',
            description='fake_description',
            owner='fake_owner',
            shared=True
        )

        mock_zc.assert_called_with(
            'localhost',
            transport=mock_tr.return_value,
        )

        mock_cli.service.CreateProject.assert_called_with(
            name='fake_name',
            description='fake_description',
            owner='fake_owner',
            shared=True
        )

    def test_delete_project(self, mock_zc, mock_tr):
        mock_cli = mock_zc.return_value

        FIMClient('localhost').delete_project(
            name='fake_name'
        )

        mock_zc.assert_called_with(
            'localhost',
            transport=mock_tr.return_value,
        )

        mock_cli.service.DeleteProject.assert_called_with(
            name='fake_name'
        )

    def test_set_project_name(self, mock_zc, mock_tr):
        mock_cli = mock_zc.return_value

        FIMClient('localhost').set_project_name(
            name='fake_name',
            new_name='new_name'
        )

        mock_zc.assert_called_with(
            'localhost',
            transport=mock_tr.return_value,
        )

        mock_cli.service.SetProjectName.assert_called_with(
            name='fake_name',
            newName='new_name'
        )

    def test_set_project_description(self, mock_zc, mock_tr):
        mock_cli = mock_zc.return_value

        FIMClient('localhost').set_project_description(
            name='fake_name',
            desc='new_description'
        )

        mock_zc.assert_called_with(
            'localhost',
            transport=mock_tr.return_value,
        )

        mock_cli.service.SetProjectDescription.assert_called_with(
            name='fake_name',
            newDescription='new_description'
        )

    def test_set_project_owner(self, mock_zc, mock_tr):
        mock_cli = mock_zc.return_value

        FIMClient('localhost').set_project_owner(
            name='fake_name',
            owner='new_owner'
        )

        mock_zc.assert_called_with(
            'localhost',
            transport=mock_tr.return_value,
        )

        mock_cli.service.SetProjectOwner.assert_called_with(
            name='fake_name',
            newOwner='new_owner'
        )

    def test_set_project_status(self, mock_zc, mock_tr):
        mock_cli = mock_zc.return_value

        FIMClient('localhost').set_project_status(
            name='fake_name',
            enabled=False
        )

        mock_zc.assert_called_with(
            'localhost',
            transport=mock_tr.return_value,
        )

        mock_cli.service.SetProjectStatus.assert_called_with(
            name='fake_name',
            enabled=False
        )

    def test_is_valid_owner(self, mock_zc, mock_tr):
        mock_cli = mock_zc.return_value

        FIMClient('localhost').is_valid_owner(
            username='fake_name'
        )

        mock_zc.assert_called_with(
            'localhost',
            transport=mock_tr.return_value,
        )

        mock_cli.service.ValidateOwner.assert_called_with(
            'fake_name'
        )
