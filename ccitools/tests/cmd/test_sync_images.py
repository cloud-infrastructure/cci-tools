import logging

from ccitools.cmd.sync_images import main
from unittest import mock
from unittest import TestCase


class TestReplicatorImagesCMD(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('openstack.config')
    @mock.patch('openstack.connection')
    def test_dryrun_no_params(self, mock_cloud, mock_cc, mock_co):

        with self.assertRaises(SystemExit) as cm:
            main(['--dryrun'])

        self.assertEqual(cm.exception.code, 2)

        mock_cc.get_cloud_region.assert_called_with(
            cloud='cern',
            region_name='cern',
            interface='public'
        )
