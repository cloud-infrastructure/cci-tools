import dateutil.parser
import logging

from ccitools.cleanup import base
from ccitools import common
from datetime import datetime
from datetime import timezone

# configure logging
logger = logging.getLogger(__name__)


class ShareResourceCleanup(base.ResourceCleanup):
    def __init__(self, cloud, quiet, project_id):
        self.project_id = project_id
        super(ShareResourceCleanup, self).__init__('SHARES', cloud, quiet)

    def get_pending(self):
        shares_pending = []
        logger.info("Looking for leftover shares on %s ...",
                    self.project_id)
        for share in self.cloud.get_shares_by_project_id(
                self.project_id, tag='cleanup'):
            created_at = dateutil.parser.parse(share.created_at)
            days_since_created = (
                datetime.now(timezone.utc)
                - created_at.replace(tzinfo=timezone.utc)).days
            if (base.RALLY_REGEX.match(share.name)
                    and days_since_created >= 1):
                shares_pending.append(share)
        return shares_pending

    def delete_leftovers_before(self):
        if self.leftovers:
            self.cloud.delete_shares(self.leftovers)

    def delete_leftovers_after(self):
        pass

    def get_broken(self):
        broken_shares = []
        for share in self.cloud.get_shares_by_project_id(
                self.project_id, tag='cleanup'):
            if share in self.leftovers:
                broken_shares.append(share)
        return broken_shares

    def format_table(self):
        share_fields = [
            ('ID', 'id'),
            ('Name', 'name'),
            ('Status', 'status'),
            ('Created', 'created_at'),
            ('Project ID', 'project_id'),
        ]
        shares_table = common.get_resources_info_table(
            self.leftovers, share_fields)

        return shares_table
