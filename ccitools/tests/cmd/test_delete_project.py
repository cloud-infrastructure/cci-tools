import logging

from ccitools.cmd import delete_project
from tenacity import stop_after_attempt
from tenacity import wait_none
from unittest import mock
from unittest import TestCase


class TestDeleteProject(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch('ccitools.utils.cloud.cloud_config')
    def test_default_args(self, mock_cc):
        with self.assertRaises(SystemExit) as cm:
            delete_project.main([])
        self.assertEqual(cm.exception.code, 2)

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.delete_project.FIMClient')
    @mock.patch('ccitools.cmd.delete_project.ServiceNowClient')
    def test_snow_no_deleted(self,
                             mock_snc,
                             mock_fc,
                             mock_crc):
        mock_cl = mock_crc.return_value
        mock_fc.return_value.delete_project.return_value = 0
        mock_cl.find_project.return_value = mock.MagicMock(id='fake_id')
        mock_snow = mock_snc.return_value
        mock_snow.get_project_deletion_rp.return_value.project_name = (
            'fake_project')

        cmd = delete_project.DeleteProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(1)

        cmd.main([
            'from-snow',
            '--instance', 'f_cern',
            '--ticket-number', 'fake_ticket',
            '--resolver', 'fake_resolve'
        ])

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.delete_project.FIMClient')
    @mock.patch('ccitools.cmd.delete_project.ServiceNowClient')
    def test_from_input(self,
                        mock_snc,
                        mock_fc,
                        mock_crc):
        cmd = delete_project.DeleteProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(1)

        with self.assertRaises(Exception):
            cmd.main([
                'from-input',
                '--project-name', 'fake_n',
            ])

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.delete_project.FIMClient')
    @mock.patch('ccitools.cmd.delete_project.ServiceNowClient')
    def test_fim_delete_fail(self,
                             mock_snc,
                             mock_fc,
                             mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.get_volumes_snapshots_by_project.return_value = False
        mock_cl.get_volumes.return_value = False
        mock_cl.get_images_by_project.return_value = False
        mock_cl.get_servers.return_value = False
        mock_cl.get_shares_by_project.return_value = False
        mock_fc.return_value.delete_project.return_value = 1
        mock_snow = mock_snc.return_value
        mock_snow.get_project_deletion_rp.return_value.project_name = (
            'fake_project')

        cmd = delete_project.DeleteProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(1)

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--check',
                'from-snow',
                '--instance', 'cern',
                '--ticket-number', 'fake_ticket',
                '--resolver', 'fake_resolve'
            ])
        self.assertEqual(cm.exception.code, 1)

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.delete_project.FIMClient')
    @mock.patch('ccitools.cmd.delete_project.ServiceNowClient')
    def test_exception_could_not_be_deleted(self,
                                            mock_snc,
                                            mock_fc,
                                            mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.find_project.side_effect = iter([
            mock.MagicMock(name='faken1', id='fakei1'),
            mock.MagicMock(name='faken2', id='fakei2')
        ])
        mock_cl.get_volumes_snapshots_by_project.return_value = True
        mock_cl.get_volumes.return_value = "False"
        mock_cl.get_images_by_project.return_value = "False"
        mock_cl.get_servers.return_value = "False"
        mock_cl.get_shares_by_project.return_value = "False"
        mock_snow = mock_snc.return_value
        mock_snow.get_project_deletion_rp.return_value.project_name = (
            'fake_project')

        cmd = delete_project.DeleteProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(1)

        with self.assertRaises(SystemExit) as cm:
            cmd.main([
                '--check',
                'from-snow',
                '--instance', 'cern',
                '--ticket-number', 'fake_ticket',
                '--resolver', 'fake_resolve'
            ])
        self.assertEqual(cm.exception.code, 1)

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.delete_project.FIMClient')
    @mock.patch('ccitools.cmd.delete_project.ServiceNowClient')
    def test_snow_project_empty_in_fim(self,
                                       mock_snc,
                                       mock_fc,
                                       mock_crc):
        mock_cl = mock_crc.return_value
        mock_fc.return_value.delete_project.return_value = 0
        mock_cl.find_project.return_value = mock.MagicMock(id='fake_id')
        mock_cl.get_volumes_snapshots_by_project.return_value = False
        mock_cl.get_volumes.return_value = False
        mock_cl.get_images_by_project.return_value = False
        mock_cl.get_servers.return_value = False
        mock_cl.get_shares_by_project.return_value = False
        mock_snow = mock_snc.return_value
        mock_snow.get_project_deletion_rp.return_value.project_name = (
            'fake_project')

        cmd = delete_project.DeleteProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(2)

        cmd.main([
            'from-snow',
            '--instance', 'cern',
            '--ticket-number', 'fake_ticket',
            '--resolver', 'fake_resolve'
        ])

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.delete_project.ResourcesClient')
    @mock.patch('ccitools.cmd.delete_project.CornerstoneClient')
    @mock.patch('ccitools.cmd.delete_project.ServiceNowClient')
    def test_snow_project_empty_in_mim(self,
                                       mock_snc,
                                       mock_cornerstone,
                                       mock_resources,
                                       mock_crc):
        mock_cl = mock_crc.return_value
        mock_cl.find_project.return_value = mock.MagicMock(id='fake_id')
        mock_cl.get_volumes_snapshots_by_project.return_value = False
        mock_cl.get_volumes.return_value = False
        mock_cl.get_images_by_project.return_value = False
        mock_cl.get_servers.return_value = False
        mock_cl.get_shares_by_project.return_value = False
        mock_snow = mock_snc.return_value
        mock_snow.get_project_deletion_rp.return_value.project_name = (
            'fake_project')
        mock_res = mock_resources.return_value
        mock_cor = mock_cornerstone.return_value

        cmd = delete_project.DeleteProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(2)

        cmd.main([
            '--mim',
            'from-snow',
            '--instance', 'cern',
            '--ticket-number', 'fake_ticket',
            '--resolver', 'fake_resolve'
        ])

        mock_res.delete_project.assert_called_with(
            id='fake_id'
        )

        mock_cor.delete_project.assert_called_with(
            id='fake_id'
        )
