import ccitools.conf
import logging

from ccitools.cmd.base import BaseCMD
from ccitools.common import auth_get_sso_cookie
from ccitools.utils.rundeck import RundeckAPIClient

CONF = ccitools.conf.CONF
logger = logging.getLogger(__name__)


class BaseRundeckCMD(BaseCMD):
    def __init__(self, description):
        super(BaseRundeckCMD, self).__init__(description=description)
        self.parser.add_argument(
            '--auth',
            choices=['token', 'sso'],
            required=True
        )

    def parse_args(self, args):
        args = super(BaseRundeckCMD, self).parse_args(args)

        logger.info("Instantiating Rundeck client...")
        if args.auth.lower() == 'token':
            logger.warning("Using token auth mech...")
            self.rundeckcli = RundeckAPIClient(
                server=CONF.rundeck.alias,
                port=CONF.rundeck.port,
                token=CONF.rundeck.api_token,
                verify=CONF.rundeck.verify,
                api_version=CONF.rundeck.api_version)
        else:
            logger.warning("Getting SSO cookie...")
            cookie = auth_get_sso_cookie(
                "https://%s" % CONF.rundeck.alias,
                "/tmp/rundeck_sso_cookie")  # nosec
            self.rundeckcli = RundeckAPIClient(
                server=CONF.rundeck.alias,
                port=CONF.rundeck.port,
                sso_cookie=cookie,
                verify=CONF.rundeck.verify,
                api_version=CONF.rundeck.api_version)
        return args
