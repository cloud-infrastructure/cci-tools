#!/usr/bin/python3

import ccitools.conf
import json
import logging
import prettytable
import sys

from ccitools.cmd.base.rundeck import BaseRundeckCMD
from ccitools.common import normalize_hostname
from dateutil import parser
from dateutil.parser import parse
from dateutil import tz

# configure logging
logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s %(levelname)s %(name)s - %(message)s")
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF

#
# NOTE: Rundeck API will return a JSON object that contains the list
# of scheduled jobs in a specified project. Its structure is as follows:
#
# {
#   "executions": [
#     {
#       "argstring": "-behaviour dryrun",
#       "date-started": {
#         "date": "2018-03-28T12:55:00Z",
#         "unixtime": 1522241700000
#       },
#       "description": "",
#       "executionType": "user-scheduled",
#       "href": "https://cci-rundeck-dev.cern.ch/api/22/execution/75703",
#       "id": 75703,
#       "job": {
#         "averageDuration": 1330,
#         "description": "",
#         "group": "",
#         "href": "https://cci-rundeck-dev.cern.ch/api/22/job/...8e2d3d9b4",
#         "id": "a76a9b9e-95f6-4147-80ff-8d68e2d3d9b4",
#         "name": "Print dryrun banner",
#         "options": {
#           "behaviour": "dryrun"
#         },
#         "permalink": "https://.../project/Common/job/show/...8e2d3d9b4",
#         "project": "Common"
#       },
#       "permalink": "https://.../project/Common/execution/show/75703",
#       "project": "Common",
#       "serverUUID": "0a377c5b-0c05-4850-8bc2-06f07a3e4dbd",
#       "status": "scheduled",
#       "user": "svcrdeck"
#     }
#   ],
#   "paging": {
#     "count": 1,
#     "max": 20,
#     "offset": 0,
#     "total": 1
#   }
# }


class InterventionsManagerCMD(BaseRundeckCMD):

    def __init__(self):
        super(InterventionsManagerCMD, self).__init__(
            description="Manages Interventions scheduled in Rundeck")

    def build_filtered_table(self, filtered_list):

        jobs_table = prettytable.PrettyTable(["Host", "Date", "Link",
                                              "Job", "User",
                                              "Ticket", "Scheduled"])
        jobs_table.align["Host"] = 'c'
        jobs_table.align["Date"] = 'c'
        jobs_table.align["Link"] = 'c'
        jobs_table.align["Job"] = 'c'
        jobs_table.align["User"] = 'c'
        jobs_table.align["Ticket"] = 'c'
        jobs_table.align["Scheduled"] = 'c'

        for execution in filtered_list:
            for host in execution['job']['options']['hosts'].split():
                parsed_date = self.rundeck_date_parser(
                    execution['date-started']['date'])
                jobs_table.add_row([
                    host,
                    parsed_date,
                    execution['permalink'],
                    execution['job']['name'],
                    execution['user'],
                    execution['job']['options']['snow_ticket'],
                    execution['status']])

        return jobs_table

    def date_parser(self, datetime):
        """Check date format and parses it to meet ISO-8601 criteria.

        :param datetime: Datetime introduced by user
        :returns: dateutil datetime object formatted to ISO-8601
        """
        try:
            logger.info("Parsing introduced datetime...")
            dt = parse(datetime)
        except ValueError:
            # Input date follows common format instead of ISO-8601
            dt = parse(datetime, dayfirst=True)
            dt = dt.replace(tzinfo=tz.gettz('Europe/Zurich'))
            logger.info("Introduced datetime follows common "
                        "format DD-MM-YYYY HH:mm. Datetime requires to be "
                        "formatted to ISO-8601.")
        else:
            dt = dt.replace(tzinfo=tz.gettz('Europe/Zurich'))
            logger.info("Introduced datetime follows ISO-8601."
                        "No need to format.")
        datetime_iso = dt.isoformat(timespec='seconds')

        return datetime_iso

    def is_intervention(self, execution, history):
        """Check whether the passed execution is an intervention.

        :param execution: Rundeck execution
        :returns: True if execution is an intervention
        """
        if 'job' not in execution:
            logger.debug("Found execution with ID %s does not seem to be "
                         "an intervention. Reason: missing 'job' attribute. "
                         "Discarding it...",
                         execution['id'])
            return False

        if 'options' not in execution['job']:
            logger.debug(
                "Found execution with ID %s does not seem to be "
                "an intervention. Reason: missing 'options' attribute. "
                "Discarding it...",
                execution['id'])
            return False

        else:
            if 'hosts' not in execution['job']['options']:
                logger.debug("Found execution with ID %s does not seem to be "
                             "an intervention. Reason: missing 'hosts' "
                             "attribute. Discarding it...",
                             execution['id'])
                return False

            if 'snow_ticket' not in execution['job']['options']:
                logger.debug(
                    "Found execution with ID %s does not seem to be an "
                    "intervention. Reason: missing 'snow_ticket' "
                    "attribute. Discarding it...",
                    execution['id'])
                return False

        if not history:
            # This hacking is required in order to run the script from Rundeck
            # During execution the job itself enters "running" state and
            # has to be discarded among all the possible interventions
            if execution['job']['id'] in CONF.rundeck.interventions:
                logger.debug(
                    "Found execution with ID %s does not seem to be a "
                    "scheduled intervention. Reason: execution is an "
                    "intervention trigger. Discarding it...",
                    execution['id'])
                return False

        # We don't want to mix results with dryrun executions
        if 'behaviour' in execution['job']['options']:
            if execution['job']['options']['behaviour'] == 'dryrun':
                logger.debug("Found execution with ID %s tagged as 'dryrun'. "
                             "Discarding it...",
                             execution['id'])
                return False

        return True

    def normalize_hostname_list(self, hostname_list):
        """Normalize list of hostnames.

        :param hostname_list: List of hostnames
        :returns: Generator with list of hostnames
        """
        for hostname in hostname_list:
            yield (normalize_hostname(hostname))

    def rundeck_date_parser(self, datetime):
        """Parse Rundeck UTC format to ISO-8601.

        :param datetime: UTC datetime taken from Rundeck
        :returns: datetime object formatted to ISO-8601
        """
        parsed_date = parser.parse(datetime)

        # Convert to 'Europe/Zurich' timezone
        zurich_tz = tz.gettz('Europe/Zurich')
        zurich_dt = parsed_date.astimezone(zurich_tz)

        # Return date in ISO-8601 format
        return zurich_dt.isoformat()

    def retrieve_interventions(self, args, rdeckclient):
        if args.history:
            if args.within_last:
                query = {'recentFilter': args.within_last}

            elif args.search_from_date:
                parsed_date = self.date_parser(
                    args.search_from_date).to('utc').isoformat().replace(
                        '+00:00', 'Z')
                query = {'begin': parsed_date}

            else:
                query = {}

            executions = json.loads(
                rdeckclient.project_executions('CC-SysAdmins',
                                               query))['executions']
        else:
            executions = json.loads(
                rdeckclient.list_scheduled_executions(
                    'CC-SysAdmins'))['executions']

        logger.info("Cleaning executions which are not interventions...")
        executions = [
            ex for ex in executions if self.is_intervention(ex, args.history)]

        if args.user:
            logger.info("Filter executions by user %s", args.user)
            executions = [ex for ex in executions if ex['user'] == args.user]

        if args.start_date:
            # Format user-specified start date to ISO-8601
            parsed_start_date = self.date_parser(args.start_date).isoformat()
            logger.info("Filter executions by date %s", parsed_start_date)

            executions = (
                [ex for ex in executions
                 if self.rundeck_date_parser(
                     ex['date-started'][
                         'date']) == parsed_start_date]
            )

        if args.execution_ID:
            filtered_execution_ids = []
            logger.info("Filter executions by execution id %s",
                        args.execution_ID)
            for ex_id in args.execution_ID.split():
                filtered_execution_ids += ([ex for ex in executions
                                            if ex_id == str(ex['idargs.'])])
            executions = filtered_execution_ids

        if args.job_ID:
            logger.info("Filter executions by job id %s", args.job_ID)
            executions = [ex for ex in executions if ex[
                'job']['id'] == args.job_ID]

        if args.hosts:
            filtered_hosts = []
            logger.info("Filter executions by hosts %s", args.hosts)
            for host in self.normalize_hostname_list(args.hosts.split()):
                filtered_hosts += ([ex for ex in executions
                                    if host in self.normalize_hostname_list(ex[
                                        'job']['options']['hosts'].split())])
            executions = filtered_hosts

        if args.tickets:
            filtered_tickets = []
            logger.info("Filter executions by tickets %s", args.tickets)
            for ticket in args.tickets.split():
                filtered_tickets += ([ex for ex in executions
                                      if ticket in ex[
                                          'job']['options'][
                                          'snow_ticket'].split()])
            executions = filtered_tickets

        if args.exclude_ID:
            logger.info("Removing exclude %s from executions", args.exclude_ID)
            executions = [ex for ex in executions if ex[
                'id'] != args.exclude_ID]

        executions = [i for n, i in enumerate(executions)
                      if i not in executions[n + 1:]]

        return executions

    def list_jobs(self, args, interventions, rdeckclient):
        """Return a list of scheduled jobs resulting from the filters."""
        if args.json:
            logger.info("Output will be JSON formatted.")
            output_list = []
            for execution in interventions:
                for host in execution['job']['options']['hosts'].split():
                    logger.info("Including intervention for host %s...", host)
                    output_job = {
                        'host': host,
                        'date': execution['date-started']['date'],
                        'link': execution['permalink'],
                        'job': execution['job']['name'],
                        'user': execution['user'],
                        'ticket': execution['job']['options']['snow_ticket'],
                        'scheduled': execution['status']
                    }
                    output_list.append(output_job)

            return output_list

        elif args.exit_code:
            logger.info("Sending exit code...")
            if interventions:
                filtered_table = self.build_filtered_table(interventions)
                logger.error("Failed. Intervention(s) found:\n%s",
                             filtered_table.get_string(sortby="Date"))
                sys.exit(1)
            else:
                logger.info("Success! No interventions found.")
                sys.exit()

        else:
            filtered_table = self.build_filtered_table(interventions)
            logger.info("Found scheduled intervention(s):\n%s",
                        filtered_table.get_string(sortby="Date"))

    def kill_jobs(self, args, interventions, rdeckclient):
        """Kill all found scheduled jobs resulting from the passed filters."""
        if args.exec_mode:

            if not interventions:
                logger.error(
                    "No interventions found for the provided filters. "
                    "Quitting 'kill intervention' operation...")
                sys.exit(1)

            filtered_table = self.build_filtered_table(interventions)
            logger.info("Cancelling the following listed intervention(s):\n%s",
                        filtered_table.get_string(sortby="Date"))

            for execution in interventions:
                logger.info("Deleting intervention %s...", execution['id'])
                rdeckclient.abort_execution_by_id(execution['id'])

            logger.info("Intervention(s) cancelled successfully!")

        else:
            filtered_table = self.build_filtered_table(interventions)
            logger.info("[DRYRUN] Cancelling the following listed"
                        " intervention(s):\n%s",
                        filtered_table.get_string(sortby="Date"))

    def postpone_jobs(self, args, interventions, rdeckclient):
        """Postpone all found scheduled jobs for the new specified date."""
        if args.exec_mode:

            if not interventions:
                logger.error(
                    "No interventions found for the provided filters. "
                    "Quitting 'postpone intervention' operation...")
                sys.exit(1)

            # We will kill the job but keep all the parameters except for the
            # date which will be replaced by the new one.

            # Parse datetime to meet ISO-8601 criteria
            date_parsed = self.date_parser(args.new_date)

            # Convert ISO-8601 format date to string
            datetime_iso = date_parsed.isoformat()

            filtered_table = self.build_filtered_table(interventions)
            logger.info("Postponing the following listed interventions(s) to "
                        "the new date %s :\n%s",
                        args.new_date,
                        filtered_table.get_string(sortby="Date"))

            postponed_list = []
            for execution in interventions:
                logger.info("Deleting intervention %s...", execution['id'])
                rdeckclient.abort_execution_by_id(execution['id'])
                logger.info("Intervention cancelled successfully. "
                            "Rescheduling same intervention for the new date.")

                options = execution['job']['options']
                job_id = execution['job']['id']
                user = execution['user']

                # Replace date attribute if present with the new date
                if 'date' in options:
                    options['date'] = args.new_date

                logger.info("Scheduling intervention with ID %s for %s...",
                            job_id, args.new_date)
                output = rdeckclient.run_scheduled_job_by_id(
                    job_id, datetime_iso, options, user)
                postponed_job = json.loads(output)
                logger.info("Follow the intervention here: %s",
                            postponed_job['permalink'])
                postponed_list.append(postponed_job)

            logger.info("Intervention(s) postponed successfully!")
            postponed_table = self.build_filtered_table(postponed_list)
            logger.info("Summary of postponed intervention(s):\n%s",
                        postponed_table.get_string(sortby="Date"))

        else:
            filtered_table = self.build_filtered_table(interventions)
            logger.info("[DRYRUN] Postponing the following listed "
                        "interventions(s) to the new date %s :\n%s",
                        args.new_date,
                        filtered_table.get_string(sortby="Date"))

    def main(self, args=None):
        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true')
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')

        self.parser.add_argument(
            "--history", action='store_true',
            help="Search only for executed interventions.")
        self.parser.add_argument(
            "--within-last",
            help="Format 'XY' where X is an integer, and 'Y' \
                  'h' (hour),'d' (day),'w' (week),'m' (month), \
                  or 'y' (year). \
                  Returns interventions that completed \
                  within a period of time.")
        self.parser.add_argument(
            "--search-from-date",
            help="Format DD-MM-YYYY HH:mm. \
                  Exact date for earliest execution completion time.")
        self.parser.add_argument(
            "--user",
            help="User that will execute the job.")
        self.parser.add_argument(
            "--start-date",
            help="Scheduled date for execution. \
                  Common date format 'DD-MM-YYYY HH:mm' and \
                  ISO-8601 format are accepted.")
        self.parser.add_argument(
            "--execution-ID",
            help="List (space separated) of execution ID \
                 of the scheduled jobs.")
        self.parser.add_argument(
            "--job-ID",
            help="Rundeck ID of the scheduled job.")
        self.parser.add_argument(
            "--exclude-ID",
            type=int,
            help="Rundeck ID to exclude from the search (normally itself)")
        self.parser.add_argument(
            "--hosts",
            help="List (space separated) of affected hosts.")
        self.parser.add_argument(
            "--tickets",
            help="Filter based on a list (space separated) of tickets.")

        subparsers = self.parser.add_subparsers(metavar='<subcommand>')

        parser_list = subparsers.add_parser(
            'list',
            help="List interventions that match the filter")
        parser_list.add_argument(
            '--json', default=False, help="Filtered list in JSON format.")
        parser_list.add_argument(
            '--exit-code', default=False, action="store_true",
            help="Returns exit code depending on found interventions. \
                  Exit code 0 (success) when NO interventions are found. \
                  Exit code 1 (success) in case of found interventions.")
        parser_list.set_defaults(func=self.list_jobs)

        parser_kill = subparsers.add_parser(
            'kill', help="Kill interventions that match the filter")
        parser_kill.set_defaults(func=self.kill_jobs)

        parser_postpone = subparsers.add_parser(
            'postpone', help="Postpone interventions that match the filter")
        parser_postpone.add_argument(
            "--new-date", required=True, help="New date for the execution.")
        parser_postpone.set_defaults(func=self.postpone_jobs)

        args = self.parse_args(args)

        try:
            call = args.func
        except AttributeError:
            self.parser.error("Too few arguments")

        # Retrieve the scheduled interventions that match with the filters
        interventions = self.retrieve_interventions(args, self.rundeckcli)

        # Execute the command
        call(args, interventions, self.rundeckcli)


# Needs static method for setup.cfg
def main(args=None):
    InterventionsManagerCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
