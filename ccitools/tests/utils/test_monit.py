
from ccitools import conf
from unittest import mock
from unittest import TestCase

from ccitools.utils.monit import MonitClient

CONF = conf.CONF


@mock.patch('ccitools.utils.cloud.cloud_config')
class TestCloud(TestCase):

    def test_send_service_metrics_empty(self, mock_cc):
        monitcli = MonitClient()
        with mock.patch('requests.post') as mock_req:
            monitcli.send_service_metrics(metric_family="test_metrics",
                                          metrics={})
            mock_req.assert_not_called()

    def test_send_service_metrics_without_tags(self, mock_cc):
        monitcli = MonitClient()
        with mock.patch('requests.post') as mock_req:
            monitcli.send_service_metrics(metric_family="test_metrics",
                                          metrics={'that': 1})
            mock_req.assert_called_with(
                url="http://monit-metrics.cern.ch:10012",
                json={
                    "producer": "cloud",
                    "type": "test_metrics",
                    "that": 1,
                    "idb_fields": ["that"],
                    "idb_tags": []
                },
                timeout=60
            )

    def test_send_service_metrics_with_tags(self, mock_cc):
        monitcli = MonitClient()
        with mock.patch('requests.post') as mock_req:
            monitcli.send_service_metrics(metric_family="test_metrics",
                                          metrics={'that': 1},
                                          tags={'planet': 'earth'})
            mock_req.assert_called_once()

            self.assertDictEqual(
                mock_req.call_args.kwargs['json'], {
                    "producer": "cloud",
                    "type": "test_metrics",
                    "that": 1,
                    "planet": "earth",
                    "idb_fields": ["that"],
                    "idb_tags": ["planet"]
                }
            )
