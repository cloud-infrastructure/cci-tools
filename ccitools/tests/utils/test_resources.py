import logging

from ccitools.utils.resources import ResourcesClient
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.utils.resources.BackendApplicationClient')
@mock.patch('ccitools.utils.resources.OAuth2Session')
class TestResources(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_get_user_id(self, mock_session, mock_client):
        mock_ses = mock_session.return_value
        mock_ses.request.return_value.text = (
            '{"data": {"id": "fake_id"} }')

        ResourcesClient().get_user_id('fake_user')

        mock_ses.request.assert_called_with(
            method='GET',
            url='https://authorization-service-api.web.cern.ch/api/v1.0/'
                'Identity/fake_user',
            data={},
            headers={'Content-Type': 'application/json'}
        )

    def test_get_group_id(self, mock_session, mock_client):
        mock_ses = mock_session.return_value
        mock_ses.request.return_value.text = '{"data": {"id": "fake_id"} }'

        ResourcesClient().get_group_id('fake_group')

        mock_ses.request.assert_called_with(
            method='GET',
            url='https://authorization-service-api.web.cern.ch/api/v1.0/'
                'Group/fake_group',
            data={},
            headers={'Content-Type': 'application/json'}
        )

    def test_activate_project(self, mock_session, mock_client):
        mock_ses = mock_session.return_value

        ResourcesClient().activate_project('fake_project')

        mock_ses.request.assert_called_with(
            method='PUT',
            url='https://authorization-service-api.web.cern.ch/api/v1.0/'
                'OpenstackProject/fake_project/Activate',
            data={},
            headers={'Content-Type': 'application/json'}
        )

    def test_delete_project(self, mock_session, mock_client):
        mock_ses = mock_session.return_value

        ResourcesClient().delete_project('fake_project')

        mock_ses.request.assert_called_with(
            method='DELETE',
            url='https://authorization-service-api.web.cern.ch/api/v1.0/'
                'OpenstackProject/fake_project',
            data={},
            headers={'Content-Type': 'application/json'}
        )

    def test_create_project(self, mock_session, mock_client):
        mock_ses = mock_session.return_value

        mock_ses.request.side_effect = iter([
            mock.Mock(text='{"data": {"id": "fake_user_id"} }'),
            mock.Mock(text='{"data": {"id": "fake_group_id"} }'),
            mock.Mock(text='{"data": {"id": "fake_project_id"} }')])

        ResourcesClient().create_project(
            name='fake_name',
            description='fake_description',
            category='fake_category',
            owner='fake_owner',
            administrators='fake_administrators'
        )

        mock_ses.request.assert_has_calls([
            mock.call(
                method='GET',
                url='https://authorization-service-api.web.cern.ch/api/v1.0/'
                    'Identity/fake_owner',
                data={},
                headers={'Content-Type': 'application/json'}
            ),
            mock.call(
                method='GET',
                url='https://authorization-service-api.web.cern.ch/api/v1.0/'
                    'Group/fake_administrators',
                data={},
                headers={'Content-Type': 'application/json'}
            ),
            mock.call(
                method='POST',
                url='https://authorization-service-api.web.cern.ch/api/v1.0/'
                    'OpenstackProject',
                data='{'
                     '"administratorsId": "fake_group_id", '
                     '"resourceIdentifier": "fake_name", '
                     '"displayName": "fake_name", '
                     '"description": "fake_description", '
                     '"resourceCategory": "fake_category", '
                     '"ownerId": "fake_user_id"'
                     '}',
                headers={'Content-Type': 'application/json'})
        ])
