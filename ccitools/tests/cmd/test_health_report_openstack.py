import logging

from ccitools.cmd.health_report_openstack import main
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.cmd.health_report_openstack.os')
@mock.patch('ccitools.cmd.health_report_openstack.subprocess')
@mock.patch('ccitools.cmd.health_report_openstack.socket')
@mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
class TestHealthReport(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self, mock_crc, mock_so, mock_su, mock_os):
        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)

    def test_vms_proper_data(self, mock_crc, mock_so, mock_su, mock_os):
        mock_cl = mock_crc.return_value
        mock_cl.get_servers.return_value = (
            fixtures.FAKE_HEALTH_OUTPUT
        )
        mock_su.Popen().poll.return_value = None
        main([
            'vms'
        ])

    def test_vms_proper_data_returncode(self, mock_crc, mock_so, mock_su,
                                        mock_os):
        mock_cl = mock_crc.return_value
        mock_cl.get_servers.return_value = (
            fixtures.FAKE_HEALTH_OUTPUT
        )
        mock_su.Popen.return_value = mock.MagicMock(returncode=1)
        mock_su.Popen().poll.return_value = 'fake'
        main([
            'vms'
        ])

    def test_vms_proper_data_vmid_out(self, mock_crc, mock_so, mock_su,
                                      mock_os):
        mock_cl = mock_crc.return_value
        mock_cl.get_servers.return_value = (
            fixtures.FAKE_HEALTH_OUTPUT
        )
        mock_su.Popen.return_value = mock.MagicMock(returncode=0)
        mock_su.Popen().poll.return_value = 'fake'
        mock_su.Popen().stdout.read.return_value = 'fake_id'
        mock_su.Popen().process.stderr.read.return_value = 'fake_error'
        main([
            'vms'
        ])

    def test_vms_proper_data_not_in_hv(self, mock_crc, mock_so, mock_su,
                                       mock_os):
        mock_cl = mock_crc.return_value
        mock_cl.get_servers.return_value = (
            fixtures.FAKE_HEALTH_OUTPUT
        )
        mock_su.Popen.return_value = mock.MagicMock(returncode=0)
        mock_su.Popen().poll.return_value = 'fake'
        mock_su.Popen().stdout.read.return_value = 'fake_fake'
        mock_su.Popen().process.stderr.read.return_value = 'fake_error'
        main([
            'vms'
        ])

    def test_vms_proper_no_ssh(self, mock_crc, mock_so, mock_su,
                               mock_os):
        mock_cl = mock_crc.return_value
        mock_cl.get_servers.return_value = (
            fixtures.FAKE_HEALTH_OUTPUT
        )
        main([
            'vms',
            '--no-ssh',
        ])

    def test_vms_relevant_output3(self, mock_crc, mock_so, mock_su,
                                  mock_os):
        mock_cl = mock_crc.return_value
        mock_cl.get_servers.return_value = (
            fixtures.FAKE_HEALTH_OUTPUT3
        )
        main([
            'vms'
        ])

    def test_vms_relevant_output4(self, mock_crc, mock_so, mock_su,
                                  mock_os):
        mock_cl = mock_crc.return_value
        mock_cl.get_servers.return_value = (
            fixtures.FAKE_HEALTH_OUTPUT4
        )
        main([
            'vms'
        ])

    def test_volumes_relevant(self, mock_crc, mock_so, mock_su,
                              mock_os):
        mock_cl = mock_crc.return_value
        mock_cl.get_volumes.return_value = (
            fixtures.FAKE_HEALTH_OUTPUT4
        )
        mock_cl.get_server.return_value = 'fake_s'
        main([
            'volumes'
        ])

    def test_volumes_server_none(self, mock_crc, mock_so, mock_su,
                                 mock_os):
        mock_cl = mock_crc.return_value
        mock_cl.get_volumes.return_value = (
            fixtures.FAKE_HEALTH_OUTPUT4
        )
        mock_cl.get_server.return_value = None
        main([
            'volumes'
        ])

    def test_volumes_attach_empty(self, mock_crc, mock_so, mock_su, mock_os):
        mock_cl = mock_crc.return_value
        mock_cl.get_volumes.side_effect = iter(
            [
                fixtures.FAKE_HEALTH_OUTPUT4,
                fixtures.FAKE_HEALTH_OUTPUT4,
                fixtures.FAKE_HEALTH_OUTPUT5
            ]
        )
        main([
            'volumes'
        ])
