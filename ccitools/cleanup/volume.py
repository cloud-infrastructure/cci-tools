import dateutil.parser
import logging

from ccitools.cleanup import base
from ccitools import common
from datetime import datetime
from datetime import timezone

# configure logging
logger = logging.getLogger(__name__)


class VolumeResourceCleanup(base.ResourceCleanup):
    def __init__(self, cloud, quiet):
        super(VolumeResourceCleanup, self).__init__(
            'VOLUMES', cloud, quiet)

    def get_pending(self):
        volumes_pending = []
        logger.info("Looking for leftover volumes ...")
        for volume in self.cloud.get_volumes(
                user_name='svcprobe', tag='cleanup'):
            created_at = dateutil.parser.parse(volume.created_at)
            days_since_created = (
                datetime.now(timezone.utc)
                - created_at.replace(tzinfo=timezone.utc)).days
            if base.RALLY_REGEX.match(volume.name) and days_since_created >= 1:
                volumes_pending.append(volume)
        return volumes_pending

    def delete_leftovers_before(self):
        if self.leftovers:
            self.cloud.delete_volumes(self.leftovers)

    def delete_leftovers_after(self):
        pass

    def get_broken(self):
        broken_volumes = []
        for volume in self.cloud.get_volumes(
                user_name='svcprobe', tag='cleanup'):
            if volume in self.leftovers:
                broken_volumes.append(volume)
        return broken_volumes

    def format_table(self):
        volume_fields = [
            ('ID', 'id'),
            ('Name', 'name'),
            ('Status', 'status'),
            ('Created', 'created_at'),
            ('Project ID', 'os-vol-tenant-attr:tenant_id'),
        ]
        volumes_table = common.get_resources_info_table(
            self.leftovers, volume_fields)

        return volumes_table
