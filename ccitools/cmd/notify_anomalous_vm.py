#!/usr/bin/python3

import ccitools.conf
import logging
import sys

from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools.utils.sendmail import MailClient
from ccitools.utils.servicenowv2 import ServiceNowClient
from ccitools.utils.snow.ticket import IncidentState
from ccitools.utils.xldap import XldapClient


# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class NotifyAnomalousVmCMD(BaseCloudCMD):
    def __init__(self):
        super(NotifyAnomalousVmCMD, self).__init__(
            description="Script to notify user about anomalous VM")

    def open_incident(self, args, vm, project, caller, snowclient, xldap):
        hypervisor = getattr(vm, 'OS-EXT-SRV-ATTR:hypervisor_hostname')
        hv = hypervisor.replace('.cern.ch', '')
        short_description = CONF.anomalous_vm.description % vm.name
        watch_list = ''
        if 'egroup' in caller:
            watch_list = xldap.get_email(caller['egroup'])
        comment = CONF.anomalous_vm.message_snow % (
            caller['info'].first_name,
            vm.name,
            project.name,
            args.issue,
            args.extra_info,
            vm.name
        )
        work_note = CONF.anomalous_vm.work_note % (hv)
        if args.exec_mode:
            try:
                incident = snowclient.ticket.create_INC(
                    assignment_group=CONF.anomalous_vm.assignment_group,
                    caller_id=caller['info'].sys_id,
                    comments=comment,
                    description=comment,
                    functional_element=CONF.anomalous_vm.functional_element,
                    incident_state=IncidentState.WAITING_FOR_USER,
                    short_description=short_description,
                    work_notes=work_note
                )
                if watch_list != '':
                    incident.add_email_to_watch_list(watch_list)
                incident.save()
                logger.info("Ticket %s created by: %s. Description: %s" % (
                    CONF.anomalous_vm.inc_url % incident.info.number,
                    caller['info'].name,
                    short_description)
                )
                return incident
            except Exception as ex:
                logger.error("Error creating INC: %s" % ex)
        else:
            logger.info(
                "[DRYRUN] Ticket created:\n"
                "Short description: '%s'\n"
                "Functional Element: '%s'\n"
                "Assignment Group: '%s'\n"
                "Caller: '%s'\n"
                "Watchlist: '%s'\n"
                "Description: '%s'",
                short_description,
                CONF.anomalous_vm.functional_element,
                CONF.anomalous_vm.assignment_group,
                caller['info'].name,
                watch_list,
                comment)

    def send_email(self, args, vm, project, caller, mailclient, xldap,
                   inc=None):
        if 'egroup' in caller:
            mail_to = xldap.get_email(caller['egroup'])
            vm_responsible_first_name = caller['egroup']
        else:
            mail_to = caller['info'].email
            vm_responsible_first_name = caller['info'].first_name

        subject = CONF.anomalous_vm.description % vm.name
        if args.exec_mode:
            ticket_number = CONF.anomalous_vm.inc_url % inc.info.number
        else:
            ticket_number = 'fake_number'
        body = CONF.anomalous_vm.message_email % (
            vm_responsible_first_name,
            vm.name,
            project.name,
            args.issue,
            args.extra_info,
            vm.name,
            ticket_number
        )
        if args.exec_mode:
            try:
                mailclient.send_mail(
                    mail_to=mail_to,
                    mail_subject=subject,
                    mail_body=body,
                    mail_from=args.mail_from,
                    mail_cc=args.mail_cc,
                    mail_bcc=args.mail_bcc,
                    mail_content_type=args.mail_content_type)
                logger.info("Mail sent to: %s Subject: \"%s\"",
                            mail_to, subject)
            except Exception as ex:
                logger.error(
                    "Impossible to send email to '%s'. "
                    "Error message: '%s'", mail_to, ex)
        else:
            logger.info('[DRYRUN] Mail sent to: %s Subject:"%s"\n'
                        'Email body: "%s"',
                        mail_to, subject, body)

    def get_vm_user_info(self, vm, project, cloud, snowclient, xldap):
        try:
            if 'landb-responsible' in vm.metadata:
                vm_responsible = vm.metadata['landb-responsible']
            else:
                vm_responsible = vm.user_id

            if xldap.is_existing_egroup(vm_responsible):
                owner = cloud.get_project_owner(project)
                project_owner_info =\
                    snowclient.user.get_user_info_by_user_name(owner)
                responsible = {
                    'info': project_owner_info,
                    'egroup': vm_responsible
                }
                return responsible
            else:
                vm_responsible_info =\
                    snowclient.user.get_user_info_by_user_name(vm_responsible)
                responsible = {'info': vm_responsible_info}
                return responsible
        except Exception as ex:
            logger.error("Error getting VM's owner: %s" % ex)

    def notify_anomalous_vm(self, args, cloud, snowclient, mailclient, xldap):
        try:
            vm = cloud.get_servers_by_name(args.vm)
            inc_number = snowclient.ticket.get_duplicated_active_incident(
                CONF.anomalous_vm.description % vm.name
            )
            if inc_number is None or args.force_execution:
                project = cloud.get_project(vm.tenant_id)
                caller = self.get_vm_user_info(
                    vm, project, cloud, snowclient, xldap)
                if args.exec_mode:
                    inc = self.open_incident(
                        args, vm, project, caller, snowclient, xldap)
                    self.send_email(
                        args, vm, project, caller, mailclient, xldap, inc)
                else:
                    self.open_incident(
                        args, vm, project, caller, snowclient, xldap)
                    self.send_email(
                        args, vm, project, caller, mailclient, xldap)
            else:
                message = CONF.anomalous_vm.inc_url % inc_number
                logger.info(
                    "There is already an active incident regarding the "
                    "anomaly on the machine \"%s\". Please, have a look at %s"
                    % (vm.name, message))
        except Exception as ex:
            logger.error("Error notifying about anomalous VM: %s" % ex)

    def main(self, args=None):
        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true',
                               help="If present, performs the action")
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')

        self.parser.add_argument("--vm", required=True,
                                 help="Anomalous VM")
        self.parser.add_argument("--issue", required=True,
                                 help="VM issue")
        self.parser.add_argument("--extra_info", default="",
                                 help="Extra information")
        self.parser.add_argument("--mail_from", default="noreply@cern.ch",
                                 nargs='?')
        self.parser.add_argument("--mail_cc", default="",
                                 nargs='?')
        self.parser.add_argument("--mail_bcc", default="",
                                 nargs='?')
        self.parser.add_argument("--mail_content_type", default="plain",
                                 help="Mail content type (plain/html)")
        self.parser.add_argument("--instance", default="cern",
                                 help="Service now instance")
        self.parser.add_argument("--force_execution", default=False,
                                 help="Force execution of the job. "
                                 "Useful when there is already an active INC")

        args = self.parse_args(args)

        mailclient = MailClient("localhost")
        snowclient = ServiceNowClient(instance=args.instance)
        xldap = XldapClient(CONF.ldap.server)

        self.notify_anomalous_vm(
            args,
            self.cloudclient,
            snowclient,
            mailclient,
            xldap
        )


# Needs static method for setup.cfg
def main(args=None):
    NotifyAnomalousVmCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
