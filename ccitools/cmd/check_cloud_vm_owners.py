#!/usr/bin/python3

import ccitools.conf
import logging
import sys

from ccitools.cmd.base.puppetdb import BasePuppetDBCMD
from urllib.parse import urlencode

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class CheckCloudVMOwnerCMD(BasePuppetDBCMD):

    def __init__(self):
        super(CheckCloudVMOwnerCMD, self).__init__(
            description="Checks Ownership of VMs owned by cloud team")

    def check_cloud_vm_owners(self):
        """Check vms whose hostgroup matches cloud naming convention.

        cloud_*SERVICE*
        """
        cloud_hostgroups_name_convention = "cloud_infrastructure_%s_admin"

        hostgroup = "cloud_"
        fact = "is_virtual"
        query_str = '["and",  ["=", "name", "%s"], ["in", "certname", '\
                    '["extract", "certname", ["select-facts", ["and", '\
                    '["=", "name", "hostgroup"], '\
                    '["%s", "value", "^%s"]]]]]]' % (fact, "~", hostgroup)

        query = urlencode({"query": query_str})
        endpoint = "/v3/facts"

        (code, j) = self.pdb.raw_request("%s?%s" % (endpoint, query))
        error_messages = []

        for host in j:
            server_name = host['certname']
            if host['value'] == 'true':
                endpoint = "/v3/nodes/%s/facts/%s" % (
                    server_name, 'hostgroup_0')
                (code, hostgroup) = self.pdb.raw_request("%s?" % (endpoint,))
                try:
                    service_name = hostgroup[0]['value'].split('_')[1]
                except Exception:
                    error_messages.append(
                        "'%s' does not belong to a hostgroup"
                        " that matches cloud_*SERVICE*/ naming "
                        "convention" % server_name)
                    continue

                endpoint = "/v3/nodes/%s/facts/%s" % (
                    server_name, 'landb_responsible_name')
                (code, landb_responsible) = self.pdb.raw_request(
                    "%s?" % (endpoint,))
                landb_responsible = landb_responsible[0]['value'].split(
                )[1].lower()

                if service_name in ["infrastructure", "clients", "adm"]:
                    hostgroup = "cloud-infrastructure-core-admin"
                elif service_name == "workflow":
                    hostgroup = "cloud-infrastructure-workflows-admin"
                else:
                    hostgroup = (cloud_hostgroups_name_convention
                                 % service_name).replace("_", '-')

                if landb_responsible != hostgroup:
                    try:
                        server = self.cloudclient.get_servers_by_name(
                            server_name)

                        if 'landb-mainuser' not in server.metadata.keys(
                        ) or server.metadata['landb-mainuser'] != hostgroup:
                            logger.info(
                                "# %s should be owned by '%s' instead of '%s'",
                                server_name, hostgroup, landb_responsible)
                            logger.info(
                                "nova meta %s set landb-responsible='%s' "
                                "landb-mainuser='%s' || echo Failed %s",
                                server.id, hostgroup, hostgroup, server.id)
                    except Exception as ex:
                        error_messages.append(
                            "Host '%s' (owner: %s) not found in Nova"
                            ".\nReason: %s" % (
                                server_name,
                                landb_responsible, ex)
                        )

        if len(error_messages):
            logger.error("----  ERROR MESSAGES ----")
            for vm_error in error_messages:
                logger.error(vm_error)

    def main(self, args=None):
        self.parse_args(args)
        self.check_cloud_vm_owners()


# Needs static method for setup.cfg
def main(args=None):
    CheckCloudVMOwnerCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
