"""Fixture for tests."""
from ccitools.tests import fixtures as base_fixtures
from unittest import mock  # py3

LDAP_SEARCH_VM_MAINUSER = [
    (
        'CN=example,OU=CERN Linux Computers,DC=cern,DC=ch',
        {
            'manager': [
                b'CN=mainuser,OU=Users,OU=Organic Units,DC=cern,DC=ch'
            ]
        }
    )
]

LDAP_SEARCH_VM_OWNER = [
    (
        'CN=example,OU=CERN Linux Computers,DC=cern,DC=ch',
        {
            'managedBy': [
                b'CN=owner,OU=Users,OU=Organic Units,DC=cern,DC=ch'
            ]
        }
    )
]

LDAP_SEARCH_GROUP_MAIL = [
    (
        'CN=group,OU=e-groups,OU=Workgroups,DC=cern,DC=ch',
        {
            'mail': [
                b'group@cern.ch'
            ]
        }
    )
]

LDAP_SEARCH_USER_MAIL = [
    (
        'CN=user,OU=Users,OU=Organic Units,DC=cern,DC=ch',
        {
            'mail': [
                b'user@cern.ch'
            ]
        }
    )
]

LDAP_SEARCH_PRIMARY = [
    (
        'CN=primary,OU=Users,OU=Organic Units,DC=cern,DC=ch',
        {
            'sAMAccountName': [
                b'primary'
            ],
            'cernAccountType': [
                b'Primary'
            ]
        }
    )
]

LDAP_SEARCH_SECONDARY = [
    (
        'CN=secondary,OU=Users,OU=Organic Units,DC=cern,DC=ch',
        {
            'cernAccountOwner': [
                b'CN=primary,OU=Users,OU=Organic Units,DC=cern,DC=ch'
            ],
            'sAMAccountName': [
                b'secondary'
            ],
            'cernAccountType': [
                b'Secondary'
            ]
        }
    )
]

LDAP_SEARCH_SERVICE = [
    (
        'CN=service,OU=Users,OU=Organic Units,DC=cern,DC=ch', {
            'cernAccountOwner': [
                b'CN=primary,OU=Users,OU=Organic Units,DC=cern,DC=ch'
            ],
            'sAMAccountName': [
                b'service'
            ],
            'cernAccountType': [
                b'Service'
            ]
        }
    )
]

LDAP_SEARCH_FAKE = [
    (
        'CN=fake,OU=Users,OU=Organic Units,DC=cern,DC=ch', {
            'sAMAccountName': [
                b'fake'
            ],
            'cernAccountType': [
                b'fake'
            ]
        }
    )
]

EMPTY_CATALOG = {
    'catalog': []
}

CATALOG_ALT_REGION = {
    "catalog": [
        {
            "endpoints": [
                {
                    "url": "http://host.domain/v3",
                    "interface": "public",
                    "region": "pdc",
                    "id": "1"
                }
            ],
            "type": "identity",
            "id": "1",
            "name": "keystone"
        },
        {
            "endpoints": [
                {
                    "url": "http://host.domain:9774",
                    "interface": "public",
                    "region": "pdc",
                    "id": "1"
                }
            ],
            "type": "compute",
            "id": "2",
            "name": "nova"
        },
        {
            "endpoints": [
                {
                    "url": "http://host.domain:9696",
                    "interface": "public",
                    "region": "pdc",
                    "id": "1"
                }
            ],
            "type": "network",
            "id": "3",
            "name": "neutron"
        },
        {
            "endpoints": [
                {
                    "url": "http://host.domain:8776",
                    "interface": "public",
                    "region": "pdc",
                    "id": "1"
                }
            ],
            "type": "volumev3",
            "id": "4",
            "name": "cinder"
        },
        {
            "endpoints": [
                {
                    "url": "http://host.domain:8786",
                    "interface": "public",
                    "region": "pdc",
                    "id": "1"
                }
            ],
            "type": "sharev2",
            "id": "5",
            "name": "manila"
        },
        {
            "endpoints": [
                {
                    "url": "http://host.domain:80",
                    "interface": "public",
                    "region": "pdc",
                    "id": "1"
                }
            ],
            "type": "s3",
            "id": "6",
            "name": "s3"
        },
        {
            "endpoints": [
                {
                    "url": "http://host.domain:9292",
                    "interface": "public",
                    "region": "pdc",
                    "id": "1"
                }
            ],
            "type": "image",
            "id": "7",
            "name": "glance"
        },
        {
            "endpoints": [
                {
                    "url": "http://host.domain:9511/v1",
                    "interface": "public",
                    "region": "pdc",
                    "id": "1"
                }
            ],
            "type": "container-infra",
            "id": "10",
            "name": "magnum"
        },
        {
            "endpoints": [
                {
                    "url": "http://host.domain",
                    "interface": "public",
                    "region": "pdc",
                    "id": "1"
                }
            ],
            "type": "load-balancer",
            "id": "8",
            "name": "octavia"
        },
        {
            "endpoints": [
                {
                    "url": "http://host.domain:8778",
                    "interface": "public",
                    "region": "pdc",
                    "id": "1"
                }
            ],
            "type": "placement",
            "id": "9",
            "name": "placement"
        },
    ]
}

MULTI_REGION_CATALOG = {
    "catalog": [
        {
            "endpoints": [
                {
                    "url": "http://identitycern.domain/v3",
                    "interface": "public",
                    "region": "cern",
                    "id": "1"
                }
            ],
            "type": "identity",
            "id": "1",
            "name": "keystone"
        },
        {
            "endpoints": [
                {
                    "url": "http://identityalt.domain/v3",
                    "interface": "public",
                    "region": "alt",
                    "id": "1"
                }
            ],
            "type": "identity",
            "id": "1",
            "name": "keystone"
        },
    ]
}

CATALOG = {
    "catalog": [
        {
            "endpoints": [
                {
                    "url": "http://host.domain/v3",
                    "interface": "public",
                    "region": "cern",
                    "id": "1"
                }
            ],
            "type": "identity",
            "id": "1",
            "name": "keystone"
        },
        {
            "endpoints": [
                {
                    "url": "http://host.domain:9774",
                    "interface": "public",
                    "region": "cern",
                    "id": "1"
                }
            ],
            "type": "compute",
            "id": "2",
            "name": "nova"
        },
        {
            "endpoints": [
                {
                    "url": "http://host.domain:9696",
                    "interface": "public",
                    "region": "cern",
                    "id": "1"
                }
            ],
            "type": "network",
            "id": "3",
            "name": "neutron"
        },
        {
            "endpoints": [
                {
                    "url": "http://host.domain:8776",
                    "interface": "public",
                    "region": "cern",
                    "id": "1"
                }
            ],
            "type": "volumev3",
            "id": "4",
            "name": "cinder"
        },
        {
            "endpoints": [
                {
                    "url": "http://host.domain:8786",
                    "interface": "public",
                    "region": "cern",
                    "id": "1"
                }
            ],
            "type": "sharev2",
            "id": "5",
            "name": "manila"
        },
        {
            "endpoints": [
                {
                    "url": "http://host.domain:80",
                    "interface": "public",
                    "region": "cern",
                    "id": "1"
                }
            ],
            "type": "s3",
            "id": "6",
            "name": "s3"
        },
        {
            "endpoints": [
                {
                    "url": "http://host.domain:9292",
                    "interface": "public",
                    "region": "cern",
                    "id": "1"
                }
            ],
            "type": "image",
            "id": "7",
            "name": "glance"
        },
        {
            "endpoints": [
                {
                    "url": "http://host.domain:9511/v1",
                    "interface": "public",
                    "region": "cern",
                    "id": "1"
                }
            ],
            "type": "container-infra",
            "id": "10",
            "name": "magnum"
        },
        {
            "endpoints": [
                {
                    "url": "http://host.domain",
                    "interface": "public",
                    "region": "cern",
                    "id": "1"
                }
            ],
            "type": "load-balancer",
            "id": "8",
            "name": "octavia"
        },
        {
            "endpoints": [
                {
                    "url": "http://host.domain:8778",
                    "interface": "public",
                    "region": "cern",
                    "id": "1"
                }
            ],
            "type": "placement",
            "id": "9",
            "name": "placement"
        },
    ]
}

PRODUCTION_MULTI_REGION = (
    base_fixtures.Resource({
        'id': 'cern',
        'description': 'production,lifecycle'
    }),
    base_fixtures.Resource({
        'id': 'alt',
        'description': 'production,lifecycle'
    })
)

PRODUCTION_REGION = (
    base_fixtures.Resource({
        'id': 'cern',
        'description': 'production,lifecycle'
    })
)

ALT_REGION = (
    base_fixtures.Resource({
        'id': 'pdc',
        'description': 'production,lifecycle'
    })
)

DEVELOPMENT_REGION = (
    base_fixtures.Resource({
        'id': 'development',
        'description': 'development'
    })
)

SERVER = {
    "id": "1",
    "name": "fake_vm",
    "created": "2001-01-01T12:00:00Z",
    "tenant_id": "fake_project",
    "host": "fake_host.cern.ch",
    "status": "ACTIVE",
    "user_id": "fake_user"
}

PROJECT = {
    'id': 'fake_id',
    'name': 'fake_project'
}

SERVICE = {
    'id': 'fake_id',
    'host': 'fake_host',
    'binary': 'nova-compute'
}

AGGREGATE = {
    'id': '1',
    'name': 'aggregate_name',
}

HYPERVISOR_ONE = {
    'id': '1',
    'hypervisor_hostname': 'fake_hostname',
}

HYPERVISOR_TWO = {
    'id': '2',
    'hypervisor_hostname': 'fake_fake',
}

AGENT = {
    'id': 'fake_id',
    'host': 'fake_host',
    'binary': 'neutron-linuxbridge-agent'
}

NETWORKS = {
    'networks': [
        {
            'id': 'fake_network_id',
            'name': 'CERN_NETWORK',
        }
    ]
}

VOLUME = {
    "id": "1",
    "name": "fake_volume",
    "user_id": "fake_user",
    "status": 'available',
    "tenant_id": "fake_project"
}

VOLUME_TYPE_PUBLIC = {
    'id': '1',
    'name': 'type_public',
    'is_public': True,
    'description': 'unit_value: 1'
}

VOLUME_TYPE_PRIVATE = {
    'id': '2',
    'name': 'type_private',
    'is_public': False,
    'description': 'unit_value: 2'
}

VOLUME_TYPE_NO_DESCRIPTION = {
    'id': '3',
    'name': 'type_no_description',
    'is_public': True,
    'description': 'unit_value: 1'
}

VOLUME_TYPE_EMPTY_DESCRIPTION = {
    'id': '4',
    'name': 'type_empty_description',
    'is_public': True,
    'description': ''
}

VOLUME_SNAPSHOT = {
    'id': '1',
    "name": "snapshot",
    "user_id": "fake_user",
    "tenant_id": "fake_project"
}

IMAGE = {
    'id': '1',
    'name': 'image',
    'owner': 'fake_project',
    'visibility': 'private'
}

SHARE = {
    "id": "1",
    "name": "fake_share",
    "user_id": "fake_user",
    "status": 'available',
    "tenant_id": "fake_project"
}

SHARE_TYPE_PUBLIC = {
    'id': '1',
    'name': 'type_public',
    'is_public': True,
    'description': 'unit_value: 1'
}

SHARE_TYPE_PRIVATE = {
    'id': '2',
    'name': 'type_private',
    'is_public': False,
    'description': 'unit_value: 2'
}

SHARE_TYPE_NO_DESCRIPTION = {
    'id': '3',
    'name': 'type_public',
    'is_public': True,
}

SHARE_TYPE_EMPTY_DESCRIPTION = {
    'id': '4',
    'name': 'type_private',
    'is_public': False,
    'description': ''
}

ACTION = {
    "action": "start",
    "request_id": "req_fake_id",
    "project_id": "fake_project",
    "user_id": "fake_user"
}

ASIGN_USER = {
    "user": {
        "id": 'fake_user'
    },
    "role": {
        "name": "owner"
    },
    "scope": {
        "project": {
            "id": "1",
            "name": "fake_project"
        }
    }
}

ASIGN_GROUP = {
    "group": {
        "id": 'fake_group'
    },
    "role": {
        "name": "Member"
    },
    "scope": {
        "project": {
            "id": "1",
            "name": "fake_project"
        }
    }
}

GROUP = {
    "id": "fake_group_id",
    "name": "fake_group"
}

ENDPOINT_GROUP_PRODUCTION = {
    "id": "base_id",
    "name": "base_name",
    "filters": {
        "service_id": ['cern_service_id'],
        "region_id": "cern"
    }
}

ENDPOINT_GROUP_BASE = {
    "id": "base_id",
    "name": "base_name",
    "filters": {
        "service_id": ['cern_service_id'],
        "region_id": "cern"
    }
}

ENDPOINT_GROUP_S3 = {
    "id": "base_id",
    "name": "s3",
    "filters": {
        "service_id": ['s3_service_id'],
        "region_id": "cern"
    }
}

MEMBER_ROLE = {
    "id": "fake_role_id",
    "name": "Member"
}

NOVA_QUOTA = {
    "instances": 5,
    "cores": 20,
    "ram": 37500
}

NOVA_QUOTA_DETAIL = {
    "instances": {
        'in_use': 1,
        'limit': 5,
        'reserved': 0
    },
    "cores": {
        'in_use': 1,
        'limit': 20,
        'reserved': 0
    },
    "ram": {
        'in_use': 7500,
        'limit': 37500,
        'reserved': 0
    }
}

CINDER_QUOTA = {
    "gigabytes": 0,
    "gigabytes_cp1": 0,
    "gigabytes_cpio1": 0,
    "gigabytes_io1": 0,
    "gigabytes_standard": 0,
    "snapshots": 0,
    "snapshots_cp1": -1,
    "snapshots_cpio1": -1,
    "snapshots_io1": -1,
    "snapshots_standard": -1,
    "volumes": 0,
    "volumes_cp1": 0,
    "volumes_cpio1": 0,
    "volumes_io1": 0,
    "volumes_standard": 0,
    "backups": 0,
    "backup_gigabytes": 0,
}

MANILA_QUOTA_GLOBAL = {
    "gigabytes": 0,
    "shares": 0,
    "snapshot_gigabytes": 0,
    "snapshots": 0
}

MANILA_QUOTA_GLOBAL_DETAIL = {
    "gigabytes": {
        'in_use': 0,
        'limit': 0,
        'reserved': 0
    },
    "shares": {
        'in_use': 0,
        'limit': 0,
        'reserved': 0
    },
    "snapshot_gigabytes": {
        'in_use': 0,
        'limit': 0,
        'reserved': 0
    },
    "snapshots": {
        'in_use': 0,
        'limit': 0,
        'reserved': 0
    }
}

MANILA_QUOTA_PRODUCTION = {
    "gigabytes": 0,
    "shares": 0,
    "snapshot_gigabytes": 0,
    "snapshots": 0
}

MANILA_QUOTA_PRODUCTION_DETAIL = {
    "gigabytes": {
        'in_use': 0,
        'limit': 0,
        'reserved': 0
    },
    "shares": {
        'in_use': 0,
        'limit': 0,
        'reserved': 0
    },
    "snapshot_gigabytes": {
        'in_use': 0,
        'limit': 0,
        'reserved': 0
    },
    "snapshots": {
        'in_use': 0,
        'limit': 0,
        'reserved': 0
    }
}

MANILA_QUOTA_TESTING = {
    "gigabytes": 0,
    "shares": 0,
    "snapshot_gigabytes": 0,
    "snapshots": 0
}

MANILA_QUOTA_TESTING_DETAIL = {
    "gigabytes": {
        'in_use': 0,
        'limit': 0,
        'reserved': 0
    },
    "shares": {
        'in_use': 0,
        'limit': 0,
        'reserved': 0
    },
    "snapshot_gigabytes": {
        'in_use': 0,
        'limit': 0,
        'reserved': 0
    },
    "snapshots": {
        'in_use': 0,
        'limit': 0,
        'reserved': 0
    }
}

MANILA_QUOTA_NON_PUBLIC = {
    "gigabytes": 0,
    "shares": 0,
    "snapshot_gigabytes": 0,
    "snapshots": 0
}

MANILA_QUOTA_NON_PUBLIC_DETAIL = {
    "gigabytes": {
        'in_use': 0,
        'limit': 0,
        'reserved': 0
    },
    "shares": {
        'in_use': 0,
        'limit': 0,
        'reserved': 0
    },
    "snapshot_gigabytes": {
        'in_use': 0,
        'limit': 0,
        'reserved': 0
    },
    "snapshots": {
        'in_use': 0,
        'limit': 0,
        'reserved': 0
    }
}

SHARE_TYPE_PRODUCTION = {
    "id": 1,
    "name": "production",
    "is_public": True
}

SHARE_TYPE_TESTING = {
    "id": 2,
    "name": "testing",
    "is_public": True
}

SHARE_TYPE_NON_PUBLIC = {
    "id": 3,
    "name": "non_public",
    "is_public": False
}

S3_USER_STATS = {
    "num_objects": 1,
    "size": 1024,
    "size_actual": 4096,
    "size_kb": 1,
    "size_kb_actual": 4,
    "size_kb_utilized": 0,
    "size_utilized": 0
}

S3_USER = mock.Mock()
S3_USER.get_buckets.return_value = ['0', '1']
type(S3_USER).max_buckets = mock.PropertyMock(
    return_value=2)
type(S3_USER).stats = mock.PropertyMock(
    return_value=mock.Mock(**S3_USER_STATS))

S3_QUOTA = {
    "quota_type": "user",
    "max_size_kb": 1024
}

NEUTRON_QUOTA = {
    'quota': {
        'subnet': 0,
        'network': 0,
        'security_group_rule': 4,
        'pool': 0,
        'subnetpool': -1,
        'listener': -1,
        'member': 0,
        'floatingip': 0,
        'security_group': 1,
        'router': 0,
        'port': 0,
    }
}

OCTAVIA_QUOTA = {
    'load_balancer': 0,
    'listener': 0,
    'pool': 0,
    'health_monitor': 0,
    'member': 0,
    'l7policy': 0,
    'l7rule': 0,
}

QUOTA_VOLUME_TYPES = [
    base_fixtures.Resource({
        'id': '1',
        'name': 'standard',
        'description': None
    }),
    base_fixtures.Resource({
        'id': '2',
        'name': 'io1',
        'description': None
    }),
    base_fixtures.Resource({
        'id': '3',
        'name': 'cp1',
        'description': None
    }),
    base_fixtures.Resource({
        'id': '4',
        'name': 'cpio1',
        'description': None
    }),
    base_fixtures.Resource({
        'id': '5',
        'name': 'vault-100',
        'description': 'vault_100'
    }),
    base_fixtures.Resource({
        'id': '6',
        'name': 'vault-500',
        'description': 'vault_500'
    })
]

QUOTA_SHARE_TYPES = [
    base_fixtures.Resource({
        'id': '1',
        'name': 'Geneva CephFS Testing',
        'description': 'geneva_testing'
    }),
    base_fixtures.Resource({
        'id': '2',
        'name': 'Meyrin CephFS',
        'description': 'meyrin'
    })
]

MERGE_EMPTY_QUOTA = {
    'quota': {}
}

MERGE_QUOTA_COMPUTE = {
    'quota': {
        'cern': {
            'compute': {
                'instances': 5,
                'cores': 10,
                'ram': 20,
            }
        }
    }
}

MERGE_QUOTA_INSTANCES_COMPUTE = {
    'quota': {
        'cern': {
            'compute': {
                'instances': 5,
            }
        }
    }
}

MERGE_QUOTA_UNLIMITED_RAM_COMPUTE = {
    'quota': {
        'cern': {
            'compute': {
                'ram': -1,
            }
        }
    }
}

MERGE_QUOTA_COMPUTE_AND_NETWORK = {
    'quota': {
        'cern': {
            'compute': {
                'instances': 5,
                'cores': 10,
                'ram': 20,
            },
            'network': {
                'floatingips': 4,
            },
            'loadbalancer': {
                'loadbalancers': 2,
            }
        }
    }
}

MERGE_FULL_QUOTA = {
    'quota': {
        'cern': {
            'compute': {
                'instances': 5,
                'cores': 10,
                'ram': 20,
            },
            'blockstorage': {
                'standard': {
                    'volumes': 1,
                    'gigabytes': 2,
                },
                'io1': {
                    'volumes': 3,
                    'gigabytes': 4,
                },
                'cp1': {
                    'volumes': 5,
                    'gigabytes': 6,
                },
                'cpio1': {
                    'volumes': 7,
                    'gigabytes': 8,
                }
            },
            'object': {
                'buckets': 1,
                'gigabytes': 2,
            },
            'network': {
                'floatingips': 4,
            },
            'loadbalancer': {
                'loadbalancers': 2,
            }
        }
    }
}

MERGE_FULL_QUOTA_MODIFIED = {
    'quota': {
        'cern': {
            'compute': {
                'instances': 10,
                'cores': 20,
                'ram': 40,
            },
            'blockstorage': {
                'standard': {
                    'volumes': 2,
                    'gigabytes': 4,
                },
                'io1': {
                    'volumes': 6,
                    'gigabytes': 8,
                },
                'cp1': {
                    'volumes': 10,
                    'gigabytes': 12,
                },
                'cpio1': {
                    'volumes': 14,
                    'gigabytes': 16,
                }
            },
            'object': {
                'buckets': 100,
                'gigabytes': 200,
            },
            'network': {
                'floatingips': 8,
            },
            'loadbalancer': {
                'loadbalancers': 4,
            }
        }
    }
}

OBJECT_QUOTA = {
    'quota': {
        'cern': {
            'object': {
                'buckets': 1,
                'gigabytes': 2,
            }
        }
    }
}

PRODUCTION_QUOTA = {
    'quota': {
        'cern': {
            'compute': {
                'instances': 5,
                'cores': 10,
                'ram': 20,
            },
            'blockstorage': {
                'standard': {
                    'volumes': 1,
                    'gigabytes': 2,
                }
            },
            'fileshare': {
                'production': {
                    'shares': 1,
                    'gigabytes': 2,
                }
            },
            'object': {
                'buckets': 1,
                'gigabytes': 2,
            },
            'network': {
                'networks': 2,
            },
            'loadbalancer': {
                'loadbalancers': 2,
            }
        }
    }
}

CLOUD_FILTER_QUOTA = {
    'filtered': {
        'cern': {
            'compute': {
                'ram': 30
            },
            'fileshare': {
                'Geneva CephFS Testing': {
                    'shares': 20
                }
            }
        }
    },
    'quota': {
        'cern': {
            'compute': {
                'instances': 10,
                'cores': 20,
                'ram': 30,
            },
            'blockstorage': {
                'standard': {
                    'volumes': 2,
                    'gigabytes': 4,
                },
                'io1': {
                    'volumes': 6,
                    'gigabytes': 8,
                },
                'cp1': {
                    'volumes': 10,
                    'gigabytes': 12,
                },
                'cpio1': {
                    'volumes': 14,
                    'gigabytes': 16,
                },
                'vault-100': {
                    'volumes': 18,
                    'gigabytes': 20,
                },
                'vault-500': {
                    'volumes': 22,
                    'gigabytes': 24,
                }
            },
            'fileshare': {
                'Geneva CephFS Testing': {
                    'shares': 20,
                    'gigabytes': 20,
                },
                'Meyrin CephFS': {
                    'shares': 30,
                    'gigabytes': 40,
                }
            },
            'object': {
                'buckets': 100,
                'gigabytes': 200,
            },
            'network': {
                'floatingips': 4,
            }
        }
    },
    'current': {
        'cern': {
            'compute': {
                'instances': 10,
                'cores': 20,
                'ram': 40,
            },
            'blockstorage': {
                'standard': {
                    'volumes': 2,
                    'gigabytes': 4,
                },
                'io1': {
                    'volumes': 6,
                    'gigabytes': 8,
                },
                'cp1': {
                    'volumes': 10,
                    'gigabytes': 12,
                },
                'cpio1': {
                    'volumes': 14,
                    'gigabytes': 16,
                },
                'vault-100': {
                    'volumes': 18,
                    'gigabytes': 20,
                },
                'vault-500': {
                    'volumes': 22,
                    'gigabytes': 24,
                }
            },
            'fileshare': {
                'Geneva CephFS Testing': {
                    'shares': 10,
                    'gigabytes': 20,
                },
                'Meyrin CephFS': {
                    'shares': 30,
                    'gigabytes': 40,
                }
            },
            'object': {
                'buckets': 100,
                'gigabytes': 200,
            },
            'network': {
                'floatingips': 4,
            }
        }
    }
}

FILTER_QUOTA_EMPTY = {
    'filtered': {},
    'quota': {
        'cern': {
            'compute': {
                'instances': 10,
                'cores': 20,
                'ram': 30,
            },
            'blockstorage': {
                'standard': {
                    'volumes': 2,
                    'gigabytes': 4,
                },
                'io1': {
                    'volumes': 6,
                    'gigabytes': 8,
                },
                'cp1': {
                    'volumes': 10,
                    'gigabytes': 12,
                },
                'cpio1': {
                    'volumes': 14,
                    'gigabytes': 16,
                },
                'vault-100': {
                    'volumes': 18,
                    'gigabytes': 20,
                },
                'vault-500': {
                    'volumes': 22,
                    'gigabytes': 24,
                }
            },
            'fileshare': {
                'Geneva CephFS Testing': {
                    'shares': 10,
                    'gigabytes': 20,
                },
                'Meyrin CephFS': {
                    'shares': 30,
                    'gigabytes': 40,
                }
            },
            'object': {
                'buckets': 100,
                'gigabytes': 200,
            }
        }
    },
    'current': {
        'cern': {
            'compute': {
                'instances': 10,
                'cores': 20,
                'ram': 30,
            },
            'blockstorage': {
                'standard': {
                    'volumes': 2,
                    'gigabytes': 4,
                },
                'io1': {
                    'volumes': 6,
                    'gigabytes': 8,
                },
                'cp1': {
                    'volumes': 10,
                    'gigabytes': 12,
                },
                'cpio1': {
                    'volumes': 14,
                    'gigabytes': 16,
                },
                'vault-100': {
                    'volumes': 18,
                    'gigabytes': 20,
                },
                'vault-500': {
                    'volumes': 22,
                    'gigabytes': 24,
                }
            },
            'fileshare': {
                'Geneva CephFS Testing': {
                    'shares': 10,
                    'gigabytes': 20,
                },
                'Meyrin CephFS': {
                    'shares': 30,
                    'gigabytes': 40,
                }
            },
            'object': {
                'buckets': 100,
                'gigabytes': 200,
            }
        }
    }
}

FILTER_QUOTA_MISSING_DATA = {
    'filtered': {},
    'quota': {
        'cern': {
            'blockstorage': {
                'standard': {
                    'volumes': 2,
                    'gigabytes': 4,
                },
                'io1': {
                    'volumes': 6,
                    'gigabytes': 8,
                },
                'cp1': {
                    'volumes': 10,
                    'gigabytes': 12,
                },
                'cpio1': {
                    'gigabytes': 16,
                }
            },
            'fileshare': {
                'Geneva CephFS Testing': {
                    'shares': 10,
                    'gigabytes': 20,
                },
                'Meyrin CephFS': {
                    'shares': 30,
                    'gigabytes': 40,
                }
            },
            'object': {
                'gigabytes': 200,
            }
        }
    },
    'current': {
        'cern': {
            'blockstorage': {
                'standard': {
                    'volumes': 2,
                    'gigabytes': 4,
                },
                'io1': {
                    'volumes': 6,
                    'gigabytes': 8,
                },
                'cp1': {
                    'volumes': 10,
                    'gigabytes': 12,
                },
                'cpio1': {
                    'gigabytes': 16,
                }
            },
            'fileshare': {
                'Geneva CephFS Testing': {
                    'shares': 10,
                    'gigabytes': 20,
                },
                'Meyrin CephFS': {
                    'shares': 30,
                    'gigabytes': 40,
                }
            },
            'object': {
                'gigabytes': 200,
            }
        }
    }
}

OWNER_ROLE = base_fixtures.Resource({
    "id": "owner_id",
    "name": "owner"
})

RESOURCE_PROVIDERS = {
    "resource_providers": [
        {
            "uuid": "b203eae5-7596-483b-8265-2203f990fb18",
            "name": "p06636663y12286.cern.ch",
            "generation": 29011,
        }
    ]
}

ALLOCATIONS = {
    "regionOne": {
        "allocations": {
            "b203eae5-7596-483b-8265-2203f990fb18": {
                "resources": {
                    "VCPU": 4,
                    "MEMORY_MB": 7500,
                    "DISK_GB": 40
                },
                "generation": 29011
            }
        },
        "project_id": "c34de133-1e4c-4a7f-b7b1-97fadb07ad61",
        "user_id": "svcprobe"
    }
}
