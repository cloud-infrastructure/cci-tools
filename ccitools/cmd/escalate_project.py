#!/usr/bin/python3

import ccitools.conf
import logging
import prettytable
import sys

from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools.utils.fim import FIMClient
from ccitools.utils.servicenowv2 import ServiceNowClient

WATCHLIST_DEPARTMENTS = ['atlas', 'alice', 'cms', 'lhcb']
WATCHLIST_EGROUP = "cloud-infrastructure-%s-resource-coordinators@cern.ch"
WORKNOTE_MESSAGE = """Dear HW Resources manager,

Could you please review the following project creation request?

%s
Best regards,
        Cloud Infrastructure Team"""
MESSAGE = """Dear %s,

Your Project Creation request has been received and sent to
HW Resources management in order to be evaluated.

Your request will be applied after approval.

Thank you,
        Cloud Infrastructure Team"""

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class EscalateProjectCMD(BaseCloudCMD):

    def __init__(self):
        super(EscalateProjectCMD, self).__init__(
            description="Script to handle quota updates requests")

    def request_summary(self, rp_obj):
        t = prettytable.PrettyTable(["Field", "Value"])
        t.add_row(["Project Name:", "%s" % (rp_obj.projectname)])
        t.add_row(["Chargegroup:", "%s" % (rp_obj.chargegroup)])
        t.add_row(["Chargerole:", "%s" % (rp_obj.chargerole)])
        t.add_row(["Description:", "%s" % (rp_obj.description)])
        t.add_row(["", ""])
        t.add_row(["Number of virtual machines:", "%s" % (rp_obj.instances)])
        t.add_row(["Number of cores:", "%s" % (rp_obj.cores)])
        t.add_row(["Amount of RAM:", "%s GB" % (rp_obj.ram)])
        t.add_row(["Amount of diskspace:", "%s GB" % (rp_obj.gigabytes)])
        t.add_row(["Number of volumes:", "%s" % (rp_obj.volumes)])
        t.border = True
        t.header = True
        t.align["Field"] = 'l'
        t.align["Value"] = 'l'
        return t

    def worknote_message(self, summary):
        return WORKNOTE_MESSAGE % (self.summary_to_monospace(summary))

    def summary_to_monospace(self, summary):
        summary = "<br/>".join(summary.get_string().split("\n"))
        summary = "%s%s%s" % ("[code]<pre>", summary, "</pre>[/code]")
        return summary

    def verify_values(self, rp, fimclient):
        logger.info("Verifying request...")
        if fimclient.is_valid_owner(rp.owner):
            logger.info("User '%s' is a valid primary account", rp.owner)
        else:
            raise Exception("User '%s' is not a primary account"
                            "Please check RP values.", rp.owner)

        if self.cloudclient.is_group(rp.egroup):
            logger.info("'%s' is an existing e-group", rp.egroup)
            return True
        else:
            raise Exception("e-group '%s' not found in LDAP.It either "
                            "does not exist or -if it does- it is "
                            "configured as 'Egroups only'. Please "
                            "verify egroup properties.", rp.egroup)

    def preprocess_request(self, ticket_number, fe, assignment_group,
                           fimclient, snowclient, exec_mode=False):
        rp = snowclient.get_project_creation_rp(ticket_number)
        if self.verify_values(rp.info, fimclient):
            if exec_mode:
                if hasattr(rp.info, 'chargegroup'):
                    for dep in WATCHLIST_DEPARTMENTS:
                        if dep in rp.info.chargegroup.lower().split():
                            logger.info("Adding '%s' to %s",
                                        WATCHLIST_EGROUP % dep,
                                        ticket_number)
                            rp.add_email_watch_list(
                                WATCHLIST_EGROUP % dep)

                username = snowclient.user.get_user_info_by_sys_id(
                    rp.info.u_caller_id).first_name
                rp.add_comment(MESSAGE % username)
                logger.info(
                    "Add comment to '%s' with information for the user",
                    ticket_number)
                rp.add_work_note(
                    self.worknote_message(self.request_summary(rp.info)))
                logger.info("Add worknote to '%s' with requested information",
                            ticket_number)

                snowclient.change_functional_element(ticket_number, fe,
                                                     assignment_group)
                logger.info("Change FE and assignment_group to %s and %s",
                            fe, assignment_group)

            else:
                logger.info("[DRYRUN] Please verify the rest of the values "
                            "before running it in perform mode.")

    def main(self, args=None):
        self.parser.add_argument(
            "--ticket-number", required=True,
            help="Quota update request ticket number")
        self.parser.add_argument(
            "--instance", default="cern",
            help="Service-Now instance")

        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true',
                               help="If present, sends email")
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')

        self.parser.add_argument(
            "--fe", required=True,
            help="Functional element to assign the ticket to")
        self.parser.add_argument(
            "--assignment-group", required=True,
            help="Group to assign the ticket to")

        args = self.parse_args(args)

        snowclient = ServiceNowClient(instance=args.instance)

        fimclient = FIMClient(CONF.fim.webservice)

        self.preprocess_request(args.ticket_number, args.fe,
                                args.assignment_group, fimclient, snowclient,
                                args.exec_mode)


# Needs static method for setup.cfg
def main(args=None):
    EscalateProjectCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logging.exception(e)
        sys.exit(-1)
