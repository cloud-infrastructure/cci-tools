from oslo_config import cfg

functional_element = cfg.StrOpt(
    'functional_element',
    default='Cloud Infrastructure',
    help="functional element to create the ticket in case of error")

assignment_group = cfg.StrOpt(
    'assignment_group',
    default='Cloud Infrastructure 3rd Line Support',
    help="assignment group to create the ticket in case of error")

available_jobs = cfg.DictOpt(
    'available_jobs',
    default={
        'servers': {
            'driver': 'server',
        },
        'volumes': {
            'driver': 'volume',
        },
        'shares_global': {
            'driver': 'share',
            'kwargs': {
                'project_id': 'fc1053ce-677f-4010-838a-01cfb7ce2ea4',
            }
        },
        'keypairs': {
            'driver': 'keypair',
        },
        'loadbalancers': {
            'driver': 'loadbalancer',
        },
    }
)

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    functional_element,
    assignment_group,
    available_jobs
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
