#!/usr/bin/python3

import logging
import stevedore
import sys

from itertools import compress
from subprocess import call  # nosec

from ccitools.cmd.base.cloud import BaseCloudCMD
from cinderclient.v3 import client as cinder_client
from keystoneclient.v3 import client as keystone_client
from neutronclient.v2_0 import client as neutron_client

from novaclient import client as nova_client
from osc_lib import utils

_logger = logging.getLogger(__name__)

# formatters
_formatter_namespace = 'cliff.formatter.list'
_formatter_default = 'table'


def _load_formatter_plugins():
    # Here so tests can override
    return stevedore.ExtensionManager(
        _formatter_namespace,
        invoke_on_load=True, )


_formatter_plugins = _load_formatter_plugins()
_non_desired_columns = [
    'x_openstack_request_ids', 'links', '_info', '_loaded', 'manager'
]


class ProjectIteratorCMD(BaseCloudCMD):

    def __init__(self):
        super(ProjectIteratorCMD, self).__init__(
            description="Tool to iterate over projects and trigger actions")

    def project_iterator(self, args):
        session = self.cloudclient.session
        keystoneclient = keystone_client.Client(
            session=session, region_name=self.cloudclient.region_name)
        cinderclient = cinder_client.Client(
            session=session, region_name=self.cloudclient.region_name)
        neutronclient = neutron_client.Client(
            session=session, region_name=self.cloudclient.region_name)
        novaclient = nova_client.Client(
            version='2', session=session,
            region_name=self.cloudclient.region_name)

        _logger.info("Retrieving list of projects on default domain")
        projects = keystoneclient.projects.list(domain='default')
        _logger.info("Retrieved %s project(s)", len(projects))

        filtered = [
            project for project in projects
            if eval(args.filter)] if args.filter else projects  # nosec
        _logger.info("Apply action on %s project(s)", len(filtered))

        for project in filtered:
            if args.script:
                _logger.info('Execute script %s', args.script)
                if args.dry_run:
                    print(args.script)
                else:
                    exec(args.script, globals(), locals())  # nosec
            if args.action:
                action = args.action.format(project.id)
                _logger.info('Execute action %s', action)
                if args.dry_run:
                    print(action)
                else:
                    try:
                        retcode = call(action.split(' '))  # nosec
                        if retcode < 0:
                            _logger.error("Child was terminated by signal %s",
                                          -retcode)
                        else:
                            _logger.info("Child returned %s", retcode)
                    except OSError:
                        _logger.exception("Execution failed")

        # Format output
        if not filtered:
            print('Filter return no project')
            raise SystemExit(1)
        if not args.quiet:
            formatter = _formatter_plugins[args.formatter].obj
            # Only consider columns that have desired elements
            columns = []
            for entry in filtered:
                for c in entry.__dict__.keys():
                    if c not in _non_desired_columns and c not in columns:
                        columns.append(c)

            data = (utils.get_item_properties(s, columns, formatters={})
                    for s in filtered)
            columns_to_include, selector = self._generate_columns_and_selector(
                args, columns)
            if selector:
                data = (list(compress(row, selector)) for row in data)
            formatter.emit_list(columns_to_include, data, sys.stdout, args)

    def _generate_columns_and_selector(self, args, column_names):
        if not args.columns:
            columns_to_include = column_names
            selector = None
        else:
            columns_to_include = [c for c in column_names if c in args.columns]
            if not columns_to_include:
                raise ValueError(
                    'No recognized column names in %s' % str(args.columns))
            # Set up argument to compress()
            selector = [(c in columns_to_include) for c in column_names]
        return columns_to_include, selector

    def main(self, args=None):
        self.parser.add_argument(
            '--filter',
            dest='filter',
            help='filter in python to reduce the list of projects', )
        self.parser.add_argument(
            '--action',
            dest='action',
            help='command to execute on each project', )
        self.parser.add_argument(
            '--script',
            dest='script',
            help='script to execute on each project', )
        self.parser.add_argument(
            '--dry-run',
            default=False,
            action='store_true',
            help='Do not execute the commands, only print them', )
        self.parser.add_argument(
            '--quiet',
            default=False,
            action='store_true',
            help='Do not display the results at the end', )
        formatter_group = self.parser.add_argument_group(
            title='output formatters',
            description='output formatter options', )
        formatter_choices = sorted(_formatter_plugins.names())
        formatter_default = _formatter_default
        if formatter_default not in formatter_choices:
            formatter_default = formatter_choices[0]
        formatter_group.add_argument(
            '-f',
            '--format',
            dest='formatter',
            action='store',
            choices=formatter_choices,
            default=formatter_default,
            help='the output format, defaults to %s' % formatter_default, )
        formatter_group.add_argument(
            '-c',
            '--column',
            action='append',
            default=[],
            dest='columns',
            metavar='COLUMN',
            help='specify the column(s) to include, can be repeated', )
        for formatter in _formatter_plugins:
            formatter.obj.add_argument_group(self.parser)

        args = self.parse_args(args)
        self.project_iterator(args)


def merge(objectToMergeFrom, objectToMergeTo):
    """Copy properties from one object to another."""
    for property in objectToMergeFrom._asdict():
        # Check to make sure it can't be called... ie a method.
        # Also make sure the objectobjectToMergeTo doesn't have
        #  a property of the same name.
        if (not callable(objectToMergeFrom._asdict()[property])
                and not hasattr(objectToMergeTo, property)):
            setattr(objectToMergeTo, property,
                    getattr(objectToMergeFrom, property))
    return objectToMergeTo


# Needs static method for setup.cfg
def main(args=None):
    ProjectIteratorCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        _logger.exception(e)
        sys.exit(1)
