#!/usr/bin/python3

import json
import logging
import re
import requests
import sys

from ccitools.cmd.base.cloud import BaseCloudCMD
import ccitools.conf
from osc_lib import exceptions

# configure logging
logger = logging.getLogger(__name__)

CONF = ccitools.conf.CONF

REGEXP = (r"Instance ([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}"
          "-[0-9a-f]{4}-[0-9a-f]{12}) has allocations against this "
          "compute host but is not found in the database.")


class CleanProbeAllocationCMD(BaseCloudCMD):
    def __init__(self):
        super(CleanProbeAllocationCMD, self).__init__(
            description="Clean Probe Allocations")
        self.error_nodes = {}

    def query_es_logs(self):
        """Query logs from Elastic search.

        Search for the logs from the last week indicating
        the nodes with ipmi connectivity issues. This log
        occures only once after the issue is found.
        """
        server = CONF.es.server
        user = CONF.es.user
        password = CONF.es.password
        query_string = "metadata.toplevel_hostgroup: cloud_compute "\
            "AND \"has allocations against this compute host but is not "\
            "found in the database\""
        url = server + '/monit_private_openstack_logs_generic-*/_search'
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        query = json.dumps({
            "_source": ["data.raw"],
            "size": 1000,
            "query": {
                "bool": {
                    "must": [
                        {
                            "query_string": {
                                "query": query_string,
                                "analyze_wildcard": True
                            }
                        },
                        {
                            "match_phrase": {
                                "data.source": {
                                    "query": "nova.compute"
                                }
                            }
                        },
                        {
                            "range": {
                                "metadata.timestamp": {
                                    "format": "strict_date_optional_time",
                                    "gte": "now-1d"
                                }
                            }
                        }
                    ]
                }
            }})
        response = requests.get(url, auth=(
            user, password), data=query, headers=headers,
            verify="/etc/pki/tls/certs/"
            "CERN_Root_Certification_Authority_2.pem",
            timeout=120)
        return json.loads(response.text)['hits']['hits']

    def clean_allocation_on_instance(self, dryrun, user, instance_uuid):
        # Check if the machine exists skip if the machine doesn't
        # Find the instance information to identify the host
        logger.info('Checking if instance %s exists', instance_uuid)
        instance = self.cloudclient.get_server(instance_uuid)

        if instance:
            logger.info('Instance %s exists skipping...',
                        instance_uuid)
            return

        logger.info('Checking allocations for instance %s', instance_uuid)
        entries = self.cloudclient.get_allocations(
            instance_uuid)
        for region, entry in entries.items():
            if (not entry
                    or 'allocations' not in entry
                    or not entry['allocations']):
                logger.info('There are no allocations for instance %s, '
                            'on region %s. skipping...',
                            instance_uuid, region)
                continue

            for allocation in entry['allocations']:
                # Retrieve the user_id who created the machine
                # skip if it's not the user specified in the command args
                if user == '*' or ('user_id' in entry
                                   and entry['user_id']) == user:
                    # Deletes the allocation directly'
                    if dryrun:
                        logger.info(
                            "DRYRUN: Clean allocation for instance_uuid: %s",
                            instance_uuid)
                    else:
                        logger.info("Deleting allocation for %s",
                                    instance_uuid)
                        try:
                            self.cloudclient.delete_allocations(
                                instance_uuid, region)
                        except exceptions.NotFound:
                            logger.info('Allocation already deleted')

    def main(self, args=None):
        """Get broken nodes and send email."""
        self.parser.add_argument(
            '--dryrun',
            action='store_true',
            help='read-only mode')
        self.parser.add_argument(
            '--user',
            default="svcprobe",
            help='User to clean allocations')
        args = self.parse_args(args)

        if args.dryrun:
            logger.info(
                "--- THIS IS A DRYRUN. NO CHANGES WILL BE MADE ---")

        logger.info("Querying logs from opensearch...")
        messages = self.query_es_logs()

        logger.info('Processing messages')
        for message in messages:
            match = re.match(REGEXP, message['_source']['data']['raw'])
            if match and match[0] and match[1]:
                instance_uuid = match[1]
                self.clean_allocation_on_instance(
                    args.dryrun, args.user, instance_uuid)


def main(args=None):
    CleanProbeAllocationCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
