#!/usr/bin/python3

import ccitools.conf
import logging
import sys

from ccitools.cmd.base.cloud import BaseCloudCMD
from datetime import datetime

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class CheckDateFormatCMD(BaseCloudCMD):

    def __init__(self):
        super(CheckDateFormatCMD, self).__init__(
            description="Checks a date to validate format")

    def main_check_date(self, date):
        # must be  (DD-MM-YYYY HH:MM)
        # EX 22-01-2020 15:42
        try:
            date = datetime.strptime(date, '%d-%m-%Y %H:%M')
            logger.info(date, 'Date format ok')
        except Exception:
            logger.error('Date format error,'
                         'must be DD-MM-YYYY HH:MM & cannot be empty'
                         'Ex: 21-02-2020 12:30')
            sys.exit(-1)

    def main(self, args=None):
        self.parser.add_argument(
            "--date", help="String date format dd-mm-aaaa hh:mm")

        args = self.parse_args(args)

        self.main_check_date(args.date)


def main(args=None):
    CheckDateFormatCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
