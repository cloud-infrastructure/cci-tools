import logging
import requests_mock

from ccitools.cmd.accounting.dbreporter import main
from ccitools.tests.cmd import fixtures
from influxdb.resultset import ResultSet
from unittest import mock
from unittest import TestCase


class TestAccountingDBReporter(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch('ccitools.cmd.accounting.dbreporter.InfluxDBClient')
    @requests_mock.Mocker()
    def test_dryrun(self, mock_influx, mock_req):
        mock_req.get(
            'https://gar.cern.ch/public/list_full',
            text=fixtures.CHARGEGROUPS)
        mock_cli = mock_influx.return_value
        mock_cli.query.side_effect = iter([
            ResultSet(fixtures.CORE_METRICS),
            ResultSet(fixtures.VOLUME_METRICS),
            ResultSet(fixtures.FILESHARE_METRICS)
        ])

        main(['--dryrun'])

        mock_influx.assert_called_with(
            'dbod-ccinflux.cern.ch',
            8087,
            'fake_user',
            'fake_password',
            'dblogger',
            ssl=True
        )

    @mock.patch('ccitools.cmd.accounting.dbreporter.InfluxDBClient')
    @requests_mock.Mocker()
    def test_dryrun_and_no_data(self, mock_influx, mock_req):
        mock_req.get(
            'https://gar.cern.ch/public/list_full',
            text=fixtures.CHARGEGROUPS)
        mock_cli = mock_influx.return_value
        mock_cli.query.return_value = ResultSet({})

        main(['--dryrun'])

        mock_influx.assert_called_with(
            'dbod-ccinflux.cern.ch',
            8087,
            'fake_user',
            'fake_password',
            'dblogger',
            ssl=True
        )

    @mock.patch('ccitools.cmd.accounting.dbreporter.InfluxDBClient')
    @requests_mock.Mocker()
    def test_default_args_no_data(self, mock_influx, mock_req):
        mock_req.get(
            'https://gar.cern.ch/public/list_full',
            text=fixtures.CHARGEGROUPS)
        mock_cli = mock_influx.return_value
        mock_cli.query.return_value = ResultSet({})

        main([])

        mock_influx.assert_called_with(
            'dbod-ccinflux.cern.ch',
            8087,
            'fake_user',
            'fake_password',
            'dblogger',
            ssl=True
        )

    @mock.patch('ccitools.cmd.accounting.dbreporter.InfluxDBClient')
    @requests_mock.Mocker()
    def test_default_args(self, mock_influx, mock_req):
        mock_req.get(
            'https://gar.cern.ch/public/list_full',
            text=fixtures.CHARGEGROUPS)
        mock_req.post(
            'https://accounting-receiver.cern.ch/v3/fe',
            text=fixtures.ACCOUNTING_RECEIVER_RESPONSE,
            status_code=200
        )
        mock_cli = mock_influx.return_value
        mock_cli.query.side_effect = iter([
            ResultSet(fixtures.CORE_METRICS),
            ResultSet(fixtures.VOLUME_METRICS),
            ResultSet(fixtures.FILESHARE_METRICS)
        ])

        main([])

        mock_influx.assert_called_with(
            'dbod-ccinflux.cern.ch',
            8087,
            'fake_user',
            'fake_password',
            'dblogger',
            ssl=True
        )

    @mock.patch('ccitools.cmd.accounting.dbreporter.InfluxDBClient')
    @requests_mock.Mocker()
    def test_default_args_error(self, mock_influx, mock_req):
        mock_req.get(
            'https://gar.cern.ch/public/list_full',
            text=fixtures.CHARGEGROUPS)
        mock_req.post(
            'https://accounting-receiver.cern.ch/v3/fe',
            text=fixtures.ACCOUNTING_RECEIVER_RESPONSE,
            status_code=400
        )
        mock_cli = mock_influx.return_value
        mock_cli.query.side_effect = iter([
            ResultSet(fixtures.CORE_METRICS),
            ResultSet(fixtures.VOLUME_METRICS),
            ResultSet(fixtures.FILESHARE_METRICS)
        ])

        main([])

        mock_influx.assert_called_with(
            'dbod-ccinflux.cern.ch',
            8087,
            'fake_user',
            'fake_password',
            'dblogger',
            ssl=True
        )

    @mock.patch('ccitools.cmd.accounting.dbreporter.InfluxDBClient')
    @requests_mock.Mocker()
    def test_default_args_override(self, mock_influx, mock_req):
        mock_req.get(
            'https://gar.cern.ch/public/list_full',
            text=fixtures.CHARGEGROUPS)
        mock_req.post(
            'https://accounting-receiver.cern.ch/v3/fe',
            text=fixtures.ACCOUNTING_RECEIVER_RESPONSE,
            status_code=400
        )
        mock_cli = mock_influx.return_value
        mock_cli.query.side_effect = iter([
            ResultSet(fixtures.CORE_METRICS),
            ResultSet(fixtures.VOLUME_METRICS),
            ResultSet(fixtures.FILESHARE_METRICS)
        ])

        main(['--override'])

        mock_influx.assert_called_with(
            'dbod-ccinflux.cern.ch',
            8087,
            'fake_user',
            'fake_password',
            'dblogger',
            ssl=True
        )
