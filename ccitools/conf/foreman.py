from oslo_config import cfg


host = cfg.StrOpt(
    'host',
    default='foreman.cern.ch',
    help="Host to use for Foreman")

port = cfg.StrOpt(
    'port',
    default='8443',
    help="Port to use for Foreman")

timeout = cfg.IntOpt(
    'timeout',
    default=60,
    help="Timeout to use for Foreman")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    host,
    port,
    timeout,
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
