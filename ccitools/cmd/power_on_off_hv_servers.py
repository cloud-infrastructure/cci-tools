#!/usr/bin/python3

import ccitools.conf
import logging
import sys
import tenacity

from ccitools.cmd.base.cloud import BaseCloudCMD
from ccitools.utils.sendmail import MailClient

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF

WAIT_TIMEOUT = 300  # 5 minutes
WAIT_RETRY_INTERVAL = 30  # 30 seconds

BROKEN_MESSAGE = """Dear Cloud Automation Service administrador,

Some VMs in hypervisor {hypervisor} have been found
in a wrong state while trying to power them on/off:

{servers}

Best regards,
Cloud Automation Service.
"""


class PowerOnOffHVServersCMD(BaseCloudCMD):

    def __init__(self):
        super(PowerOnOffHVServersCMD, self).__init__(
            description="Power ON/OFF VMs in the hypervisor")

    @tenacity.retry(stop=tenacity.stop_after_delay(WAIT_TIMEOUT),
                    wait=tenacity.wait_fixed(WAIT_RETRY_INTERVAL),
                    before_sleep=tenacity.before_sleep_log(
                        logger,
                        logging.WARNING))
    def wait_for_active(self, target_servers):
        for server in target_servers:
            server_now = server.manager.get(server)
            if getattr(server_now, 'OS-EXT-STS:vm_state') != 'active':
                logger.info(
                    "Server %s not active yet. Retrying...", server.name)
                raise tenacity.TryAgain
        logger.info("All servers active.")

    @tenacity.retry(stop=tenacity.stop_after_delay(WAIT_TIMEOUT),
                    wait=tenacity.wait_fixed(WAIT_RETRY_INTERVAL),
                    before_sleep=tenacity.before_sleep_log(
                        logger,
                        logging.WARNING))
    def wait_for_stopped(self, target_servers):
        for server in target_servers:
            server_now = server.manager.get(server)
            if getattr(server_now, 'OS-EXT-STS:vm_state') != 'stopped':
                logger.info(
                    "Server %s not stopped yet. Retrying...", server.name)
                raise tenacity.TryAgain
        logger.info("All servers stopped.")

    def send_broken_servers(self, mailto, target_servers, hv_name):
        logger.info("Collecting state of all servers for the email.")
        servers_state = {}
        for server in target_servers:
            server_now = server.manager.get(server)
            servers_state[server.name] = getattr(
                server_now, 'OS-EXT-STS:vm_state')

        notification = MailClient("localhost")
        mail_subject = "[Rundeck] Broken servers found after power on job"
        servers_mail = "\n".join(
            [' %s => %s' % (key, value) for (
                key, value) in servers_state.items()])
        mail_body = BROKEN_MESSAGE.format(
            hypervisor=hv_name,
            servers=servers_mail)
        notification.send_mail(mailto, mail_subject, mail_body,
                               "noreply@cern.ch", "", "", "plain")
        logger.info("To: %-20s Subject: \"%s\"", mailto, mail_subject)

    def power_on_off_hv_servers(self, cloud, action, hypervisors,
                                username, mailto):

        # Perform action on list of servers
        for hv_name in hypervisors.split():
            servers = cloud.get_servers_by_hypervisor(hv_name)
            if servers:
                if action == 'on':
                    logger.info("Username %s provided. "
                                "Only checking VMs that were previously "
                                "powered off by such user." % username)
                    t_servers = cloud.get_servers_by_last_action_user(
                        servers, username)
                    if t_servers:
                        server_names = [server.name for server in t_servers]
                        logger.info("Powering on the following servers"
                                    " as user %s: %s",
                                    username, ", ".join(server_names))
                        cloud.power_on_servers(t_servers)
                        try:
                            self.wait_for_active(t_servers)
                        except Exception:
                            self.send_broken_servers(
                                mailto, t_servers, hv_name)
                    else:
                        logger.info(
                            "No servers found in %s whose last action "
                            "was performed by user %s. Skipping power "
                            "on action.", hv_name, username)
                else:
                    t_servers = cloud.get_servers_by_vm_state(
                        servers, 'active')
                    server_names = [server.name for server in t_servers]
                    logger.info("Powering off the following servers: %s",
                                ", ".join(server_names))
                    cloud.power_off_servers(t_servers)
                    try:
                        self.wait_for_stopped(t_servers)
                    except Exception:
                        self.send_broken_servers(mailto, t_servers,
                                                 hv_name)
            else:
                logger.info("No servers found in %s.", hv_name)

    def main(self, args=None):
        self.parser.add_argument(
            "--action", required=True,
            help="Action to perfom on servers (on/off)")
        self.parser.add_argument(
            "--hypervisors", required=True, help="List of hypervisors")
        self.parser.add_argument(
            "--username", required=True, help="User who will perform")
        self.parser.add_argument("--mailto", required=True)

        args = self.parse_args(args)

        self.power_on_off_hv_servers(
            self.cloudclient, args.action, args.hypervisors,
            args.username, args.mailto)


# Needs static method for setup.cfg
def main(args=None):
    PowerOnOffHVServersCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
