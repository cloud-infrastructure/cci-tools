#!/usr/bin/python3
import json
import logging
import os
import re
import sys
import yaml

from ccitools.cmd.base.cloud import BaseCloudCMD
from keystoneclient.v3 import client as keystone_client

# configure logging
logger = logging.getLogger(__name__)


class VerboseSafeDumper(yaml.SafeDumper):
    def ignore_aliases(self, data):
        return True


class ProcessUpgradeReportCMD(BaseCloudCMD):

    def __init__(self):
        super(ProcessUpgradeReportCMD, self).__init__(
            description="Process upgrade report in yaml prior"
                        "to send notifications")
        self.assign = {}
        self.project = {}

    def resolve_project_id(self, id):
        if id not in self.project:
            result = self.kc.projects.get(id)
            if id not in self.project:
                self.project[id] = result.name
        return self.project[id]

    def resolve_project_role(self, project, role):
        if project not in self.assign or role not in self.assign[project]:
            result = list(set([
                a.user['id'] for a in self.kc.role_assignments.list(
                    project=project,
                    role=self.roles[role],
                    effective=True,
                )]))
            if project not in self.assign:
                self.assign[project] = {}
            self.assign[project][role] = result
        return self.assign[project][role]

    def read_file(self, filename, type):
        data = []
        with open(filename) as stream:
            try:
                if type == 'json':
                    data = json.load(stream)
                else:
                    data = yaml.safe_load(stream)
            except json.JSONDecodeError as exc:
                logger.exception(exc)
                raise SystemExit(1)
            except yaml.YAMLError as exc:
                logger.exception(exc)
                raise SystemExit(1)
        return data

    def write_file(self, filename, type, data):
        with open(filename, 'w') as outfile:
            if type == 'json':
                json.dump(data, outfile, ensure_ascii=False, indent=4)
            else:
                yaml.dump(data, outfile,
                          default_flow_style=False, sort_keys=True,
                          Dumper=VerboseSafeDumper)

    def process_report(self, args):
        logger.info("Loading helper clients for processing")
        self.kc = keystone_client.Client(session=self.cloudclient.session)
        self.roles = {r.name: r.id for r in self.kc.roles.list()}

        # Open the transformations
        temp_files = []
        transformations = self.read_file(args.transformations, 'yaml')

        for transformation in transformations:
            logger.info('Executing transformation %s -> %s',
                        transformation['source']['name'],
                        transformation['target']['name'])
            source = []

            # Open the report from the source
            source = self.read_file(
                transformation['source']['name'],
                transformation['source']['type']
            )
            target = []
            group_clause = None
            for entry in source:
                if 'add' in transformation:
                    for key, value in transformation['add'].items():
                        entry[key] = eval(value)  # nosec
                if 'remove' in transformation:
                    for key in transformation['remove']:
                        entry.pop(key)
                if 'filter' in transformation:
                    for key, value in transformation['filter'].items():
                        if re.match(value, entry[key]):
                            break
                    else:
                        target.append(entry)
                else:
                    target.append(entry)
                if 'group' in transformation:
                    group_clause = transformation['group']
            if 'temp' in transformation and transformation['temp']:
                temp_files.append(transformation['target']['name'])

            # Reorganize entries by group_clause if present
            if group_clause:
                groups = {}
                for entry in target:
                    if entry[group_clause] not in groups:
                        groups[entry[group_clause]] = {}
                    if 'id' not in groups[entry[group_clause]]:
                        groups[entry[group_clause]]['id'] = entry[group_clause]
                    if 'entries' not in groups[entry[group_clause]]:
                        groups[entry[group_clause]]['entries'] = []
                    groups[entry[group_clause]]['entries'].append(entry)
                    entry.pop(group_clause)
                target = list(groups.values())

            self.write_file(
                transformation['target']['name'],
                transformation['target']['type'],
                target
            )

        # Delete temporary files at the end of the job
        if args.clean_mode:
            logger.info('Cleaning files')
            for f in temp_files:
                logger.info('Deleting files %s', f)
                os.remove(f)

    def main(self, args=None):
        self.parser.add_argument(
            "--transformations",
            required=True,
            help="YAML file containing transformations to apply")
        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument(
            '--clean',
            dest="clean_mode",
            action='store_true',
            help="If present, cleans temporary files")
        behaviour.add_argument(
            '--no-clean',
            dest="exec_mode",
            action='store_false')
        args = self.parse_args(args)
        self.process_report(args)


# Needs static method for setup.cfg
def main(args=None):
    ProcessUpgradeReportCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
