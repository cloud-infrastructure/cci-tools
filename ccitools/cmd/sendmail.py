#!/usr/bin/python3

import logging
import sys

from ccitools.cmd import base
from ccitools.utils.sendmail import MailClient

# configure logging
logger = logging.getLogger(__name__)


class SendmailCMD(base.BaseCMD):

    def __init__(self):
        super(SendmailCMD, self).__init__(
            description="Send notification via email")

    def sendmail(self, mail_to, mail_subject, mail_body, mail_from, mail_cc,
                 mail_bcc, mail_content_type, smtp_server, sendmail):
        notification = MailClient(smtp_server)
        if sendmail:
            notification.send_mail(mail_to, mail_subject, mail_body,
                                   mail_from, mail_cc, mail_bcc,
                                   mail_content_type)
            logger.info("Email sent successfully")
            logger.info("To: %-20s Subject: \"%s\"", mail_to, mail_subject)
        else:
            logger.info("[DRYRUN] To: %-20s Subject: \"%s\"",
                        mail_to, mail_subject)
            logger.info("Example mail:\n%s", mail_body)

    def main(self, args=None):
        self.parser.add_argument("--mailto", dest='mail_to', required=True)
        self.parser.add_argument("--mailfrom", dest='mail_from',
                                 nargs='?', default="noreply@cern.ch")
        self.parser.add_argument("--mailcc", dest='mail_cc', nargs='?',
                                 default="")
        self.parser.add_argument("--mailbcc", dest='mail_bcc', nargs='?',
                                 default="")
        self.parser.add_argument("--subject", dest='mail_subject', nargs='?',
                                 default="")
        self.parser.add_argument("--body", dest='mail_body', nargs='?',
                                 default="")
        self.parser.add_argument("--sendmail", dest='sendmail',
                                 action='store_true',
                                 help="if present, sends email")
        self.parser.add_argument("--contenttype", dest='mail_content_type',
                                 nargs='?', default="plain")
        self.parser.add_argument("--smtpserver", dest='smtp_server',
                                 nargs='?', default="localhost")

        args = self.parse_args(args)
        self.sendmail(args.mail_to, args.mail_subject, args.mail_body,
                      args.mail_from, args.mail_cc, args.mail_bcc,
                      args.mail_content_type, args.smtp_server, args.sendmail)


# Needs static method for setup.cfg
def main(args=None):
    SendmailCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
