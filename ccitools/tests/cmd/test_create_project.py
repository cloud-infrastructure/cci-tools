import io
import logging
import requests_mock

from ccitools.cmd import create_project
from ccitools.tests.cmd import fixtures
from tenacity import stop_after_attempt
from tenacity import wait_none
from unittest import mock
from unittest import TestCase


class TestCreateProject(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch('ccitools.utils.cloud.cloud_config')
    def test_default_args(self, mock_cc):
        with self.assertRaises(SystemExit) as cm:
            create_project.main([])
        self.assertEqual(cm.exception.code, 2)

    @mock.patch('builtins.open',
                return_value=io.StringIO(fixtures.CHARGEGROUPS))
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    @requests_mock.Mocker()
    def test_from_snow_proper_data_in_fim(self,
                                          mock_fc,
                                          mock_snc,
                                          mock_crc,
                                          mock_open,
                                          mock_req):
        mock_fim = mock_fc.return_value
        mock_client = mock_crc.return_value
        mock_snow = mock_snc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 0
        mock_snow.get_project_creation_rp.return_value = (
            fixtures.SNOW_CREATE_RP
        )
        mock_client.find_project.side_effect = iter([
            Exception,
            fixtures.CREATE_PROJECT_NOTSYNC,
            fixtures.CREATE_PROJECT_SYNC,
            fixtures.CREATE_PROJECT_SYNC,
        ])
        mock_client.mergeQuotaMetadata.return_value = {
            'default': {
                'egroup_responsible': True,
                'egroup_mainuser': True,
            },
            'quota': {}
        }
        mock_req.get(
            'https://gar.cern.ch/public/list_full',
            text=fixtures.CHARGEGROUPS)

        cmd = create_project.CreateProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(5)

        cmd.main([
            'from-snow',
            '--instance', 'f_cern',
            '--ticket-number', 'fake_ticket',
            '--project-type', 'compute',
        ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake')
        mock_fim.create_project.assert_called_with(
            'project', 'description', 'fake'
        )
        mock_client.find_project.assert_has_calls([
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None)
        ])
        mock_client.set_project_property.assert_has_calls([
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                'type',
                'compute'),
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                'chargegroup',
                '4714e675-60b9-4fc7-80b2-61acd213f478'),
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                'chargerole',
                'charge_role'),
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                'landb-responsible',
                'fake'),
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                'landb-mainuser',
                'fake')
        ])

    @mock.patch('builtins.open',
                return_value=io.StringIO(fixtures.CHARGEGROUPS))
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.CornerstoneClient')
    @mock.patch('ccitools.cmd.create_project.ResourcesClient')
    @requests_mock.Mocker()
    def test_from_snow_proper_data_in_resources(
            self,
            mock_res,
            mock_corn,
            mock_snc,
            mock_crc,
            mock_open,
            mock_req):
        mock_cornerstone = mock_corn.return_value
        mock_resources = mock_res.return_value
        mock_resources.create_project.return_value = 'fake_id'
        mock_client = mock_crc.return_value
        mock_snow = mock_snc.return_value

        mock_snow.get_project_creation_rp.return_value = (
            fixtures.SNOW_CREATE_RP
        )
        mock_client.find_project.side_effect = iter([
            Exception,
            fixtures.CREATE_PROJECT_NOTSYNC,
            fixtures.CREATE_PROJECT_SYNC,
            fixtures.CREATE_PROJECT_SYNC,
        ])
        mock_client.mergeQuotaMetadata.return_value = {
            'default': {
                'egroup_responsible': True,
                'egroup_mainuser': True,
            },
            'quota': {}
        }
        mock_req.get(
            'https://gar.cern.ch/public/list_full',
            text=fixtures.CHARGEGROUPS)

        cmd = create_project.CreateProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(5)

        cmd.main([
            '--mim',
            'from-snow',
            '--instance', 'f_cern',
            '--ticket-number', 'fake_ticket',
            '--project-type', 'compute',
        ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)

        mock_resources.create_project.assert_called_with(
            name='project',
            description='description',
            category='official',
            owner='fake',
            administrators='fake'
        )
        mock_resources.activate_project.assert_called_with(
            id='fake_id'
        )
        mock_cornerstone.create_project.assert_called_with(
            id='fake_id',
            name='project',
            description='description',
            status='created',
            type='official',
            owner='fake',
            administrators=None
        )
        mock_client.find_project.assert_has_calls([
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None)
        ])
        mock_client.set_project_property.assert_has_calls([
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                'type',
                'compute'),
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                'chargegroup',
                '4714e675-60b9-4fc7-80b2-61acd213f478'),
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                'chargerole',
                'charge_role'),
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                'landb-responsible',
                'fake'),
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                'landb-mainuser',
                'fake')
        ])

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    def test_snow_chargegroup_uuid(self,
                                   mock_fc,
                                   mock_snc,
                                   mock_crc):
        mock_fim = mock_fc.return_value
        mock_client = mock_crc.return_value
        mock_snow = mock_snc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 0
        mock_snow.get_project_creation_rp.return_value = (
            fixtures.SNOW_CREATE_RP_UUID
        )
        mock_client.find_project.side_effect = iter([
            Exception,
            fixtures.CREATE_PROJECT_NOTSYNC,
            fixtures.CREATE_PROJECT_SYNC,
            fixtures.CREATE_PROJECT_SYNC,
        ])
        mock_client.mergeQuotaMetadata.return_value = {
            'default': {
                'egroup_responsible': True,
                'egroup_mainuser': True
            },
            'quota': {}
        }

        cmd = create_project.CreateProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(5)

        cmd.main([
            'from-snow',
            '--instance', 'f_cern',
            '--ticket-number', 'fake_ticket',
            '--project-type', 'compute',
        ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake')
        mock_fim.create_project.assert_called_with(
            'project', 'description', 'fake'
        )
        mock_client.find_project.assert_has_calls([
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None)
        ])
        mock_client.set_project_property.assert_has_calls([
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                'type',
                'compute'),
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                'chargegroup',
                '4714e675-60b9-4fc7-80b2-61acd213f478'),
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                'chargerole',
                'charge_role'),
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                'landb-responsible',
                'fake'),
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                'landb-mainuser',
                'fake')
        ])

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    def test_from_input_proper_data(self,
                                    mock_fc,
                                    mock_snc,
                                    mock_crc):

        mock_fim = mock_fc.return_value
        mock_client = mock_crc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 0
        mock_client.find_project.side_effect = iter([
            Exception,
            fixtures.CREATE_PROJECT_NOTSYNC,
            fixtures.CREATE_PROJECT_SYNC,
            fixtures.CREATE_PROJECT_SYNC,
        ])
        cmd = create_project.CreateProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(5)

        cmd.main([
            'from-input',
            '--project-name', 'fake_n',
            '--description', 'fake_d',
            '--owner', 'fake_o',
            '--egroup', 'fake_eg',
            '--cores', 'fake_core',
            '--instances', 'fake_inst',
            '--ram', '1',
            '--project-type', 'fake_ty',
            '--cell-mapping', '',
            '--cp1-gigabytes', 'fake_cp1g',
            '--cp1-volumes', 'fake_volumes',
            '--cpio1-gigabytes', 'fake_cg',
            '--cpio1-volumes', 'fake_volumes',
            '--io1-gigabytes', 'fake_io1g',
            '--io1-volumes', 'fake_volumes',
            '--standard-gigabytes', 'fake_sg',
            '--standard-volumes', 'fake_volumes',
            '--vault-100-gigabytes', 'fake_v100g',
            '--vault-100-volumes', 'fake_v100v',
            '--vault-500-gigabytes', 'fake_v500g',
            '--vault-500-volumes', 'fake_v500v',
            '--meyrin-shares', 'fake_ms',
            '--meyrin-gigabytes', 'fake_mg',
            '--geneva_testing-shares', 'fake_gts',
            '--geneva_testing-gigabytes', 'fake_gtg',
            '--s3-buckets', '1',
            '--s3-gigabytes', '1',
        ])

        mock_snc.assert_not_called()
        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake_o')
        mock_fim.create_project.assert_called_with(
            'fake_n', 'fake_d', 'fake_o'
        )
        mock_client.find_project.assert_has_calls([
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None)
        ])

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    @requests_mock.Mocker()
    def test_from_snow_update_quotas_exception(self,
                                               mock_fc,
                                               mock_snc,
                                               mock_crc,
                                               mock_req):
        mock_fim = mock_fc.return_value
        mock_client = mock_crc.return_value
        mock_snow = mock_snc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 0
        mock_snow.get_project_creation_rp.return_value = (
            fixtures.SNOW_CREATE_RP
        )
        mock_client.find_project.side_effect = iter([
            Exception,
            fixtures.CREATE_PROJECT_NOTSYNC,
            fixtures.CREATE_PROJECT_SYNC,
            fixtures.CREATE_PROJECT_SYNC,
        ])
        mock_client.set_project_quota.side_effect = iter([Exception])
        mock_client.mergeQuotaMetadata.return_value = {'quota': {}}

        mock_req.get(
            'https://gar.cern.ch/public/list_full',
            text=fixtures.CHARGEGROUPS)

        cmd = create_project.CreateProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(5)

        with self.assertRaises(Exception):
            cmd.main([
                'from-snow',
                '--instance', 'f_cern',
                '--ticket-number', 'fake_ticket',
                '--project-type', 'compute',
            ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake')
        mock_fim.create_project.assert_called_with(
            'project', 'description', 'fake'
        )
        mock_client.find_project.assert_has_calls([
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None)
        ])

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    def test_fail_fim_no_valid_owner(self,
                                     mock_fc,
                                     mock_snc,
                                     mock_crc):

        mock_fim = mock_fc.return_value
        mock_snow = mock_snc.return_value
        mock_fim.is_valid_owner.return_value = False
        mock_snow.get_project_creation_rp.return_value = (
            fixtures.SNOW_CREATE_RP
        )

        with self.assertRaises(Exception):
            create_project.main([
                'from-snow',
                '--instance', 'f_cern',
                '--ticket-number', 'fake_ticket',
                '--project-type', 'compute',
            ])

        mock_fim.is_valid_owner.assert_called_with('fake')

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    def test_fail_fim_return_code_invalid_params(self,
                                                 mock_fc,
                                                 mock_snc,
                                                 mock_crc):
        mock_fim = mock_fc.return_value
        mock_snow = mock_snc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 1
        mock_snow.get_project_creation_rp.return_value = (
            fixtures.SNOW_CREATE_RP
        )

        with self.assertRaises(Exception):
            create_project.main([
                'from-snow',
                '--instance', 'f_cern',
                '--ticket-number', 'fake_ticket',
                '--project-type', 'compute',
            ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake')
        mock_fim.create_project.assert_called_with(
            'project', 'description', 'fake'
        )

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    def test_fail_fim_return_code_not_subscribed(self,
                                                 mock_fc,
                                                 mock_snc,
                                                 mock_crc):

        mock_fim = mock_fc.return_value
        mock_snow = mock_snc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 2
        mock_snow.get_project_creation_rp.return_value = (
            fixtures.SNOW_CREATE_RP
        )

        with self.assertRaises(Exception):
            create_project.main([
                'from-snow',
                '--instance', 'f_cern',
                '--ticket-number', 'fake_ticket',
                '--project-type', 'compute',
            ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake')
        mock_fim.create_project.assert_called_with(
            'project', 'description', 'fake'
        )

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    def test_fail_fim_return_code_project_exists(self,
                                                 mock_fc,
                                                 mock_snc,
                                                 mock_crc):

        mock_fim = mock_fc.return_value
        mock_snow = mock_snc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 3
        mock_snow.get_project_creation_rp.return_value = (
            fixtures.SNOW_CREATE_RP
        )

        with self.assertRaises(Exception):
            create_project.main([
                '--default',
                'from-snow',
                '--instance', 'f_cern',
                '--ticket-number', 'fake_ticket',
                '--project-type', 'compute',
            ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake')
        mock_fim.create_project.assert_called_with(
            'project', 'description', 'fake'
        )

    @mock.patch('builtins.open',
                return_value=io.StringIO(fixtures.CHARGEGROUPS))
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    @requests_mock.Mocker()
    def test_fail_fim_return_code_project_exists_force(self,
                                                       mock_fc,
                                                       mock_snc,
                                                       mock_crc,
                                                       mock_open,
                                                       mock_req):
        mock_fim = mock_fc.return_value
        mock_client = mock_crc.return_value
        mock_snow = mock_snc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 3
        mock_snow.get_project_creation_rp.return_value = (
            fixtures.SNOW_CREATE_RP
        )
        mock_client.find_project.side_effect = iter([
            Exception,
            fixtures.CREATE_PROJECT_NOTSYNC,
            fixtures.CREATE_PROJECT_SYNC,
            fixtures.CREATE_PROJECT_SYNC,
        ])
        mock_req.get(
            'https://gar.cern.ch/public/list_full',
            text=fixtures.CHARGEGROUPS)

        cmd = create_project.CreateProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(5)

        cmd.main([
            '--force',
            'from-snow',
            '--instance', 'f_cern',
            '--ticket-number', 'fake_ticket',
            '--project-type', 'compute',
        ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake')
        mock_fim.create_project.assert_called_with(
            'project', 'description', 'fake'
        )
        mock_client.find_project.assert_has_calls([
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None)
        ])

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    def test_fail_fim_return_code_internal_error(self,
                                                 mock_fc,
                                                 mock_snc,
                                                 mock_crc):

        mock_fim = mock_fc.return_value
        mock_snow = mock_snc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = -1
        mock_snow.get_project_creation_rp.return_value = (
            fixtures.SNOW_CREATE_RP
        )

        with self.assertRaises(Exception):
            create_project.main([
                'from-snow',
                '--instance', 'f_cern',
                '--ticket-number', 'fake_ticket',
                '--project-type', 'compute',
            ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake')
        mock_fim.create_project.assert_called_with(
            'project', 'description', 'fake'
        )

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    def test_project_not_found(self,
                               mock_fc,
                               mock_snc,
                               mock_crc):

        mock_fim = mock_fc.return_value
        mock_client = mock_crc.return_value
        mock_snow = mock_snc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 0
        mock_snow.get_project_creation_rp.return_value = (
            fixtures.SNOW_CREATE_RP
        )
        mock_client.find_project.side_effect = iter([Exception])
        cmd = create_project.CreateProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(1)

        with self.assertRaises(Exception):
            cmd.main([
                'from-snow',
                '--instance', 'f_cern',
                '--ticket-number', 'fake_ticket',
                '--project-type', 'compute',
            ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake')
        mock_fim.create_project.assert_called_with(
            'project', 'description', 'fake'
        )
        mock_client.find_project.assert_called_with(
            'project', cloud='cern', region=None)

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    def test_project_not_ready(self,
                               mock_fc,
                               mock_snc,
                               mock_crc):

        mock_fim = mock_fc.return_value
        mock_client = mock_crc.return_value
        mock_snow = mock_snc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 0
        mock_snow.get_project_creation_rp.return_value = (
            fixtures.SNOW_CREATE_RP
        )
        mock_client.find_project.side_effect = iter([
            fixtures.CREATE_PROJECT_NOTSYNC
        ])
        cmd = create_project.CreateProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(1)

        with self.assertRaises(Exception):
            cmd.main([
                'from-snow',
                '--instance', 'f_cern',
                '--ticket-number', 'fake_ticket',
                '--project-type', 'compute',
            ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake')
        mock_fim.create_project.assert_called_with(
            'project', 'description', 'fake'
        )
        mock_client.find_project.assert_called_with(
            'project', cloud='cern', region=None)

    @mock.patch('builtins.open',
                return_value=io.StringIO(fixtures.CHARGEGROUPS))
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    @requests_mock.Mocker()
    def test_from_snow_invalid_egroup(self,
                                      mock_fc,
                                      mock_snc,
                                      mock_crc,
                                      mock_open,
                                      mock_req):

        mock_fim = mock_fc.return_value
        mock_client = mock_crc.return_value
        mock_snow = mock_snc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 0
        mock_snow.get_project_creation_rp.return_value = (
            fixtures.SNOW_CREATE_RP
        )
        mock_client.find_project.side_effect = iter([
            Exception,
            fixtures.CREATE_PROJECT_NOTSYNC,
            fixtures.CREATE_PROJECT_SYNC,
            fixtures.CREATE_PROJECT_SYNC,
        ])
        mock_req.get(
            'https://gar.cern.ch/public/list_full',
            text=fixtures.CHARGEGROUPS)

        cmd = create_project.CreateProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(5)

        mock_client.is_group.return_value = False
        mock_client.mergeQuotaMetadata.return_value = {'quota': {}}

        with self.assertRaises(Exception):
            cmd.main([
                'from-snow',
                '--instance', 'f_cern',
                '--ticket-number', 'fake_ticket',
                '--project-type', 'compute',
            ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake')
        mock_fim.create_project.assert_called_with(
            'project', 'description', 'fake'
        )
        mock_client.find_project.assert_has_calls([
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None)
        ])
        mock_client.set_project_quota.assert_called_with(
            project_id='1',
            quota={}
        )
        mock_client.is_group.assert_called_with('fake')

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    @requests_mock.Mocker()
    def test_set_property_exception(self,
                                    mock_fc,
                                    mock_snc,
                                    mock_crc,
                                    mock_req):

        mock_fim = mock_fc.return_value
        mock_client = mock_crc.return_value
        mock_snow = mock_snc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 0
        mock_snow.get_project_creation_rp.return_value = (
            fixtures.SNOW_CREATE_RP
        )
        mock_client.find_project.side_effect = iter([
            Exception,
            fixtures.CREATE_PROJECT_NOTSYNC,
            fixtures.CREATE_PROJECT_SYNC,
            fixtures.CREATE_PROJECT_SYNC,
        ])
        mock_req.get(
            'https://gar.cern.ch/public/list_full',
            text=fixtures.CHARGEGROUPS)

        cmd = create_project.CreateProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(5)

        mock_client.set_project_property.side_effect = iter([Exception])

        with self.assertRaises(Exception):
            cmd.main([
                'from-snow',
                '--instance', 'f_cern',
                '--ticket-number', 'fake_ticket',
                '--project-type', 'compute',
            ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake')
        mock_fim.create_project.assert_called_with(
            'project', 'description', 'fake'
        )
        mock_client.find_project.assert_has_calls([
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None)
        ])
        mock_client.set_project_property.assert_has_calls([
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                'type',
                'compute')
        ])

    @mock.patch('builtins.open',
                return_value=io.StringIO(fixtures.CHARGEGROUPS))
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    @requests_mock.Mocker()
    def test_set_defaults_exception(self,
                                    mock_fc,
                                    mock_snc,
                                    mock_crc,
                                    mock_req,
                                    mock_open):

        mock_fim = mock_fc.return_value
        mock_client = mock_crc.return_value
        mock_snow = mock_snc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 0
        mock_snow.get_project_creation_rp.return_value = (
            fixtures.SNOW_CREATE_RP
        )
        mock_client.find_project.side_effect = iter([
            Exception,
            fixtures.CREATE_PROJECT_NOTSYNC,
            fixtures.CREATE_PROJECT_SYNC,
            fixtures.CREATE_PROJECT_SYNC,
        ])
        mock_client.mergeQuotaMetadata.return_value = {
            'default': {
                'egroup_mainuser': True,
            }
        }
        mock_req.get(
            'https://gar.cern.ch/public/list_full',
            text=fixtures.CHARGEGROUPS)

        cmd = create_project.CreateProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(5)

        mock_client.set_project_property.side_effect = iter([
            None, None, None, None, Exception])

        with self.assertRaises(Exception):
            cmd.main([
                'from-snow',
                '--instance', 'f_cern',
                '--ticket-number', 'fake_ticket',
                '--project-type', 'compute',
            ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake')
        mock_fim.create_project.assert_called_with(
            'project', 'description', 'fake'
        )
        mock_client.find_project.assert_has_calls([
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None)
        ])
        mock_client.set_project_property.assert_has_calls([
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                'type',
                'compute'),
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                'chargegroup',
                '4714e675-60b9-4fc7-80b2-61acd213f478'),
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                'chargerole',
                'charge_role'),
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                'landb-mainuser',
                'fake')
        ])

    @mock.patch('builtins.open',
                return_value=io.StringIO(fixtures.CHARGEGROUPS))
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    @requests_mock.Mocker()
    def test_update_quotas_exception(self,
                                     mock_fc,
                                     mock_snc,
                                     mock_crc,
                                     mock_open,
                                     mock_req):

        mock_fim = mock_fc.return_value
        mock_client = mock_crc.return_value
        mock_snow = mock_snc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 0
        mock_snow.get_project_creation_rp.return_value = (
            fixtures.SNOW_CREATE_RP
        )
        mock_client.find_project.side_effect = iter([
            Exception,
            fixtures.CREATE_PROJECT_NOTSYNC,
            fixtures.CREATE_PROJECT_SYNC,
            fixtures.CREATE_PROJECT_SYNC,
        ])
        mock_req.get(
            'https://gar.cern.ch/public/list_full',
            text=fixtures.CHARGEGROUPS)

        cmd = create_project.CreateProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(5)

        mock_client.set_project_quota.side_effect = iter([Exception])
        mock_client.mergeQuotaMetadata.return_value = {'quota': {}}

        with self.assertRaises(Exception):
            cmd.main([
                'from-snow',
                '--instance', 'f_cern',
                '--ticket-number', 'fake_ticket',
                '--project-type', 'compute',
            ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake')
        mock_fim.create_project.assert_called_with(
            'project', 'description', 'fake'
        )
        mock_client.find_project.assert_has_calls([
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None),
            mock.call('project', cloud='cern', region=None),
        ])
        mock_client.set_project_quota.assert_called_with(
            project_id='1',
            quota={}
        )

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    @requests_mock.Mocker()
    def test_cell_mapping_baremetal(self,
                                    mock_fc,
                                    mock_snc,
                                    mock_crc,
                                    mock_req):

        mock_fim = mock_fc.return_value
        mock_client = mock_crc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 0
        mock_client.find_project.side_effect = iter([
            Exception,
            fixtures.CREATE_PROJECT_NOTSYNC,
            fixtures.CREATE_PROJECT_SYNC,
            fixtures.CREATE_PROJECT_SYNC,
        ])
        cmd = create_project.CreateProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(5)

        cmd.main([
            'from-input',
            '--project-name', 'fake_n',
            '--description', 'fake_d',
            '--owner', 'fake_o',
            '--egroup', 'fake_eg',
            '--cores', 'fake_core',
            '--instances', 'fake_inst',
            '--ram', '1',
            '--project-type', 'fake_ty',
            '--cell-mapping', 'gva_baremetal_001',
            '--cp1-gigabytes', 'fake_cp1g',
            '--cp1-volumes', 'fake_volumes',
            '--cpio1-gigabytes', 'fake_cg',
            '--cpio1-volumes', 'fake_volumes',
            '--io1-gigabytes', 'fake_io1g',
            '--io1-volumes', 'fake_volumes',
            '--standard-gigabytes', 'fake_sg',
            '--standard-volumes', 'fake_volumes',
            '--vault-100-gigabytes', 'fake_v100g',
            '--vault-100-volumes', 'fake_v100v',
            '--vault-500-gigabytes', 'fake_v500g',
            '--vault-500-volumes', 'fake_v500v',
            '--meyrin-shares', 'fake_ms',
            '--meyrin-gigabytes', 'fake_mg',
            '--geneva_testing-shares', 'fake_gts',
            '--geneva_testing-gigabytes', 'fake_gtg',
            '--s3-buckets', '1',
            '--s3-gigabytes', '1',
        ])

        mock_snc.assert_not_called()
        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake_o')
        mock_fim.create_project.assert_called_with(
            'fake_n', 'fake_d', 'fake_o'
        )
        mock_client.find_project.assert_has_calls([
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None)
        ])

        mock_client.update_project.assert_has_calls([
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                cells_mapping='gva_baremetal_001'
            )
        ])

        mock_client.addremove_project_tag.assert_called_with(
            name='ironic',
            project_id=fixtures.CREATE_PROJECT_SYNC.id,
            condition=True)

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    @requests_mock.Mocker()
    def test_cell_mapping_multiple_cells(self,
                                         mock_fc,
                                         mock_snc,
                                         mock_crc,
                                         mock_req):

        mock_fim = mock_fc.return_value
        mock_client = mock_crc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 0
        mock_client.find_project.side_effect = iter([
            Exception,
            fixtures.CREATE_PROJECT_NOTSYNC,
            fixtures.CREATE_PROJECT_SYNC,
            fixtures.CREATE_PROJECT_SYNC,
        ])
        cmd = create_project.CreateProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(5)

        cmd.main([
            'from-input',
            '--project-name', 'fake_n',
            '--description', 'fake_d',
            '--owner', 'fake_o',
            '--egroup', 'fake_eg',
            '--cores', 'fake_core',
            '--instances', 'fake_inst',
            '--ram', '1',
            '--project-type', 'fake_ty',
            '--cell-mapping', 'cell_1,cell_2,cell_3',
            '--cp1-gigabytes', 'fake_cp1g',
            '--cp1-volumes', 'fake_volumes',
            '--cpio1-gigabytes', 'fake_cg',
            '--cpio1-volumes', 'fake_volumes',
            '--io1-gigabytes', 'fake_io1g',
            '--io1-volumes', 'fake_volumes',
            '--standard-gigabytes', 'fake_sg',
            '--standard-volumes', 'fake_volumes',
            '--vault-100-gigabytes', 'fake_v100g',
            '--vault-100-volumes', 'fake_v100v',
            '--vault-500-gigabytes', 'fake_v500g',
            '--vault-500-volumes', 'fake_v500v',
            '--meyrin-shares', 'fake_ms',
            '--meyrin-gigabytes', 'fake_mg',
            '--geneva_testing-shares', 'fake_gts',
            '--geneva_testing-gigabytes', 'fake_gtg',
            '--s3-buckets', '1',
            '--s3-gigabytes', '1',
        ])

        mock_snc.assert_not_called()
        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake_o')
        mock_fim.create_project.assert_called_with(
            'fake_n', 'fake_d', 'fake_o'
        )
        mock_client.find_project.assert_has_calls([
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None)
        ])

        mock_client.update_project.assert_has_calls([
            mock.call(
                fixtures.CREATE_PROJECT_SYNC,
                cells_mapping='cell_1,cell_2,cell_3'
            )
        ])

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    @requests_mock.Mocker()
    def test_no_cell_mapping(self,
                             mock_fc,
                             mock_snc,
                             mock_crc,
                             mock_req):

        mock_fim = mock_fc.return_value
        mock_client = mock_crc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 0
        mock_client.find_project.side_effect = iter([
            Exception,
            fixtures.CREATE_PROJECT_NOTSYNC,
            fixtures.CREATE_PROJECT_SYNC,
            fixtures.CREATE_PROJECT_SYNC,
        ])
        cmd = create_project.CreateProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(5)

        cmd.main([
            'from-input',
            '--project-name', 'fake_n',
            '--description', 'fake_d',
            '--owner', 'fake_o',
            '--egroup', 'fake_eg',
            '--cores', 'fake_core',
            '--instances', 'fake_inst',
            '--ram', '1',
            '--project-type', 'fake_ty',
            '--cell-mapping', '',
            '--cp1-gigabytes', 'fake_cp1g',
            '--cp1-volumes', 'fake_volumes',
            '--cpio1-gigabytes', 'fake_cg',
            '--cpio1-volumes', 'fake_volumes',
            '--io1-gigabytes', 'fake_io1g',
            '--io1-volumes', 'fake_volumes',
            '--standard-gigabytes', 'fake_sg',
            '--standard-volumes', 'fake_volumes',
            '--vault-100-gigabytes', 'fake_v100g',
            '--vault-100-volumes', 'fake_v100v',
            '--vault-500-gigabytes', 'fake_v500g',
            '--vault-500-volumes', 'fake_v500v',
            '--meyrin-shares', 'fake_ms',
            '--meyrin-gigabytes', 'fake_mg',
            '--geneva_testing-shares', 'fake_gts',
            '--geneva_testing-gigabytes', 'fake_gtg',
            '--s3-buckets', '1',
            '--s3-gigabytes', '1',
        ])

        mock_snc.assert_not_called()
        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake_o')
        mock_fim.create_project.assert_called_with(
            'fake_n', 'fake_d', 'fake_o'
        )
        mock_client.find_project.assert_has_calls([
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None)
        ])

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    @requests_mock.Mocker()
    def test_cell_mapping_exception(self,
                                    mock_fc,
                                    mock_snc,
                                    mock_crc,
                                    mock_req):

        mock_fim = mock_fc.return_value
        mock_client = mock_crc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 0
        mock_client.find_project.side_effect = iter([
            Exception,
            fixtures.CREATE_PROJECT_NOTSYNC,
            fixtures.CREATE_PROJECT_SYNC,
            fixtures.CREATE_PROJECT_SYNC,
        ])
        cmd = create_project.CreateProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(5)

        mock_client.update_project.side_effect = iter([
            Exception
        ])

        with self.assertRaises(Exception):
            cmd.main([
                'from-input',
                '--project-name', 'fake_n',
                '--description', 'fake_d',
                '--owner', 'fake_o',
                '--egroup', 'fake_eg',
                '--cores', 'fake_core',
                '--instances', 'fake_inst',
                '--ram', '1',
                '--project-type', 'fake_ty',
                '--cell-mapping', 'fake_cell',
                '--cp1-gigabytes', 'fake_cp1g',
                '--cp1-volumes', 'fake_volumes',
                '--cpio1-gigabytes', 'fake_cg',
                '--cpio1-volumes', 'fake_volumes',
                '--io1-gigabytes', 'fake_io1g',
                '--io1-volumes', 'fake_volumes',
                '--standard-gigabytes', 'fake_sg',
                '--standard-volumes', 'fake_volumes',
                '--vault-100-gigabytes', 'fake_v100g',
                '--vault-100-volumes', 'fake_v100v',
                '--vault-500-gigabytes', 'fake_v500g',
                '--vault-500-volumes', 'fake_v500v',
                '--meyrin-shares', 'fake_ms',
                '--meyrin-gigabytes', 'fake_mg',
                '--geneva_testing-shares', 'fake_gts',
                '--geneva_testing-gigabytes', 'fake_gtg',
                '--s3-buckets', '1',
                '--s3-gigabytes', '1',
            ])

        mock_snc.assert_not_called()
        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake_o')
        mock_fim.create_project.assert_called_with(
            'fake_n', 'fake_d', 'fake_o'
        )
        mock_client.find_project.assert_has_calls([
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None),
        ])

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    @requests_mock.Mocker()
    def test_flavor_mapping(self,
                            mock_fc,
                            mock_snc,
                            mock_crc,
                            mock_req):

        mock_fim = mock_fc.return_value
        mock_client = mock_crc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 0
        mock_client.find_project.side_effect = iter([
            Exception,
            fixtures.CREATE_PROJECT_NOTSYNC,
            fixtures.CREATE_PROJECT_SYNC,
            fixtures.CREATE_PROJECT_SYNC,
        ])
        cmd = create_project.CreateProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(5)

        cmd.main([
            'from-input',
            '--project-name', 'fake_n',
            '--description', 'fake_d',
            '--owner', 'fake_o',
            '--egroup', 'fake_eg',
            '--cores', 'fake_core',
            '--instances', 'fake_inst',
            '--ram', '1',
            '--project-type', 'fake_ty',
            '--cell-mapping', 'fake_cell',
            '--flavor-mapping', 'fake_flavor,fake_flavor2',
            '--cp1-gigabytes', 'fake_cp1g',
            '--cp1-volumes', 'fake_volumes',
            '--cpio1-gigabytes', 'fake_cg',
            '--cpio1-volumes', 'fake_volumes',
            '--io1-gigabytes', 'fake_io1g',
            '--io1-volumes', 'fake_volumes',
            '--standard-gigabytes', 'fake_sg',
            '--standard-volumes', 'fake_volumes',
            '--vault-100-gigabytes', 'fake_v100g',
            '--vault-100-volumes', 'fake_v100v',
            '--vault-500-gigabytes', 'fake_v500g',
            '--vault-500-volumes', 'fake_v500v',
            '--meyrin-shares', 'fake_ms',
            '--meyrin-gigabytes', 'fake_mg',
            '--geneva_testing-shares', 'fake_gts',
            '--geneva_testing-gigabytes', 'fake_gtg',
            '--s3-buckets', '1',
            '--s3-gigabytes', '1',
        ])

        mock_snc.assert_not_called()
        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake_o')
        mock_fim.create_project.assert_called_with(
            'fake_n', 'fake_d', 'fake_o'
        )
        mock_client.find_project.assert_has_calls([
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None)
        ])

        mock_client.update_flavor_access.assert_has_calls([
            mock.call(
                'fake_flavor',
                fixtures.CREATE_PROJECT_SYNC.id,
            )
        ])

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    @requests_mock.Mocker()
    def test_no_flavor_mapping(self,
                               mock_fc,
                               mock_snc,
                               mock_crc,
                               mock_req):

        mock_fim = mock_fc.return_value
        mock_client = mock_crc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 0
        mock_client.find_project.side_effect = iter([
            Exception,
            fixtures.CREATE_PROJECT_NOTSYNC,
            fixtures.CREATE_PROJECT_SYNC,
            fixtures.CREATE_PROJECT_SYNC,
        ])
        cmd = create_project.CreateProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(5)

        cmd.main([
            'from-input',
            '--project-name', 'fake_n',
            '--description', 'fake_d',
            '--owner', 'fake_o',
            '--egroup', 'fake_eg',
            '--cores', 'fake_core',
            '--instances', 'fake_inst',
            '--ram', '1',
            '--project-type', 'fake_ty',
            '--cell-mapping', 'fake_cell',
            '--flavor-mapping', '',
            '--cp1-gigabytes', 'fake_cp1g',
            '--cp1-volumes', 'fake_volumes',
            '--cpio1-gigabytes', 'fake_cg',
            '--cpio1-volumes', 'fake_volumes',
            '--io1-gigabytes', 'fake_io1g',
            '--io1-volumes', 'fake_volumes',
            '--standard-gigabytes', 'fake_sg',
            '--standard-volumes', 'fake_volumes',
            '--vault-100-gigabytes', 'fake_v100g',
            '--vault-100-volumes', 'fake_v100v',
            '--vault-500-gigabytes', 'fake_v500g',
            '--vault-500-volumes', 'fake_v500v',
            '--meyrin-shares', 'fake_ms',
            '--meyrin-gigabytes', 'fake_mg',
            '--geneva_testing-shares', 'fake_gts',
            '--geneva_testing-gigabytes', 'fake_gtg',
            '--s3-buckets', '1',
            '--s3-gigabytes', '1',
        ])

        mock_snc.assert_not_called()
        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake_o')
        mock_fim.create_project.assert_called_with(
            'fake_n', 'fake_d', 'fake_o'
        )
        mock_client.find_project.assert_has_calls([
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None)
        ])

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.create_project.ServiceNowClient')
    @mock.patch('ccitools.cmd.create_project.FIMClient')
    @requests_mock.Mocker()
    def test_flavor_mapping_exception(self,
                                      mock_fc,
                                      mock_snc,
                                      mock_crc,
                                      mock_req):

        mock_fim = mock_fc.return_value
        mock_client = mock_crc.return_value

        mock_fim.is_valid_owner.return_value = True
        mock_fim.create_project.return_value = 0
        mock_client.find_project.side_effect = iter([
            Exception,
            fixtures.CREATE_PROJECT_NOTSYNC,
            fixtures.CREATE_PROJECT_SYNC,
            fixtures.CREATE_PROJECT_SYNC,
        ])
        cmd = create_project.CreateProjectCMD()
        cmd.wait_for_project.retry.wait = wait_none()
        cmd.wait_for_project.retry.stop = stop_after_attempt(5)

        mock_client.update_flavor_access.side_effect = iter([
            Exception,
            Exception,
            Exception,
            Exception
        ])

        with self.assertRaises(Exception):
            cmd.main([
                'from-input',
                '--project-name', 'fake_n',
                '--description', 'fake_d',
                '--owner', 'fake_o',
                '--egroup', 'fake_eg',
                '--cores', 'fake_core',
                '--instances', 'fake_inst',
                '--ram', '1',
                '--project-type', 'fake_ty',
                '--cell-mapping', 'fake_cell',
                '--flavor-mapping', 'fake_flavor',
                '--cp1-gigabytes', 'fake_cp1g',
                '--cp1-volumes', 'fake_volumes',
                '--cpio1-gigabytes', 'fake_cg',
                '--cpio1-volumes', 'fake_volumes',
                '--io1-gigabytes', 'fake_io1g',
                '--io1-volumes', 'fake_volumes',
                '--standard-gigabytes', 'fake_sg',
                '--standard-volumes', 'fake_volumes',
                '--vault-100-gigabytes', 'fake_v100g',
                '--vault-100-volumes', 'fake_v100v',
                '--vault-500-gigabytes', 'fake_v500g',
                '--vault-500-volumes', 'fake_v500v',
                '--meyrin-shares', 'fake_ms',
                '--meyrin-gigabytes', 'fake_mg',
                '--geneva_testing-shares', 'fake_gts',
                '--geneva_testing-gigabytes', 'fake_gtg',
                '--s3-buckets', '1',
                '--s3-gigabytes', '1',
            ])

        mock_snc.assert_not_called()
        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_fim.is_valid_owner.assert_called_with('fake_o')
        mock_fim.create_project.assert_called_with(
            'fake_n', 'fake_d', 'fake_o'
        )
        mock_client.find_project.assert_has_calls([
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None),
            mock.call('fake_n', cloud='cern', region=None)
        ])
