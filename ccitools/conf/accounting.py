from oslo_config import cfg

query = cfg.StrOpt(
    'query',
    default=('SELECT {0} AS {1} '
             'FROM {2} '
             'WHERE time >= {3} - 24h AND time < {3} AND region =~ /{4}/ '
             'GROUP BY {5}')
)

regions = cfg.ListOpt(
    'regions',
    default=['cern', 'pdc']
)

project_metrics = cfg.DictOpt(
    'project_metrics',
    default={
        'cores_used': {
            'select': 'round(mean(usage))',
            'field': 'usage',
            'from': 'cores_quota_per_project',
            'as': 'cores_quota',
            'group': ['project_id', 'region'],
            'display': 'Cores Used',
        },
        'cores_quota': {
            'select': 'round(mean(quota))',
            'field': 'quota',
            'from': 'cores_quota_per_project',
            'group': ['project_id', 'region'],
            'display': 'Cores Quota',
        },
        'instances_used': {
            'select': 'round(mean(usage))',
            'field': 'usage',
            'from': 'instances_quota_per_project',
            'as': 'instances_quota',
            'group': ['project_id', 'region'],
            'display': 'Instances Used',
        },
        'instances_quota': {
            'select': 'round(mean(quota))',
            'field': 'quota',
            'from': 'instances_quota_per_project',
            'group': ['project_id', 'region'],
            'display': 'Instances Quota',
        },
        'ram_used': {
            'select': 'round(mean(usage)) / 1024',
            'field': 'usage',
            'from': 'ram_quota_per_project',
            'as': 'ram_quota',
            'group': ['project_id', 'region'],
            'display': 'RAM Used [GB]',
        },
        'ram_quota': {
            'select': 'round(mean(quota)) / 1024',
            'field': 'quota',
            'from': 'ram_quota_per_project',
            'group': ['project_id', 'region'],
            'display': 'RAM Quota [GB]',
        },
        'volumes_used': {
            'select': 'round(mean(usage))',
            'field': 'usage',
            'from': 'volumes_gb_quota_per_project',
            'as': 'volumes_quota',
            'group': ['project_id', 'region'],
            'display': 'Volumes Used [GB]',
        },
        'volumes_quota': {
            'select': 'round(mean(quota))',
            'field': 'quota',
            'from': 'volumes_gb_quota_per_project',
            'group': ['project_id', 'region'],
            'display': 'Volumes Quota [GB]',
        },
        's3_used': {
            'select': 'round(mean(usage))',
            'field': 'usage',
            'from': 's3_gb_quota_per_project',
            'as': 's3_quota',
            'group': ['project_id', 'region'],
            'display': 'S3 Used [GB]',
        },
        's3_quota': {
            'select': 'round(mean(quota))',
            'field': 'quota',
            'from': 's3_gb_quota_per_project',
            'group': ['project_id', 'region'],
            'display': 'S3 Quota [GB]',
        },
        'fileshares_used': {
            'select': 'round(mean(usage))',
            'field': 'usage',
            'from': 'fileshares_gb_quota_per_project',
            'as': 'fileshares_quota',
            'group': ['project_id', 'region'],
            'display': 'Shares Used [GB]',
        },
        'fileshares_quota': {
            'select': 'round(mean(quota))',
            'field': 'quota',
            'from': 'fileshares_gb_quota_per_project',
            'group': ['project_id', 'region'],
            'display': 'Shares Quota [GB]',
        },
        'loadbalancers_used': {
            'select': 'round(mean(usage))',
            'field': 'usage',
            'from': 'loadbalancer_quota_per_project',
            'as': 'loadbalancers_quota',
            'group': ['project_id', 'region'],
            'display': 'Loadbalancers Used',
        },
        'loadbalancers_quota': {
            'select': 'round(mean(quota))',
            'field': 'quota',
            'from': 'loadbalancer_quota_per_project',
            'group': ['project_id', 'region'],
            'display': 'Loadbalancers Quota',
        },
    }
)

chg_metrics = cfg.DictOpt(
    'chg_metrics',
    default={
        'cores_used': {
            'select': 'round(mean(usage))',
            'field': 'usage',
            'from': 'cores_quota_per_chargegroup_and_chargerole',
            'as': 'cores_quota',
            'group': ['chargegroup', 'region'],
            'display': 'Cores Used',
        },
        'cores_quota': {
            'select': 'round(mean(quota))',
            'field': 'quota',
            'from': 'cores_quota_per_chargegroup_and_chargerole',
            'group': ['chargegroup', 'region'],
            'display': 'Cores Quota',
        },
        'instances_used': {
            'select': 'round(mean(usage))',
            'field': 'usage',
            'from': 'instances_quota_per_chargegroup_and_chargerole',
            'as': 'instances_quota',
            'group': ['chargegroup', 'region'],
            'display': 'Instances Used',
        },
        'instances_quota': {
            'select': 'round(mean(quota))',
            'field': 'quota',
            'from': 'instances_quota_per_chargegroup_and_chargerole',
            'group': ['chargegroup', 'region'],
            'display': 'Instances Quota',
        },
        'ram_used': {
            'select': 'round(mean(usage)) / 1024',
            'field': 'usage',
            'from': 'ram_quota_per_chargegroup_and_chargerole',
            'as': 'ram_quota',
            'group': ['chargegroup', 'region'],
            'display': 'RAM Used [GB]',
        },
        'ram_quota': {
            'select': 'round(mean(quota)) / 1024',
            'field': 'quota',
            'from': 'ram_quota_per_chargegroup_and_chargerole',
            'group': ['chargegroup', 'region'],
            'display': 'RAM Quota [GB]',
        },
        'volumes_used': {
            'select': 'round(mean(usage))',
            'field': 'usage',
            'from': 'volumes_gb_quota_per_chargegroup_and_chargerole',
            'as': 'volumes_quota',
            'group': ['chargegroup', 'region'],
            'display': 'Volumes Used [GB]',
        },
        'volumes_quota': {
            'select': 'round(mean(quota))',
            'field': 'quota',
            'from': 'volumes_gb_quota_per_chargegroup_and_chargerole',
            'group': ['chargegroup', 'region'],
            'display': 'Volumes Quota [GB]',
        },
        's3_used': {
            'select': 'round(mean(usage))',
            'field': 'usage',
            'from': 's3_gb_quota_per_chargegroup_and_chargerole',
            'as': 's3_quota',
            'group': ['chargegroup', 'region'],
            'display': 'S3 Used [GB]',
        },
        's3_quota': {
            'select': 'round(mean(quota))',
            'field': 'quota',
            'from': 's3_gb_quota_per_chargegroup_and_chargerole',
            'group': ['chargegroup', 'region'],
            'display': 'S3 Quota [GB]',
        },
        'fileshares_used': {
            'select': 'round(mean(usage))',
            'field': 'usage',
            'from': 'fileshares_gb_quota_per_chargegroup_and_chargerole',
            'as': 'fileshares_quota',
            'group': ['chargegroup', 'region'],
            'display': 'Shares Used [GB]',
        },
        'fileshares_quota': {
            'select': 'round(mean(quota))',
            'field': 'quota',
            'from': 'fileshares_gb_quota_per_chargegroup_and_chargerole',
            'group': ['chargegroup', 'region'],
            'display': 'Shares Quota [GB]',
        },
        'loadbalancers_used': {
            'select': 'round(mean(usage))',
            'field': 'usage',
            'from': 'loadbalancer_quota_per_chargegroup_and_chargerole',
            'as': 'loadbalancers_quota',
            'group': ['chargegroup', 'region'],
            'display': 'Loadbalancers Used',
        },
        'loadbalancers_quota': {
            'select': 'round(mean(quota))',
            'field': 'quota',
            'from': 'loadbalancer_quota_per_chargegroup_and_chargerole',
            'group': ['chargegroup', 'region'],
            'display': 'Loadbalancers Quota',
        }
    }
)

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    query,
    regions,
    project_metrics,
    chg_metrics,
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
