import logging

from ccitools import common
from ccitools.errors import CciXldapNotFoundError
import ldap
import ldap.sasl

logger = logging.getLogger(__name__)


class XldapClient(object):
    def __init__(self, server, kerberized=False):
        self.server = server
        self.kerberized = kerberized

    def _init_connection(self):
        connection = ldap.initialize(self.server)
        if self.kerberized:
            auth_tokens = ldap.sasl.gssapi()
            connection.sasl_interactive_bind_s('', auth_tokens)
        return connection

    def _search(self, searchFilter, retrieveAttributes,
                baseDN="DC=cern,DC=ch"):
        connection = self._init_connection()
        searchScope = ldap.SCOPE_SUBTREE
        return connection.search_s(baseDN, searchScope,
                                   searchFilter, retrieveAttributes)

    @staticmethod
    def _convert(text):
        return text.decode('ASCII')

    def get_vm_main_user(self, vm_name):
        """Query ldap to return the main user of the given VM.

        :param vm_name: VM name
        :returns: VM main user
        """
        vm_name = common.normalize_hostname(vm_name)

        searchFilter = "(&(objectClass=computer)(cn=%s))" % vm_name
        retrieveAttributes = ['manager']
        result = self._search(searchFilter, retrieveAttributes)
        try:
            cn = self._convert(result[0][1]['manager'][0])
            return cn.split(',')[0].split('=')[1]
        except Exception:
            logger.warning("'%s' main user not found", vm_name)
            raise CciXldapNotFoundError(
                "VM '%s' main user not found" % (vm_name))

    def get_vm_owner(self, vm_name):
        """Query ldap to return the username who manages the given VM.

        :param vm_name: VM name
        :returns: VM owner
        """
        vm_name = common.normalize_hostname(vm_name)

        searchFilter = "(&(objectClass=computer)(cn=%s))" % vm_name
        retrieveAttributes = ['managedBy']
        result = self._search(searchFilter, retrieveAttributes)
        try:
            cn = self._convert(result[0][1]['managedBy'][0])
            return cn.split(',')[0].split('=')[1]
        except Exception:
            logger.warning("'%s' owner not found", vm_name)
            raise CciXldapNotFoundError("VM '%s' owner not found" % (vm_name))

    def get_egroup_email(self, egroup):
        """Queriy ldap to return email address of the given egroup.

        :param egroup: E-group name
        :returns: E-group email address
        """
        searchFilter = "(&(objectClass=group)(cn=%s))" % egroup
        baseDN = "OU=e-groups,OU=Workgroups,DC=cern,DC=ch"
        retrieveAttributes = ['mail']
        result = self._search(searchFilter, retrieveAttributes, baseDN)
        try:
            return self._convert(result[0][1]['mail'][0])
        except Exception:
            logger.warning("'%s' egroup not found", egroup)
            raise CciXldapNotFoundError("'%s' egroup not found" % (egroup))

    def get_primary_account(self, username):
        """Query ldap to return the main user assigned to the given username.

        :param username: User name
        :returns: Username responsible
        """
        searchFilter = "(&(objectClass=user)(cn=%s))" % username
        baseDN = "OU=Users,OU=Organic Units,DC=cern,DC=ch"
        retrieveAttributes = ['cernAccountType',
                              'cernAccountOwner', 'sAMAccountName']
        result = self._search(searchFilter, retrieveAttributes, baseDN)
        if result:
            account_type = self._convert(result[0][1]['cernAccountType'][0])
            if account_type.lower() == 'primary':
                return self._convert(result[0][1]['sAMAccountName'][0])
            elif account_type.lower() in ['secondary', 'service']:
                return self._convert(
                    result[0][1]['cernAccountOwner'][0]
                ).split(',')[0].split('=')[1]
        raise CciXldapNotFoundError("User '%s' not found" % (username))

    def get_user_email(self, username):
        """Query ldap to return email address of the given username.

        :param username: User name
        :returns: Username's email address
        """
        searchFilter = "(&(objectClass=user)(cn=%s))" % username
        baseDN = "OU=Users,OU=Organic Units,DC=cern,DC=ch"
        retrieveAttributes = ['mail']
        result = self._search(searchFilter, retrieveAttributes, baseDN)
        if result and result[0][1]:
            return self._convert(result[0][1]['mail'][0])
        logger.warning("User '%s' not found" % (username))
        raise CciXldapNotFoundError("User '%s' not found", username)

    def get_email(self, name):
        """Get email from user.

        Query ldap to return email address of the given username or
        egroup

        :param name: Username or egroup to get the email address for
        :returns: Params's email address
        """
        if self.is_existing_user(name):
            return self.get_user_email(name)
        if self.is_existing_egroup(name):
            return self.get_egroup_email(name)
        raise CciXldapNotFoundError("'%s' not found" % (name))

    def is_existing_user(self, username):
        """Check on ldap if the given user exists.

        :param username: User name
        :returns: bool
        """
        searchFilter = "(&(objectClass=user)(cn=%s))" % username
        baseDN = "OU=Users,OU=Organic Units,DC=cern,DC=ch"
        result = self._search(searchFilter, None, baseDN)
        if result and result[0][0]:
            return True
        return False

    def is_existing_egroup(self, egroup):
        """Check on ldap if the given e-group exists.

        :param egroup: E-group name
        :returns: bool
        """
        searchFilter = "(&(objectClass=group)(cn=%s))" % egroup
        baseDN = "OU=e-groups,OU=Workgroups,DC=cern,DC=ch"
        result = self._search(searchFilter, None, baseDN)
        if result and result[0][0]:
            return True
        return False
