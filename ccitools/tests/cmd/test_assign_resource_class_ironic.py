from ccitools.cmd.assign_resource_class_ironic import main
import logging
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.utils.cloud.cloud_config')
@mock.patch('builtins.print')
@mock.patch('ccitools.cmd.assign_resource_class_ironic.LanDB')
class TestAssignResourceClassIronic(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_p_flavor(self, mock_landb, mock_print, mock_cc):
        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            Interfaces=[mock.MagicMock(Name='fake_device_name.CERN.CH',
                                       ServiceName='S513-V-IP562')],
            DeviceName='fake_device_name',
        )

        main(['--node', 'dl8200652-0052-d'])
        mock_print.assert_called_with('openstack baremetal node set '
                                      'dl8200652-0052-d --resource-class '
                                      'BAREMETAL_P1_DL8200652_S513_V_IP562')

    def test_s_flavor(self, mock_landb, mock_print, mock_cc):
        mock_landb_client = mock_landb.return_value
        mock_landb_client.device_info.return_value = mock.MagicMock(
            Interfaces=[mock.MagicMock(Name='fake_device_name.CERN.CH',
                                       ServiceName='S513-V-IP562')],
            DeviceName='fake_device_name',
        )

        main(['--node', 'dl8200652-0052-d', '--flavor_prefix', 's1'])
        mock_print.assert_called_with('openstack baremetal node set '
                                      'dl8200652-0052-d --resource-class '
                                      'BAREMETAL_S1_DL8200652_S513_V_IP562')
