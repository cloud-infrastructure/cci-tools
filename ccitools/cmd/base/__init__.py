import logging
import urllib3

from argparse import ArgumentParser
from ccitools.utils.metrics import MetricsHelper

# configure logging
logging.getLogger('requests').setLevel(logging.WARNING)
logger = logging.getLogger(__name__)
urllib3.disable_warnings()

CONSOLE_MESSAGE_FORMAT = '%(levelname)s: %(message)s'
dump_stack_trace = False


class BaseCMD(object):
    def __init__(self, description):
        self.metrics = MetricsHelper()

        self.parser = ArgumentParser(description=description)

        self.parser.add_argument(
            '-v',
            '--verbose',
            action='count',
            dest='verbose_level',
            default=0,
            help='Increase verbosity of output. Can be repeated.', )
        self.parser.add_argument(
            '-d',
            '--debug',
            action='store_true',
            default=False,
            help='increase output to debug messages', )

    def parse_args(self, args):
        args = self.parser.parse_args(args)
        self.configure_logging(args)
        return args

    def configure_logging(self, args):
        if args.verbose_level >= 1:
            logging.basicConfig(
                level=logging.INFO,
                format="%(asctime)s %(levelname)s %(name)s - %(message)s")
        elif args.debug:
            logging.basicConfig(
                level=logging.DEBUG,
                format="%(asctime)s %(levelname)s %(name)s - %(message)s")
        else:
            logging.basicConfig(
                level=logging.WARNING,
                format="%(asctime)s %(levelname)s %(name)s - %(message)s")
