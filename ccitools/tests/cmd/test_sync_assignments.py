import logging

from ccitools.cmd.sync_assignments import main
from unittest import mock
from unittest import TestCase


class TestSyncAssignmentsCMD(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    def test_dryrun_no_params(self, mock_cloud):
        with self.assertRaises(SystemExit) as cm:
            main(['--dryrun'])

        self.assertEqual(cm.exception.code, 2)

    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    @mock.patch('ccitools.cmd.sync_assignments.keystone_client')
    def test_dryrun(self, mock_keystone, mock_cloud):
        mock_kc = mock_keystone.Client.return_value

        main([
            '--dryrun',
            '--source', 'cern',
            '--target', 'pdc',
        ])

        mock_kc.assert_has_calls([
            mock.call.projects.list(
                domain='default', user=None, parent=None, region='cern'),
            mock.call.projects.list().__len__(),
            mock.call.projects.list().__iter__(),
            mock.call.projects.list(
                domain='default', user=None, parent=None, region='pdc'),
            mock.call.projects.list().__len__(),
            mock.call.projects.list().__iter__(),
            mock.call.projects.list().__iter__(),
            mock.call.roles.list(),
            mock.call.roles.list().__len__(),
            mock.call.roles.list().__iter__(),
            mock.call.roles.list(),
            mock.call.roles.list().__len__(),
            mock.call.roles.list().__iter__(),
            mock.call.roles.list().__iter__(),
            mock.call.role_assignments.list(),
            mock.call.role_assignments.list().__len__(),
            mock.call.role_assignments.list(),
            mock.call.role_assignments.list().__len__(),
            mock.call.role_assignments.list().__iter__(),
            mock.call.role_assignments.list().__iter__()
        ])
