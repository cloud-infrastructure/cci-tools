import logging

from ccitools.cmd.update_quota import main
from ccitools.cmd.update_quota import UpdateQuotaCMD
from ccitools.cmd.update_quota import UserNotProjectMemberError
from ccitools.tests.cmd import fixtures
from keystoneclient import exceptions
from unittest import mock
from unittest import TestCase


class TestUpdateQuota(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.cloud_config')
    def test_default_args(self, mock_cc, mock_ks):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value

        with self.assertRaises(SystemExit) as cm:
            main([])
            mock_cc.OpenStackConfig.assert_called_with()
            mock_ks.Session.assert_called_with(
                auth=mock_cloud.get_auth.return_value)
        self.assertEqual(cm.exception.code, 2)

    def test_UserNot_Project(self):
        UNP = UserNotProjectMemberError('fake')
        UNP.__str__()

    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.cmd.update_quota.ServiceNowClient')
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    def test_update_quota_execute(self, mock_crc, mock_snc, mock_cc, mock_ks):
        mock_snow = mock_snc.return_value
        mock_client = mock_crc.return_value
        mock_snow.get_quota_update_rp.return_value = (
            fixtures.SNOW_UPDATE_RP
        )

        UpdateQuotaCMD().main([
            '--instance', 'fake',
            '--ticket-number', 'INCXXXXXXX',
            'execute',
        ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_client.find_project.assert_called_with('project')

    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.cmd.update_quota.ServiceNowClient')
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    def test_update_quota_preprocess(self, mock_crc, mock_snc, mock_cc,
                                     mock_ks):
        mock_snow = mock_snc.return_value
        mock_client = mock_crc.return_value
        mock_snow.get_quota_update_rp.return_value = (
            fixtures.SNOW_UPDATE_RP
        )

        UpdateQuotaCMD().main(
            [
                '--instance', 'fake',
                '--ticket-number', 'INCXXXXXXX',
                'preprocess',
                '--fe', 'fake',
                '--assignment-group', 'fake_g',
            ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_client.find_project.assert_called_with('project')

    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.cmd.update_quota.ServiceNowClient')
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    def test_update_quota_preprocess_perform(self, mock_crc, mock_snc, mock_cc,
                                             mock_ks):
        mock_snow = mock_snc.return_value
        mock_client = mock_crc.return_value
        mock_snow.get_quota_update_rp.return_value = (
            fixtures.SNOW_UPDATE_RP
        )

        UpdateQuotaCMD().main(
            [
                '--instance', 'fake',
                '--ticket-number', 'INCXXXXXXX',
                '--perform',
                'preprocess',
                '--fe', 'fake',
                '--assignment-group', 'fake_g',
            ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_client.find_project.assert_called_with('project')

    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.cmd.update_quota.ServiceNowClient')
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    def test_update_quota_preprocess_no_project(self, mock_crc, mock_snc,
                                                mock_cc,
                                                mock_ks):
        mock_snow = mock_snc.return_value
        mock_client = mock_crc.return_value
        mock_client.find_project.side_effect = iter(
            [exceptions.NotFound('error')])
        mock_snow.get_quota_update_rp.return_value = (
            fixtures.SNOW_UPDATE_RP
        )

        with self.assertRaises(Exception):
            UpdateQuotaCMD().main(
                [
                    '--instance', 'fake',
                    '--ticket-number', 'INCXXXXXXX',
                    'preprocess',
                    '--fe', 'fake',
                    '--assignment-group', 'fake_g',
                ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_client.find_project.assert_called_with('project')

    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.cmd.update_quota.ServiceNowClient')
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    def test_update_quota_execute_perform(self, mock_crc, mock_snc,
                                          mock_cc, mock_ks):
        mock_snow = mock_snc.return_value
        mock_client = mock_crc.return_value
        mock_snow.get_quota_update_rp.return_value = (
            fixtures.SNOW_UPDATE_RP
        )

        UpdateQuotaCMD().main([
            '--instance', 'fake',
            '--ticket-number', 'INCXXXXXXX',
            '--perform',
            'execute',
        ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_client.find_project.assert_called_with('project')

    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.cmd.update_quota.ServiceNowClient')
    @mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
    def test_update_quota_preprocess_perform_no_project(self, mock_crc,
                                                        mock_snc, mock_cc,
                                                        mock_ks):
        mock_snow = mock_snc.return_value
        mock_client = mock_crc.return_value
        mock_client.find_project.side_effect = iter(
            [exceptions.NotFound('error')])
        mock_snow.get_quota_update_rp.return_value = (
            fixtures.SNOW_UPDATE_RP
        )

        with self.assertRaises(Exception):
            UpdateQuotaCMD().main(
                [
                    '--instance', 'fake',
                    '--ticket-number', 'INCXXXXXXX',
                    '--perform',
                    'preprocess',
                    '--fe', 'fake',
                    '--assignment-group', 'fake_g',
                ])

        mock_crc.assert_called_with(cloud='cern', region_name=None)
        mock_client.find_project.assert_called_with('project')
