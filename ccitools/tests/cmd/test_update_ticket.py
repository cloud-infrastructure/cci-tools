import logging

from ccitools.cmd.update_ticket import main
from ccitools.errors import CciSnowTicketError
from unittest import mock
from unittest import TestCase


class TestUpdateTicket(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 2)

    def test_update_ticket_no_action(self):
        with self.assertRaises(SystemExit) as cm:
            main([
                '--ticket-number', 'INCXXXXXXX',
            ])
        self.assertEqual(cm.exception.code, 2)

    @mock.patch('ccitools.cmd.update_ticket.ServiceNowClient')
    def test_update_ticket_worknote(self, mock_snow):
        mockedSC = mock_snow.return_value
        mockedTC = mockedSC.ticket.get_ticket.return_value

        main([
            '--ticket-number', 'INCXXXXXXX',
            '--worknote', 'fake_worknote'
        ])
        mock_snow.assert_called_with(
            instance='cern'
        )
        mockedTC.add_work_note.assert_called_with(
            'fake_worknote'
        )

    @mock.patch('ccitools.cmd.update_ticket.ServiceNowClient')
    def test_update_ticket_comment(self, mock_snow):
        mockedSC = mock_snow.return_value
        mockedTC = mockedSC.ticket.get_ticket.return_value

        main([
            '--ticket-number', 'INCXXXXXXX',
            '--comment', 'fake_comment'
        ])
        mock_snow.assert_called_with(
            instance='cern'
        )
        mockedTC.add_comment.assert_called_with(
            'fake_comment'
        )

    @mock.patch('ccitools.cmd.update_ticket.ServiceNowClient')
    def test_update_ticket_worknote_exception(self, mock_snow):
        mockedSC = mock_snow.return_value
        mockedTC = mockedSC.ticket.get_ticket.return_value
        mockedTC.add_work_note.side_effect = iter(
            [CciSnowTicketError('error')])

        main([
            '--ticket-number', 'INCXXXXXXX',
            '--worknote', 'fake_worknote'
        ])
        mock_snow.assert_called_with(
            instance='cern'
        )
        mockedTC.add_work_note.assert_called_with(
            'fake_worknote'
        )

    @mock.patch('ccitools.cmd.update_ticket.ServiceNowClient')
    def test_update_ticket_comment_exception(self, mock_snow):
        mockedSC = mock_snow.return_value
        mockedTC = mockedSC.ticket.get_ticket.return_value
        mockedTC.add_comment.side_effect = iter([CciSnowTicketError('error')])

        main([
            '--ticket-number', 'INCXXXXXXX',
            '--comment', 'fake_comment'
        ])

        mock_snow.assert_called_with(
            instance='cern'
        )
        mockedTC.add_comment.assert_called_with(
            'fake_comment'
        )
