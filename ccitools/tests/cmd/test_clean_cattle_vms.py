import logging

from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase


@mock.patch('ccitools.utils.cloud.cloud_config')
@mock.patch('ccitools.cmd.base.cloud.CloudRegionClient')
class TestCleanCattleVMs(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.aitools_mock = mock.MagicMock()
        modules = {
            'aitools': self.aitools_mock,
            'aitools.pdb': self.aitools_mock.pdb,
        }
        self.patcher = mock.patch.dict('sys.modules', modules)
        self.patcher.start()
        self.addCleanup(self.patcher.stop)

    def test_default_args(self, mock_cc, mock_cl):
        from ccitools.cmd.clean_cattle_vms import main

        with self.assertRaises(SystemExit) as cm:
            main([])
        self.assertEqual(cm.exception.code, 2)

    def test_proper_data(self, mock_cc, mock_cl):
        from ccitools.cmd.clean_cattle_vms import main

        mock_cl.return_value = mock_cl
        mock_cl.get_servers_by_hypervisor.return_value = (
            fixtures.CATTLE_PROJECT_OUTPUT
        )
        mock_cl.find_project.return_value = (
            fixtures.CATTLE_PROJECT_OUTPUT2
        )

        main([
            '--hypervisors', 'fake_hv1',
        ])

    def test_fake_group_starting_cloud(self, mock_cc, mock_cl):
        from ccitools.cmd.clean_cattle_vms import main
        mock_cl.return_value = mock_cl
        mock_cl.get_servers_by_hypervisor.return_value = (
            fixtures.CATTLE_PROJECT_OUTPUT
        )
        mock_cl.find_project.return_value = (
            fixtures.CATTLE_PROJECT_OUTPUT3
        )

        main([
            '--hypervisors', 'fake_hv1',
        ])

    def test_not_cattle_server(self, mock_cc, mock_cl):
        from ccitools.cmd.clean_cattle_vms import main
        mock_cl.return_value = mock_cl
        mock_cl.get_servers_by_hypervisor.return_value = (
            fixtures.CATTLE_PROJECT_OUTPUT
        )
        mock_cl.find_project.return_value = (
            fixtures.CATTLE_PROJECT_OUTPUT4
        )

        main([
            '--hypervisors', 'fake_hv1',
        ])

    def test_perform(self, mock_cc, mock_cl):
        from ccitools.cmd.clean_cattle_vms import main
        mock_cl.return_value = mock_cl
        mock_cl.get_servers_by_hypervisor.return_value = (
            fixtures.CATTLE_PROJECT_OUTPUT
        )
        mock_cl.find_project.return_value = (
            fixtures.CATTLE_PROJECT_OUTPUT2
        )

        main([
            # The parser requires 1 argument with --perform or fails.
            '--perform',
            '--hypervisors', 'fake_hv1',
        ])
