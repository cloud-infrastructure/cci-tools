from zeep import Transport

import lxml.html  # nosec


class IISTransport(Transport):

    def _load_remote_data(self, url):
        self.logger.debug("Loading remote data from: %s", url)
        response = self.session.get(
            url, timeout=self.load_timeout, allow_redirects=True)
        response.raise_for_status()
        if not response.content.startswith(b'<?xml'):
            root = lxml.html.fromstring(response.content)
            payload = {
                'code': root.find('.//input[@name="code"]').value,
                'state': root.find('.//input[@name="state"]').value,
                'session_state': root.find(
                    './/input[@name="session_state"]').value
            }
            url_callback = root.find('.//form').action
            response = self.session.post(
                url_callback,
                data=payload,
                allow_redirects=True,
                timeout=self.load_timeout)
            response.raise_for_status()
        return response.content
