import logging

from ccitools.cmd.assignment_cleanup import main
from ccitools.tests.cmd import fixtures
from unittest import mock
from unittest import TestCase


class TestAssignmentCleanup(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    @mock.patch('ccitools.utils.cloud.cloud_config')
    def test_default_args_no_auth(self, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value

        main([])

        mock_config.get_one_cloud.assert_called_with(
            cloud='cern',
            region_name=None,
            argparse=None
        )

    @mock.patch('ccitools.utils.cloud.cloud_config')
    def test_default_args_raise_IOError(self, mock_cc):
        mock_cc.OpenStackConfig.side_effect = (
            iter([IOError])
        )

        with self.assertRaises(IOError):
            main([])

        mock_cc.OpenStackConfig.assert_called_with()

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.assignment_cleanup.keystone_client')
    def test_missing_empty_role_assignments(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.role_assignments.list.return_value = []

        main(['--missing'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(domain='default')
        mock_client.users.list.assert_called_with(domain='default')
        mock_client.groups.list.assert_called_with(domain='default')
        mock_client.roles.list.assert_called_with()
        mock_client.role_assignments.list.assert_called_with()

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.assignment_cleanup.keystone_client')
    def test_missing_role_assignments(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = fixtures.CLEANUP_PROJECTS
        mock_client.users.list.return_value = fixtures.CLEANUP_USERS
        mock_client.groups.list.return_value = fixtures.CLEANUP_GROUPS
        mock_client.roles.list.return_value = fixtures.CLEANUP_ROLES
        mock_client.role_assignments.list.return_value = (
            fixtures.CLEANUP_ASSIGNMENTS)

        main(['--missing'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(domain='default')
        mock_client.users.list.assert_called_with(domain='default')
        mock_client.groups.list.assert_called_with(domain='default')
        mock_client.roles.list.assert_called_with()

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.assignment_cleanup.keystone_client')
    def test_missing_role_assignments_clean(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = fixtures.CLEANUP_PROJECTS
        mock_client.users.list.return_value = fixtures.CLEANUP_USERS
        mock_client.groups.list.return_value = fixtures.CLEANUP_GROUPS
        mock_client.roles.list.return_value = fixtures.CLEANUP_ROLES
        mock_client.role_assignments.list.return_value = (
            fixtures.CLEANUP_ASSIGNMENTS)

        main(['--missing', '--clean'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(domain='default')
        mock_client.users.list.assert_called_with(domain='default')
        mock_client.groups.list.assert_called_with(domain='default')
        mock_client.roles.list.assert_called_with()

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.assignment_cleanup.keystone_client')
    def test_personal_empty_projects(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = []
        mock_client.roles.list.return_value = fixtures.CLEANUP_ROLES

        main(['--personal'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(
            domain='default',
            enabled=True,
            tags_any='expiration'
        )
        mock_client.roles.list.assert_called_with()
        mock_client.role_assignments.list.assert_called_with(
            role='member_id'
        )

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.assignment_cleanup.keystone_client')
    def test_personal_empty_roles(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = []
        mock_client.roles.list.return_value = []

        with self.assertRaises(Exception):
            main(['--personal'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(
            domain='default',
            enabled=True,
            tags_any='expiration'
        )
        mock_client.roles.list.assert_called_with()

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.assignment_cleanup.keystone_client')
    def test_empty_role_assignments(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = fixtures.ACLS_CLEANUP_PROJECTS
        mock_client.roles.list.return_value = fixtures.CLEANUP_ROLES
        mock_client.role_assignments.list.return_value = []

        main(['--personal'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(
            domain='default',
            enabled=True,
            tags_any='expiration'
        )
        mock_client.roles.list.assert_called_with()
        mock_client.role_assignments.list.assert_called_with(
            role='member_id'
        )

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.assignment_cleanup.keystone_client')
    def test_role_assignments(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = fixtures.ACLS_CLEANUP_PROJECTS
        mock_client.roles.list.return_value = fixtures.CLEANUP_ROLES
        mock_client.role_assignments.list.return_value = (
            fixtures.ACL_CLEANUP_ASSIGNMENTS)

        main(['--personal'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(
            domain='default',
            enabled=True,
            tags_any='expiration'
        )
        mock_client.roles.list.assert_called_with()
        mock_client.role_assignments.list.assert_called_with(
            role='member_id'
        )

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.cmd.assignment_cleanup.keystone_client')
    def test_role_assignments_clean(self, mock_kc, mock_ks, mock_cc):
        mock_config = mock_cc.OpenStackConfig.return_value
        mock_cloud = mock_config.get_one_cloud.return_value
        mock_client = mock_kc.Client.return_value

        mock_client.projects.list.return_value = fixtures.ACLS_CLEANUP_PROJECTS
        mock_client.roles.list.return_value = fixtures.CLEANUP_ROLES
        mock_client.role_assignments.list.return_value = (
            fixtures.ACL_CLEANUP_ASSIGNMENTS)

        main(['--personal', '--clean'])

        mock_cc.OpenStackConfig.assert_called_with()
        mock_ks.Session.assert_called_with(
            auth=mock_cloud.get_auth.return_value)
        mock_client.projects.list.assert_called_with(
            domain='default',
            enabled=True,
            tags_any='expiration'
        )
        mock_client.roles.list.assert_called_with()
        mock_client.role_assignments.list.assert_called_with(
            role='member_id'
        )
