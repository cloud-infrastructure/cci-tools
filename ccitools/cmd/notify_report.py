#!/usr/bin/python3

import logging
import sys
import tenacity
import urllib.request
import yaml

from ccitools.cmd.base import BaseCMD
from ccitools.utils.sendmail import MailClient
from jinja2 import Environment

# configure logging
logger = logging.getLogger(__name__)

MAX_TIMEOUT = 300
RETRY_INTERVAL = 30


class NotifyUpgradeReportCMD(BaseCMD):

    def __init__(self):
        super(NotifyUpgradeReportCMD, self).__init__(
            description="Send notifications based on the upgrade"
                        "report filtered")

    def generate_project_report(self, args):
        report = {}
        # Open the report from the source
        with open(args.report) as stream:
            try:
                report = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                logger.exception(exc)
                raise SystemExit(1)
        return report

    @tenacity.retry(stop=tenacity.stop_after_delay(MAX_TIMEOUT),
                    wait=tenacity.wait_fixed(RETRY_INTERVAL),
                    before_sleep=tenacity.before_sleep_log(
                        logger,
                        logging.WARNING))
    def readTemplate(self, url):
        """Read content of a remote template.

        :param url URL where the template is located
        """
        output = urllib.request.urlopen(url)  # nosec
        if output.getcode() == 200:
            return output.read().decode()
        else:
            raise Exception("Template '%s' not found" % url)

    def retrieve_mail_addresses(self, args, entry):
        mail_to = []
        mail_cc = []
        mail_bcc = [args.mail_bcc]
        for attr in args.mail_to:
            mail_to.extend([f"{m}@cern.ch" for m in entry[attr]])
        for attr in args.mail_cc:
            mail_to.extend([f"{m}@cern.ch" for m in entry[attr]])
        # Remove duplicates if any
        return list(set(mail_to)), list(set(mail_cc)), list(set(mail_bcc))

    def send_notifications(self, args, data):
        mailclient = MailClient("localhost")
        content = self.readTemplate(args.template)
        env = Environment(autoescape=True)
        subject_template = env.from_string(args.subject)
        body_template = env.from_string(content)
        for entry in data:
            mail_subject = subject_template.render(entry)
            mail_body = body_template.render(entry)
            mail_to, mail_cc, mail_bcc = self.retrieve_mail_addresses(
                args, entry)
            if args.exec_mode:
                try:
                    mailclient.send_mail(
                        ", ".join(mail_to),
                        mail_subject,
                        mail_body,
                        args.mail_from,
                        ", ".join(mail_cc),
                        ", ".join(mail_bcc),
                        "html")
                    logger.info("Email sent successfully to %s", mail_to)
                except Exception as ex:
                    logger.error("Impossible to send mailto '%s'. "
                                 "Error message: '%s'", mail_to, ex)
                logger.debug("To: %-20s Subject: \"%s\"",
                             mail_to, mail_subject)
            else:
                logger.info("[DRYRUN] To: %-20s Subject: \"%s\"",
                            mail_to, mail_subject)
        logger.info("Example mail:\n%s", mail_body)

    def main(self, args=None):
        self.parser.add_argument(
            "--report",
            required=True)
        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument(
            '--perform',
            dest="exec_mode",
            action='store_true',
            help="If present, sends email")
        behaviour.add_argument(
            '--dryrun',
            dest="exec_mode",
            action='store_false',
            help="If present, DOES NOT send email")
        self.parser.add_argument(
            "--subject",
            required=True,
            help="email subject template")
        self.parser.add_argument(
            "--template",
            required=True,
            help="Url to the template to be used as email body")
        self.parser.add_argument(
            "--mail-from",
            default="noreply@cern.ch",
            help="address to be used to send mail from "
                 "(Default: noreply@cern.ch)")
        self.parser.add_argument(
            "--mail-to",
            action='append',
            default=[],
            help="list of properties to extract accounts to send mail to "
                 "(Default: []")
        self.parser.add_argument(
            "--mail-cc",
            action='append',
            default=[],
            help="list of properties to extract accounts to send mail cc "
                 "(Default: [])")
        self.parser.add_argument(
            "--mail-bcc",
            default="cloud-infrastructure-migration-campaign@cern.ch",
            help="email address for mail bcc (Default: "
                 "cloud-infrastructure-migration-campaign@cern.ch)")
        args = self.parse_args(args)

        data = self.generate_project_report(args)
        self.send_notifications(args, data)


# Needs static method for setup.cfg
def main(args=None):
    NotifyUpgradeReportCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
