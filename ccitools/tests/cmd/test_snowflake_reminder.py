import logging

from ccitools.cmd import snowflake_reminder
from unittest import TestCase


class TestSnowflakeReminder(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):

        cmd = snowflake_reminder.SnowflakeReminderCMD()

        with self.assertRaises(SystemExit) as cm:
            cmd.main([])
        self.assertEqual(cm.exception.code, 2)

    def test_snowflake_reminder_dryrun(self):
        cmd = snowflake_reminder.SnowflakeReminderCMD()
        cmd.main([
            '--dryrun',
            '--mm_url', 'url',
        ])

    def test_snowflake_reminder_perform(self):
        cmd = snowflake_reminder.SnowflakeReminderCMD()
        cmd.main([
            '--perform',
            '--mm_url', 'url',
        ])
