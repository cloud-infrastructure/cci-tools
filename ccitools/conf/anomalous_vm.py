from oslo_config import cfg

functional_element = cfg.StrOpt(
    'functional_element',
    default='Cloud Infrastructure',
    help="functional element to create the ticket in case of error")

assignment_group = cfg.StrOpt(
    'assignment_group',
    default='Cloud Infrastructure 3rd Line Support',
    help="assignment group to create the ticket in case of error")

description = cfg.StrOpt(
    'description',
    default=('[Cloud] Please verify the behaviour '
             'of your Virtual Machine "%s"'),
    help="Description for the snow incident")

grafana_url_vm = "https://monit-grafana.cern.ch/d/zkxxHDP7z/"\
    "openstack-vms-simple?orgId=1&var-instance_name=%s"\
    "&var-bin=1h&var-rp=two_weeks"

grafana_url_hv = "https://monit-grafana.cern.ch/d/dY830gUWk/"\
    "single-host-in-one-page?orgId=3&var-host=%s"

message_snow = cfg.StrOpt(
    'message_snow',
    default="""
    Dear %s,

The Cloud Anomaly Detection has identified an anomaly in the compute server
that hosts your Virtual Machine "%s" (OpenStack project "%s").
This seems to be related to %s on your machine. %s

Could you please check if you can fix this Virtual Machine?

You can find information about your node's performance
in the following link:

    """
    + grafana_url_vm
    + """\n
Thanks in advance for your collaboration,

Best regards, Cloud Infrastructure Team
    """,
    help="Comment added to INC created by the Notify Anomalous VM script")

message_email = cfg.StrOpt(
    'message_email',
    default="""
    Dear %s,

The Cloud Anomaly Detection has identified an anomaly in the compute server
that hosts your Virtual Machine "%s" (OpenStack project "%s").
This seems to be related to %s on your machine. %s

Could you please check if you can fix this Virtual Machine?

You can find information about your node's performance
in the following link:

    """
    + grafana_url_vm
    + """\n
For further communications with the Cloud Infrastructure team,
please refer to the SNOW ticket %s.

Thanks in advance for your collaboration,

Best regards, Cloud Infrastructure Team
    """,
    help="Comment added to email created by the Notify Anomalous VM script")

work_note = cfg.StrOpt(
    'work_note',
    default="""
    Dear supporter,\n
    The following link shows stats of the hypervisor hosting the VM:\n
    """
    + grafana_url_hv
)

inc_url = cfg.StrOpt(
    'inc_url',
    default=('https://cern.service-now.com/'
             'service-portal?id=ticket&table=incident&n=%s'),
    help="URL to access a servicenow incident")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    functional_element,
    assignment_group,
    description,
    message_snow,
    message_email,
    work_note,
    inc_url
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
