#!/usr/bin/python3

import ccitools.conf
import logging
import prettytable
import sys

from ccitools.cmd.base.cloud import BaseCloudCMD

# configure logging
logger = logging.getLogger(__name__)
CONF = ccitools.conf.CONF


class CleanCattleVMsCMD(BaseCloudCMD):

    def __init__(self):
        super(CleanCattleVMsCMD, self).__init__(
            description="Clean cattle VMs in the hypervisor")

    def is_cattle_server(self, cloudclient, server):
        project = cloudclient.find_project(server.tenant_id)
        acct_group = project.__getattribute__('accounting-group')
        acct_type = project.type
        if (acct_group == "IT-Batch" and acct_type == "compute"):
            return True
        elif project.name.startswith("Cloud Probe"):
            return True
        else:
            return False

    def clean_cattle_vms(self, cloudclient, perform, force, hypervisors):

        # Build pretty table with VMs per host
        servers_table = prettytable.PrettyTable(["Hypervisor", "VM"])
        servers_table.align["Hypervisor"] = 'c'
        servers_table.align["VM"] = 'c'

        # Perform action on list of servers
        for hv_name in hypervisors.split():
            servers = cloudclient.get_servers_by_hypervisor(hv_name)
            cattle_servers = [
                server for server in servers if self.is_cattle_server(
                    cloudclient, server)]
            if cattle_servers:
                for server in cattle_servers:
                    servers_table.add_row([hv_name, server.name])

                if perform:
                    logger.info("Deleting the following low-SLA VMs:\n%s",
                                servers_table.get_string(sortby="Hypervisor"))
                    cloudclient.delete_servers(cattle_servers, force)
                else:
                    logger.info("[DRYRUN] deleting the following low-SLA VMs:"
                                "\n%s", servers_table.get_string(
                                    sortby="Hypervisor"))
            else:
                logger.info("No Batch VMS found in %s.", hv_name)

    def main(self, args=None):
        behaviour = self.parser.add_mutually_exclusive_group()
        behaviour.add_argument('--perform',
                               dest="exec_mode",
                               action='store_true',
                               help="If present, sends email")
        behaviour.add_argument('--dryrun',
                               dest="exec_mode",
                               action='store_false')
        self.parser.add_argument(
            "--force", default=False, help="Force deletion of servers")
        self.parser.add_argument(
            "--hypervisors", required=True, help="List of hypervisors")

        args = self.parse_args(args)

        self.clean_cattle_vms(
            self.cloudclient,
            args.exec_mode,
            args.force,
            args.hypervisors
        )


# Needs static method for setup.cfg
def main(args=None):
    CleanCattleVMsCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.exception(e)
        sys.exit(1)
