import logging

from ccitools.cmd import manage_nova_compute
from ccitools.tests.cmd import fixtures
from ccitools.tests import fixtures as base_fixtures
from novaclient import exceptions as nova_exceptions
from unittest import mock
from unittest import TestCase


class TestManageNovaCompute(TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_default_args(self):
        with self.assertRaises(SystemExit) as cm:
            manage_nova_compute.main([])
        self.assertEqual(cm.exception.code, 2)

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient'
                '.get_nova_services_list')
    def test_enable_compute_throws_exception(
            self, mock_list, mock_ks, mock_cc):
        mock_list.side_effect = iter([
            nova_exceptions.ClientException('Fail'),
        ])
        with self.assertRaises(SystemExit) as cm:
            manage_nova_compute.main([
                '--hosts', 'raise_exception',
                '--enable'
            ])
        self.assertEqual(cm.exception.code, 3)
        mock_list.assert_called_with(host='raise_exception.cern.ch')

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient'
                '.get_nova_services_list')
    def test_enable_compute_list_empty(self, mock_list, mock_ks, mock_cc):
        mock_list.return_value = []
        with self.assertRaises(SystemExit) as cm:
            manage_nova_compute.main([
                '--hosts', 'non_existing',
                '--enable'
            ])
        self.assertEqual(cm.exception.code, 3)
        mock_list.assert_called_with(host='non_existing.cern.ch')

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient'
                '.get_nova_services_list')
    def test_enable_compute_enable_dryrun(
            self, mock_list, mock_ks, mock_cc):
        mock_list.return_value = [base_fixtures.Resource(fixtures.SERVICE)]
        manage_nova_compute.main([
            '--hosts', 'dry_run',
            '--enable'
        ])
        mock_list.assert_called_with(host='dry_run.cern.ch')

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient'
                '.get_nova_services_list')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient.enable_nova_service')
    def test_enable_compute_enable_perform(
            self, mock_enable, mock_list, mock_ks, mock_cc):
        host1_srv = base_fixtures.Resource(fixtures.SERVICE)
        mock_list.return_value = [host1_srv]
        manage_nova_compute.main([
            '--hosts', 'host1',
            '--enable',
            '--perform'
        ])
        mock_list.assert_called_with(host='host1.cern.ch')
        mock_enable.assert_called_with(host1_srv)

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient'
                '.get_nova_services_list')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient.enable_nova_service')
    def test_enable_compute_enable_perform_raise(
            self, mock_enable, mock_list, mock_ks, mock_cc):
        host1_srv = base_fixtures.Resource(fixtures.SERVICE)
        mock_list.return_value = [host1_srv]
        mock_enable.side_effect = iter([
            nova_exceptions.ClientException('Fail'),
        ])
        with self.assertRaises(SystemExit) as cm:
            manage_nova_compute.main([
                '--hosts', 'host1',
                '--enable',
                '--perform'
            ])
        self.assertEqual(cm.exception.code, 3)
        mock_list.assert_called_with(host='host1.cern.ch')
        mock_enable.assert_called_with(host1_srv)

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient'
                '.get_nova_services_list')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient.disable_nova_service')
    def test_enable_compute_disable_perform_no_reason(
            self, mock_enable, mock_list, mock_ks, mock_cc):
        host1_srv = base_fixtures.Resource(fixtures.SERVICE)
        mock_list.return_value = [host1_srv]
        manage_nova_compute.main([
            '--hosts', 'host1',
            '--disable',
            '--perform'
        ])
        mock_list.assert_called_with(host='host1.cern.ch')
        mock_enable.assert_called_with(host1_srv, None)

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient'
                '.get_nova_services_list')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient.disable_nova_service')
    def test_enable_compute_disable_perform_reason(
            self, mock_enable, mock_list, mock_ks, mock_cc):
        host1_srv = base_fixtures.Resource(fixtures.SERVICE)
        mock_list.return_value = [host1_srv]
        manage_nova_compute.main([
            '--hosts', 'host1',
            '--disable',
            '--perform',
            '--reason', 'fake'
        ])
        mock_list.assert_called_with(host='host1.cern.ch')
        mock_enable.assert_called_with(host1_srv, 'fake')

    @mock.patch('ccitools.utils.cloud.cloud_config')
    @mock.patch('ccitools.utils.cloud.keystone_session')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient'
                '.get_nova_services_list')
    @mock.patch('ccitools.utils.cloud.CloudRegionClient.delete_nova_service')
    def test_enable_compute_delete_perform(
            self, mock_enable, mock_list, mock_ks, mock_cc):
        host1_srv = base_fixtures.Resource(fixtures.SERVICE)
        mock_list.return_value = [host1_srv]
        manage_nova_compute.main([
            '--hosts', 'host1',
            '--delete',
            '--perform'
        ])
        mock_list.assert_called_with(host='host1.cern.ch')
        mock_enable.assert_called_with(host1_srv)
